exports.seed = function (knex) {
  return knex('viewQuery_strategy').insert([
    {
      name: 'No Op',
      version: 1,
      description: 'This strategy does nothing',
      codePath: 'no-op',
      paramSchema: '{"$schema":"http://json-schema.org/draft-07/schema","$id":"http://example.com/example.json","type":"object","title":"The root schema","description":"The root schema comprises the entire JSON document.","default":{},"examples":[{}],"required":[],"additionalProperties":true}'
    }
  ]).then(function () {
    return knex('viewQuery_strategyInstance').insert([
      {
        id: '692833b7-a658-4ebf-b959-3cdcb23bb09d',
        strategyId: 1,
        strategyParams: '{}',
        tradingInstrument: 'FXCM-1-m1',
        dataStreams: '["FXCM-1-m1"]',
        isEnabled: false
      }
    ])
  })
}
