import { subscribe, __test__ } from '../error-publishing-event-subscriber'
import * as eventBus from '../../../common/event/event-bus'
import { mockDateNow, unmockDateNow } from '../../../common/test-mock/mock-date-now'

jest.mock('uuid-js', () => ({
  create: () => ({ hex: 'test-context-id' })
}))

describe('subscribeSingle', () => {
  const subscribeSingle = __test__.subscribeSingle

  const eventMessage = {
    header: {
      eventType: 'testEventType'
    }
  }

  const originalSubscribe = eventBus.subscribe
  const originalPublish = eventBus.publish
  beforeEach(() => {
    mockDateNow()
  })

  afterEach(() => {
    eventBus.subscribe = originalSubscribe
    eventBus.publish = originalPublish
    unmockDateNow()
  })

  test('executed eventListener successfully', done => {
    const eventListener = () => {
      done()
    }
    subscribeSingle(
      'StrategyService',
      'testErrorEventType',
      eventMessage.header.eventType,
      eventListener
    )
    eventBus.publish(eventMessage)
  })

  test('error from eventListener', async () => {
    eventBus.subscribe = jest.fn()
    eventBus.publish = jest.fn()

    const eventListener = () => {
      throw new Error('testError')
    }
    subscribeSingle(
      'StrategyService',
      'testErrorEventType',
      eventMessage.header.eventType,
      eventListener
    )

    // pretend publish to event bus happened and the event listener is called
    // we can't use publish here because publish itself is mocked for testing
    const errorCatchListener = eventBus.subscribe.mock.calls[0][1]
    await errorCatchListener()

    expect(eventBus.publish).toBeCalled()
    const exceptionEventMessage = eventBus.publish.mock.calls[0][0]
    expect(exceptionEventMessage).toMatchSnapshot({
      errorStack: expect.anything()
    })
  })
})

test('subscribe calls subscribeSingle for all subscribes', async () => {
  const eventChecks = {
    testEvent1: false,
    testEvent2: false,
    testEvent3: false
  }
  const subscribes = [
    ['test-event-1', () => { eventChecks.testEvent1 = true }],
    ['test-event-2', () => { eventChecks.testEvent2 = true }],
    ['test-event-3', () => { eventChecks.testEvent3 = true }]
  ]

  subscribe('TestService', 'testErrorEventType', subscribes)

  await eventBus.publish({
    header: {
      eventType: 'test-event-1'
    }
  })
  expect(eventChecks.testEvent1).toBe(true)

  await eventBus.publish({
    header: {
      eventType: 'test-event-2'
    }
  })
  expect(eventChecks.testEvent2).toBe(true)

  await eventBus.publish({
    header: {
      eventType: 'test-event-3'
    }
  })
  expect(eventChecks.testEvent3).toBe(true)
})
