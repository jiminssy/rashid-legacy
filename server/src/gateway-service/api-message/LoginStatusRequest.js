import { authorizedRoles, ApiMessage } from './ApiMessage'
import { UserRole } from '../../user-management-service/definition/UserRole'

@authorizedRoles([UserRole.READ_ONLY])
class LoginStatusRequest extends ApiMessage {
  constructor (userId, id = null) {
    super(id)
    this.userId = userId
  }
}

export { LoginStatusRequest }
