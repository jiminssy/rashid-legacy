import { BaseStrategy } from '../base-strategy'

export default class extends BaseStrategy {
  async onDataStreamReceived (candleData) {
    console.log('NO-OP strategy received data.', candleData)
  }
}
