import React, { useEffect, useRef, useState } from 'react'
import Highcharts from 'highcharts'

import styles from './index.module.scss'
import { useSelector } from '../../../hooks/selector'
import {
  CHANGE_DAILY_PERFORMANCE_DAYS,
  LOAD_REPORT_DAILY_PERFORMANCE,
  LOAD_REPORT_DP_STRATEGY_INSTANCES,
  REPORT_DAILY_PERFORMANCE_ENDED
} from '../../../constants/events'
import {
  getChartData,
  getPerformanceTotals,
  getStrategyInstancesAndTotals
} from './utils'
import { publish } from '../../../utils/event-bus'
import { store } from '../../../store'

const PERIODIC_LOAD_INTERVAL = 1000 * 60

export const DailyPerformance = () => {
  const topPerformancesChartContainer = useRef(null)
  const bottomPerformancesChartContainer = useRef(null)
  const [numDays, setNumDays] = useState(store.report.dailyPerformance.numDays)
  const allPerformances = useSelector('report.dailyPerformance.allPerformances')
  const numStrategyInstancesInCharts = useSelector('report.dailyPerformance.numStrategyInstancesInCharts')
  const topStrategyInstances = useSelector('report.dailyPerformance.topStrategyInstances')
  const bottomStrategyInstances = useSelector('report.dailyPerformance.bottomStrategyInstances')
  
  const jsonAllPerformances = JSON.stringify(allPerformances)

  // set up periodic data loading
  useEffect(() => {
    publish(LOAD_REPORT_DAILY_PERFORMANCE)
    const periodicLoad = setInterval(() => {
      publish(LOAD_REPORT_DAILY_PERFORMANCE)
    }, PERIODIC_LOAD_INTERVAL)
    return () => {
      clearInterval(periodicLoad)
      publish(REPORT_DAILY_PERFORMANCE_ENDED)
    }
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  // render charts
  useEffect(() => {
    const emptyChartData = {
      categories: [],
      series: []
    }
    let topChartData, bottomChartData
    if (allPerformances.length === 0) {
      topChartData = emptyChartData
      bottomChartData = emptyChartData
    } else {
      const performanceTotals = getPerformanceTotals(allPerformances)
      topChartData = getChartData(
        allPerformances,
        performanceTotals,
        numStrategyInstancesInCharts,
        true
      )
      bottomChartData = getChartData(
        allPerformances,
        performanceTotals,
        numStrategyInstancesInCharts,
        false
      )
    }
    
    Highcharts.chart(topPerformancesChartContainer.current, {
      chart: { type: 'line', height: '500px' },
      title: { text: null },
      yAxis: {
        title: { text: 'Accumulated Daily Outcome Scores' }
      },
      xAxis: {
        title: { text: 'Date' },
        categories: topChartData.categories
      },
      series: topChartData.series
    })

    Highcharts.chart(bottomPerformancesChartContainer.current, {
      chart: { type: 'line', height: '500px' },
      title: { text: null },
      yAxis: {
        title: { text: 'Accumulated Daily Outcome Scores' }
      },
      xAxis: {
        title: { text: 'Date' },
        categories: bottomChartData.categories
      },
      series: bottomChartData.series
    })

    if (allPerformances.length > 0) {
      const topStrategyInstances = getStrategyInstancesAndTotals(allPerformances, topChartData)
      publish(LOAD_REPORT_DP_STRATEGY_INSTANCES, {
        strategyInstances: topStrategyInstances,
        isTop: true
      })

      const bottomStrategyInstances = getStrategyInstancesAndTotals(allPerformances, bottomChartData)
      publish(LOAD_REPORT_DP_STRATEGY_INSTANCES, {
        strategyInstances: bottomStrategyInstances,
        isTop: false
      })
    }
  }, [  // eslint-disable-line react-hooks/exhaustive-deps
    jsonAllPerformances,
    numStrategyInstancesInCharts
  ])

  const onNumDaysChangeApplyClick = event => {
    event.preventDefault()
    publish(CHANGE_DAILY_PERFORMANCE_DAYS, parseInt(numDays))
  }

  const renderStrategyInstancesTable = strategyInstances => {
    if (strategyInstances.length === 0) {
      return null
    }

    const rows = strategyInstances.map((instance, index) => {
      const id = instance.strategyInstanceId.substring(0, 6)
      let name = 'Deleted Strategy Instance...'
      let version = ''
      let instrument = ''
      let parameters = ''
      let score = instance.totalPerformance
      if (!instance.isRemoved) {
        name = instance.name
        version = instance.version
        instrument = instance.tradingInstrument
        parameters = JSON.stringify(instance.strategyParams)
      }
      return (
        <tr key={index}>
          <td>{id}</td>
          <td>{name}</td>
          <td>{version}</td>
          <td>{instrument}</td>
          <td>{parameters}</td>
          <td>{score}</td>
        </tr>
      )
    })

    return (
      <div className={styles.tableWrapper}>
        <table className="table table-dark">
          <thead>
            <tr>
              <td>ID</td>
              <td>Name</td>
              <td>Version</td>
              <td>Instrument</td>
              <td>Parameters</td>
              <td>Score</td>
            </tr>
          </thead>
          <tbody>
            {rows}
          </tbody>
        </table>
      </div>
    )
  }

  return (
    <div>
      <h4 className="page-title">
        Daily Performance
        <sub className={styles.moreInfo}>
          (
          <a 
            href="https://docs.google.com/document/d/1GAf8yw90kk8jq5d-jVwp1WZ3kRxh1yeCTUHz9rd3MDs/edit?usp=sharing"
            target="_blank"
            rel="noopener noreferrer"
          >
            more info
          </a>
          )
        </sub>
      </h4>
      
      <div>
        Numer of days: 
        <input
          type="number"
          step="1"
          className={styles.numDays}
          value={numDays}
          onChange={e => setNumDays(e.target.value)}
        />
        <button
          type="button"
          className={`btn btn-primary ${styles.applyBtn}`}
          onClick={onNumDaysChangeApplyClick}
        >
          Apply
        </button>
      </div>
      
      <div className="container ml-0 mt-4">

        <div className="row">
          <div className="w-100">
            <div className="card">
              <div className="card-body">
                <h5 className="card-title">
                  Top {numStrategyInstancesInCharts} Strategy Instsances
                </h5>
                <div ref={topPerformancesChartContainer}></div>
                {renderStrategyInstancesTable(topStrategyInstances)}
              </div>              
            </div>
          </div>
        </div>

        <div className="row mt-4">
          <div className="w-100">
            <div className="card">
              <div className="card-body">
                <h5 className="card-title">
                  Bottom {numStrategyInstancesInCharts} Strategy Instsances
                </h5>
                <div ref={bottomPerformancesChartContainer}></div>
                {renderStrategyInstancesTable(bottomStrategyInstances)}
              </div>              
            </div>
          </div>
        </div>

      </div>

    </div>
  )
}
