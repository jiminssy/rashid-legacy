import { __test__ } from '../fxcm-trade-position-listener'

test('calcOutcomeScore works correctly', () => {
  const { calcOutcomeScore } = __test__

  // long position profit
  let score = calcOutcomeScore(6.235, 3.463, 8.234, 0.65)
  expect(score).toBe(0.47)

  // long position loss
  score = calcOutcomeScore(6.235, 3.463, 1.652, 1)
  expect(score).toBe(-1.65)

  // short position profit
  score = calcOutcomeScore(6.235, 10.234, 2.1234, 1)
  expect(score).toBe(1.03)

  // short position loss
  score = calcOutcomeScore(6.235, 10.234, 9.7623, 3.2)
  expect(score).toBe(-2.82)
})
