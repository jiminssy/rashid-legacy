import { getMockKnex } from '../../../common/test-mock/mock-knex'
import { eventListener } from '../edit-strategy-instance'
import { StrategyInstance } from '../../../common/definition/StrategyInstance'

const knex = getMockKnex()

test('update db', async () => {
  const eventMessage = {
    previousStrategyInstanceId: 'test-previous-strategy-instance-id',
    strategyInstance: new StrategyInstance({
      id: 'test-new-strategy-instance-id',
      strategyId: 1,
      strategyParams: '{"test": "params"}',
      tradingInstrument: 'FXCM-1-m1',
      dataStreams: '["FXCM-1-m5", "FXCM-2-h1"]',
      isEnabled: true
    })
  }

  await eventListener(eventMessage)

  expect(knex.where).toHaveBeenCalled()
  const whereArgs = knex.where.mock.calls[0][0]
  expect(whereArgs).toMatchSnapshot()

  expect(knex.update).toHaveBeenCalled()
  const updateArgs = knex.update.mock.calls[0][0]
  expect(updateArgs).toMatchSnapshot()
})
