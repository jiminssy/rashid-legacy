import { EventHandler } from './EventHandler'
import { EDIT_STRATEGY_INSTANCE } from '../constants/events'
import { editStrategyInstance } from '../utils/rashid-client'
import { notification } from '../utils/notification'

class EditStrategyInstance extends EventHandler {
  constructor () {
    super([EDIT_STRATEGY_INSTANCE])
  }

  async handle (instance) {
    const result = await editStrategyInstance(instance)
    if (result.isSuccess) {
      notification.success('Strategy instance edited')
      window.rrHistory.push('/admin/strategy')
    } else {
      notification.error(result.failReason)
    }
  }
}

export const eventHandler = new EditStrategyInstance()
