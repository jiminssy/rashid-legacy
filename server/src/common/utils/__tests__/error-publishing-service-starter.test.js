import { publish } from '../../event/event-bus'
import { mockPublish, unmockPublish } from '../../test-mock/mock-event-bus'
import { catchErrorAndPublish } from '../error-publishing-service-starter'
import { ExceptionEventMessage } from '../../event/message/ExceptionEventMessage'
import { ERROR_SERVICES_START } from '../../event/event-type'

jest.mock('uuid-js', () => ({
  create: () => ({ hex: 'test-context-id' })
}))

test('correct event is published on error', async () => {
  mockPublish()
  const funcToRun = () => {
    throw new Error('Mock error message')
  }

  await catchErrorAndPublish(funcToRun)

  const expectedErrorEventMessage = new ExceptionEventMessage(
    ERROR_SERVICES_START,
    expect.anything(),
    'Mock error message',
    expect.anything()
  )

  expect(publish).toHaveBeenCalledWith(expectedErrorEventMessage)

  unmockPublish()
})
