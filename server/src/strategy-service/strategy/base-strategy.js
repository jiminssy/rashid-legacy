import { publish, subscribe, unsubscribe } from '../../common/event/event-bus'
import { SubscribeDataStream } from '../../common/event/message/SubscribeDataStream'
import { UnsubscribeDataStream } from '../../common/event/message/UnsubscribeDataStream'
import {
  DATA_STREAM_UPDATE,
  ERROR_STRATEGY_DATA_STREAM_LISTENER
} from '../../common/event/event-type'
import { StrategyTradeSignal } from '../../common/event/message/StrategyTradeSignal'
import { OrderType } from '../../common/definition/OrderType'
import { parseDataStreamId } from '../../common/utils/data-stream-id-parser'
import { calcStopLossRange } from '../util/stop-loss-range-calculator'
import { calcStopLoss } from '../util/stop-loss-calculator'
import { calcTakeProfit } from '../util/take-profit-calculator'
import { ExceptionEventMessage } from '../../common/event/message/ExceptionEventMessage'

const STOPLOSS_LOOKBACK = 14

export class BaseStrategy {
  constructor (strategyInstanceInfo) {
    this.isActive = false
    this.dataStreamSubscriptions = []
    this.strategyInstanceInfo = strategyInstanceInfo
  }

  activate () {
    this._subscribeDataStreams()
    this._subscribeDataStreamEvents()
    this.isActive = true
  }

  deactivate () {
    this._unsubscribeDataStreams()
    this._unsubscribeDataStreamEvents()
    this.isActive = false
  }

  async onDataStreamReceived () {
    throw new Error('Not implemented')
  }

  async signalMarketOrderByRatio (
    tradeDirection,
    lastKnownBidClose,
    lastKnownAskClose,
    profitTargetToStopLossRatio = 3
  ) {
    const {
      brokerInstrumentId,
      periodId
    } = parseDataStreamId(this.strategyInstanceInfo.tradingInstrument)

    const stopLossRange = await calcStopLossRange(
      brokerInstrumentId,
      periodId,
      STOPLOSS_LOOKBACK
    )

    const stopLoss = calcStopLoss(
      tradeDirection,
      lastKnownBidClose,
      lastKnownAskClose,
      stopLossRange
    )

    const takeProfit = calcTakeProfit(
      tradeDirection,
      lastKnownBidClose,
      lastKnownAskClose,
      stopLoss,
      profitTargetToStopLossRatio
    )

    publish(new StrategyTradeSignal(
      this.strategyInstanceInfo.id,
      this.strategyInstanceInfo.tradingInstrument,
      tradeDirection,
      OrderType.MARKET,
      stopLossRange,
      stopLoss,
      takeProfit
    ))
  }

  _subscribeDataStreams () {
    this.strategyInstanceInfo.dataStreams.forEach(dataStreamId => {
      publish(new SubscribeDataStream(dataStreamId))
    })
  }

  _subscribeDataStreamEvents () {
    this.strategyInstanceInfo.dataStreams.forEach(dataStreamId => {
      const eventType = `${DATA_STREAM_UPDATE}/${dataStreamId}`
      this.dataStreamSubscriptions.push(
        subscribe(eventType, eventMessage => {
          (async () => {
            try {
              await this.onDataStreamReceived(eventMessage.candleData)
            } catch (error) {
              publish(new ExceptionEventMessage(
                ERROR_STRATEGY_DATA_STREAM_LISTENER,
                error.name,
                error.message,
                error.stack
              ))
            }
          })()
        })
      )
    })
  }

  _unsubscribeDataStreams () {
    this.strategyInstanceInfo.dataStreams.forEach(dataStreamId => {
      publish(new UnsubscribeDataStream(dataStreamId))
    })
  }

  _unsubscribeDataStreamEvents () {
    this.dataStreamSubscriptions.forEach(subscription => unsubscribe(subscription))
    this.dataStreamSubscriptions = []
  }
}
