
exports.up = function (knex) {
  return knex.schema.table('userManagement_user', function (table) {
    table.string('role').notNullable().defaultTo('ReadOnly')
  })
}

exports.down = function (knex) {
  return knex.schema.table('userManagement_user', function (table) {
    table.dropColumn('role')
  })
}
