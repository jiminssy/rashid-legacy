import { subscribe } from '../common/utils/error-publishing-event-subscriber'
import {
  CLOSE_SIMULATION_POSITION,
  ERROR_SIMULATION_SERVICE_EVENT_LISTENER,
  STRATEGY_TRADE_SIGNAL
} from '../common/event/event-type'
import { SystemService } from '../common/definition/SystemService'
import { getDataClient, getOrderClient, getPositionClient } from '../common/fxcm/client-factory'
import { updateClosedPosition, updateOpenedPosition } from './utils/fxcm-trade-position-listener'
import { publishDailyOutcomeScoresPeriodically } from './utils/outcome-score-publisher'

const subscribes = [
  [CLOSE_SIMULATION_POSITION, require('./event-listener/close-position').eventListener],
  [STRATEGY_TRADE_SIGNAL, require('./event-listener/strategy-trade-signal').eventListener]
]

export const start = async () => {
  const dataClient = getDataClient(SystemService.SIMULATION)
  await dataClient.initialize()

  const orderClient = getOrderClient(SystemService.SIMULATION)
  await orderClient.initialize()

  const positionClient = getPositionClient(SystemService.SIMULATION)
  positionClient.addOpenedPositionListener(updateOpenedPosition)
  positionClient.addClosedPositionListener(updateClosedPosition)
  await positionClient.initialize()

  publishDailyOutcomeScoresPeriodically()

  subscribe(
    'SimulationService',
    ERROR_SIMULATION_SERVICE_EVENT_LISTENER,
    subscribes
  )
}
