
exports.up = function (knex) {
  return knex.schema.createTable('analytics_liveStrategyInstance', function (table) {
    table.string('strategyInstanceId').primary()
  })
}

exports.down = function (knex) {
  return knex.schema.dropTable('analytics_liveStrategyInstance')
}
