import { LogInWithCredsRequest } from '../../api-message/LogInWithCredsRequest'
import { LogInWithTokenRequest } from '../../api-message/LogInWithTokenRequest'

export const register = registerPost => {
  registerPost('/api/users/login-with-creds', LogInWithCredsRequest)
  registerPost('/api/users/login-with-token', LogInWithTokenRequest)
}
