import { ApiMessage } from './ApiMessage'

export class EditStrategyRequest extends ApiMessage {
  constructor ({
    strategyId,
    name,
    version,
    description,
    codePath,
    paramSchema
  }) {
    super()
    this.strategyId = strategyId
    this.name = name
    this.version = version
    this.description = description
    this.codePath = codePath
    this.paramSchema = paramSchema
  }
}
