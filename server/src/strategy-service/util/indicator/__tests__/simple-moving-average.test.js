import { SimpleMovingAverage } from '../simple-moving-average'

const sma = new SimpleMovingAverage()

test('_computeSingleValue', () => {
  const result = sma._computeSingleValue([1, 2, 3, 4, 5, 6, 7, 8])
  expect(result).toBe(4.5)
})

test('_toIndicatorData', () => {
  const result = sma._toIndicatorData({
    bidClose: 2,
    askClose: 6
  })
  expect(result).toBe(4)
})
