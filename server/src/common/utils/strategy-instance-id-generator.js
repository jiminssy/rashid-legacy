import sha3 from 'crypto-js/sha3'

export const generateId = (
  strategyId,
  strategyParams,
  tradingInstrument, // data stream id
  dataStreams // array of data stream ids
) => {
  const values = [
    strategyId,
    strategyParams,
    tradingInstrument,
    dataStreams
  ]
  let strToHash = ''
  for (const value of values) {
    strToHash += JSON.stringify(value)
  }
  return sha3(strToHash).toString()
}
