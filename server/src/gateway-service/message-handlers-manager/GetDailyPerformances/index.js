import moment from 'moment-timezone'
import { MessageHandler } from '../MessageHandler'
import { outMessageManager } from '../../out-message-manager'
import { getDailyPerformances } from '../../../view-query-service/query-interface/report/daily-performance'

class GetDailyPerformances extends MessageHandler {
  async handle (message) {
    const today = moment.utc().set({
      hour: 0,
      minute: 0,
      second: 0,
      millisecond: 0
    })
    const from = today.subtract(message.numDays, 'days')
    const dailyPerformances = await getDailyPerformances(from.format())
    outMessageManager.sendOkResponse(message.id, dailyPerformances)
  }
}

export const handler = new GetDailyPerformances()
