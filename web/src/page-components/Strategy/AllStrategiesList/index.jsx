import React, { useEffect } from 'react'
import { useSelector } from '../../../hooks/selector'
import { ALL_STRATEGIES_LIST_STARTED, REMOVE_STRATEGY } from '../../../constants/events'
import { publish } from '../../../utils/event-bus'
import { useHistory } from 'react-router-dom'
import styles from './index.module.scss'

export const AllStrategiesList = () => {
  const history = useHistory()
  const strategies = useSelector('strategy.allStrategiesList.strategies')

  useEffect(() => {
    publish(ALL_STRATEGIES_LIST_STARTED)
  }, [])

  const onEditClick = strategyId => {
    history.push(`/admin/strategy/edit/${strategyId}`)
  }

  const onRemoveClick = strategyId => {
    publish(REMOVE_STRATEGY, strategyId)
  }

  const rows = strategies.map((strategy, index) => {
    return (
      <tr key={index}>
        <td>{strategy.id}</td>
        <td>{strategy.name}</td>
        <td>{strategy.description}</td>
        <td>{strategy.codePath}</td>
        <td>{JSON.stringify(strategy.paramSchema)}</td>
        <td>
          <i className={`fas fa-pencil-alt ${styles.editIcon}`}
            onClick={() => onEditClick(strategy.id)}
          />
        </td>
        <td>
          <i className={`fas fa-times-circle ${styles.removeIcon}`}
            onClick={() => onRemoveClick(strategy.id)}
          />
        </td>
      </tr>
    )
  })

  return (
    <div>
      <h4 className="page-title">List All Strategies</h4>
      <table className="table table-striped table-dark">
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Name</th>
            <th scope="col">Description</th>
            <th scope="col">Code Path</th>
            <th scope="col">Parameter Schema</th>
            <th scope="col"></th>
            <th scope="col"></th>
          </tr>
        </thead>
        <tbody>
          {rows}
        </tbody>
      </table>
    </div>
  )
}
