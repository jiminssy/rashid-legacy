import { EventMessage } from './EventMessage'
import { EDIT_STRATEGY_INSTANCE } from '../event-type'

export class EditStrategyInstance extends EventMessage {
  constructor (previousStrategyInstanceId, strategyInstance) {
    super(EDIT_STRATEGY_INSTANCE)
    this.previousStrategyInstanceId = previousStrategyInstanceId
    this.strategyInstance = strategyInstance
  }
}
