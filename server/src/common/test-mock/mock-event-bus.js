import * as eventBus from '../../common/event/event-bus'
import { mockDateNow, unmockDateNow } from './mock-date-now'

let publish

export const mockPublish = () => {
  mockDateNow()
  publish = eventBus.publish
  eventBus.publish = jest.fn()
}

export const unmockPublish = () => {
  eventBus.publish = publish
  unmockDateNow()
}
