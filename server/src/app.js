import { catchErrorAndPublish } from './common/utils/error-publishing-service-starter'

import { start as startAnalyticsService } from './analytics-service'
import { start as startDataStreamService } from './data-stream-service'
import { start as startEventLogService } from './event-log-service'
import { start as startGatewayService } from './gateway-service'
// import { start as startRelaySignalService } from './relay-signal-service'
import { start as startScheduledEventService } from './scheduled-event-service'
import { start as startSimulationService } from './simulation-service'
import { start as startStrategyService } from './strategy-service'
import { start as startTradeSignalService } from './trade-signal-service'
import { start as startViewQueryService } from './view-query-service'

// start services
(async function () {
  // pubsub initialization
  global.pubsub = {
    async: true
  }
  require('pubsub.js')

  startEventLogService()
  await catchErrorAndPublish(startGatewayService)
  await catchErrorAndPublish(startAnalyticsService)
  await catchErrorAndPublish(startTradeSignalService)
  // await catchErrorAndPublish(startRelaySignalService)
  await catchErrorAndPublish(startViewQueryService)
  await catchErrorAndPublish(startDataStreamService)
  await catchErrorAndPublish(startSimulationService)
  await catchErrorAndPublish(startStrategyService)
  await catchErrorAndPublish(startScheduledEventService)
}())
