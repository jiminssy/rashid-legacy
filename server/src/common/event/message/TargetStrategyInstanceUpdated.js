import { EventMessage } from './EventMessage'
import { STRATEGY_TARGET_STRATEGY_INSTANCE_UPDATED } from '../event-type'

export class TargetStrategyInstanceUpdated extends EventMessage {
  constructor (reactorStrategyInstanceId, targetStrategyInstanceId) {
    super(STRATEGY_TARGET_STRATEGY_INSTANCE_UPDATED)
    this.reactorStrategyInstanceId = reactorStrategyInstanceId
    this.targetStrategyInstanceId = targetStrategyInstanceId
  }
}
