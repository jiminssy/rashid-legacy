import { EventMessage } from './EventMessage'
import { REMOVE_STRATEGY_INSTANCE } from '../event-type'

export class RemoveStrategyInstance extends EventMessage {
  constructor (strategyInstanceId) {
    super(REMOVE_STRATEGY_INSTANCE)
    this.strategyInstanceId = strategyInstanceId
  }
}
