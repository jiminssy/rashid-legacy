import moment from 'moment-timezone'
import DatePicker from 'react-datepicker'
import Highcharts from 'highcharts'
import React, { useEffect, useRef, useState } from 'react'
import { useSelector } from '../../../hooks/selector'
import { LOAD_REPORT_LIVE_ACCOUNT_PERFORMANCE } from '../../../constants/events'
import { publish } from '../../../utils/event-bus'
import { Color } from '../../../styles/colors'

const today = moment().set({
  hour: 0,
  minute: 0,
  second: 0,
  millisecond: 0
})

const yearAgoFromToday = today.clone().subtract(365, 'days')

export const LiveAccountPerformance = () => {
  const [fromDate, setFromDate] = useState(yearAgoFromToday.toDate())
  const [toDate, setToDate] = useState(today.toDate())
  const accumulatedReturnsChartContainer = useRef(null)
  const discreteReturnsChartContainer = useRef(null)
  const dailyReturnDistributionContainer = useRef(null)
  const { accumulatedReturns, discreteReturns } = useSelector('report.liveAccountPerformance')

  useEffect(() => {
    const onDateChanged = async () => {
      publish(LOAD_REPORT_LIVE_ACCOUNT_PERFORMANCE, {
        fromDate: fromDate.toISOString(),
        toDate: toDate.toISOString()
      })
    }
    onDateChanged()
  }, [fromDate, toDate])

  useEffect(() => {
    if (accumulatedReturns.length > 0) {
      const dates = accumulatedReturns.map(dataPoint => dataPoint.date.substring(
        dataPoint.date.indexOf('-') + 1,
        dataPoint.date.indexOf('T')
      ))
      const returns = accumulatedReturns.map(dataPoint => dataPoint.accumulatedReturn)
      Highcharts.chart(accumulatedReturnsChartContainer.current, {
        chart: { type: 'line' },
        title: { text: 'Accumulated Returns (Equity Curve)' },
        yAxis: {
          title: { text: 'Accumulated Returns %' },
          labels: {
            formatter: function() {
              return `${this.value * 100}%`
            }
          }
        },
        xAxis: {
          title: { text: 'Date' },
          categories: dates
        },
        tooltip: {
          formatter: function() {
            return `${(this.y * 100).toFixed(2)}%`
          } 
        },
        series: [{
          name: 'Returns',
          data: returns,
          negativeColor: Color.CHART_NEGATIVE
        }]
      })
    }

    if (discreteReturns.length > 0) {
      const dates = discreteReturns.map(dataPoint => dataPoint.date.substring(
        dataPoint.date.indexOf('-') + 1,
        dataPoint.date.indexOf('T')
      ))
      const returns = discreteReturns.map(dataPoint => dataPoint.discreteReturn)
      Highcharts.chart(discreteReturnsChartContainer.current, {
        chart: { type: 'column' },
        title: { text: 'Daily Returns' },
        yAxis: {
          title: { text: 'Returns %' },
          labels: {
            formatter: function() {
              return `${this.value * 100}%`
            }
          }
        },
        xAxis: {
          title: { text: 'Date' },
          categories: dates
        },
        tooltip: {
          formatter: function() {
            return `${(this.y * 100).toFixed(2)}%`
          } 
        },
        series: [{
          name: 'Returns',
          data: returns,
          negativeColor: Color.CHART_NEGATIVE
        }]
      })

      Highcharts.chart(dailyReturnDistributionContainer.current, {
        title: { text: 'Daily Returns Distribution' },
        yAxis: [
          {
            title: { text: 'Data' },
            opposite: true,
            visible: false
          },
          {
            title: { text: '# of days' },
          }
        ],
        xAxis: [
          {
            title: { text: 'Data' },
            alignTicks: false,
            opposite: true,
            visible: false
          },
          {
            title: { text: 'Distribution' },
            alignTicks: false,
            labels: {
              formatter: function() {
                return `${(this.value * 100).toFixed(2)}%`
              }
            }
          }
        ],
        tooltip: {
          formatter: function() {
            const x1 = `${(this.point.options.x * 100).toFixed(2)}%`
            const x2 = `${(this.point.options.x2 * 100).toFixed(2)}%`
            return `y: ${this.y} , x: ${x1} - ${x2}`
          } 
        },
        series: [
          {
            name: 'Distribution',
            type: 'histogram',
            xAxis: 1,
            yAxis: 1,
            baseSeries: 's1'
          },
          {
            type: 'scatter',
            id: 's1',
            data: returns,
            visible: false
          }
        ]
      })
    }
  })

  return (
    <div>
      <h4 className="page-title">Live Account Performance</h4>
      <table>
        <tbody>
          <tr>
            <td>From:</td>
            <td><DatePicker selected={fromDate} onChange={date => setFromDate(date)} /></td>
          </tr>
          <tr>
            <td>To:</td>
            <td><DatePicker selected={toDate} onChange={date => setToDate(date)} /></td>
          </tr>
        </tbody>
      </table>

      <div className="container ml-0 mt-4">
        <div className="row">
          <div className="w-100">
            <div className="card">
              <div ref={accumulatedReturnsChartContainer} className="card-body"></div>
            </div>
          </div>
        </div>

        <div className="row mt-4">
          <div className="w-100">
            <div className="card">
              <div ref={discreteReturnsChartContainer} className="card-body"></div>
            </div>
          </div>
        </div>

        <div className="row mt-4">
          <div className="w-100">
            <div className="card">
              <div ref={dailyReturnDistributionContainer} className="card-body"></div>
            </div>
          </div>
        </div>

      </div>
    </div>
  )
}
