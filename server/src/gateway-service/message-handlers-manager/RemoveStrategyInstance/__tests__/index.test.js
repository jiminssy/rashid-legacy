import { handler } from '..'
import { outMessageManager } from '../../../out-message-manager'
import * as queryInterface from '../../../../view-query-service/query-interface/strategy'
import { mockPublish, unmockPublish } from '../../../../common/test-mock/mock-event-bus'
import { publish } from '../../../../common/event/event-bus'
import { RemoveStrategyInstanceRequest } from '../../../api-message/RemoveStrategyInstanceRequest'

jest.mock('uuid-js', () => ({
  create: () => ({ hex: 'test-context-id' })
}))
outMessageManager.sendOkResponse = jest.fn()

const testApiMessage = new RemoveStrategyInstanceRequest({
  strategyInstanceId: 'test-strategy-instance-id'
})

describe('handle', () => {
  beforeEach(() => {
    outMessageManager.sendOkResponse.mockReset()
    mockPublish()
  })

  afterEach(() => {
    unmockPublish()
  })

  test('fail - strategy does not exist', async () => {
    queryInterface.doesStrategyInstanceExist = jest.fn().mockResolvedValue(false)

    await handler.handle(testApiMessage)

    expect(outMessageManager.sendOkResponse).toHaveBeenCalledWith(testApiMessage.id, {
      isSuccess: false,
      failReason: 'Strategy instance does not exist'
    })
  })

  test('success', async () => {
    queryInterface.doesStrategyInstanceExist = jest.fn().mockResolvedValue(true)

    await handler.handle(testApiMessage)

    expect(outMessageManager.sendOkResponse).toHaveBeenCalledWith(testApiMessage.id, {
      isSuccess: true
    })
    const publishedMessage = publish.mock.calls[0][0]
    expect(publishedMessage).toMatchSnapshot()
  })
})
