import { calcStopLoss } from '../stop-loss-calculator'
import { TradeDirection } from '../../../common/definition/TradeDirection'

describe('calcStopLoss', () => {
  test('calculate for long direction', () => {
    const stopLoss = calcStopLoss(
      TradeDirection.LONG,
      25.3465,
      27.2342,
      5.8629
    )
    expect(stopLoss).toBe(19.4836)
  })

  test('calculate for short direction', () => {
    const stopLoss = calcStopLoss(
      TradeDirection.SHORT,
      25.3465,
      27.2342,
      5.8629
    )
    expect(stopLoss).toBe(33.0971)
  })
})
