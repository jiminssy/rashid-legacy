
exports.up = function (knex) {
  return knex.schema.createTable('analytics_strategyInstance', function (table) {
    table.string('id').primary()
    table.boolean('isEnabled').notNullable()
  })
}

exports.down = function (knex) {
  return knex.schema.dropTable('analytics_strategyInstance')
}
