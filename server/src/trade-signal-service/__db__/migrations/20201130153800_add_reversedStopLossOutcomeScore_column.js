
exports.up = function (knex) {
  return knex.schema.table('tradeSignal_strategyInstanceDailyStopLoss', function (table) {
    table.decimal('reversedStopLossOutcomeScore', 4, 2).notNullable().defaultTo(0)
  })
}

exports.down = function (knex) {
  return knex.schema.table('tradeSignal_strategyInstanceDailyStopLoss', function (table) {
    table.dropColumn('reversedStopLossOutcomeScore')
  })
}
