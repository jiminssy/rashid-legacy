import { eventListener } from '../strategy-trade-signal'
import { TradeDirection } from '../../../common/definition/TradeDirection'
import { StrategyTradeSignal } from '../../../common/event/message/StrategyTradeSignal'
import { OrderType } from '../../../common/definition/OrderType'
import * as clientFactory from '../../../common/fxcm/client-factory'
import * as database from '../../utils/database'

describe('eventListener', () => {
  test('skip if position already opened', async () => {
    database.isPositionOpen = jest.fn().mockResolvedValue(true)
    clientFactory.getOrderClient = jest.fn()

    await eventListener(new StrategyTradeSignal(
      'mock-strategy-instance-id',
      'FXCM-1-m1',
      TradeDirection.LONG,
      OrderType.MARKET,
      1.88,
      25.3465,
      27.2342
    ))

    expect(clientFactory.getOrderClient).not.toBeCalled()
  })

  test('works correctly', async () => {
    // mocks
    const tradeResults = {
      orderId: 'mock-order-id',
      tradeId: 'mock-trade-id',
      entry: 27.9635,
      stopLoss: 19.4836,
      takeProfit: 50.4860
    }
    const orderClient = {
      openTradeMarket: jest.fn().mockResolvedValue(tradeResults)
    }
    clientFactory.getOrderClient = jest.fn().mockReturnValue(orderClient)
    database.isPositionOpen = jest.fn().mockResolvedValue(false)
    database.getInstrumentId = jest.fn().mockResolvedValue(1)
    database.savePositionOpened = jest.fn()

    // test
    await eventListener(new StrategyTradeSignal(
      'mock-strategy-instance-id',
      'FXCM-1-m1',
      TradeDirection.LONG,
      OrderType.MARKET,
      10.7532,
      19.4836,
      50.4860
    ))

    // assert
    const saveParams = database.savePositionOpened.mock.calls[0][0]
    expect(saveParams).toMatchSnapshot()
  })
})
