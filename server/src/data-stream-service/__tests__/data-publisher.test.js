
import { CronJob } from 'cron'
import { subscribe, unsubscribe, __test__ } from '../data-publisher'
import { DataPeriod } from '../../common/definition/DataPeriod'
import { CandleData } from '../../common/definition/CandleData'
import { isTradingHour } from '../../common/fxcm/instruments'
import { publish } from '../../common/event/event-bus'
import { mockPublish, unmockPublish } from '../../common/test-mock/mock-event-bus'
import { DataStreamUpdate } from '../../common/event/message/DataStreamUpdate'
import { getResultWithRetries, GetResultRetriesError } from '../../common/utils/get-result-with-retries'
import * as retriesModule from '../../common/utils/get-result-with-retries'
import { ExceptionEventMessage } from '../../common/event/message/ExceptionEventMessage'
import { ERROR_FXCM_NO_DATA, ERROR_DATA_STREAM_PUBLISH } from '../../common/event/event-type'
import { mockDateNow, unmockDateNow } from '../../common/test-mock/mock-date-now'

jest.mock('cron')
jest.mock('uuid-js', () => ({
  create: () => ({ hex: 'test-context-id' })
}))
jest.mock('../../common/fxcm/instruments', () => ({ isTradingHour: jest.fn() }))
retriesModule.getResultWithRetries = jest.fn()

describe('getCronTime', () => {
  const { getCronTime } = __test__

  test('getCronTime works correctly', () => {
    expect(getCronTime(DataPeriod.m1)).toBe('0 */1 * * * *')
    expect(getCronTime(DataPeriod.m5)).toBe('0 */5 * * * *')
    expect(getCronTime(DataPeriod.m15)).toBe('0 */15 * * * *')
    expect(getCronTime(DataPeriod.m30)).toBe('0 */30 * * * *')
    expect(getCronTime(DataPeriod.H1)).toBe('0 0 */1 * * *')
    expect(getCronTime(DataPeriod.H2)).toBe('0 0 */2 * * *')
    expect(getCronTime(DataPeriod.H3)).toBe('0 0 */3 * * *')
    expect(getCronTime(DataPeriod.H4)).toBe('0 0 */4 * * *')
    expect(getCronTime(DataPeriod.H6)).toBe('0 0 */6 * * *')
    expect(getCronTime(DataPeriod.H8)).toBe('0 0 */8 * * *')
    expect(getCronTime(DataPeriod.D1)).toBe('0 0 0 * * *')
    expect(getCronTime(DataPeriod.W1)).toBe('0 0 0 * * 1')
    expect(getCronTime(DataPeriod.M1)).toBe('0 0 0 1 * *')
  })

  test('getCronTime throws exception for unknown periodId', () => {
    expect.assertions(2)
    try {
      getCronTime('TestPeriodId')
    } catch (e) {
      expect(e).toBeInstanceOf(Error)
      expect(e).toHaveProperty('message', 'Unknown periodId for getCronTime.')
    }
  })
})

describe('getTimezone', () => {
  const { getTimezone } = __test__

  test('FXCM instruments', () => {
    const brokerName = 'FXCM'

    const timezone = getTimezone(brokerName)

    expect(timezone).toBe('America/New_York')
  })

  test('Unknown instrument', () => {
    expect.assertions(2)

    try {
      getTimezone('TestInstrumentId')
    } catch (e) {
      expect(e).toBeInstanceOf(Error)
      expect(e).toHaveProperty('message', 'Unknown instrument for timezone.')
    }
  })
})

describe('subscribe', () => {
  const { subscriptions } = __test__
  const mockCronJob = jest.fn()

  beforeAll(() => {
    CronJob.mockImplementation(mockCronJob)
  })

  afterAll(() => {
    CronJob.mockRestore()
  })

  afterEach(() => {
    for (const dataStreamId in subscriptions) {
      delete subscriptions[dataStreamId]
    }
  })

  test('subscribe works correctly', () => {
    subscribe('FXCM-12-m5')
    subscribe('FXCM-12-m5')
    subscribe('FXCM-12-m5')
    expect(mockCronJob).toHaveBeenCalledWith(
      '0 */5 * * * *',
      expect.anything(),
      null,
      true,
      'America/New_York'
    )
    expect('FXCM-12-m5' in subscriptions)
    expect(subscriptions['FXCM-12-m5'].count).toBe(3)
  })
})

describe('unsubscribe', () => {
  const { subscriptions } = __test__
  const dataStreamId = 'FXCM-12-m5'
  const mockCronJob = {
    stop: jest.fn()
  }

  afterEach(() => {
    for (const dataStreamId in subscriptions) {
      delete subscriptions[dataStreamId]
    }
  })

  test('unsubscribe from counter >1', () => {
    subscriptions[dataStreamId] = { count: 3 }
    unsubscribe(dataStreamId)
    unsubscribe(dataStreamId)
    expect(dataStreamId in subscriptions).toBe(true)
    expect(subscriptions[dataStreamId].count).toBe(1)
  })

  test('unsubscribe from counter is 1', () => {
    subscriptions[dataStreamId] = {
      count: 1,
      cronJob: mockCronJob
    }
    unsubscribe(dataStreamId)
    expect(dataStreamId in subscriptions).toBe(false)
    expect(mockCronJob.stop).toBeCalled()
  })
})

describe('tryPublish', () => {
  const { tryPublish } = __test__
  const dataStreamId = 'FXCM-1-m5'

  beforeAll(() => {
    isTradingHour.mockImplementation(() => true)
    mockDateNow()
  })
  afterAll(() => {
    getResultWithRetries.mockReset()
    unmockDateNow()
  })

  beforeEach(() => {
    mockPublish()
  })

  afterEach(() => {
    unmockPublish()
  })

  test('data stream success', async () => {
    const expectedCandleData = new CandleData(
      1594328400, 1.12872,
      1.1274, 1.12899,
      1.1269, 1.12879,
      1.12745, 1.12906,
      1.12696, 21382
    )

    getResultWithRetries.mockResolvedValue([expectedCandleData])

    const expectedEventMessage = new DataStreamUpdate(
      dataStreamId,
      expectedCandleData,
      'test-context-id'
    )
    await tryPublish(dataStreamId)

    expect(publish).toHaveBeenCalledWith(expectedEventMessage)
  })

  test('data stream fail - empty data from FXCM', async () => {
    getResultWithRetries.mockRejectedValue(
      new GetResultRetriesError('Could not get successful result.')
    )

    await tryPublish(dataStreamId)

    const expectedErrorEventMessage = new ExceptionEventMessage(
      ERROR_FXCM_NO_DATA,
      expect.anything(),
      'Communication with FXCM is ok but candle data was not available for time: Mon Jul 02 2018 08:56:00 GMT+0100',
      expect.anything()
    )

    expect(publish).toHaveBeenCalledWith(expectedErrorEventMessage)
  })

  test('data stream fail - error from FXCM', async () => {
    getResultWithRetries.mockRejectedValue(
      new Error('test error from FXCM')
    )

    await tryPublish(dataStreamId)

    const expectedErrorEventMessage = new ExceptionEventMessage(
      ERROR_DATA_STREAM_PUBLISH,
      expect.anything(),
      'Unexpected error occurred during data stream publish',
      expect.anything()
    )

    expect(publish).toHaveBeenCalledWith(expectedErrorEventMessage)
  })
})
