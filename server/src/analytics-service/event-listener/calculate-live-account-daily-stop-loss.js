import moment from 'moment-timezone'
import { mean, std } from 'mathjs'
import {
  getDailyOutcomeScoresSum,
  getLiveAccountDailyOutcomeScores,
  getLastTradingDay,
  getLiveTradingAllowedStrategyInstsanceIds,
  insertLiveAccountDailyOutcomeScore
} from '../utils/database'
import { LiveAccountDailyStopLoss } from '../../common/event/message/LiveAccountDailyStopLoss'
import { publish } from '../../common/event/event-bus'
import { FXCM_LIVE_ACCOUNT_ID } from '../../settings'

const NUM_MOST_RECENT_DAILY_OUTCOME_SCORE = 100

const getStopLossOutcomeScore = (scoresMean, scoresStd) => {
  const getAtleastStd = (boundary, scoresStd) => {
    if (boundary > (-1 * scoresStd)) {
      return -1 * scoresStd
    } else {
      return boundary
    }
  }

  // 1 STD
  let boundary = scoresMean - scoresStd
  if (boundary < 0) {
    return getAtleastStd(boundary, scoresStd)
  }

  // 2 STD
  boundary -= scoresStd
  if (boundary < 0) {
    return getAtleastStd(boundary, scoresStd)
  }

  // 3 STD
  boundary -= scoresStd
  return getAtleastStd(boundary, scoresStd)
}

export const eventListener = async () => {
  const today = moment.utc().set({
    hour: 0,
    minute: 0,
    second: 0,
    millisecond: 0
  }).format()

  const lastTradingDay = await getLastTradingDay(today)

  const liveTradingAllowedStrategyInstanceIds = await getLiveTradingAllowedStrategyInstsanceIds()

  const liveAccountLastTradingDayOutcomeScore = await getDailyOutcomeScoresSum(
    liveTradingAllowedStrategyInstanceIds,
    lastTradingDay
  )

  if (liveAccountLastTradingDayOutcomeScore != null) {
    try {
      await insertLiveAccountDailyOutcomeScore(lastTradingDay, liveAccountLastTradingDayOutcomeScore)
    } catch (e) {
      if (e.message.includes('duplicate key value violates')) {
        // this means there was no trades in the last weekday
        // we don't have to do anything in this case
      } else {
        throw e
      }
    }
  }

  const outcomeScores = await getLiveAccountDailyOutcomeScores(NUM_MOST_RECENT_DAILY_OUTCOME_SCORE)

  const scoresMean = mean(outcomeScores)
  const scoresStd = std(outcomeScores)
  const stopLossOutcomeScore = getStopLossOutcomeScore(scoresMean, scoresStd)

  publish(new LiveAccountDailyStopLoss(
    'FXCM',
    FXCM_LIVE_ACCOUNT_ID,
    today,
    stopLossOutcomeScore
  ))
}
