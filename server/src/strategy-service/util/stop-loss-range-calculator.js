import _ from 'lodash'
import { ATR } from 'technicalindicators'
import { DataPeriod } from '../../common/definition/DataPeriod'
import { getDataClient } from '../../common/fxcm/client-factory'
import { SystemService } from '../../common//definition/SystemService'

const periodToAtr = {
  [DataPeriod.m1]: DataPeriod.m15,
  [DataPeriod.m5]: DataPeriod.m30,
  [DataPeriod.m15]: DataPeriod.H1,
  [DataPeriod.m30]: DataPeriod.H2,
  [DataPeriod.H1]: DataPeriod.H3
}

/**
 * This function estimates the stoploss range by looking at the bigger time scale ATR.
 * @param {string} fxcmInstrumentId
 * @param {string} periodId This is the period ID that the trade will be made in (ex. m5)
 * @param {integer} rangeLookBack Number of candle data to look back to estimate the price range.
 * @returns Single number value that is the price range stop loss should be.
 */
export const calcStopLossRange = async (
  fxcmInstrumentId,
  periodId,
  rangeLookBack
) => {
  const dataClient = getDataClient(SystemService.SIMULATION)
  const candleData = await dataClient.getMostRecentHistoricalData(
    fxcmInstrumentId,
    periodToAtr[periodId],
    rangeLookBack + 1
  )
  const atrInput = {
    high: [],
    low: [],
    close: [],
    period: rangeLookBack
  }
  candleData.forEach(data => {
    atrInput.high.push(_.mean([data.bidHigh, data.askHigh]))
    atrInput.low.push(_.mean([data.bidLow, data.askLow]))
    atrInput.close.push(_.mean([data.bidClose, data.askClose]))
  })
  return ATR.calculate(atrInput)[0]
}
