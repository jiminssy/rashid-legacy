import { getClosedPositions } from './database'

export const getDailyOutcomeScores = async dateStamp => {
  const dayStart = dateStamp.clone().set({
    hour: 0,
    minute: 0,
    second: 0,
    millisecond: 0
  })
  const dayEnd = dateStamp.clone().set({
    hour: 23,
    minute: 59,
    second: 59,
    millisecond: 999
  })
  const closedPositions = await getClosedPositions(dayStart.format(), dayEnd.format())

  const aggregatedScores = {}
  closedPositions.forEach(position => {
    if (!(position.strategyInstanceId in aggregatedScores)) {
      aggregatedScores[position.strategyInstanceId] = 0
    }
    aggregatedScores[position.strategyInstanceId] += position.outcomeScore
  })

  for (const instanceId in aggregatedScores) {
    let score = aggregatedScores[instanceId]
    score = Number(score.toFixed(2))
    aggregatedScores[instanceId] = score
  }

  return aggregatedScores
}
