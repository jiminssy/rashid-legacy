import moment from 'moment-timezone'
import {
  getDailyOutcomeScore,
  getDailyOutcomeScoresSum,
  getLiveAccountDailyStopLoss,
  getLiveTradingAllowedStrategyInstsances,
  getStrategyInstanceDailyStopLoss,
  setLiveAccountStoppedOut,
  setStrategyInstanceStoppedOut
} from '../utils/database'
import { FXCM_LIVE_ACCOUNT_ID } from '../../settings'

const isTradeAllowedStrategyInstance = async tradeSignal => {
  const liveTradingAllowedStrategyInstances = await getLiveTradingAllowedStrategyInstsances()
  const allowedStrategyInstance = liveTradingAllowedStrategyInstances
    .find(instance => instance.strategyInstanceId === tradeSignal.strategyInstanceId)
  return allowedStrategyInstance != null && allowedStrategyInstance.openedPositionId == null
}

const isStrategyInstanceStopLossValidationPass = async (strategyInstanceId, isReversedTradeSignal = false) => {
  const today = moment.utc().set({
    hour: 0,
    minute: 0,
    second: 0,
    millisecond: 0
  }).format()
  const dailyStopLoss = await getStrategyInstanceDailyStopLoss(strategyInstanceId, today)
  if (dailyStopLoss == null || dailyStopLoss.stoppedInLive) {
    return false
  }

  const currentDailyOutcomeScore = await getDailyOutcomeScore(strategyInstanceId, today)
  if (currentDailyOutcomeScore == null) {
    // if record doesn't exist, it means this is the strategy instnace's first trade of the day
    // always let first trade of the day pass
    return true
  } else if (isReversedTradeSignal && currentDailyOutcomeScore.outcomeScore >= dailyStopLoss.reversedStopLossOutcomeScore) {
    await setStrategyInstanceStoppedOut(strategyInstanceId, today)
    return false
  } else if (!isReversedTradeSignal && currentDailyOutcomeScore.outcomeScore <= dailyStopLoss.stopLossOutcomeScore) {
    await setStrategyInstanceStoppedOut(strategyInstanceId, today)
    return false
  }

  return true
}

const isLiveAccountStopLossValidationPass = async () => {
  const today = moment.utc().set({
    hour: 0,
    minute: 0,
    second: 0,
    millisecond: 0
  }).format()
  const dailyStopLoss = await getLiveAccountDailyStopLoss('FXCM', FXCM_LIVE_ACCOUNT_ID, today)
  if (dailyStopLoss == null || dailyStopLoss.stoppedInLive) {
    return false
  }

  const liveTradingAllowedStrategyInstanceIds = (await getLiveTradingAllowedStrategyInstsances())
    .map(instance => instance.strategyInstanceId)

  const currentDailyOutcomeScore = await getDailyOutcomeScoresSum(
    liveTradingAllowedStrategyInstanceIds,
    today
  )

  if (currentDailyOutcomeScore == null || currentDailyOutcomeScore > dailyStopLoss.stopLossOutcomeScore) {
    // null means there hasn't been any live strategy instance position closed today
    return true
  } else {
    await setLiveAccountStoppedOut('FXCM', FXCM_LIVE_ACCOUNT_ID, today)
    return false
  }
}

export const validateRisk = async tradeSignal => {
  if (!(await isTradeAllowedStrategyInstance(tradeSignal))) {
    return { isSuccess: false }
  }

  if (tradeSignal.miscData && tradeSignal.miscData.isReactionaryTradeSignal) {
    if (!(await isStrategyInstanceStopLossValidationPass(
      tradeSignal.miscData.sourceStrategyInstanceId,
      tradeSignal.miscData.isReversedTradeSignal
    ))) {
      return { isSuccess: false }
    }
  } else {
    if (!(await isStrategyInstanceStopLossValidationPass(
      tradeSignal.strategyInstanceId
    ))) {
      return { isSuccess: false }
    }
  }

  if (!(await isLiveAccountStopLossValidationPass())) {
    return { isSuccess: false }
  }

  // currently there is no position size multiplier adjustments
  return { isSuccess: true, positionSizeMultiplier: 1 }
}
