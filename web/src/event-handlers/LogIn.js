import jwtDecode from 'jwt-decode'
import { EventHandler } from './EventHandler'
import { LOGIN_ATTEMPTED } from '../constants/events'
import { logInWithCreds, logInWithToken } from '../utils/rashid-client'
import { notification } from '../utils/notification'
import { ACCESS_TOKEN } from '../constants/local-storage'
import { setValue } from '../store'
import { LOGGING_IN, LOGGED_IN, NOT_LOGGED_IN } from '../constants/user-login-state'

class LogIn extends EventHandler {
  constructor () {
    super([LOGIN_ATTEMPTED])
  }

  async handle (eventMessage) {
    const isLogInWithCreds = eventMessage.userName != null
    const loginStatePath = 'user.loginState'
    const rolePath = 'user.role'
    await setValue(loginStatePath, LOGGING_IN)
    try {
      let accessToken
      if (isLogInWithCreds) {
        accessToken = await logInWithCreds(
          eventMessage.userName,
          eventMessage.password
        )

        localStorage.setItem(ACCESS_TOKEN, accessToken)
        notification.success('Log In Success')
      } else {
        accessToken = eventMessage.accessToken
        await logInWithToken(accessToken)
      }
      const decoded = jwtDecode(accessToken)
      await setValue(loginStatePath, LOGGED_IN)
      await setValue(rolePath, decoded.role)
    } catch (e) {
      if (e.response.status === 401 && isLogInWithCreds) {
        notification.error('Log In Failed. Check your email or password again.')
      }
      localStorage.removeItem(ACCESS_TOKEN)
      await setValue(loginStatePath, NOT_LOGGED_IN)
      await setValue(rolePath, null)
    }
  }
}

export const eventHandler = new LogIn()
