import React from 'react'
import ReactDOM from 'react-dom'
import './index.scss'
import 'react-datepicker/dist/react-datepicker.min.css'

// Highcharts
import Highcharts from 'highcharts'
import darkUnica from 'highcharts/themes/dark-unica'
import addHistogramModule from 'highcharts/modules/histogram-bellcurve'

import { App } from './page-components/App'
import * as serviceWorker from './serviceWorker'
import { subscribeAllHandlers } from './event-handlers'

// Highcharts
darkUnica(Highcharts)
addHistogramModule(Highcharts)

// pubsub.js library configuration
window.pubsub = {
  async: true
}

require('./utils/notification')

subscribeAllHandlers()

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
