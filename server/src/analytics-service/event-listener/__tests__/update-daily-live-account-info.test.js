import * as database from '../../utils/database'
import { eventListener } from '../update-daily-live-account-info'
import * as factory from '../../../common/fxcm/client-factory'

jest.mock('moment-timezone', () => {
  const today = '2020-11-03T00:00:00.000Z'
  const mockedMomentTimezone = () => jest.requireActual('moment-timezone')(today)
  mockedMomentTimezone.utc = () => jest.requireActual('moment-timezone').utc(today)
  return mockedMomentTimezone
})

const todayDate = '2020-11-03T00:00:00Z'
const lastDayDate = '2020-11-02T00:00:00.000Z'
const accountId = '829164'
const dbAccountId = 1

test('profit day, no deposit, no withdraw', async () => {
  factory.getDataClient = jest.fn().mockReturnValue({
    getTradingAccount: jest.fn().mockResolvedValue({
      accountId,
      balance: 28736.59
    })
  })
  database.getDbAccountId = jest.fn().mockResolvedValue(dbAccountId)
  database.getLastAccountHistoryRecord = jest.fn().mockResolvedValue({
    id: dbAccountId,
    date: lastDayDate,
    dayStartBalance: 26374.32,
    intradayDeposit: 0,
    intradayWithdraw: 0,
    dayReturn: null
  })
  database.updateLastAccountHistoryRecordDayReturn = jest.fn()
  database.insertAccountHistoryRecord = jest.fn()

  await eventListener()

  expect(database.updateLastAccountHistoryRecordDayReturn).toHaveBeenCalledWith(
    dbAccountId,
    lastDayDate,
    0.08956704855328973
  )
  expect(database.insertAccountHistoryRecord).toHaveBeenCalledWith(
    dbAccountId,
    todayDate,
    28736.59
  )
})

test('loss day, no deposit, no withdraw', async () => {
  factory.getDataClient = jest.fn().mockReturnValue({
    getTradingAccount: jest.fn().mockResolvedValue({
      accountId,
      balance: 24635.16
    })
  })
  database.getDbAccountId = jest.fn().mockResolvedValue(dbAccountId)
  database.getLastAccountHistoryRecord = jest.fn().mockResolvedValue({
    id: dbAccountId,
    date: lastDayDate,
    dayStartBalance: 26374.32,
    intradayDeposit: 0,
    intradayWithdraw: 0,
    dayReturn: null
  })
  database.updateLastAccountHistoryRecordDayReturn = jest.fn()
  database.insertAccountHistoryRecord = jest.fn()

  await eventListener()

  expect(database.updateLastAccountHistoryRecordDayReturn).toHaveBeenCalledWith(
    dbAccountId,
    lastDayDate,
    -0.06594141574076601
  )
  expect(database.insertAccountHistoryRecord).toHaveBeenCalledWith(
    dbAccountId,
    todayDate,
    24635.16
  )
})

test('profit day, with deposit, with withdraw', async () => {
  factory.getDataClient = jest.fn().mockReturnValue({
    getTradingAccount: jest.fn().mockResolvedValue({
      accountId,
      balance: 28736.59
    })
  })
  database.getDbAccountId = jest.fn().mockResolvedValue(dbAccountId)
  database.getLastAccountHistoryRecord = jest.fn().mockResolvedValue({
    id: dbAccountId,
    date: lastDayDate,
    dayStartBalance: 26374.32,
    intradayDeposit: 524.31,
    intradayWithdraw: 126.89,
    dayReturn: null
  })
  database.updateLastAccountHistoryRecordDayReturn = jest.fn()
  database.insertAccountHistoryRecord = jest.fn()

  await eventListener()

  expect(database.updateLastAccountHistoryRecordDayReturn).toHaveBeenCalledWith(
    dbAccountId,
    lastDayDate,
    0.0733926894553734
  )
  expect(database.insertAccountHistoryRecord).toHaveBeenCalledWith(
    dbAccountId,
    todayDate,
    28736.59
  )
})
