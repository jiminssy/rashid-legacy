import React from 'react'
import {
  BrowserRouter as Router,
} from 'react-router-dom'
import { AuthApp } from '../AuthApp'
import './index.scss'

export const App = () => {
  return (
    <Router>
      <AuthApp />
    </Router>
  )
}
