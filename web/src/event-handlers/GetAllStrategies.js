import { EventHandler } from './EventHandler'
import { ALL_STRATEGIES_LIST_STARTED } from '../constants/events'
import { getAllStrategies } from '../utils/rashid-client'
import { setValue } from '../store'

class GetAllStrategies extends EventHandler {
  constructor () {
    super([ALL_STRATEGIES_LIST_STARTED])
  }

  async handle () {
    const strategies = await getAllStrategies()
    await setValue('strategy.allStrategiesList.strategies', strategies)
  }
}

export const eventHandler = new GetAllStrategies()
