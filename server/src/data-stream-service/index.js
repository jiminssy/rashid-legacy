import { subscribe as subscribeService } from '../common/utils/error-publishing-event-subscriber'
import {
  SUBSCRIBE_DATA_STREAM,
  UNSUBSCRIBE_DATA_STREAM,
  ERROR_DATA_STREAM_SERVICE_EVENT_LISTENER
} from '../common/event/event-type'
import { subscribe, unsubscribe } from './data-publisher'
import { SystemService } from '../common/definition/SystemService'
import { getDataClient } from '../common/fxcm/client-factory'

const subscribes = [
  [SUBSCRIBE_DATA_STREAM, e => subscribe(e.dataStreamId)],
  [UNSUBSCRIBE_DATA_STREAM, e => unsubscribe(e.dataStreamId)]
]

export const start = async () => {
  const dataClient = getDataClient(SystemService.DATA_STREAM)
  await dataClient.initialize()

  subscribeService(
    'DataStreamService',
    ERROR_DATA_STREAM_SERVICE_EVENT_LISTENER,
    subscribes
  )
}
