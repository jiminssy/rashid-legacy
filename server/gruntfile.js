module.exports = function (grunt) {
  'use strict';

  var path = require('path');
  var jestExecPath = path.join('.', 'node_modules', '.bin', 'jest')
  var eslintExecPath = path.join('.', 'node_modules', '.bin', 'eslint')

  grunt.loadNpmTasks('grunt-babel');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-shell');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-env');

  grunt.initConfig({
    env: {
      build: {
        CI: 'true',
      },
    },
    babel: {
      options: {
        sourceMap: true,
        presets: ['@babel/preset-env'],
      },
      dist: {
        files: [{
          expand: true,     // Enable dynamic expansion.
          cwd: 'src/',      // Src matches are relative to this path.
          src: ['**/*.js'],
          dest: 'dist/',    // Destination path prefix.
          ext: '.js',       // Dest filepaths will have this extension.
          extDot: 'first'   // Extensions in filenames begin after the first dot
        }],
      },

    },
    clean: {
      options: {
        force: true
      },
      server: ['dist'],
      publicFolder: ['public'],
      web: ['../web/build'],
    },
    shell: {
      options: {
        stderr: false,
        cwd: '../web',
      },
      runServer: 'node dist/app.js',
      webBuild: {
        command: 'yarn build',
        options: {
          execOptions: {
            cwd: '../web',
          },
        },
      },
      webInstall: {
        command: 'yarn',
        options: {
          execOptions: {
            cwd: '../web',
          },
        },
      },
      webLint: {
        command: 'yarn eslint src',
        options: {
          execOptions: {
            cwd: '../web',
          },
        },
      },
      webLintFix: {
        command: 'yarn eslint src --fix',
        options: {
          execOptions: {
            cwd: '../web',
          },
        },
      },
      webTest: {
        command: 'yarn test',
        options: {
          execOptions: {
            cwd: '../web',
          },
        },
      },
      serverInstall: {
        command: 'yarn',
      },
      serverMigrateDb: {
        command: 'knex migrate:latest'
      },
      serverLint: {
        command: eslintExecPath + ' src',
      },
      serverLintFix: {
        command: eslintExecPath + ' src --fix',
      },
      serverTest: {
        command: jestExecPath + ' src',
      }
    },
    copy: {
      webBuildToServerPublic: {
        expand: true,
        cwd: '../web/build',
        src: '**',
        dest: 'public/',
      },
    }
  });

  grunt.registerTask('server-build-no-test', [
    'clean:server',
    'babel',
  ])

  grunt.registerTask('server-build', [
    'shell:serverTest',
    'shell:serverLint',
    'server-build-no-test',
  ])

  grunt.registerTask('web-copy-to-public-folder', [
    'clean:publicFolder',
    'copy:webBuildToServerPublic'
  ])

  grunt.registerTask('web-build', [
    'env:build',
    'clean:web',
    'shell:webTest',
    'shell:webLint',
    'shell:webBuild',
    'web-copy-to-public-folder',
  ])

  grunt.registerTask('web-build-no-test', [
    'env:build',
    'clean:web',
    'shell:webBuild',
    'web-copy-to-public-folder',
  ])

  grunt.registerTask('dev-server', [
    'server-build-no-test',
    'shell:runServer',
  ])

  grunt.registerTask('build', [
    'web-build',
    'server-build',
  ])

  grunt.registerTask('prod-server', [
    'shell:webInstall',
    'shell:serverInstall',
    'shell:serverMigrateDb',
    'build',
    'shell:runServer'
  ])
};
