import { EventMessage } from './EventMessage'
import { LIVE_TRADE_SIGNAL } from '../event-type'

export class LiveTradeSignal extends EventMessage {
  constructor (
    tradingStrategyInstanceId,
    tradingInstrument, // data stream id format
    tradeDirection,
    orderType,
    quantity,
    stopLoss,
    takeProfit,
    limitPrice = null
  ) {
    super(LIVE_TRADE_SIGNAL)
    this.tradingStrategyInstanceId = tradingStrategyInstanceId
    this.tradingInstrument = tradingInstrument
    this.tradeDirection = tradeDirection
    this.orderType = orderType
    this.quantity = quantity
    this.stopLoss = stopLoss
    this.takeProfit = takeProfit
    this.limitPrice = limitPrice
  }
}
