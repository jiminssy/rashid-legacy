import { knex } from '../../common/database/knex'

const broker = 'simulation_broker'
const instrument = 'simulation_instrument'
const position = 'simulation_position'

export const doesPositionRecordExist = async (brokerId, brokerTradeId) => {
  const result = await knex(position)
    .select()
    .innerJoin(instrument, `${instrument}.id`, `${position}.instrumentId`)
    .where({
      brokerTradeId,
      [`${instrument}.brokerId`]: brokerId
    })
  return result.length > 0
}

export const getBrokerInstrumentId = async instrumentId => {
  const { brokerInstrumentId } = await knex(instrument).select(
    'brokerInstrumentId'
  ).where({ id: instrumentId }).first()

  return brokerInstrumentId
}

export const getClosedPositions = async (from, to) => {
  return await knex(position)
    .select()
    .where('isClosed', true)
    .whereBetween('closedTimeStamp', [from, to])
}

export const getInstrumentId = async (brokerName, brokerInstrumentId) => {
  const result = await knex(instrument)
    .select(`${instrument}.id as id`)
    .innerJoin(broker, `${broker}.id`, `${instrument}.brokerId`)
    .where({
      [`${broker}.name`]: brokerName,
      [`${instrument}.brokerInstrumentId`]: brokerInstrumentId
    })
    .first()
  return result.id
}

export const getPosition = async (brokerId, brokerTradeId) => {
  return await knex(position)
    .select(`${position}.*`)
    .innerJoin(instrument, `${instrument}.id`, `${position}.instrumentId`)
    .where({
      brokerTradeId,
      [`${instrument}.brokerId`]: brokerId
    })
    .first()
}

export const isPositionOpen = async strategyInstanceId => {
  const result = await knex(position)
    .select()
    .where({
      strategyInstanceId,
      isClosed: false
    })
  return result.length > 0
}

export const savePositionOpened = async positionData => {
  await knex(position).insert(positionData)
}

export const updatePositionCloseTime = async (
  brokerTradeId,
  closedTimeStamp,
  close, // price, rate
  outcomeScore
) => {
  await knex(position)
    .where({ brokerTradeId })
    .update({
      closedTimeStamp,
      close,
      outcomeScore,
      isClosed: true
    })
}

export const updatePositionOpenTime = async (brokerTradeId, openedTimeStamp) => {
  await knex(position)
    .where({ brokerTradeId })
    .update({ openedTimeStamp })
}
