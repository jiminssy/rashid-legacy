import { __test__ } from '../live-account-performance'

describe('reduceDataPoints', () => {
  const { reduceDataPoints } = __test__

  const mockData = [
    {
      date: '2020-01-01T00:00:00Z',
      dateStartBalance: 10.73,
      intradayDeposit: 3.24,
      intradayWithdraw: 1.53,
      dayReturn: 0.03425
    },
    {
      date: '2020-01-02T00:00:00Z',
      dateStartBalance: 12.86,
      intradayDeposit: 0,
      intradayWithdraw: 0,
      dayReturn: -0.01684
    },
    {
      date: '2020-01-03T00:00:00Z',
      dateStartBalance: 12.64,
      intradayDeposit: 0,
      intradayWithdraw: 2.34,
      dayReturn: 0.01741
    },
    {
      date: '2020-01-06T00:00:00Z',
      dateStartBalance: 10.47,
      intradayDeposit: 5.21,
      intradayWithdraw: 0,
      dayReturn: -0.04777
    },
    {
      date: '2020-01-08T00:00:00Z',
      dateStartBalance: 14.93,
      intradayDeposit: 0,
      intradayWithdraw: 0,
      dayReturn: 0.0249
    },
    {
      date: '2020-01-09T00:00:00Z',
      dateStartBalance: 15.30,
      intradayDeposit: 3.24,
      intradayWithdraw: 5.53,
      dayReturn: 0.05814
    },
    {
      date: '2020-01-10T00:00:00Z',
      dateStartBalance: 13.76,
      intradayDeposit: 0,
      intradayWithdraw: 0,
      dayReturn: -0.02276
    },
    {
      date: '2020-01-11T00:00:00Z',
      dateStartBalance: 13.44,
      intradayDeposit: 0,
      intradayWithdraw: 0,
      dayReturn: 0.04767
    },
    {
      date: '2020-01-12T00:00:00Z',
      dateStartBalance: 14.08,
      intradayDeposit: 0,
      intradayWithdraw: 0,
      dayReturn: 0.01658
    },
    {
      date: '2020-01-17T00:00:00Z',
      dateStartBalance: 14.31,
      intradayDeposit: 10.23,
      intradayWithdraw: 0,
      dayReturn: 0.01472
    },
    {
      date: '2020-01-18T00:00:00Z',
      dateStartBalance: 24.90,
      intradayDeposit: 0,
      intradayWithdraw: 4.27,
      dayReturn: -0.04221
    },
    {
      date: '2020-01-19T00:00:00Z',
      dateStartBalance: 19.75,
      intradayDeposit: 0,
      intradayWithdraw: 0,
      dayReturn: 0.03498
    },
    {
      date: '2020-01-20T00:00:00Z',
      dateStartBalance: 20.44,
      intradayDeposit: 0,
      intradayWithdraw: 0,
      dayReturn: null
    }
  ]

  test('3 maxDataPoints', async () => {
    const maxDataPoints = 3
    const reduced = await reduceDataPoints(mockData, maxDataPoints)
    expect(reduced).toMatchSnapshot()
  })

  test('4 maxDataPoints', async () => {
    const maxDataPoints = 4
    const reduced = await reduceDataPoints(mockData, maxDataPoints)
    expect(reduced).toMatchSnapshot()
  })

  test('5 maxDataPoints', async () => {
    const maxDataPoints = 5
    const reduced = await reduceDataPoints(mockData, maxDataPoints)
    expect(reduced).toMatchSnapshot()
  })
})
