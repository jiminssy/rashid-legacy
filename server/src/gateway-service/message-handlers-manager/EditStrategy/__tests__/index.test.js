import { handler } from '..'
import { EditStrategyRequest } from '../../../api-message/EditStrategyRequest'
import { outMessageManager } from '../../../out-message-manager'
import * as queryInterface from '../../../../view-query-service/query-interface/strategy'
import { mockPublish, unmockPublish } from '../../../../common/test-mock/mock-event-bus'
import { publish } from '../../../../common/event/event-bus'

jest.mock('uuid-js', () => ({
  create: () => ({ hex: 'test-uuid' })
}))
outMessageManager.sendOkResponse = jest.fn()

const testApiMessage = new EditStrategyRequest({
  strategyId: 57,
  name: 'Test Strategy',
  version: 1,
  description: 'Test description.',
  codePath: 'test/code/path',
  paramSchema: '{ "test": "schema" }'
})

describe('handle', () => {
  beforeEach(() => {
    outMessageManager.sendOkResponse.mockReset()
    mockPublish()
  })

  afterEach(() => {
    unmockPublish()
  })

  test('strategy does not exist', async () => {
    queryInterface.doesStrategyExistById = jest.fn().mockResolvedValue(false)

    await handler.handle(testApiMessage)

    expect(outMessageManager.sendOkResponse).toHaveBeenCalledWith(testApiMessage.id, {
      isSuccess: false,
      failReason: 'Strategy with ID 57 does not exist'
    })
  })

  test('success', async () => {
    queryInterface.doesStrategyExistById = jest.fn().mockResolvedValue(true)

    await handler.handle(testApiMessage)

    expect(outMessageManager.sendOkResponse).toHaveBeenCalledWith(testApiMessage.id, {
      isSuccess: true
    })
    const publishedMessage = publish.mock.calls[0][0]
    expect(publishedMessage).toMatchSnapshot()
  })
})
