import { EventMessage } from './EventMessage'
import { REMOVE_STRATEGY } from '../event-type'

export class RemoveStrategy extends EventMessage {
  constructor (strategyId) {
    super(REMOVE_STRATEGY)
    this.strategyId = strategyId
  }
}
