import _ from 'lodash'
import { Broker } from '../../../common/definition/Broker'
import { getBrokerId } from '../database'
import { getDataClient } from '../../../common/fxcm/client-factory'
import { SystemService } from '../../../common/definition/SystemService'
import { parseDataStreamId } from '../../../common/utils/data-stream-id-parser'

const MAX_NUM_DATA = 200

export class Indicator {
  constructor (dataStreamId, numLookBack) {
    this.dataStreamId = dataStreamId
    this.numLookBack = numLookBack
    this.lookBackData = [] // look back data to analyze
    this.values = [] // indicator values
  }

  async initialize () {
    const {
      brokerName,
      brokerInstrumentId,
      periodId
    } = parseDataStreamId(this.dataStreamId)
    const brokerId = await getBrokerId(brokerName)
    const rawLookBackData = await this._getLookBackData(
      brokerId,
      brokerInstrumentId,
      periodId,
      this.numLookBack
    )
    rawLookBackData.forEach(candleData => {
      this.updateWithNewData(candleData)
    })
  }

  updateWithNewData (candleData) {
    const indicatorData = this._toIndicatorData(candleData)
    this.lookBackData.push(indicatorData)
    if (this.lookBackData.length > MAX_NUM_DATA) {
      this.lookBackData.shift()
    }
    const computableData = _.takeRight(this.lookBackData, this.numLookBack)
    if (computableData.length === this.numLookBack) {
      const newValue = this._computeSingleValue(computableData)
      this.values.push(newValue)
      if (this.values.length > MAX_NUM_DATA) {
        this.values.shift()
      }
    }
  }

  /**
   * @param {any[]} indicatorData data used to compute indicator value
   * @returns computed new indicator value
   */
  _computeSingleValue (indicatorData) {
    throw new Error('Not implemented')
  }

  async _getLookBackData (
    brokerId,
    brokerInstrumentId,
    periodId,
    lookBack
  ) {
    if (brokerId === Broker.FXCM) {
      const dataClient = getDataClient(SystemService.STRATEGY)
      return await dataClient.getMostRecentHistoricalData(
        brokerInstrumentId,
        periodId,
        lookBack
      )
    }

    throw new Error('Unknown broker')
  }

  /**
   * This should convert raw streamed candle data to the indicator
   * data the indicator expects to be able to compute its new value
   * @param {CandleData} candleData raw streamed candle data
   */
  _toIndicatorData (candleData) {
    throw new Error('Not implemented')
  }
}
