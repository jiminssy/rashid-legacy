import pubsub from 'pubsub.js'

export const publish = (event, payload) => {
  pubsub.publish(event, [payload], {
    recurrent: true
  })
}

export const subscribe = (event, eventHandler) => {
  return pubsub.subscribe(event, eventHandler)
}

export const subscribeOnce = (event, eventHandler) => {
  pubsub.subscribeOnce(event, eventHandler)
}

export const unsubscribe = subscription => {
  pubsub.unsubscribe(subscription)
}
