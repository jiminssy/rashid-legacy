
exports.up = function (knex) {
  return knex.schema.createTable('viewQuery_strategy', function (table) {
    table.increments()
    table.string('name').notNullable()
    table.integer('version').notNullable()
    table.string('description').notNullable()
    table.string('codePath').notNullable()
    table.json('paramSchema').notNullable()
    table.unique(['name', 'version'])
  })
}

exports.down = function (knex) {
  return knex.schema.dropTable('viewQuery_strategy')
}
