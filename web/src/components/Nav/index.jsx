import React from 'react'
import styles from './index.module.scss'
import { useHistory } from 'react-router-dom'

export const Nav = () => {
  const history = useHistory()
  window.rrHistory = history

  return (
    <nav className={`navbar navbar-dark navbar-expand-lg ${styles.navbar}`}>
      <span className={`navbar-brand ${styles.navbarBrand}`} onClick={() => history.push('/')}>
        <span className={styles.navbarBrandFirstLetter}>R</span>
        <span className={styles.navbarBrandOtherLetters}>ashid</span>
      </span>
      
      <div className={`${styles.sidebarToggle}`}>
        <button className="navbar-toggler position-absolute d-md-none collapsed" type="button" data-toggle="collapse" data-target="#sidebarMenu" area-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
      </div>
      
    </nav>
  )
}
