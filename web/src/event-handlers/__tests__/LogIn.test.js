import { eventHandler } from '../LogIn'
import { ACCESS_TOKEN } from '../../constants/local-storage'
import { logInWithCreds } from '../../utils/rashid-client'
import { notification } from '../../utils/notification'

jest.mock('../../utils/rashid-client', () => {
  return {
    logInWithCreds: jest.fn()
  }
})

notification.success = jest.fn()
notification.error = jest.fn()

const creds = {
  userName: 'testUserName',
  password: 'testPassword'
}

beforeEach(() => {
  // eslint-disable-next-line no-proto
  jest.spyOn(window.localStorage.__proto__, 'setItem')
})

afterEach(() => {
  jest.clearAllMocks()
})

test('log in success', async () => {
  const accessToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjEsInJvbGUiOiJSZWFkT25seSIsImlhdCI6MTYwNTkxOTc2MCwiZXhwIjozMTU3MzM0MzYyMTYwfQ.wbrOaP97zrh8ep2rvJWbm4KKcRYvZpSLehlvJTuAnu8'
  logInWithCreds.mockResolvedValue(accessToken)

  await eventHandler.handle(creds)

  expect(window.localStorage.setItem)
    .toHaveBeenCalledWith(ACCESS_TOKEN, accessToken)
  expect(notification.success).toHaveBeenCalledWith('Log In Success')
})

test('log in fail', async () => {
  const mockedError = {
    response: {
      status: 401
    }
  }
  logInWithCreds.mockRejectedValue(mockedError)

  await eventHandler.handle(creds)

  expect(window.localStorage.setItem).not.toBeCalled()
  expect(notification.error).toHaveBeenCalledWith(
    'Log In Failed. Check your email or password again.'
  )
})
