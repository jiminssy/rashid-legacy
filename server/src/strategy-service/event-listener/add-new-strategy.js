import { knex } from '../../common/database/knex'

export const eventListener = async (
  { strategy: { codePath, paramSchema } }
) => {
  await knex('strategy_strategy').insert({
    codePath, paramSchema
  })
}
