import _ from 'lodash'
import { addNewStrategyInstance, activateStrategyInstance } from '../util/strategy-instance-activator'
import { insertStrategyInstance, getStrategyCodePath } from '../util/database'

export const eventListener = async ({ strategyInstance }) => {
  const strategyInstanceInfo = _.clone(strategyInstance, true)
  const codePath = await getStrategyCodePath(strategyInstanceInfo.strategyId)
  const strategyInstanceId = addNewStrategyInstance(codePath, strategyInstanceInfo)
  if (strategyInstance.isEnabled) {
    activateStrategyInstance(strategyInstanceId)
  }
  await insertStrategyInstance(strategyInstance)
}
