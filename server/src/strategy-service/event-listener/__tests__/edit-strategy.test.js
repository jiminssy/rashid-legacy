import { getMockKnex } from '../../../common/test-mock/mock-knex'
import { eventListener } from '../edit-strategy'
import { Strategy } from '../../../common/definition/Strategy'

const knex = getMockKnex()

test('update db', async () => {
  const eventMessage = {
    strategy: new Strategy({
      id: 1,
      name: 'Test Name',
      version: 5,
      description: 'Some description',
      codePath: 'src/some/path',
      paramSchema: '{ "someParam": 1234 }'
    })
  }

  await eventListener(eventMessage)

  expect(knex.where).toHaveBeenCalled()
  const whereArgs = knex.where.mock.calls[0][0]
  expect(whereArgs).toMatchSnapshot()

  expect(knex.update).toHaveBeenCalled()
  const updateArgs = knex.update.mock.calls[0][0]
  expect(updateArgs).toMatchSnapshot()
})
