import { validateStrategyParams } from '../json-schema-validator'
import { getStrategyParamSchema } from '../../../view-query-service/query-interface/strategy'
import * as queryInterfaceModule from '../../../view-query-service/query-interface/strategy'

queryInterfaceModule.getStrategyParamSchema = jest.fn()
describe('json-schema-validator', () => {
  // This schema requires a name value string.
  const testSchema = {
    $schema: 'http://json-schema.org/draft-07/schema',
    $id: 'http://example.com/example.json',
    type: 'object',
    title: 'The root schema',
    description: 'The root schema comprises the entire JSON document.',
    default: {},
    examples: [
      {
        name: 'A green door'
      }
    ],
    required: [
      'name'
    ],
    properties: {
      name: {
        $id: '#/properties/name',
        type: 'string',
        title: 'The name schema',
        description: 'An explanation about the purpose of this instance.',
        default: '',
        examples: [
          'A green door'
        ]
      }
    },
    additionalProperties: true
  }

  test('success', async () => {
    const strategyId = 1
    const strategyParams = '{"name":"soohan"}'

    getStrategyParamSchema.mockResolvedValue(testSchema)

    const isValid = await validateStrategyParams(strategyId, strategyParams)

    expect(isValid).toBe(true)
  })

  test('fail', async () => {
    const strategyId = 1
    const strategyParams = '{"name":1}'

    getStrategyParamSchema.mockResolvedValue(testSchema)

    const isValid = await validateStrategyParams(strategyId, strategyParams)

    expect(isValid).toBe(false)
  })
})
