import jwt from 'jsonwebtoken'
import { knex } from '../common/database/knex'
import { JWT_SECRET } from '../settings'

const userTable = 'userManagement_user'

export const getUserRole = async userId => {
  const result = await knex(userTable).select('role')
    .where({ id: userId })
    .first()
  return result.role
}

export const logIn = async (userName, password) => {
  const user = await knex(userTable).select()
    .where({
      email: userName,
      password: password
    })
    .first()

  if (user) {
    return jwt.sign({
      userId: user.id,
      role: user.role
    }, JWT_SECRET, { expiresIn: '99999 years' })
  } else {
    return null
  }
}

export const verifyAccessToken = accessToken => {
  try {
    jwt.verify(accessToken, JWT_SECRET)
    return true
  } catch (e) {
    return false
  }
}
