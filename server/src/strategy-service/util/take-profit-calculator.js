import { TradeDirection } from '../../common/definition/TradeDirection'

export const calcTakeProfit = (
  tradeDirection,
  lastKnownBidClose,
  lastKnownAskClose,
  stopLoss,
  profitTargetToStopLossRatio
) => {
  if (tradeDirection === TradeDirection.LONG) {
    const stopLossRange = lastKnownAskClose - stopLoss
    return lastKnownAskClose + (profitTargetToStopLossRatio * stopLossRange)
  } else {
    const stopLossRange = stopLoss - lastKnownBidClose
    return lastKnownBidClose - (profitTargetToStopLossRatio * stopLossRange)
  }
}
