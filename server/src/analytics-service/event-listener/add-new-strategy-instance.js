import { insertStrategyInstance } from '../utils/database'

export const eventListener = async ({ strategyInstance }) => {
  await insertStrategyInstance(strategyInstance)
}
