import * as userManagementService from '../../../../user-management-service'
import { outMessageManager } from '../../../out-message-manager'
import { handler } from '../index'

jest.mock('../../../../user-management-service')

const testLogInMessage = {
  id: 'testMessageId',
  acessToken: 'testAccessToken'
}

test('log in success', async () => {
  outMessageManager.sendOkResponse = jest.fn()
  userManagementService.verifyAccessToken.mockReturnValue(true)
  await handler.handle(testLogInMessage)
  expect(outMessageManager.sendOkResponse).toHaveBeenCalledWith(
    testLogInMessage.id, true
  )
})

test('log in fail', async () => {
  outMessageManager.sendFailedAuthentication = jest.fn()
  userManagementService.verifyAccessToken.mockReturnValue(false)
  await handler.handle(testLogInMessage)
  expect(outMessageManager.sendFailedAuthentication)
    .toHaveBeenCalledWith(testLogInMessage.id)
})
