export const parseDataStreamId = dataStreamId => {
  const tokens = dataStreamId.split('-')
  return {
    brokerName: tokens[0],
    brokerInstrumentId: tokens[1],
    periodId: tokens[2]
  }
}
