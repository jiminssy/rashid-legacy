import { updateClosedPosition } from '../utils/fxcm-trade-position-listener'

export const eventListener = async ({ brokerTradeId, closedTime, close }) => {
  const mockSocketMessage = {
    tradeId: brokerTradeId,
    closedTime,
    close
  }
  await updateClosedPosition(mockSocketMessage)
}
