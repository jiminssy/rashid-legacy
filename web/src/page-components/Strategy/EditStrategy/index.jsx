import React, { useEffect } from 'react'
import { useParams } from 'react-router-dom'
import styles from './index.module.scss'
import { StrategyForm } from '../../../components/StrategyForm'
import { useSelector } from '../../../hooks/selector'
import {
  STRATEGY_EDIT_STARTED,
  STRATEGY_EDIT_ENDED
} from '../../../constants/events'
import { publish } from '../../../utils/event-bus'

export const EditStrategy = () => {
  const { strategyId } = useParams()
  const strategy = useSelector('strategy.edit.strategy')

  useEffect(() => {
    publish(STRATEGY_EDIT_STARTED, strategyId)
    return () => {
      publish(STRATEGY_EDIT_ENDED)
    }
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <div>
      <h4 className="page-title">Edit Strategy</h4>
      <div className={`card ${styles.formContainer}`}>
        <div className="card-body">
          {strategy && <StrategyForm isEdit strategy={strategy} />}
        </div>
      </div>
    </div>
  )
}
