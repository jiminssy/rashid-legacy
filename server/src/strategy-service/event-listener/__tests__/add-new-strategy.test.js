import { getMockKnex } from '../../../common/test-mock/mock-knex'
import { eventListener } from '../add-new-strategy'

const knex = getMockKnex()

test('insert to db', async () => {
  const eventMessage = {
    strategy: {
      name: 'Test Name',
      version: 5,
      codePath: 'src/some/path',
      paramSchema: '{ "someParam": 1234 }'
    }
  }

  await eventListener(eventMessage)

  const insertArgs = knex.insert.mock.calls[0][0]
  expect(insertArgs).toMatchSnapshot()
  expect(knex.insert).toHaveBeenCalled()
})
