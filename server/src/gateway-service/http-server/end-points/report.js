import { GetDailyPerformancesRequest } from '../../api-message/GetDailyPerformancesRequest'
import { GetReportLiveAccountPerformanceRequest } from '../../api-message/GetReportLiveAccountPerformanceRequest'

const baseUrl = '/api/report'

export const register = (_registerPost, registerGet) => {
  // GET
  registerGet(`${baseUrl}/daily-performances/:numDays`, GetDailyPerformancesRequest)
  registerGet(`${baseUrl}/live-account-performance/:fromDate/:toDate`, GetReportLiveAccountPerformanceRequest)

  // POST
}
