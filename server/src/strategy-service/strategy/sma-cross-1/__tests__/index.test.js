import SmaCross1 from '..'
import { mockPublish, unmockPublish } from '../../../../common/test-mock/mock-event-bus'
import { publish } from '../../../../common/event/event-bus'
import * as stopLossRangeCalculator from '../../../util/stop-loss-range-calculator'

jest.mock('uuid-js', () => ({
  create: () => ({ hex: 'test-uuid' })
}))

describe('onDataStreamReceived', () => {
  const smaCross1 = new SmaCross1({ id: 'mock-id', tradingInstrument: 'FXCM-1-m1' })
  smaCross1.fastMa = {
    updateWithNewData: jest.fn()
  }
  smaCross1.slowMa = {
    updateWithNewData: jest.fn()
  }

  beforeEach(() => {
    mockPublish()
  })

  afterEach(() => {
    unmockPublish()
  })

  test('bottom to top cross', async () => {
    smaCross1.fastMa.values = [10, 30]
    smaCross1.slowMa.values = [15, 25]

    stopLossRangeCalculator.calcStopLossRange = jest.fn().mockResolvedValue(1)

    await smaCross1.onDataStreamReceived({
      tradingInstrument: 'FXCM-1-m1',
      bidClose: 35,
      askClose: 40
    })

    expect(publish.mock.calls[0][0]).toMatchSnapshot()
  })

  test('top to bottom cross', async () => {
    smaCross1.fastMa.values = [30, 10]
    smaCross1.slowMa.values = [25, 15]

    stopLossRangeCalculator.calcStopLossRange = jest.fn().mockResolvedValue(1)

    await smaCross1.onDataStreamReceived({
      tradingInstrument: 'FXCM-1-m1',
      bidClose: 35,
      askClose: 40
    })

    expect(publish.mock.calls[0][0]).toMatchSnapshot()
  })

  test('no cross', () => {
    smaCross1.fastMa.values = [30, 30]
    smaCross1.slowMa.values = [20, 20]

    smaCross1.onDataStreamReceived({})

    expect(publish).not.toHaveBeenCalled()
  })
})
