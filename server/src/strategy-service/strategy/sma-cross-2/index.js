import _ from 'lodash'
import { BaseStrategy } from '../base-strategy'
import { SimpleMovingAverage } from '../../util/indicator/simple-moving-average'
import { RelativeStrengthIndex } from '../../util/indicator/relative-strength-index'
import { TradeDirection } from '../../../common/definition/TradeDirection'

// strategy parameter
/*
{
  fastMaLookBack: integer,
  slowMaLookBack: integer,
  rsiLookBack: integer,
  trendMaLookBack: integer,
  overBoughtLevel: integer,
  overSoldLevel: integer
}
*/
export default class extends BaseStrategy {
  async activate () {
    super.activate()
    this.fastMa = new SimpleMovingAverage(
      this.strategyInstanceInfo.dataStreams[0],
      this.strategyInstanceInfo.strategyParams.fastMaLookBack
    )
    this.slowMa = new SimpleMovingAverage(
      this.strategyInstanceInfo.dataStreams[0],
      this.strategyInstanceInfo.strategyParams.slowMaLookBack
    )
    this.rsi = new RelativeStrengthIndex(
      this.strategyInstanceInfo.dataStreams[0],
      this.strategyInstanceInfo.strategyParams.rsiLookBack
    )
    this.trendMa = new SimpleMovingAverage(
      this.strategyInstanceInfo.dataStreams[0],
      this.strategyInstanceInfo.strategyParams.trendMaLookBack
    )

    await Promise.all([
      this.fastMa.initialize(),
      this.slowMa.initialize(),
      this.rsi.initialize(),
      this.trendMa.initialize()
    ])
  }

  async onDataStreamReceived (candleData) {
    this.fastMa.updateWithNewData(candleData)
    this.slowMa.updateWithNewData(candleData)
    this.rsi.updateWithNewData(candleData)
    this.trendMa.updateWithNewData(candleData)

    if (
      this._isBottomToTopCross() &&
      this._isOverSold() &&
      this._isUpTrend(candleData.bidClose)
    ) {
      await this.signalMarketOrderByRatio(
        TradeDirection.LONG,
        candleData.bidClose,
        candleData.askClose
      )
    } else if (
      this._isTopToBottomCross() &&
      this._isOverBought() &&
      this._isDownTrend(candleData.askClose)
    ) {
      await this.signalMarketOrderByRatio(
        TradeDirection.SHORT,
        candleData.bidClose,
        candleData.askClose
      )
    }
  }

  _isBottomToTopCross () {
    const fastMaCurrent = _.nth(this.fastMa.values, -1)
    const fastMaPrevious = _.nth(this.fastMa.values, -2)
    const slowMaCurrent = _.nth(this.slowMa.values, -1)
    const slowMaPrevious = _.nth(this.slowMa.values, -2)
    return fastMaPrevious < slowMaPrevious && fastMaCurrent >= slowMaCurrent
  }

  _isDownTrend (currentAsk) {
    const trendMaCurrent = _.nth(this.trendMa.values, -1)
    return currentAsk < trendMaCurrent
  }

  _isOverBought () {
    const rsiCurrent = _.nth(this.rsi.values, -1)
    return rsiCurrent > this.strategyInstanceInfo.strategyParams.overBoughtLevel
  }

  _isOverSold () {
    const rsiCurrent = _.nth(this.rsi.values, -1)
    return rsiCurrent < this.strategyInstanceInfo.strategyParams.overSoldLevel
  }

  _isTopToBottomCross () {
    const fastMaCurrent = _.nth(this.fastMa.values, -1)
    const fastMaPrevious = _.nth(this.fastMa.values, -2)
    const slowMaCurrent = _.nth(this.slowMa.values, -1)
    const slowMaPrevious = _.nth(this.slowMa.values, -2)
    return fastMaPrevious > slowMaPrevious && fastMaCurrent <= slowMaCurrent
  }

  _isUpTrend (currentBid) {
    const trendMaCurrent = _.nth(this.trendMa.values, -1)
    return currentBid > trendMaCurrent
  }
}
