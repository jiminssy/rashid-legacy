
exports.up = function (knex) {
  return knex.schema.table('tradeSignal_liveStrategyInstance', function (table) {
    table.integer('positionSize').nullable().defaultTo(null)
    table.string('coupledSimulationPositionId').nullable().defaultTo(null)
  })
}

exports.down = function (knex) {
  return knex.schema.table('tradeSignal_liveStrategyInstance', function (table) {
    table.dropColumn('positionSize')
    table.dropColumn('coupledSimulationPositionId')
  })
}
