import { getMockKnex } from '../../../common/test-mock/mock-knex'
import { eventListener } from '../edit-strategy-instance'
import { StrategyInstance } from '../../../common/definition/StrategyInstance'
import * as database from '../../util/database'
import * as instanceActivator from '../../util/strategy-instance-activator'

database.getStrategyCodePath = jest.fn().mockResolvedValue('no-op')

const mockedActivate = jest.fn()
const mockedDeactivate = jest.fn()
const mockedReplacer = jest.fn()
instanceActivator.activateStrategyInstance = mockedActivate
instanceActivator.deactivateStrategyInstance = mockedDeactivate
instanceActivator.replaceStrategyInstance = mockedReplacer

const knex = getMockKnex()

const eventMessage = {
  previousStrategyInstanceId: 'test-previous-strategy-instance-id',
  strategyInstance: new StrategyInstance({
    id: 'test-new-strategy-instance-id',
    strategyId: 1,
    strategyParams: '{"test": "params"}',
    tradingInstrument: 'FXCM-1-m1',
    dataStreams: '["FXCM-1-m5", "FXCM-2-h1"]',
    isEnabled: true
  })
}

afterEach(() => {
  knex.where.mockClear()
  knex.update.mockClear()
  mockedDeactivate.mockClear()
  mockedActivate.mockClear()
  mockedReplacer.mockClear()
})

test('activate instance and update db', async () => {
  await eventListener(eventMessage)

  expect(mockedActivate).toHaveBeenCalled()
  expect(mockedReplacer).toHaveBeenCalledWith(
    eventMessage.previousStrategyInstanceId,
    'no-op',
    eventMessage.strategyInstance
  )

  expect(knex.where).toHaveBeenCalled()
  const whereArgs = knex.where.mock.calls[0][0]
  expect(whereArgs).toMatchSnapshot()

  expect(knex.update).toHaveBeenCalled()
  const updateArgs = knex.update.mock.calls[0][0]
  expect(updateArgs).toMatchSnapshot()
})

test('deactivate instance and update db', async () => {
  const eventMessage = {
    previousStrategyInstanceId: 'test-previous-strategy-instance-id',
    strategyInstance: new StrategyInstance({
      id: 'test-new-strategy-instance-id',
      strategyId: 1,
      strategyParams: '{"test": "params"}',
      instrumentId: 1,
      dataStreams: '["FXCM-1-m5", "FXCM-2-h1"]',
      isEnabled: false
    })
  }

  await eventListener(eventMessage)

  expect(mockedDeactivate).toHaveBeenCalled()
  expect(mockedReplacer).toHaveBeenCalledWith(
    eventMessage.previousStrategyInstanceId,
    'no-op',
    eventMessage.strategyInstance
  )

  expect(knex.where).toHaveBeenCalled()
  const whereArgs = knex.where.mock.calls[0][0]
  expect(whereArgs).toMatchSnapshot()

  expect(knex.update).toHaveBeenCalled()
  const updateArgs = knex.update.mock.calls[0][0]
  expect(updateArgs).toMatchSnapshot()
})

test('update activated instance and leave it activated', async () => {
  await eventListener(eventMessage)

  expect(mockedDeactivate).toHaveBeenCalled()
  expect(mockedReplacer).toHaveBeenCalledWith(
    eventMessage.previousStrategyInstanceId,
    'no-op',
    eventMessage.strategyInstance
  )
  expect(mockedActivate).toHaveBeenCalled()

  expect(knex.where).toHaveBeenCalled()
  const whereArgs = knex.where.mock.calls[0][0]
  expect(whereArgs).toMatchSnapshot()

  expect(knex.update).toHaveBeenCalled()
  const updateArgs = knex.update.mock.calls[0][0]
  expect(updateArgs).toMatchSnapshot()
})
