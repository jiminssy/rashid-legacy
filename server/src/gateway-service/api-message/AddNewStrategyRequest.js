import { ApiMessage } from './ApiMessage'

export class AddNewStrategyRequest extends ApiMessage {
  constructor ({
    name,
    version,
    description,
    codePath,
    paramSchema
  }) {
    super()
    this.name = name
    this.version = version
    this.description = description
    this.codePath = codePath
    this.paramSchema = paramSchema
  }
}
