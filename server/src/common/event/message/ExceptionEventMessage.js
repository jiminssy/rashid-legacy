import UUID from 'uuid-js'
import { EventMessage } from './EventMessage'

export class ExceptionEventMessage extends EventMessage {
  constructor (
    eventType,
    errorName,
    errorMessage,
    errorStack,
    contextId = UUID.create().hex
  ) {
    super(eventType, contextId)
    this.errorName = errorName
    this.errorMessage = errorMessage
    this.errorStack = errorStack
  }
}
