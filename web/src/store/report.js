export const report = {
  dailyPerformance: {
    numDays: 14,
    numStrategyInstancesInCharts: 10,
    allPerformances: [],
    topStrategyInstances: [],
    bottomStrategyInstances: []
  },
  liveAccountPerformance: {
    accumulatedReturns: [],
    discreteReturns: []
  }
}
