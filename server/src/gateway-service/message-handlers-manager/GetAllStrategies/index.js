import { MessageHandler } from '../MessageHandler'
import { outMessageManager } from '../../out-message-manager'
import { getAllStrategies } from '../../../view-query-service/query-interface/strategy'

class GetAllStrategies extends MessageHandler {
  async handle (message) {
    const strategies = await getAllStrategies()
    outMessageManager.sendOkResponse(message.id, strategies)
  }
}

export const handler = new GetAllStrategies()
