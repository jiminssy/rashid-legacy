import React from 'react'
import jwt_decode from 'jwt-decode'
import {
  Switch,
  Route,
  useHistory
} from 'react-router-dom'
import { Nav } from '../../components/Nav'
import { LogIn } from '../LogIn'
import { Admin } from '../Admin'
import { useSelector } from '../../hooks/selector'
import { ACCESS_TOKEN } from '../../constants/local-storage'
import { publish } from '../../utils/event-bus'
import { LOGIN_ATTEMPTED } from '../../constants/events'
import { NOT_LOGGED_IN, LOGGED_IN } from '../../constants/user-login-state'

export const AuthApp = () => {
  const history = useHistory()
  let content = <div />
  const loginState = useSelector('user.loginState')

  const isLatestVersionToken = accessToken => {
    const decoded = jwt_decode(accessToken)
    return decoded.role != null
  }

  const handleNotLoggedIn = () => {
    const accessToken = localStorage.getItem(ACCESS_TOKEN)
    if (accessToken && isLatestVersionToken(accessToken)) {
      publish(LOGIN_ATTEMPTED, { accessToken: accessToken })
    } else {
      localStorage.removeItem(ACCESS_TOKEN)
      content = (
        <>
          <Nav />
          <LogIn />
        </>
      )
    }
  }

  const navigateToAdmin = () => {
    setTimeout(() => {
      history.push('/admin')
    }, 0)
  }

  if (loginState === NOT_LOGGED_IN) {
    handleNotLoggedIn()
  } else if (loginState === LOGGED_IN) {
    content = (
      <>
        <Nav />
        <Switch>
          <Route path="/admin">
            <Admin />
          </Route>
        </Switch>
      </>
    )

    if (window.location.pathname === '/') {
      navigateToAdmin()
    }
  }

  return (
    <main className="main-container">
      { content }
    </main>
  )
}
