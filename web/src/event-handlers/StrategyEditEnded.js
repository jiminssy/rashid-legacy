import { EventHandler } from './EventHandler'
import { STRATEGY_EDIT_ENDED } from '../constants/events'
import { setValue } from '../store'

class StrategyEditEnded extends EventHandler {
  constructor () {
    super([STRATEGY_EDIT_ENDED])
  }

  async handle () {
    await setValue('strategy.edit.strategy', null)
  }
}

export const eventHandler = new StrategyEditEnded()
