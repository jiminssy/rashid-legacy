import React, { useState } from 'react'
import { LOGIN_ATTEMPTED } from '../../constants/events'
import { publish } from '../../utils/event-bus'

export const LoginForm = () => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const onSubmit = e => {
    publish(LOGIN_ATTEMPTED, {
      userName: email,
      password: password,
    })
    e.preventDefault()
  }

  return (
    <form onSubmit={onSubmit}>
      <div className="form-group">
        <label htmlFor="exampleInputEmail1">Email address</label>
        <input
          type="email"
          className="form-control"
          placeholder="Enter email"
          autoComplete="username"
          value={email}
          onChange={e => setEmail(e.target.value)}
        />
      </div>
      <div className="form-group">
        <label htmlFor="exampleInputPassword1">Password</label>
        <input
          type="password"
          className="form-control"
          placeholder="Password"
          autoComplete="current-password"
          value={password}
          onChange={e => setPassword(e.target.value)}
        />
      </div>
      <button type="submit" className="btn btn-primary">Login</button>
      <small className="form-text text-muted">If you are having trouble, email jiminparky@gmail.com</small>
    </form>
  )
}
