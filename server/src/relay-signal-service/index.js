import { subscribe } from '../common/utils/error-publishing-event-subscriber'
import {
  ERROR_RELAY_SIGNAL_SERVICE_EVENT_LISTENER,
  LIVE_TRADE_SIGNAL
} from '../common/event/event-type'
import { getOrderClientsForRelayAccounts } from '../common/fxcm/client-factory'

const subscribes = [
  [LIVE_TRADE_SIGNAL, require('./event-listener/place-order-to-relay-trading-accounts').eventListener]
]

let orderClients

export const getOrderClients = () => orderClients

export const start = async () => {
  orderClients = getOrderClientsForRelayAccounts()
  for (let i = 0; i < orderClients.length; i++) {
    await orderClients[i].initialize()
  }

  subscribe(
    'RelaySignalService',
    ERROR_RELAY_SIGNAL_SERVICE_EVENT_LISTENER,
    subscribes
  )
}
