import { handler } from '..'
import { outMessageManager } from '../../../out-message-manager'
import * as queryInterface from '../../../../view-query-service/query-interface/strategy'
import { mockPublish, unmockPublish } from '../../../../common/test-mock/mock-event-bus'
import { publish } from '../../../../common/event/event-bus'
import { RemoveStrategyRequest } from '../../../api-message/RemoveStrategyRequest'

jest.mock('uuid-js', () => ({
  create: () => ({ hex: 'test-context-id' })
}))
outMessageManager.sendOkResponse = jest.fn()

const testApiMessage = new RemoveStrategyRequest({
  strategyId: 'test-strategy-id'
})

describe('handle', () => {
  beforeEach(() => {
    outMessageManager.sendOkResponse.mockReset()
    mockPublish()
  })

  afterEach(() => {
    unmockPublish()
  })

  test('fail - strategy does not exist', async () => {
    queryInterface.doesStrategyExistById = jest.fn().mockResolvedValue(false)

    await handler.handle(testApiMessage)

    expect(outMessageManager.sendOkResponse).toHaveBeenCalledWith(testApiMessage.id, {
      isSuccess: false,
      failReason: 'Strategy does not exist'
    })
  })

  test('fail - strategy instances exist', async () => {
    queryInterface.doesStrategyExistById = jest.fn().mockResolvedValue(true)
    queryInterface.doesStrategyHaveInstances = jest.fn().mockResolvedValue(true)

    await handler.handle(testApiMessage)

    expect(outMessageManager.sendOkResponse).toHaveBeenCalledWith(testApiMessage.id, {
      isSuccess: false,
      failReason: 'All instances of this strategy need to be removed first'
    })
  })

  test('success', async () => {
    queryInterface.doesStrategyExistById = jest.fn().mockResolvedValue(true)
    queryInterface.doesStrategyHaveInstances = jest.fn().mockResolvedValue(false)

    await handler.handle(testApiMessage)

    expect(outMessageManager.sendOkResponse).toHaveBeenCalledWith(testApiMessage.id, {
      isSuccess: true
    })
    const publishedMessage = publish.mock.calls[0][0]
    expect(publishedMessage).toMatchSnapshot()
  })
})
