import { MessageHandler } from '../MessageHandler'
import { outMessageManager } from '../../out-message-manager'
import { doesStrategyExistByNameVersion } from '../../../view-query-service/query-interface/strategy'
import { publish } from '../../../common/event/event-bus'
import { AddNewStrategy as AddNewStrategyMessage } from '../../../common/event/message/AddNewStrategy'
import { Strategy } from '../../../common/definition/Strategy'

class AddNewStrategy extends MessageHandler {
  async handle (message) {
    if (await doesStrategyExistByNameVersion(message.name, message.version)) {
      this._handleFail(message.id, 'Strategy with same name and version exists')
      return
    }

    publish(new AddNewStrategyMessage(
      new Strategy(message)
    ))
    outMessageManager.sendOkResponse(message.id, { isSuccess: true })
  }

  _handleFail (messageId, failReason) {
    outMessageManager.sendOkResponse(messageId, {
      isSuccess: false,
      failReason
    })
  }
}

export const handler = new AddNewStrategy()
