import moment from 'moment-timezone'
import {
  doesPositionRecordExist,
  getPosition,
  updatePositionCloseTime,
  updatePositionOpenTime
} from './database'
import { getResultWithRetries, GetResultRetriesError } from '../../common/utils/get-result-with-retries'
import { publish } from '../../common/event/event-bus'
import { ExceptionEventMessage } from '../../common/event/message/ExceptionEventMessage'
import { SimulationPositionClosed } from '../../common/event/message/SimulationPositionClosed'
import {
  ERROR_SIMULATION_CLOSE_POSITION_UPDATE_FAIL,
  ERROR_SIMULATION_OPEND_POSITION_UPDATE_FAIL
} from '../../common/event/event-type'

const calcOutcomeScore = (open, stopLoss, close, sizeMultiplier) => {
  const unitValue = Math.abs(open - stopLoss)
  const outcomeValue = Math.abs(close - open)
  const outcomeScoreRaw = (outcomeValue / unitValue) * sizeMultiplier
  let outcomeScore = Number(outcomeScoreRaw.toFixed(2))
  if (
    (stopLoss < open && close < open) ||
    (stopLoss > open && close > open)
  ) {
    outcomeScore *= -1
  }
  return outcomeScore
}

const waitPositionDbRecordExist = async (brokerId, brokerTradeId) => {
  const closureToRetry = () => {
    return doesPositionRecordExist(brokerId, brokerTradeId)
  }
  const isResultSuccess = result => result
  try {
    await getResultWithRetries(
      closureToRetry,
      isResultSuccess,
      500,
      120 // try 120 times for 1 minute
    )
  } catch (error) {
    if (error instanceof GetResultRetriesError) {
      throw new Error(`Timeout: did not find position db record for tradeId: ${brokerTradeId}`)
    } else {
      throw error
    }
  }
}

export const updateOpenedPosition = async socketMessage => {
  try {
    const brokerTradeId = socketMessage.tradeId
    await waitPositionDbRecordExist(1, brokerTradeId) // brokerId 1 is FXCM
    const openedRawTimestamp = socketMessage.time
    const openedTime = moment.utc(openedRawTimestamp, 'MMDDYYYYHHmmss').toISOString()
    await updatePositionOpenTime(brokerTradeId, openedTime)
  } catch (error) {
    publish(new ExceptionEventMessage(
      ERROR_SIMULATION_OPEND_POSITION_UPDATE_FAIL,
      error.name,
      error.message,
      error.stack
    ))
  }
}

export const updateClosedPosition = async socketMessage => {
  try {
    const brokerTradeId = socketMessage.tradeId
    await waitPositionDbRecordExist(1, brokerTradeId) // brokerId 1 is FXCM
    const {
      open,
      sizeMultiplier,
      stopLoss
    } = await getPosition(1, brokerTradeId) // brokerId 1 is FXCM
    const closedRawTimestamp = socketMessage.closeTime

    let closedTime = moment.utc(closedRawTimestamp, 'MMDDYYYYHHmmss')
    if (closedTime.isoWeekday() > 5) {
      // there are usually very few trades that close on Sunday that we are
      // considering them as trades that happened on Friday.
      // This is important when calculating daily outcome score because the few
      // closed positions become entire day's outcome score and it's unbalanced
      const subtractDays = closedTime.isoWeekday() - 5
      closedTime = closedTime.subtract(subtractDays, 'days')
        .set({
          hour: 23,
          minute: 59,
          second: 59,
          millisecond: 999
        })
    }
    closedTime = closedTime.format()

    const outcomeScore = calcOutcomeScore(
      open,
      stopLoss,
      socketMessage.close,
      sizeMultiplier
    )

    publish(new SimulationPositionClosed(
      brokerTradeId,
      closedTime,
      socketMessage.close,
      outcomeScore
    ))

    await updatePositionCloseTime(
      brokerTradeId,
      closedTime,
      socketMessage.close,
      outcomeScore
    )
  } catch (error) {
    publish(new ExceptionEventMessage(
      ERROR_SIMULATION_CLOSE_POSITION_UPDATE_FAIL,
      error.name,
      error.message,
      error.stack
    ))
  }
}

export const __test__ = {
  calcOutcomeScore
}
