import { EventMessage } from './EventMessage'
import { EDIT_STRATEGY } from '../event-type'

export class EditStrategy extends EventMessage {
  constructor (strategy) {
    super(EDIT_STRATEGY)
    this.strategy = strategy
  }
}
