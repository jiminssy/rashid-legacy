const eventHandlers = [
  require('./LogIn').eventHandler,
  require('./AddNewStrategy').eventHandler,
  require('./AddNewStrategyInstance').eventHandler,
  require('./ChangeDailyPerformanceDays').eventHandler,
  require('./EditStrategy').eventHandler,
  require('./EditStrategyInstance').eventHandler,
  require('./GetAllStrategies').eventHandler,
  require('./GetAllStrategyInstances').eventHandler,
  require('./LoadReportDailyPerformance').eventHandler,
  require('./LoadReportDPStrategyInstances').eventHandler,
  require('./LoadReportLiveAccountPerformance').eventHandler,
  require('./RemoveStrategy').eventHandler,
  require('./RemoveStrategyInstance').eventHandler,
  require('./StrategyEditEnded').eventHandler,
  require('./StrategyEditStrarted').eventHandler,
  require('./StrategyInstanceEditStarted').eventHandler,
  require('./StrategyInstanceEditEnded').eventHandler
]

export const subscribeAllHandlers = () => {
  eventHandlers.forEach(eventHandler => eventHandler.subscribe())
}
