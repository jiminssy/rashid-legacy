import { Instrument } from './Instrument'

export class FxcmInstrument extends Instrument {
  constructor (assetClass, symbol, fxcmInstrumentId) {
    super(assetClass, symbol)
    this.fxcmInstrumentId = fxcmInstrumentId
  }
}
