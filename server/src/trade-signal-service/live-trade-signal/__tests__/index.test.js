import { convertToLiveTradeSignal } from '../index'
import * as factory from '../../../common/fxcm/client-factory'
import * as dataStreamIdParser from '../../../common/utils/data-stream-id-parser'
import * as settings from '../../../settings'
import { TradeDirection } from '../../../common/definition/TradeDirection'
import { OrderType } from '../../../common/definition/OrderType'
import { mockDateNow } from '../../../common/test-mock/mock-date-now'

jest.mock('uuid-js', () => ({
  create: () => ({ hex: 'test-context-id' })
}))
mockDateNow()

describe('convertToLiveTradeSignal', () => {
  settings.CAPITAL_RISK_PER_TRADE_PERCENT = 0.002
  factory.getDataClient = jest.fn().mockReturnValue({
    getTradingAccount: jest.fn().mockResolvedValue({ balance: 50000 }),
    getInstrumentRealtimeInfo: jest.fn().mockResolvedValue({ pip: 0.0001, pipCost: 0.07235 })
  })
  dataStreamIdParser.parseDataStreamId = jest.fn().mockReturnValue('FXCM-1-m1')

  test('market long with 1 tradeSignalSizeMultiplier and 1 riskResultSizeMultiplier', async () => {
    const strategyTradeSignal = {
      strategyInstanceId: 'mock-strategy-instance-id-1',
      tradingInstrument: 'FXCM-1-m1',
      tradeDirection: TradeDirection.LONG,
      orderType: OrderType.MARKET,
      stopLossRange: 0.0009625,
      stopLoss: 1.18549,
      takeProfit: 1.18934,
      limitPrice: null,
      positionSizeMultiplier: 1
    }
    const riskResult = {
      isSuccess: true,
      positionSizeMultiplier: 1
    }

    const liveTradeSignal = await convertToLiveTradeSignal(strategyTradeSignal, riskResult)

    expect(liveTradeSignal).toMatchSnapshot()
  })

  test('market short with 3 tradeSignalSizeMultiplier and 1 riskResultSizeMultiplier', async () => {
    const strategyTradeSignal = {
      strategyInstanceId: 'mock-strategy-instance-id-1',
      tradingInstrument: 'FXCM-1-m1',
      tradeDirection: TradeDirection.SHORT,
      orderType: OrderType.MARKET,
      stopLossRange: 0.0008825,
      stopLoss: 1.18921,
      takeProfit: 1.18568,
      limitPrice: null,
      positionSizeMultiplier: 3
    }
    const riskResult = {
      isSuccess: true,
      positionSizeMultiplier: 1
    }

    const liveTradeSignal = await convertToLiveTradeSignal(strategyTradeSignal, riskResult)

    expect(liveTradeSignal).toMatchSnapshot()
  })

  test('market short with 3 tradeSignalSizeMultiplier and 0.75 riskResultSizeMultiplier', async () => {
    const strategyTradeSignal = {
      strategyInstanceId: 'mock-strategy-instance-id-1',
      tradingInstrument: 'FXCM-1-m1',
      tradeDirection: TradeDirection.SHORT,
      orderType: OrderType.MARKET,
      stopLossRange: 0.0008825,
      stopLoss: 1.18921,
      takeProfit: 1.18568,
      limitPrice: null,
      positionSizeMultiplier: 3
    }
    const riskResult = {
      isSuccess: true,
      positionSizeMultiplier: 0.75
    }

    const liveTradeSignal = await convertToLiveTradeSignal(strategyTradeSignal, riskResult)

    expect(liveTradeSignal).toMatchSnapshot()
  })
})
