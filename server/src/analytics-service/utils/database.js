import { knex } from '../../common/database/knex'

const broker = 'analytics_broker'
const dailyOutcomeScore = 'analytics_dailyOutcomeScore'
const liveAccount = 'analytics_liveAccount'
const liveAccountHistory = 'analytics_liveAccountHistory'
const liveAccountDailyOutcomeScore = 'analytics_liveAccountDailyOutcomeScore'
const liveStrategyInstance = 'analytics_liveStrategyInstance'
const strategyInstance = 'analytics_strategyInstance'

const doesDailyOutcomeScoreExist = async (strategyInstanceId, date) => {
  const result = await knex(dailyOutcomeScore)
    .select()
    .where({ strategyInstanceId, date })
  return result.length > 0
}

const insertDailyOutcomeScore = async (strategyInstanceId, date, outcomeScore) => {
  await knex(dailyOutcomeScore).insert({
    strategyInstanceId,
    date,
    outcomeScore
  })
}

const updateDailyOutcomeScore = async (strategyInstanceId, date, outcomeScore) => {
  await knex(dailyOutcomeScore)
    .where({ strategyInstanceId, date })
    .update({ outcomeScore })
}

export const addStrategyInstance = async (id, isEnabled) => {
  await knex(strategyInstance).insert({ id, isEnabled })
}

export const editStrategyInstance = async (previousId, id, isEnabled) => {
  await knex(strategyInstance)
    .where({ id: previousId })
    .update({
      id,
      isEnabled
    })
}

export const getDailyOutcomeScores = async (strategyInstanceId, numMostRecentDays) => {
  const rows = await knex(dailyOutcomeScore)
    .select('outcomeScore')
    .where({ strategyInstanceId })
    .orderBy('date', 'desc')
    .limit(numMostRecentDays)
  return rows.map(row => row.outcomeScore)
}

export const getDailyOutcomeScoresSum = async (strategyInstanceIds, date) => {
  const rows = await knex(dailyOutcomeScore)
    .select('outcomeScore')
    .whereIn('strategyInstanceId', strategyInstanceIds)
    .andWhere({ date })

  if (rows.length === 0) {
    return null
  } else {
    return rows.map(row => row.outcomeScore)
      .reduce((total, score) => total + score, 0)
  }
}

export const getDbAccountId = async (brokerName, brokerAccountId) => {
  const account = await knex(liveAccount)
    .select()
    .innerJoin(broker, `${broker}.id`, `${liveAccount}.brokerId`)
    .where({
      [`${broker}.name`]: brokerName,
      [`${liveAccount}.brokerAccountId`]: brokerAccountId
    })
    .first()
  return account.id
}

export const getEnabledStrategyInstances = async () => {
  const rows = await knex(strategyInstance).select('id')
    .where({
      isEnabled: true
    })
  return rows.map(row => row.id)
}

export const getLastAccountHistoryRecord = async (
  accountId,
  pivotDate
) => {
  return await knex(liveAccountHistory)
    .select()
    .where({ accountId })
    .andWhere('date', '<', pivotDate)
    .orderBy('date', 'desc')
    .limit(1)
    .first()
}

export const getLastDayOutcomeScore = async (strategyInstanceId, pivotDay) => {
  return await knex(dailyOutcomeScore)
    .select()
    .where('strategyInstanceId', strategyInstanceId)
    .andWhere('date', '<', pivotDay)
    .orderBy('date', 'desc')
    .limit(1)
    .first()
}

export const getLastTradingDay = async pivotDay => {
  const { date } = await knex(dailyOutcomeScore)
    .select('date')
    .where('date', '<', pivotDay)
    .orderBy('date', 'desc')
    .limit(1)
    .first()
  return date
}

export const getLiveTradingAllowedStrategyInstsanceIds = async () => {
  const rows = await knex(liveStrategyInstance).select()
  return rows.map(row => row.strategyInstanceId)
}

export const getLiveAccountDailyOutcomeScores = async numMostRecentDays => {
  const rows = await knex(liveAccountDailyOutcomeScore)
    .select('outcomeScore')
    .orderBy('date', 'desc')
    .limit(numMostRecentDays)
  return rows.map(row => row.outcomeScore)
}

export const insertAccountHistoryRecord = async (
  accountId,
  date,
  dayStartBalance
) => {
  await knex(liveAccountHistory).insert({
    accountId,
    date,
    dayStartBalance
  })
}

export const insertLiveAccountDailyOutcomeScore = async (date, outcomeScore) => {
  await knex(liveAccountDailyOutcomeScore).insert({
    date,
    outcomeScore
  })
}

export const insertOrUpdateDailyOutcomeScore = async (strategyInstanceId, date, outcomeScore) => {
  if (await doesDailyOutcomeScoreExist(strategyInstanceId, date)) {
    await updateDailyOutcomeScore(strategyInstanceId, date, outcomeScore)
  } else {
    await insertDailyOutcomeScore(strategyInstanceId, date, outcomeScore)
  }
}

export const insertStrategyInstance = async ({ id, isEnabled }) => {
  await knex(strategyInstance).insert({ id, isEnabled })
}

export const removeStrategyInstance = async id => {
  await knex(strategyInstance)
    .where({ id })
    .del()
}

export const updateLastAccountHistoryRecordDayReturn = async (
  accountId,
  date,
  dayReturn
) => {
  await knex(liveAccountHistory)
    .where({
      accountId,
      date
    })
    .update({
      dayReturn
    })
}
