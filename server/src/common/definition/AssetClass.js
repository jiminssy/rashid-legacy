export const AssetClass = Object.freeze({
  FX: 'FX',
  Stocks: 'Stocks',
  Bonds: 'Bonds',
  CryptoCurrencies: 'CryptoCurrencies'
})
