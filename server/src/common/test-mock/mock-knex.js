/*
This module is only used for asserts on knex methods like select, insert, etc.
If there is need for mocking what the data query results should be,
consider using the _setResults in __mocks__/knex.js module.
*/

import knex from 'knex'
import * as knexUtil from '../database/knex'

jest.mock('knex')

export const getMockKnex = () => {
  knexUtil.knex = knex
  return knex()
}
