import { EventMessage } from './EventMessage'
import { ANALYTICS_LIVE_ACCOUNT_DAILY_STOP_LOSS } from '../event-type'

export class LiveAccountDailyStopLoss extends EventMessage {
  constructor (
    brokerName,
    brokerAccountId,
    date,
    stopLossOutcomeScore
  ) {
    super(ANALYTICS_LIVE_ACCOUNT_DAILY_STOP_LOSS)
    this.brokerName = brokerName
    this.brokerAccountId = brokerAccountId
    this.date = date
    this.stopLossOutcomeScore = stopLossOutcomeScore
  }
}
