import { MessageHandler } from '../MessageHandler'
import { outMessageManager } from '../../out-message-manager'
import { getStrategyInstance } from '../../../view-query-service/query-interface/strategy'

class GetStrategyInstance extends MessageHandler {
  async handle (message) {
    const instance = await getStrategyInstance(message.strategyInstanceId)
    if (instance) {
      outMessageManager.sendOkResponse(message.id, {
        isSuccess: true,
        strategyInstance: instance
      })
    } else {
      outMessageManager.sendOkResponse(message.id, {
        isSuccess: false
      })
    }
  }
}

export const handler = new GetStrategyInstance()
