require('dotenv').config()

var migrationDirs = [
  './src/user-management-service/__db__/migrations',
  './src/view-query-service/__db__/migrations',
  './src/strategy-service/__db__/migrations',
  './src/simulation-service/__db__/migrations',
  './src/trade-signal-service/__db__/migrations',
  './src/analytics-service/__db__/migrations'
]

module.exports = {

  development: {
    client: 'postgresql',
    connection: {
      host: process.env.DB_HOST,
      database: 'rashid',
      user: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      directory: migrationDirs,
      tableName: 'knex_migrations',
    },
    seeds: {
      directory: [
        './src/strategy-service/__db__/seeds/common',
        './src/view-query-service/__db__/seeds/common',
        './src/simulation-service/__db__/seeds/common',
        './src/analytics-service/__db__/seeds/common',
        './src/user-management-service/__db__/seeds/development',
        './src/strategy-service/__db__/seeds/development',
        './src/view-query-service/__db__/seeds/development'
      ],
    },
  },

  production: {
    client: 'postgresql',
    connection: {
      host: process.env.DB_HOST,
      database: 'rashid',
      user: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      directory: migrationDirs,
      tableName: 'knex_migrations',
    },
    seeds: {
      directory: [
        './src/view-query-service/__db__/seeds/common',
        './src/user-management-service/__db__/seeds/production'        
      ],
    },
  }

};
