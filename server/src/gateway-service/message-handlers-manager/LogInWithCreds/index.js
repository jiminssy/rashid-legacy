import { MessageHandler } from '../MessageHandler'
import { outMessageManager } from '../../out-message-manager'
import { logIn } from '../../../user-management-service'

class LogInWithCreds extends MessageHandler {
  async handle (message) {
    const userName = message.userName
    const password = message.password
    const accessToken = await logIn(userName, password)
    if (accessToken) {
      outMessageManager.sendOkResponse(message.id, accessToken)
    } else {
      outMessageManager.sendFailedAuthentication(message.id)
    }
  }
}

export const handler = new LogInWithCreds()
