import { outMessageManager } from '..'

test('wait for result and send ok response', () => {
  const sendOkResponse = jest.fn()
  const sendErrorResponse = jest.fn()
  const requestMessageId = 'test_id_1234'
  const response = 'test_response'

  outMessageManager.waitResultToRespond(requestMessageId, sendOkResponse, sendErrorResponse)
  expect(outMessageManager.waitingResults.size).toBe(1)

  outMessageManager.sendOkResponse(requestMessageId, response)
  expect(outMessageManager.waitingResults.size).toBe(0)

  expect(sendOkResponse).toHaveBeenCalledWith(response)
  expect(sendErrorResponse).not.toHaveBeenCalled()
})
