import { EventHandler } from './EventHandler'
import { REMOVE_STRATEGY_INSTANCE } from '../constants/events'
import { removeStrategyInstance } from '../utils/rashid-client'
import { notification } from '../utils/notification'

class RemoveStrategyInstance extends EventHandler {
  constructor () {
    super([REMOVE_STRATEGY_INSTANCE])
  }

  async handle (instanceId) {
    const result = await removeStrategyInstance(instanceId)
    if (result.isSuccess) {
      notification.success('Strategy instance removed')
      window.rrHistory.push('/admin/strategy')
    } else {
      notification.error(result.failReason)
    }
  }
}

export const eventHandler = new RemoveStrategyInstance()
