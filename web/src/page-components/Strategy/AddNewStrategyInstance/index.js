import React from 'react'
import styles from './index.module.scss'
import { StrategyInstanceForm } from '../../../components/StrategyInstanceForm'

export const AddNewStrategyInstance = () => {
  return (
    <div>
      <h4 className="page-title">Add New Strategy Instance</h4>
      <div className={`card ${styles.formContainer}`}>
        <div className="card-body">
          <StrategyInstanceForm />
        </div>
      </div>
    </div>
  )
}
