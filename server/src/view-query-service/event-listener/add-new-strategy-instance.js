import { knex } from '../../common/database/knex'
import { DbTable } from '../definition/DbTable'

export const eventListener = async ({ strategyInstance }) => {
  await knex(DbTable.STRATEGY_INSTANCE).insert({
    ...strategyInstance,
    dataStreams: JSON.stringify(strategyInstance.dataStreams),
    strategyParams: JSON.stringify(strategyInstance.strategyParams)
  })
}
