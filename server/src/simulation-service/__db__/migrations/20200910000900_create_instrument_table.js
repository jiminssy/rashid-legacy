
exports.up = function (knex) {
  return knex.schema.createTable('simulation_broker', function (table) {
    table.increments()
    table.string('name').notNullable()
  })
    .createTable('simulation_assetClass', function (table) {
      table.increments()
      table.string('name').notNullable()
    })
    .createTable('simulation_instrument', function (table) {
      table.increments()
      table.integer('assetClassId').notNullable().references('id')
        .inTable('simulation_assetClass')
      table.integer('brokerId').notNullable().references('id')
        .inTable('simulation_broker')
      table.string('brokerInstrumentId').notNullable()
      table.string('name').notNullable()
      table.unique(['brokerId', 'brokerInstrumentId'])
    })
}

exports.down = function (knex) {
  return knex.schema.dropTable('simulation_instrument')
    .dropTable('simulation_assetClass')
    .dropTable('simulation_broker')
}
