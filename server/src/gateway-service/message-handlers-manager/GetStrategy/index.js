import { MessageHandler } from '../MessageHandler'
import { outMessageManager } from '../../out-message-manager'
import { getStrategy } from '../../../view-query-service/query-interface/strategy'

class GetStrategy extends MessageHandler {
  async handle (message) {
    const strategy = await getStrategy(message.strategyId)
    if (strategy) {
      outMessageManager.sendOkResponse(message.id, {
        isSuccess: true,
        strategy
      })
    } else {
      outMessageManager.sendOkResponse(message.id, {
        isSuccess: false
      })
    }
  }
}

export const handler = new GetStrategy()
