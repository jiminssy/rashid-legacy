import { subscribe } from '../common/utils/error-publishing-event-subscriber'
import {
  ADD_NEW_STRATEGY_INSTANCE,
  EDIT_STRATEGY_INSTANCE,
  ERROR_ANALYTICS_SERVICE_EVENT_LISTENER,
  NEW_UTC_WEEK_DAY,
  REMOVE_STRATEGY_INSTANCE,
  SIMULATION_DAILY_OUTCOME_SCORES
} from '../common/event/event-type'
import { SystemService } from '../common/definition/SystemService'
import { getDataClient } from '../common/fxcm/client-factory'

const subscribes = [
  [ADD_NEW_STRATEGY_INSTANCE, require('./event-listener/add-new-strategy-instance').eventListener],
  [EDIT_STRATEGY_INSTANCE, require('./event-listener/edit-strategy-instance').eventListener],
  [NEW_UTC_WEEK_DAY, require('./event-listener/calculate-strategy-instances-daily-stop-loss').eventListener],
  [NEW_UTC_WEEK_DAY, require('./event-listener/calculate-live-account-daily-stop-loss').eventListener],
  [NEW_UTC_WEEK_DAY, require('./event-listener/update-daily-live-account-info').eventListener],
  [REMOVE_STRATEGY_INSTANCE, require('./event-listener/remove-strategy-instance').eventListener],
  [SIMULATION_DAILY_OUTCOME_SCORES, require('./event-listener/update-strategy-instance-daily-outcome-score').eventListener]
]

export const start = async () => {
  const dataClient = getDataClient(SystemService.ANALYTICS)
  await dataClient.initialize()

  subscribe(
    'AnalyticsService',
    ERROR_ANALYTICS_SERVICE_EVENT_LISTENER,
    subscribes
  )
}
