import { MessageHandler } from '../MessageHandler'
import { outMessageManager } from '../../out-message-manager'
import { getAllStrategyInstances } from '../../../view-query-service/query-interface/strategy'

class GetAllStrategies extends MessageHandler {
  async handle (message) {
    const instances = await getAllStrategyInstances()
    outMessageManager.sendOkResponse(message.id, instances)
  }
}

export const handler = new GetAllStrategies()
