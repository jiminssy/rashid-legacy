import BottomOutcomeScoreStrategyReverse from '..'
import { publish } from '../../../../common/event/event-bus'
import { mockPublish, unmockPublish } from '../../../../common/test-mock/mock-event-bus'
import { SimulationPositionOpened } from '../../../../common/event/message/SimulationPositionOpened'
import { TradeDirection } from '../../../../common/definition/TradeDirection'
import { OrderType } from '../../../../common/definition/OrderType'
import * as factory from '../../../../common/fxcm/client-factory'

jest.mock('uuid-js', () => ({
  create: () => ({ hex: 'test-context-id' })
}))

const testStrategyInstance = new BottomOutcomeScoreStrategyReverse({
  dataStreams: [],
  id: 'test-strategy-instance-id',
  tradingInstrument: 'FXCM-1-m1'
})

describe('_calcReversedStopLossAndTakeProfit', () => {
  test('source opened long position', async () => {
    factory.getDataClient = jest.fn().mockReturnValue({
      getInstrumentRealtimeInfo: jest.fn().mockResolvedValue({
        pip: 0.1,
        spread: 5
      })
    })

    const {
      reversedStopLoss,
      reversedStopLossRange,
      reversedTakeProfit
    } = await testStrategyInstance._calcReversedStopLossAndTakeProfit(
      '1',
      TradeDirection.LONG,
      10,
      6,
      18
    )

    expect(reversedStopLoss).toBe(16.5)
    expect(reversedStopLossRange).toBe(7)
    expect(reversedTakeProfit).toBe(6)
  })

  test('source opened short position', async () => {
    factory.getDataClient = jest.fn().mockReturnValue({
      getInstrumentRealtimeInfo: jest.fn().mockResolvedValue({
        pip: 0.1,
        spread: 5
      })
    })

    const {
      reversedStopLoss,
      reversedStopLossRange,
      reversedTakeProfit
    } = await testStrategyInstance._calcReversedStopLossAndTakeProfit(
      '1',
      TradeDirection.SHORT,
      10,
      14,
      2
    )

    expect(reversedStopLoss).toBe(3.5)
    expect(reversedStopLossRange).toBe(7)
    expect(reversedTakeProfit).toBe(14)
  })
})

describe('_onSimulationPositionOpened', () => {
  beforeEach(() => {
    mockPublish()
  })

  afterEach(() => {
    unmockPublish()
  })

  test('long market order', async () => {
    const worstStrategyInstanceId = 'worst-strategy-instance-id'
    testStrategyInstance._targetStrategyInstanceId = worstStrategyInstanceId
    const buyMarketSignal = new SimulationPositionOpened(
      worstStrategyInstanceId,
      'FXCM-1-m1',
      'mock-broker-order-id',
      'mock-broker-position-id',
      TradeDirection.LONG,
      OrderType.MARKET,
      35.2873,
      25.3223,
      67.3342,
      null
    )
    testStrategyInstance._calcReversedStopLossAndTakeProfit = jest.fn().mockResolvedValue({
      reversedStopLoss: 67.3342,
      reversedStopLossRange: 10.5029,
      reversedTakeProfit: 25.3223
    })

    await testStrategyInstance._onSimulationPositionOpened(buyMarketSignal)

    const reversedSignal = publish.mock.calls[0][0]
    expect(reversedSignal).toMatchSnapshot()
  })

  test('short market order', async () => {
    const worstStrategyInstanceId = 'worst-strategy-instance-id'
    testStrategyInstance._targetStrategyInstanceId = worstStrategyInstanceId
    const buyMarketSignal = new SimulationPositionOpened(
      worstStrategyInstanceId,
      'FXCM-1-m1',
      'mock-broker-order-id',
      'mock-broker-position-id',
      TradeDirection.SHORT,
      OrderType.MARKET,
      550.2354,
      623.12345,
      473.2332,
      null
    )
    testStrategyInstance._calcReversedStopLossAndTakeProfit = jest.fn().mockResolvedValue({
      reversedStopLoss: 473.2332,
      reversedStopLossRange: 37.4725,
      reversedTakeProfit: 623.12345
    })

    await testStrategyInstance._onSimulationPositionOpened(buyMarketSignal)

    const reversedSignal = publish.mock.calls[0][0]
    expect(reversedSignal).toMatchSnapshot()
  })

  test('long limit order', async () => {
    const worstStrategyInstanceId = 'worst-strategy-instance-id'
    testStrategyInstance._targetStrategyInstanceId = worstStrategyInstanceId
    const buyMarketSignal = new SimulationPositionOpened(
      worstStrategyInstanceId,
      'FXCM-1-m1',
      'mock-broker-order-id',
      'mock-broker-position-id',
      TradeDirection.LONG,
      OrderType.LIMIT,
      35.9753,
      25.3223,
      67.3342,
      35.9753
    )
    testStrategyInstance._calcReversedStopLossAndTakeProfit = jest.fn().mockResolvedValue({
      reversedStopLoss: 67.3342,
      reversedStopLossRange: 10.5029,
      reversedTakeProfit: 25.3223
    })

    await testStrategyInstance._onSimulationPositionOpened(buyMarketSignal)

    const reversedSignal = publish.mock.calls[0][0]
    expect(reversedSignal).toMatchSnapshot()
  })

  test('short limit order', async () => {
    const worstStrategyInstanceId = 'worst-strategy-instance-id'
    testStrategyInstance._targetStrategyInstanceId = worstStrategyInstanceId
    const buyMarketSignal = new SimulationPositionOpened(
      worstStrategyInstanceId,
      'FXCM-1-m1',
      'mock-broker-order-id',
      'mock-broker-position-id',
      TradeDirection.SHORT,
      OrderType.LIMIT,
      52.8753,
      67.3342,
      25.3223,
      52.8753
    )
    testStrategyInstance._calcReversedStopLossAndTakeProfit = jest.fn().mockResolvedValue({
      reversedStopLoss: 25.3223,
      reversedStopLossRange: 31.5087,
      reversedTakeProfit: 67.3342
    })

    await testStrategyInstance._onSimulationPositionOpened(buyMarketSignal)

    const reversedSignal = publish.mock.calls[0][0]
    expect(reversedSignal).toMatchSnapshot()
  })

  test('ignore not interested trade signal', async () => {
    const notInterestedId = 'not-interested-id'
    const worstStrategyInstanceId = 'worst-strategy-instance-id'
    testStrategyInstance._targetStrategyInstanceId = worstStrategyInstanceId
    const buyMarketSignal = new SimulationPositionOpened(
      notInterestedId,
      'FXCM-1-m1',
      'mock-broker-order-id',
      'mock-broker-position-id',
      TradeDirection.SHORT,
      OrderType.LIMIT,
      52.8753,
      67.3342,
      25.3223,
      52.8753
    )
    testStrategyInstance._calcReversedStopLossAndTakeProfit = jest.fn().mockResolvedValue({
      reversedStopLoss: 25.3223,
      reversedStopLossRange: 31.5087,
      reversedTakeProfit: 67.3342
    })

    await testStrategyInstance._onSimulationPositionOpened(buyMarketSignal)

    expect(publish).not.toBeCalled()
  })
})
