export const UserRole = Object.freeze({
  ADMIN: 'Admin',
  READ_ONLY: 'ReadOnly'
})
