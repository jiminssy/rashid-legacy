import React from 'react'
import styles from './index.module.scss'
import { useHistory } from 'react-router-dom'

export const AdminSideBar = () => {
  const history = useHistory()

  const getNavItem = (fasIcon, label, url) => {
    let className = `nav-item ${styles.navItem}`
    if (window.location.pathname.includes(url)) {
      className += ` ${styles.active}`
    }

    return (
      <li className={className} onClick={() => history.push(url)}>
        <i className={`fas ${styles.faIcon} ${fasIcon}`} />
        <span>{label}</span>
      </li>
    )
  }

  return (
    <nav id="sidebarMenu" className={`col-md-3 col-lg-2 d-md-block bg-light sidebar collapse ${styles.sidebar}`}>
      <div className="sidebar-sticky pt-3">
        <ul className="nav flex-column">
          { getNavItem('fa-chess', 'Strategy', '/admin/strategy') }
        </ul>
        <ul className="nav flex-column">
          { getNavItem('fa-table', 'Report', '/admin/report') }
        </ul>
      </div>
    </nav>
  )
}
