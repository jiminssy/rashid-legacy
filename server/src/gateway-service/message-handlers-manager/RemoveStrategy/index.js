import { MessageHandler } from '../MessageHandler'
import { outMessageManager } from '../../out-message-manager'
import {
  doesStrategyExistById,
  doesStrategyHaveInstances
} from '../../../view-query-service/query-interface/strategy'
import { publish } from '../../../common/event/event-bus'
import { RemoveStrategy as RemoveStrategyMessage } from '../../../common/event/message/RemoveStrategy'

class RemoveStrategy extends MessageHandler {
  async handle (message) {
    if (!(await doesStrategyExistById(message.strategyId))) {
      this._handleFail(message.id, 'Strategy does not exist')
      return
    }

    if (await doesStrategyHaveInstances(message.strategyId)) {
      this._handleFail(message.id, 'All instances of this strategy need to be removed first')
      return
    }

    publish(new RemoveStrategyMessage(message.strategyId))
    outMessageManager.sendOkResponse(message.id, { isSuccess: true })
  }

  _handleFail (messageId, failReason) {
    outMessageManager.sendOkResponse(messageId, {
      isSuccess: false,
      failReason
    })
  }
}

export const handler = new RemoveStrategy()
