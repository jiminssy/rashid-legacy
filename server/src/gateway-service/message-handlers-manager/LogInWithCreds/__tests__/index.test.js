import * as userManagementService from '../../../../user-management-service'
import { outMessageManager } from '../../../out-message-manager'
import { handler } from '../index'

jest.mock('../../../../user-management-service')

const testLogInMessage = {
  id: 'testMessageId',
  userName: 'testUserName',
  password: 'testPassword'
}

test('log in success', async () => {
  outMessageManager.sendOkResponse = jest.fn()
  const accessToken = 'test-access-token'
  userManagementService.logIn.mockResolvedValue(accessToken)
  await handler.handle(testLogInMessage)
  expect(outMessageManager.sendOkResponse).toHaveBeenCalledWith(
    testLogInMessage.id, accessToken
  )
})

test('log in fail', async () => {
  outMessageManager.sendFailedAuthentication = jest.fn()
  userManagementService.logIn.mockResolvedValue(null)
  await handler.handle(testLogInMessage)
  expect(outMessageManager.sendFailedAuthentication)
    .toHaveBeenCalledWith(testLogInMessage.id)
})
