import _ from 'lodash'
import { store } from '../../store'
import { useState, useEffect } from 'react'
import { subscribe, unsubscribe } from '../../utils/event-bus'
import { STORE_UPDATED } from '../../constants/events'

export const useSelector = selectPath => {
  const [value, setValue] = useState(0) // eslint-disable-line no-unused-vars
  useEffect(() => {
    const subscription = subscribe(STORE_UPDATED, changedStorePath => {
      if (selectPath.startsWith(changedStorePath)) {
        setValue(value => ++value)
      }
    })
    return function cleanup () {
      unsubscribe(subscription)
    }
  }, []) // eslint-disable-line
  return _.get(store, selectPath)
}
