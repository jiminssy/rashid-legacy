exports.seed = function (knex) {
  return knex('userManagement_user').insert([
    {
      email: 'test@test.com',
      password: '1234',
      firstName: 'John',
      lastName: 'Doe',
      role: 'ReadOnly'
    },
    {
      email: 'jiminparky@gmail.com',
      password: 'VFR$3edc',
      firstName: 'Jimin',
      lastName: 'Park',
      role: 'Admin'
    }
  ])
}
