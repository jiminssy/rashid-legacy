import jwt from 'jsonwebtoken'
import { LogInWithCredsRequest } from '../../api-message/LogInWithCredsRequest'
import { ingestHttp, __test__ } from '..'
import { outMessageManager } from '../../out-message-manager'
import * as messageHandlersManager from '../../message-handlers-manager'
import * as auth from '../auth'
import * as eventBus from '../../../common/event/event-bus'
import { publish } from '../../../common/event/event-bus'
import { mockDateNow, unmockDateNow } from '../../../common/test-mock/mock-date-now'

jest.mock('jsonwebtoken', () => ({
  verify: jest.fn()
}))

jest.mock('uuid-js', () => ({
  create: () => ({ hex: 'test-id' })
}))

test('wait out message manager and gave message to handler', () => {
  const message = new LogInWithCredsRequest('testId', 'testPassword')
  const messageId = message.id
  const mockSendOkResponse = () => { }
  const mockSendErrorResponse = () => { }
  const mockSendSendFailedAuthentication = () => { }
  const waitResultToRespond = outMessageManager.waitResultToRespond
  const handle = messageHandlersManager.handle
  messageHandlersManager.handle = jest.fn()
  outMessageManager.waitResultToRespond = jest.fn()

  __test__.ingest(
    1,
    message,
    mockSendSendFailedAuthentication,
    null,
    mockSendErrorResponse,
    mockSendOkResponse
  )

  expect(outMessageManager.waitResultToRespond).toHaveBeenCalledWith(
    messageId,
    mockSendOkResponse,
    mockSendErrorResponse,
    mockSendSendFailedAuthentication
  )
  expect(messageHandlersManager.handle).toHaveBeenCalledWith(message)

  outMessageManager.waitResultToRespond = waitResultToRespond
  messageHandlersManager.handle = handle
})

describe('ingest http', () => {
  let response

  beforeAll(() => {
    mockDateNow()
  })
  afterAll(() => {
    unmockDateNow()
  })

  beforeEach(() => {
    response = {
      status: jest.fn(),
      send: jest.fn()
    }
  })

  test('failed authentication should respond with status 401', async () => {
    jwt.verify.mockImplementation(() => { throw new Error('test error') })

    await ingestHttp({}, response)

    expect(response.status).toHaveBeenCalledWith(401)
    expect(response.send).toHaveBeenCalled()
  })

  test('failed authorization should respond with status 403', async () => {
    jwt.verify.mockImplementation(() => ({ userId: 1 }))
    auth.isAuthorized = jest.fn().mockResolvedValue(false)

    await ingestHttp({}, response)

    expect(response.status).toHaveBeenCalledWith(403)
    expect(response.send).toHaveBeenCalled()
  })

  test('non request type api message', async () => {
    class SomeEventMessage {
      constructor (foo) {
        this.foo = foo
      }
    }

    const originalPublish = eventBus.publish
    eventBus.publish = jest.fn()
    auth.isAuthorized = jest.fn().mockResolvedValue(true)

    jwt.verify.mockImplementation(() => ({ userId: 1 }))
    const testApiMessage = new SomeEventMessage({
      foo: 'bar'
    })

    await ingestHttp(testApiMessage, response)

    const publishArgument = publish.mock.calls[0][0]
    expect(publishArgument).toMatchSnapshot()

    eventBus.publish = originalPublish
  })
})
