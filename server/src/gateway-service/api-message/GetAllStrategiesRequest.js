import { authorizedRoles, ApiMessage } from './ApiMessage'
import { UserRole } from '../../user-management-service/definition/UserRole'

@authorizedRoles([UserRole.READ_ONLY])
class GetAllStrategiesRequest extends ApiMessage {
}

export { GetAllStrategiesRequest }
