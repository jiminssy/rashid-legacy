import { CronJob } from 'cron'
import moment from 'moment-timezone'
import { publish } from '../../common/event/event-bus'
import { DailyOutcomeScores } from '../../common/event/message/DailyOutcomeScores'
import { getDailyOutcomeScores } from './performance-analytics'

const CRON_TIME = '45 * * * * *'
const TIMEZONE = 'UTC'

export const publishDailyOutcomeScoresPeriodically = () => {
  new CronJob( // eslint-disable-line no-new
    CRON_TIME,
    () => {
      (async function () {
        let today = moment.utc().set({
          hour: 0,
          minute: 0,
          second: 0,
          millisecond: 0
        })

        if (today.isoWeekday() > 5) {
          // if today is a weekend day, need to continue to update last Friday
          const subtractDays = today.isoWeekday() - 5
          today = today.subtract(subtractDays, 'days')
        }

        const scores = await getDailyOutcomeScores(today)
        publish(new DailyOutcomeScores(
          today.format(),
          scores
        ))
      }())
    },
    null, // on complete
    true,
    TIMEZONE
  )
}
