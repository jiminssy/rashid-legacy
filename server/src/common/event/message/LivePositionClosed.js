import { EventMessage } from './EventMessage'
import { LIVE_POSITION_CLOSED } from '../event-type'

export class LivePositionClosed extends EventMessage {
  constructor (
    brokerTradeId,
    close,
    closedTime
  ) {
    super(LIVE_POSITION_CLOSED)
    this.brokerTradeId = brokerTradeId
    this.close = close
    this.closedTime = closedTime
  }
}
