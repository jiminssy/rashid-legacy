export class CandleData {
  static fromFxcmData (data) {
    return new CandleData(
      data[0],
      data[1],
      data[2],
      data[3],
      data[4],
      data[5],
      data[6],
      data[7],
      data[8],
      data[9]
    )
  }

  constructor (
    timestamp,
    bidOpen,
    bidClose,
    bidHigh,
    bidLow,
    askOpen,
    askClose,
    askHigh,
    askLow,
    tickQuantity
  ) {
    this.timestamp = timestamp
    this.bidOpen = bidOpen
    this.bidClose = bidClose
    this.bidHigh = bidHigh
    this.bidLow = bidLow
    this.askOpen = askOpen
    this.askClose = askClose
    this.askHigh = askHigh
    this.askLow = askLow
    this.tickQuantity = tickQuantity
  }
}
