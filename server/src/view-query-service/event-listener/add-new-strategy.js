import { knex } from '../../common/database/knex'
import { DbTable } from '../definition/DbTable'

export const eventListener = async (
  { strategy: { name, version, description, codePath, paramSchema } }
) => {
  await knex(DbTable.STRATEGY).insert({
    name, version, description, codePath, paramSchema
  })
}
