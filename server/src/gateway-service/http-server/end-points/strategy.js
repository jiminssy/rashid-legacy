import { AddNewStrategyRequest } from '../../api-message/AddNewStrategyRequest'
import { AddNewStrategyInstanceRequest } from '../../api-message/AddNewStrategyInstanceRequest'
import { EditStrategyRequest } from '../../api-message/EditStrategyRequest'
import { EditStrategyInstanceRequest } from '../../api-message/EditStrategyInstanceRequest'
import { GetAllStrategiesRequest } from '../../api-message/GetAllStrategiesRequest'
import { GetAllStrategyInstancesRequest } from '../../api-message/GetAllStrategyInstancesRequest'
import { GetStrategyRequest } from '../../api-message/GetStrategyRequest'
import { GetStrategyInstanceRequest } from '../../api-message/GetStrategyInstanceRequest'
import { RemoveStrategyRequest } from '../../api-message/RemoveStrategyRequest'
import { RemoveStrategyInstanceRequest } from '../../api-message/RemoveStrategyInstanceRequest'

const baseUrl = '/api/strategies'

export const register = (registerPost, registerGet) => {
  // GET
  registerGet(baseUrl, GetAllStrategiesRequest)
  registerGet(`${baseUrl}/instances`, GetAllStrategyInstancesRequest)
  registerGet(`${baseUrl}/:strategyId`, GetStrategyRequest)
  registerGet(`${baseUrl}/instances/:strategyInstanceId`, GetStrategyInstanceRequest)

  // POST
  registerPost(`${baseUrl}/add-new`, AddNewStrategyRequest)
  registerPost(`${baseUrl}/instances/add-new`, AddNewStrategyInstanceRequest)
  registerPost(`${baseUrl}/edit`, EditStrategyRequest)
  registerPost(`${baseUrl}/instances/edit`, EditStrategyInstanceRequest)
  registerPost(`${baseUrl}/remove`, RemoveStrategyRequest)
  registerPost(`${baseUrl}/instances/remove`, RemoveStrategyInstanceRequest)
}
