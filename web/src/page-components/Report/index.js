import React from 'react'
import {
  Switch,
  Route,
  useRouteMatch
} from 'react-router-dom'
import { SubMenuItem } from '../../components/SubMenuItem'
import { DailyPerformance } from './DailyPerformance'
import { LiveAccountPerformance } from './LiveAccountPerformance'
import { UserRole } from '../../constants/user-role'

export const Report = () => {
  const { path } = useRouteMatch()

  const renderSubMenuItems = () => {
    return (
      <>
        <SubMenuItem
          faIcon="fa-chart-line"
          text="Daily Performance"
          navigateTo="/admin/report/daily-performance"
          allowedRoles={[UserRole.ADMIN, UserRole.READ_ONLY]}
        />
        <SubMenuItem
          faIcon="fa-poll-h"
          text="Live Account Performance"
          navigateTo="/admin/report/live-account-performance"
          allowedRoles={[UserRole.ADMIN, UserRole.READ_ONLY]}
        />
      </>
    )
  }

  return (
    <div>
      <Switch>
        <Route path={`${path}/daily-performance`} component={DailyPerformance} />
        <Route path={`${path}/live-account-performance`} component={LiveAccountPerformance} />
        <Route path={path} exact>
          <h4 className="page-title">Report</h4>
          { renderSubMenuItems() }
        </Route>
      </Switch>
    </div>
  )
}
