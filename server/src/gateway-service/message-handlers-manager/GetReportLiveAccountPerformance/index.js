import { MessageHandler } from '../MessageHandler'
import { outMessageManager } from '../../out-message-manager'
import { getLiveAccountPerformanceReportData } from '../../../view-query-service/query-interface/report/live-account-performance'

class GetReportLiveAccountPerformance extends MessageHandler {
  async handle ({ id, fromDate, toDate }) {
    const reportData = await getLiveAccountPerformanceReportData(fromDate, toDate)
    outMessageManager.sendOkResponse(id, reportData)
  }
}

export const handler = new GetReportLiveAccountPerformance()
