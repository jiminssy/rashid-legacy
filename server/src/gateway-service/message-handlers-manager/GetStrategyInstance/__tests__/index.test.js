import { handler } from '..'
import * as queryInterface from '../../../../view-query-service/query-interface/strategy'
import { outMessageManager } from '../../../out-message-manager'
import { GetStrategyInstanceRequest } from '../../../api-message/GetStrategyInstanceRequest'
import { StrategyInstance } from '../../../../common/definition/StrategyInstance'

outMessageManager.sendOkResponse = jest.fn()

const strategyInstanceId = 'test-strategy-instance-id'
const testApiMessage = new GetStrategyInstanceRequest({
  strategyInstanceId: strategyInstanceId
})

test('strategy instance does not exist', async () => {
  queryInterface.getStrategyInstance = jest.fn().mockResolvedValue(null)

  await handler.handle(testApiMessage)

  expect(outMessageManager.sendOkResponse)
    .toHaveBeenCalledWith(testApiMessage.id, {
      isSuccess: false
    })
})

test('success', async () => {
  const testStrategyInstance = new StrategyInstance({
    id: strategyInstanceId,
    strategyId: 1,
    strategyParams: '{"test": "params"}',
    instrumentId: 1,
    dataStreams: '["FXCM-1-m5", "FXCM-2-h1"]'
  })

  queryInterface.getStrategyInstance = jest.fn().mockResolvedValue(testStrategyInstance)

  await handler.handle(testApiMessage)

  expect(outMessageManager.sendOkResponse)
    .toHaveBeenCalledWith(testApiMessage.id, {
      isSuccess: true,
      strategyInstance: testStrategyInstance
    })
})
