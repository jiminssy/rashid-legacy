import { MessageHandler } from '../MessageHandler'
import { outMessageManager } from '../../out-message-manager'
import { verifyAccessToken } from '../../../user-management-service'

class LogInWithToken extends MessageHandler {
  async handle (message) {
    const accessToken = message.accessToken
    const isValidToken = await verifyAccessToken(accessToken)
    if (isValidToken) {
      outMessageManager.sendOkResponse(message.id, true)
    } else {
      outMessageManager.sendFailedAuthentication(message.id)
    }
  }
}

export const handler = new LogInWithToken()
