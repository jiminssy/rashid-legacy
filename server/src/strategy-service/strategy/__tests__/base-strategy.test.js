import { BaseStrategy } from '../base-strategy'
import { mockPublish, unmockPublish } from '../../../common/test-mock/mock-event-bus'
import { publish } from '../../../common/event/event-bus'
import { DataStreamUpdate } from '../../../common/event/message/DataStreamUpdate'
import { TradeDirection } from '../../../common/definition/TradeDirection'
import * as stopLossRangeCalculator from '../../util/stop-loss-range-calculator'

jest.mock('uuid-js', () => ({
  create: () => ({ hex: 'test-uuid' })
}))

describe('signalMarketOrderByRatio', () => {
  const testStrategy = new BaseStrategy({
    id: 'mock-instance-id',
    tradingInstrument: 'FXCM-1-m1',
    dataStreams: ['FXCM-1-m1']
  })

  beforeEach(() => {
    mockPublish()
  })

  afterEach(() => {
    unmockPublish()
  })

  test('buy trade', async () => {
    stopLossRangeCalculator.calcStopLossRange = jest.fn().mockResolvedValue(1)

    await testStrategy.signalMarketOrderByRatio(TradeDirection.LONG, 35, 40, 3)

    expect(publish.mock.calls[0][0]).toMatchSnapshot()
  })

  test('sell trade', async () => {
    stopLossRangeCalculator.calcStopLossRange = jest.fn().mockResolvedValue(1)

    await testStrategy.signalMarketOrderByRatio(TradeDirection.SHORT, 35, 40, 3)

    expect(publish.mock.calls[0][0]).toMatchSnapshot()
  })
})

test('_subscribeDataStreams', () => {
  mockPublish()
  const testDataStreams = ['FXCM-1-m5', 'FXCM-2-h1']
  const testStrategy = new BaseStrategy({
    dataStreams: testDataStreams
  })
  testStrategy._subscribeDataStreams()

  expect(publish.mock.calls[0][0]).toMatchSnapshot()
  expect(publish.mock.calls[1][0]).toMatchSnapshot()
  unmockPublish()
})

test('_unsubscribeDataStreams', () => {
  mockPublish()
  const testDataStreams = ['FXCM-1-m5', 'FXCM-2-h1']
  const testStrategy = new BaseStrategy({
    dataStreams: testDataStreams
  })
  testStrategy._unsubscribeDataStreams()

  expect(publish.mock.calls[0][0]).toMatchSnapshot()
  expect(publish.mock.calls[1][0]).toMatchSnapshot()
  unmockPublish()
})

test('_subscribeDataStreamEvents & _unsubscribeDataStreamEvents', () => {
  const testDataStreams = ['FXCM-1-m5', 'FXCM-2-h1']
  const testStrategy = new BaseStrategy({
    dataStreams: testDataStreams
  })
  testStrategy.onDataStreamReceived = jest.fn()
  const mockCandleData1 = {
    bidOpen: 0.12,
    askOpen: 0.34
  }
  const mockCandleData2 = {
    bidOpen: 0.56,
    askOpen: 0.78
  }
  const mockCandleData3 = {
    bidOpen: 0.910,
    askOpen: 0.111
  }

  testStrategy._subscribeDataStreamEvents()

  publish(new DataStreamUpdate('FXCM-1-m5', mockCandleData1))
  expect(testStrategy.onDataStreamReceived).toHaveBeenCalledWith(mockCandleData1)

  publish(new DataStreamUpdate('FXCM-2-h1', mockCandleData2))
  expect(testStrategy.onDataStreamReceived).toHaveBeenCalledWith(mockCandleData2)

  publish(new DataStreamUpdate('FXCM-3-h2', mockCandleData3))
  expect(testStrategy.onDataStreamReceived).not.toHaveBeenCalledWith(mockCandleData3)

  testStrategy.onDataStreamReceived.mockReset()

  testStrategy._unsubscribeDataStreamEvents()

  publish(new DataStreamUpdate('FXCM-1-m5', mockCandleData1))
  expect(testStrategy.onDataStreamReceived).not.toHaveBeenCalledWith(mockCandleData1)

  publish(new DataStreamUpdate('FXCM-2-h1', mockCandleData2))
  expect(testStrategy.onDataStreamReceived).not.toHaveBeenCalledWith(mockCandleData2)
})
