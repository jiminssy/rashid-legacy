import { __test__ } from '../calculate-strategy-instances-daily-stop-loss'
import * as database from '../../utils/database'
import { mockPublish, unmockPublish } from '../../../common/test-mock/mock-event-bus'
import { publish } from '../../../common/event/event-bus'

describe('publishStrategyInstanceDailyStopLoss', () => {
  const { publishStrategyInstanceDailyStopLoss } = __test__

  beforeEach(() => {
    mockPublish()
  })

  afterEach(() => {
    unmockPublish()
  })

  test('Small, simple daily outcome score set', async () => {
    database.insertOrUpdateDailyOutcomeScore = jest.fn()
    database.getDailyOutcomeScores = jest.fn().mockResolvedValue([-4, -2, 4, 6])
    const mockStrategyInstanceId = 'mock-strategy-instance-id'

    await publishStrategyInstanceDailyStopLoss(mockStrategyInstanceId, 'mock-date', 1)

    const {
      strategyInstanceId,
      stopLossOutcomeScore,
      reversedStopLossOutcomeScore
    } = publish.mock.calls[0][0]
    expect(strategyInstanceId).toBe(mockStrategyInstanceId)
    expect(stopLossOutcomeScore).toBe(-4.76)
    expect(reversedStopLossOutcomeScore).toBe(5.76)
  })

  test('Long, complex daily outcome score set', async () => {
    database.insertOrUpdateDailyOutcomeScore = jest.fn()
    database.getDailyOutcomeScores = jest.fn().mockResolvedValue([
      2, 4, 6, -1.42, 4.35, -8.87, -6.32, 2.48, -1.11, 9.32, -10.26
    ])
    const mockStrategyInstanceId = 'mock-strategy-instance-id'

    await publishStrategyInstanceDailyStopLoss(mockStrategyInstanceId, 'mock-date', 1)

    const {
      strategyInstanceId,
      stopLossOutcomeScore,
      reversedStopLossOutcomeScore
    } = publish.mock.calls[0][0]
    expect(strategyInstanceId).toBe(mockStrategyInstanceId)
    expect(stopLossOutcomeScore).toBe(-6.29)
    expect(reversedStopLossOutcomeScore).toBe(6.3)
  })

  test('-1 STD is still positive, must return -2 STD', async () => {
    database.insertOrUpdateDailyOutcomeScore = jest.fn()
    database.getDailyOutcomeScores = jest.fn().mockResolvedValue([2, 4, 6])
    const mockStrategyInstanceId = 'mock-strategy-instance-id'

    await publishStrategyInstanceDailyStopLoss(mockStrategyInstanceId, 'mock-date', 1)

    const {
      strategyInstanceId,
      stopLossOutcomeScore,
      reversedStopLossOutcomeScore
    } = publish.mock.calls[0][0]
    expect(strategyInstanceId).toBe(mockStrategyInstanceId)
    expect(stopLossOutcomeScore).toBe(-2)
    expect(reversedStopLossOutcomeScore).toBe(6)
  })

  test('-1 STD is negative, but too small', async () => {
    database.insertOrUpdateDailyOutcomeScore = jest.fn()
    database.getDailyOutcomeScores = jest.fn().mockResolvedValue([-2, 0, 3])
    const mockStrategyInstanceId = 'mock-strategy-instance-id'

    await publishStrategyInstanceDailyStopLoss(mockStrategyInstanceId, 'mock-date', 1)

    const {
      strategyInstanceId,
      stopLossOutcomeScore,
      reversedStopLossOutcomeScore
    } = publish.mock.calls[0][0]
    expect(strategyInstanceId).toBe(mockStrategyInstanceId)
    expect(stopLossOutcomeScore).toBe(-2.52)
    expect(reversedStopLossOutcomeScore).toBe(2.85)
  })

  test('1 STD is still positive, must return 2 STD', async () => {
    database.insertOrUpdateDailyOutcomeScore = jest.fn()
    database.getDailyOutcomeScores = jest.fn().mockResolvedValue([-2, -4, -6])
    const mockStrategyInstanceId = 'mock-strategy-instance-id'

    await publishStrategyInstanceDailyStopLoss(mockStrategyInstanceId, 'mock-date', 1)

    const {
      strategyInstanceId,
      stopLossOutcomeScore,
      reversedStopLossOutcomeScore
    } = publish.mock.calls[0][0]
    expect(strategyInstanceId).toBe(mockStrategyInstanceId)
    expect(stopLossOutcomeScore).toBe(-6)
    expect(reversedStopLossOutcomeScore).toBe(2)
  })

  test('1 STD is negative, but too small', async () => {
    database.insertOrUpdateDailyOutcomeScore = jest.fn()
    database.getDailyOutcomeScores = jest.fn().mockResolvedValue([-3, 0, 2])
    const mockStrategyInstanceId = 'mock-strategy-instance-id'

    await publishStrategyInstanceDailyStopLoss(mockStrategyInstanceId, 'mock-date', 1)

    const {
      strategyInstanceId,
      stopLossOutcomeScore,
      reversedStopLossOutcomeScore
    } = publish.mock.calls[0][0]
    expect(strategyInstanceId).toBe(mockStrategyInstanceId)
    expect(stopLossOutcomeScore).toBe(-2.85)
    expect(reversedStopLossOutcomeScore).toBe(2.52)
  })
})
