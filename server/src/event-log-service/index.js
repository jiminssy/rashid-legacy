import moment from 'moment-timezone'
import winston from 'winston'
import { WebClient } from '@slack/web-api'
import {
  ALL,
  ERROR_FXCM_CONNECTION,
  ERROR_FXCM_NO_DATA,
  FXCM_WEEKLY_MAINTENANCE_BEGAN,
  FXCM_WEEKLY_MAINTENANCE_ENDED,
  SIMULATION_DAILY_OUTCOME_SCORES
} from '../common/event/event-type'
import { subscribe } from '../common/event/event-bus'
import { NODE_ENV, SLACK_ERROR_CHANNEL_ID, SLACK_TOKEN } from '../settings'
import { isServerMaintenance } from '../common/fxcm/instruments'

require('winston-daily-rotate-file')
const { format: { combine, timestamp, prettyPrint } } = winston
const slackWebClient = new WebClient(SLACK_TOKEN)

const transport = new winston.transports.DailyRotateFile({
  zippedArchive: true,
  filename: '%DATE%-all.log',
  dirname: './logs',
  utc: true,
  createSymlink: true,
  level: 'silly'
})

const current = moment().seconds(0).milliseconds(0)
let isFxcmServerMaintenanceTime = isServerMaintenance(current)

const logger = winston.createLogger({
  format: combine(
    timestamp(),
    prettyPrint()
  ),
  transports: [transport]
})

const isCriticalError = eventMessage => {
  return !(
    eventMessage.header.eventType === ERROR_FXCM_NO_DATA || // when market is slow this happens
    eventMessage.errorMessage === 'Socket was disconnected' || // FXCM doesn't know why connection problems happen, we usually reconnect within 3 secs
    eventMessage.errorMessage.includes('Socket failed to connect to FXCM server') ||
    eventMessage.errorStack.includes('520 Origin Error') || // FXCM server ran into error, still investigating
    eventMessage.errorStack.includes('524 Origin Time-out') || // FXCM server did not respond in time, nothing much we can do
    eventMessage.errorStack.includes('502 Bad Gateway') || // FXCM server is down and not responsive
    (eventMessage.errorMessage.includes('/trading/close_trade') && eventMessage.errorMessage.includes('The specified trade does not exist')) || // this is due to us attempting close live trade when its associated simulation trade closes. Live trade is already closed before our request got there.
    eventMessage.errorMessage.includes('Unauthorized') || // this has been reported numerous times to FXCM but doesn't seem like it will be fixed.
    eventMessage.errorMessage.includes('ECONNRESET') // FXCM server disconnection and there is nothing we can do
  )
}

const postErrorToSlack = eventMessage => {
  slackWebClient.chat.postMessage({
    channel: SLACK_ERROR_CHANNEL_ID,
    text: `eventType: ${eventMessage.header.eventType}
created: ${eventMessage.header.created}
name: ${eventMessage.errorName}
message: ${eventMessage.errorMessage}
stack: ${eventMessage.errorStack}`
  })
}

const shouldIgnore = eventMessage => {
  const { header: { eventType } } = eventMessage
  const isError = eventType.startsWith('all/error')
  const isIgnorableCloseTradeError = (
    isError &&
    eventMessage.errorMessage.includes('/trading/close_trade') &&
    (
      // this means close trade order is already placed for this trade
      eventMessage.errorMessage.includes('Cannot place more than one order of this type for each trade') ||
      // this means close trade for already closed position has been received
      eventMessage.errorMessage.includes('Cannot find a trade for order')
    )
  )
  return (isFxcmServerMaintenanceTime && eventType === ERROR_FXCM_CONNECTION) ||
    eventType === SIMULATION_DAILY_OUTCOME_SCORES ||
    isIgnorableCloseTradeError
}

const updateFxcmServerStatus = eventType => {
  if (eventType === FXCM_WEEKLY_MAINTENANCE_BEGAN) {
    isFxcmServerMaintenanceTime = true
  } else if (eventType === FXCM_WEEKLY_MAINTENANCE_ENDED) {
    isFxcmServerMaintenanceTime = false
  }
}

const onEvent = eventMessage => {
  updateFxcmServerStatus(eventMessage.header.eventType)
  if (shouldIgnore(eventMessage)) {
    return
  }

  const logMessage = JSON.stringify(eventMessage)
  if (eventMessage.header.eventType.startsWith('all/error')) {
    logger.error(logMessage)
    if (NODE_ENV === 'production' && isCriticalError(eventMessage)) {
      postErrorToSlack(eventMessage)
    }
  } else if (!eventMessage.header.eventType.startsWith('all/dataStream/update/')) {
    logger.info(logMessage)
  }
}

export const start = () => {
  subscribe(ALL, onEvent)
}

export const __test__ = {
  onEvent
}
