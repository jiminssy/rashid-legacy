import { CronJob } from 'cron'
import moment from 'moment-timezone'
import { DataPeriod } from '../common/definition/DataPeriod'
import { publish } from '../common/event/event-bus'
import { ExceptionEventMessage } from '../common/event/message/ExceptionEventMessage'
import { ERROR_DATA_STREAM_PUBLISH, ERROR_FXCM_NO_DATA } from '../common/event/event-type'
import { isTradingHour } from '../common/fxcm/instruments'
import { getDataClient } from '../common/fxcm/client-factory'
import { SystemService } from '../common/definition/SystemService'
import { getResultWithRetries, GetResultRetriesError } from '../common/utils/get-result-with-retries'
import { DataStreamUpdate } from '../common/event/message/DataStreamUpdate'
import { SERVER_TIMEZONE } from '../settings'
import { parseDataStreamId } from '../common/utils/data-stream-id-parser'

const DATA_FETCH_RETRY_INTERVAL = 5000
const DATA_FETCH_MAX_RETRIES = 17

const subscriptions = {}
const scheduleCron = (cronTime, timezone, dataStreamId) => {
  const onCronTriggered = () => {
    try {
      tryPublish(dataStreamId)
    } catch (e) {
      publish(new ExceptionEventMessage(
        ERROR_DATA_STREAM_PUBLISH,
        e.name,
        `Error during publishing data stream: ${dataStreamId}`,
        e.stack
      ))
    }
  }

  return new CronJob( // eslint-disable-line no-new
    cronTime,
    onCronTriggered,
    null, // on complete
    true,
    timezone
  )
}

const incrementSubscriberCount = dataStreamId => {
  if (!(dataStreamId in subscriptions)) {
    subscriptions[dataStreamId] = { count: 0 }
  }
  subscriptions[dataStreamId].count += 1
}

const getCronTime = periodId => {
  switch (periodId) {
    case DataPeriod.m1:
      return '0 */1 * * * *'
    case DataPeriod.m5:
      return '0 */5 * * * *'
    case DataPeriod.m15:
      return '0 */15 * * * *'
    case DataPeriod.m30:
      return '0 */30 * * * *'
    case DataPeriod.H1:
      return '0 0 */1 * * *'
    case DataPeriod.H2:
      return '0 0 */2 * * *'
    case DataPeriod.H3:
      return '0 0 */3 * * *'
    case DataPeriod.H4:
      return '0 0 */4 * * *'
    case DataPeriod.H6:
      return '0 0 */6 * * *'
    case DataPeriod.H8:
      return '0 0 */8 * * *'
    case DataPeriod.D1:
      return '0 0 0 * * *'
    case DataPeriod.W1:
      return '0 0 0 * * 1'
    case DataPeriod.M1:
      return '0 0 0 1 * *'
    default:
      throw new Error('Unknown periodId for getCronTime.')
  }
}

const getTimezone = brokerName => {
  if (brokerName === 'FXCM') {
    return 'America/New_York'
  }

  throw new Error('Unknown instrument for timezone.')
}

const fromCalcParams = {
  [DataPeriod.m1]: (-1, 'minute'),
  [DataPeriod.m5]: (-5, 'minute'),
  [DataPeriod.m15]: (-15, 'minute'),
  [DataPeriod.m30]: (-30, 'minute'),
  [DataPeriod.H1]: (-1, 'hour'),
  [DataPeriod.H2]: (-2, 'hour'),
  [DataPeriod.H3]: (-3, 'hour'),
  [DataPeriod.H4]: (-4, 'hour'),
  [DataPeriod.H6]: (-6, 'hour'),
  [DataPeriod.H8]: (-8, 'hour'),
  [DataPeriod.D1]: (-1, 'date'),
  [DataPeriod.W1]: (-1, 'weeks'),
  [DataPeriod.M1]: (-1, 'months')
}

const isResultSuccess = result => result.length > 0
const tryPublish = async dataStreamId => {
  const { brokerInstrumentId, periodId } = parseDataStreamId(dataStreamId)
  const current = moment().seconds(0).milliseconds(0)
  const deltaNumber = fromCalcParams[periodId][0]
  const unit = fromCalcParams[periodId][1]
  const from = current.add(deltaNumber, unit)
  const dataClient = getDataClient(SystemService.DATA_STREAM)
  if (isTradingHour(brokerInstrumentId, current)) {
    try {
      const getHistoricalDataClosure = () => {
        return dataClient.getHistoricalData(brokerInstrumentId, periodId, from, from)
      }
      const result = await getResultWithRetries(
        getHistoricalDataClosure,
        isResultSuccess,
        DATA_FETCH_RETRY_INTERVAL,
        DATA_FETCH_MAX_RETRIES
      )
      publish(new DataStreamUpdate(dataStreamId, result[0]))
    } catch (e) {
      let eventType
      let message
      if (e instanceof GetResultRetriesError) {
        eventType = ERROR_FXCM_NO_DATA
        message = `Communication with FXCM is ok but candle data was not available for time: ${from.tz(SERVER_TIMEZONE).toString()}`
      } else {
        eventType = ERROR_DATA_STREAM_PUBLISH
        message = 'Unexpected error occurred during data stream publish'
      }
      publish(new ExceptionEventMessage(
        eventType, e.name, message, e.stack
      ))
    }
  }
}

export const subscribe = dataStreamId => {
  try {
    const { brokerName, periodId } = parseDataStreamId(dataStreamId)
    incrementSubscriberCount(dataStreamId)
    const cronTime = getCronTime(periodId)
    const timezone = getTimezone(brokerName)
    const cronSchedule = scheduleCron(cronTime, timezone, dataStreamId)
    subscriptions[dataStreamId].cronJob = cronSchedule
  } catch (e) {
    console.log(e)
  }
}

export const unsubscribe = dataStreamId => {
  if (!(dataStreamId in subscriptions)) {
    throw new Error(`Subscription does not exist for data stream: ${dataStreamId}`)
  }

  subscriptions[dataStreamId].count -= 1
  if (subscriptions[dataStreamId].count === 0) {
    subscriptions[dataStreamId].cronJob.stop()
    delete subscriptions[dataStreamId]
  }
}

export const __test__ = {
  getCronTime,
  getTimezone,
  subscriptions,
  tryPublish
}
