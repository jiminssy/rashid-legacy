import { EventHandler } from './EventHandler'
import { LOAD_REPORT_LIVE_ACCOUNT_PERFORMANCE } from '../constants/events'
import { getReportLiveAccountPerformance } from '../utils/rashid-client'
import { setValue } from '../store'

class LoadReportLiveAccountPerformance extends EventHandler {
  constructor () {
    super([LOAD_REPORT_LIVE_ACCOUNT_PERFORMANCE])
  }

  async handle ({ fromDate, toDate }) {
    const reportData = await getReportLiveAccountPerformance(fromDate, toDate)
    setValue('report.liveAccountPerformance', reportData)
  }
}

export const eventHandler = new LoadReportLiveAccountPerformance()
