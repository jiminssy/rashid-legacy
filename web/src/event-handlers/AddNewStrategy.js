import { EventHandler } from './EventHandler'
import { ADD_NEW_STRATEGY } from '../constants/events'
import { addNewStrategy } from '../utils/rashid-client'
import { notification } from '../utils/notification'

class AddNewStrategy extends EventHandler {
  constructor () {
    super([ADD_NEW_STRATEGY])
  }

  async handle (eventMessage) {
    const result = await addNewStrategy(eventMessage)
    if (result.isSuccess) {
      notification.success('New strategy added')
      window.rrHistory.push('/admin/strategy')
    } else {
      notification.error(result.failReason)
    }
  }
}

export const eventHandler = new AddNewStrategy()
