import pubsub from 'pubsub.js'

export const subscribe = (eventType, eventHandler) => pubsub.subscribe(eventType, eventHandler)

export const publish = eventMessage => pubsub.publish(
  eventMessage.header.eventType, [eventMessage], { recurrent: true }
)

export const unsubscribe = subscription => pubsub.unsubscribe(subscription)
