import {
  DB_HOST,
  DB_USER,
  DB_PASSWORD,
  DB_NAME
} from '../../settings'

const types = require('pg').types
const toNumber = val => Number(val)
types.setTypeParser(types.builtins.NUMERIC, toNumber)
types.setTypeParser(types.builtins.INT2, toNumber)
types.setTypeParser(types.builtins.INT4, toNumber)
types.setTypeParser(types.builtins.INT8, toNumber)

export const knex = require('knex')({
  client: 'postgres',
  connection: {
    host: DB_HOST,
    user: DB_USER,
    password: DB_PASSWORD,
    database: DB_NAME
  }
})
