import { EventHandler } from './EventHandler'
import { REMOVE_STRATEGY } from '../constants/events'
import { removeStrategy } from '../utils/rashid-client'
import { notification } from '../utils/notification'

class RemoveStrategy extends EventHandler {
  constructor () {
    super([REMOVE_STRATEGY])
  }

  async handle (strategyId) {
    const result = await removeStrategy(strategyId)
    if (result.isSuccess) {
      notification.success('Strategy removed')
      window.rrHistory.push('/admin/strategy')
    } else {
      notification.error(result.failReason)
    }
  }
}

export const eventHandler = new RemoveStrategy()
