import { knex } from '../../common/database/knex'
import { DbTable } from '../definition/DbTable'

export const eventListener = async (
  { strategy: { id, name, version, description, codePath, paramSchema } }
) => {
  await knex(DbTable.STRATEGY)
    .where({ id })
    .update({
      name, version, description, codePath, paramSchema
    })
}
