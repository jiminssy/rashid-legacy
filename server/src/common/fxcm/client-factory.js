import { BaseClient } from './base-client'
import { SystemService } from '../definition/SystemService'
import {
  FXCM_API_PORT,
  FXCM_API_PROTOCOL,
  FXCM_DEMO_API_HOST,
  FXCM_DEMO_API_TOKEN,
  FXCM_LIVE_API_HOST,
  FXCM_LIVE_API_TOKEN,
  FXCM_LIVE_RELAY_API_TOKENS
} from '../../settings'
import { DataClient } from './data-client'
import { OrderClient } from './order-client'
import { PositionClient } from './position-client'

let demoBaseClient
let liveBaseClient

const serviceLevelClients = {
  dataClient: {},
  orderClient: {},
  positionClient: {}
}

const getBaseClient = requestingService => {
  if (
    requestingService === SystemService.DATA_STREAM ||
    requestingService === SystemService.SIMULATION ||
    requestingService === SystemService.STRATEGY
  ) {
    // services that should use demo
    if (!demoBaseClient) {
      demoBaseClient = new BaseClient(
        FXCM_API_PROTOCOL,
        FXCM_DEMO_API_HOST,
        FXCM_API_PORT,
        FXCM_DEMO_API_TOKEN
      )
    }
    return demoBaseClient
  } else if (
    requestingService === SystemService.TRADE_SIGNAL ||
    requestingService === SystemService.ANALYTICS
  ) {
    // services that should use live
    if (!liveBaseClient) {
      liveBaseClient = new BaseClient(
        FXCM_API_PROTOCOL,
        FXCM_LIVE_API_HOST,
        FXCM_API_PORT,
        FXCM_LIVE_API_TOKEN
      )
    }
    return liveBaseClient
  } else {
    throw new Error(`Invalid service requested to get FXCM base client: ${requestingService}`)
  }
}

export const getDataClient = requestingService => {
  const baseClient = getBaseClient(requestingService)

  if (
    requestingService === SystemService.DATA_STREAM ||
    requestingService === SystemService.SIMULATION ||
    requestingService === SystemService.STRATEGY ||
    requestingService === SystemService.TRADE_SIGNAL ||
    requestingService === SystemService.ANALYTICS
  ) {
    if (!serviceLevelClients.dataClient[requestingService]) {
      serviceLevelClients.dataClient[requestingService] = new DataClient(baseClient)
    }
    return serviceLevelClients.dataClient[requestingService]
  } else {
    throw new Error(`Invalid service requested to get FXCM data client: ${requestingService}`)
  }
}

export const getOrderClient = requestingService => {
  const baseClient = getBaseClient(requestingService)

  if (
    requestingService === SystemService.SIMULATION ||
    requestingService === SystemService.TRADE_SIGNAL
  ) {
    if (!serviceLevelClients.orderClient[requestingService]) {
      serviceLevelClients.orderClient[requestingService] = new OrderClient(baseClient)
    }
    return serviceLevelClients.orderClient[requestingService]
  } else {
    throw new Error(`Invalid service requested to get FXCM order client: ${requestingService}`)
  }
}

export const getOrderClientsForRelayAccounts = () => {
  return FXCM_LIVE_RELAY_API_TOKENS.map(apiToken => {
    const baseClient = new BaseClient(
      FXCM_API_PROTOCOL,
      FXCM_LIVE_API_HOST,
      FXCM_API_PORT,
      apiToken
    )
    return new OrderClient(baseClient)
  })
}

export const getPositionClient = requestingService => {
  const baseClient = getBaseClient(requestingService)

  if (
    requestingService === SystemService.SIMULATION ||
    requestingService === SystemService.TRADE_SIGNAL
  ) {
    if (!serviceLevelClients.positionClient[requestingService]) {
      serviceLevelClients.positionClient[requestingService] = new PositionClient(baseClient)
    }
    return serviceLevelClients.positionClient[requestingService]
  } else {
    throw new Error(`Invalid service requested to get FXCM position client: ${requestingService}`)
  }
}
