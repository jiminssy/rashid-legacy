import { update } from '../strategy-instance-daily-scores-updater'
import * as database from '../../database/strategy-instance-outcome-scores'

test('all new scores', async () => {
  const mockTable = 'mock-table'
  const date = '2020-10-30T00:00:00Z'
  const scores = {
    'strategy-instance-id-1': 2.35,
    'strategy-instance-id-2': -4.23
  }

  database.getStrategyInstanceOutcomeScores = jest.fn().mockResolvedValue({})
  database.insertStrategyInstanceOutcomeScores = jest.fn()
  database.updateStrategyInstanceOutcomeScores = jest.fn()

  await update(mockTable, date, scores)

  expect(database.insertStrategyInstanceOutcomeScores)
    .toHaveBeenCalledWith(mockTable, date, scores)
  expect(database.updateStrategyInstanceOutcomeScores).not.toBeCalled()
})

test('all update scores', async () => {
  const mockTable = 'mock-table'
  const date = '2020-10-30T00:00:00Z'
  const scores = {
    'strategy-instance-id-1': 2.35,
    'strategy-instance-id-2': -4.23
  }

  database.getStrategyInstanceOutcomeScores = jest.fn()
    .mockResolvedValue({
      'strategy-instance-id-2': 6.3,
      'strategy-instance-id-1': -1
    })
  database.insertStrategyInstanceOutcomeScores = jest.fn()
  database.updateStrategyInstanceOutcomeScores = jest.fn()

  await update(mockTable, date, scores)

  expect(database.insertStrategyInstanceOutcomeScores).not.toBeCalled()
  expect(database.updateStrategyInstanceOutcomeScores)
    .toHaveBeenCalledWith(mockTable, date, scores)
})

test('some insert, some update scores', async () => {
  const mockTable = 'mock-table'
  const date = '2020-10-30T00:00:00Z'
  const scores = {
    'strategy-instance-id-1': 2.35,
    'strategy-instance-id-2': -4.23,
    'strategy-instance-id-3': -7.2
  }

  database.getStrategyInstanceOutcomeScores = jest.fn()
    .mockResolvedValue({
      'strategy-instance-id-3': 6.3,
      'strategy-instance-id-1': -1
    })
  database.insertStrategyInstanceOutcomeScores = jest.fn()
  database.updateStrategyInstanceOutcomeScores = jest.fn()

  await update(mockTable, date, scores)

  expect(database.insertStrategyInstanceOutcomeScores)
    .toHaveBeenCalledWith(mockTable, date, {
      'strategy-instance-id-2': -4.23
    })
  expect(database.updateStrategyInstanceOutcomeScores)
    .toHaveBeenCalledWith(mockTable, date, {
      'strategy-instance-id-1': 2.35,
      'strategy-instance-id-3': -7.2
    })
})
