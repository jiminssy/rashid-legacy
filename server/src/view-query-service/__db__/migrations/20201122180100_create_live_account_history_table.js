
exports.up = function (knex) {
  return knex.schema.createTable('viewQuery_liveAccountHistory', function (table) {
    table.increments()
    table.integer('accountId').notNullable().references('id')
      .inTable('viewQuery_liveAccount')
    table.datetime('date').notNullable()
    table.decimal('dayStartBalance', 20, 9).notNullable()
    table.decimal('intradayDeposit', 20, 9).notNullable().defaultTo(0)
    table.decimal('intradayWithdraw', 20, 9).notNullable().defaultTo(0)
    table.decimal('dayReturn', 20, 9).nullable().defaultTo(null)
  })
}

exports.down = function (knex) {
  return knex.schema.dropTable('viewQuery_liveAccountHistory')
}
