import fs from 'fs'
import http from 'http'
import https from 'https'
import express from 'express'
import path from 'path'
import bodyParser from 'body-parser'
import { HTTP_SERVER_PORT, HTTPS_SERVER_PORT } from '../../settings'
import { ingestHttp } from '../in-message-manager'
import { register as registerUserEP } from './end-points/user'
import { register as registerStrategyEP } from './end-points/strategy'
import { register as registerReportEP } from './end-points/report'
import { register as registerSimulationEP } from './end-points/simulation'

const getRegisterPostFunc = app => {
  return (url, MessageClass) => {
    app.post(url, (request, response) => {
      const accessToken = request.headers.authorization
      const message = new MessageClass(request.body)
      ingestHttp(message, response, accessToken)
    })
  }
}

const getRegisterGetFunc = app => {
  return (url, MessageClass) => {
    app.get(url, (request, response) => {
      const accessToken = request.headers.authorization
      const message = new MessageClass(request.params)
      ingestHttp(message, response, accessToken)
    })
  }
}

const startHttp = () => {
  const app = express()
  const httpServer = http.createServer(app)

  app.get('*', (request, response) => {
    response.redirect(`https://${request.headers.host}${request.url}`)
  })

  httpServer.listen(HTTP_SERVER_PORT, () => console.log(`HTTP Server started at port ${HTTP_SERVER_PORT}`))
}

const startHttps = () => {
  const sslPrivateKey = fs.readFileSync(path.resolve(__dirname, '../../../ssl/server.key'), 'utf8')
  const sslCertificate = fs.readFileSync(path.resolve(__dirname, '../../../ssl/server.crt'), 'utf8')
  const credentials = { key: sslPrivateKey, cert: sslCertificate }

  const app = express()
  const httpsServer = https.createServer(credentials, app)

  app.use(express.static('public'))
  app.use(bodyParser.json())

  const registerPost = getRegisterPostFunc(app)
  const registerGet = getRegisterGetFunc(app)
  registerUserEP(registerPost)
  registerStrategyEP(registerPost, registerGet)
  registerReportEP(registerPost, registerGet)
  registerSimulationEP(registerPost, registerGet)

  app.get('*', (_request, response) => {
    response.sendFile(path.resolve(__dirname, '../../../public/index.html'))
  })

  httpsServer.listen(HTTPS_SERVER_PORT, () => console.log(`HTTPS Server started at port ${HTTPS_SERVER_PORT}`))
}

export const start = () => {
  startHttps()
  startHttp()
}
