import * as ApiMessage from '../../constants/ws-api-messages'

const CONN_CHECK_INTERVAL = 30000

// decided not to use WS and use REST polling from front-end instead
// left this function as it is fully functional code and could be used later.
export const connectWs = async accessToken => {
  let pingTimeout

  const socket = new WebSocket('ws://localhost:8080/')

  function onPingReceived () {
    clearTimeout(pingTimeout)

    pingTimeout = setTimeout(() => {
      socket.close()
    }, CONN_CHECK_INTERVAL + 2000)
  }

  socket.onopen = function onOpen () {
    onPingReceived() // to kick start the ping timeout
    socket.send(JSON.stringify({
      type: ApiMessage.AUTHENTICATE,
      accessToken: accessToken
    }))
  }
  socket.onclose = function clear () {
    clearTimeout(pingTimeout)
  }
  socket.onmessage = message => {
    message = JSON.parse(message.data)
    if (message.type === ApiMessage.PING) {
      onPingReceived()
      return socket.send(JSON.stringify({ type: ApiMessage.PONG }))
    }
  }
}
