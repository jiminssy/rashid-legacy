export const strategy = {
  allStrategiesList: {
    strategies: []
  },
  allStrategyInstancesList: {
    strategyInstances: []
  },
  edit: {
    strategy: null
  },
  instance: {
    edit: {
      strategyInstance: null
    }
  }
}
