import { EventHandler } from './EventHandler'
import { STRATEGY_EDIT_STARTED } from '../constants/events'
import { getStrategy } from '../utils/rashid-client'
import { notification } from '../utils/notification'
import { setValue } from '../store'

class StrategyEditStarted extends EventHandler {
  constructor () {
    super([STRATEGY_EDIT_STARTED])
  }

  async handle (strategyId) {
    const response = await getStrategy(strategyId)
    if (response.isSuccess) {
      await setValue('strategy.edit.strategy', response.strategy)
    } else {
      notification.error(`Failed to get data for strategy ID: ${strategyId}`)
    }
  }
}

export const eventHandler = new StrategyEditStarted()
