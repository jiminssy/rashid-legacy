import { authorizedRoles, ApiMessage } from './ApiMessage'
import { UserRole } from '../../user-management-service/definition/UserRole'

@authorizedRoles([UserRole.READ_ONLY])
class GetStrategyRequest extends ApiMessage {
  constructor (params) {
    super()
    this.strategyId = params.strategyId
  }
}

export { GetStrategyRequest }
