exports.seed = function (knex) {
  return knex('strategy_strategy').insert([
    {
      codePath: 'sma-cross-1',
      paramSchema: '{"$schema":"http://json-schema.org/draft-07/schema","$id":"http://example.com/example.json","type":"object","title":"The root schema","description":"The root schema comprises the entire JSON document.","default":{},"examples":[{"fastMaLookBack":14,"slowMaLookBack":25}],"required":["fastMaLookBack","slowMaLookBack"],"properties":{"fastMaLookBack":{"$id":"#/properties/fastMaLookBack","type":"integer","title":"The fastMaLookBack schema","description":"An explanation about the purpose of this instance.","default":0,"examples":[14]},"slowMaLookBack":{"$id":"#/properties/slowMaLookBack","type":"integer","title":"The slowMaLookBack schema","description":"An explanation about the purpose of this instance.","default":0,"examples":[25]}},"additionalProperties":true}'
    }
  ]).then(function () {
    return knex('strategy_strategyInstance').insert([
      {
        id: '692833b7-a658-4ebf-b959-3cdcb23bb09e',
        strategyId: 2,
        strategyParams: '{"fastMaLookBack":14,"slowMaLookBack":25}',
        tradingInstrument: 'FXCM-1-m1',
        dataStreams: '["FXCM-1-m1"]',
        isEnabled: false
      }
    ])
  })
}
