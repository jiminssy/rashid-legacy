import { ApiMessage } from './ApiMessage'

export class EditStrategyInstanceRequest extends ApiMessage {
  constructor ({
    strategyInstanceId,
    strategyId,
    strategyParams,
    tradingInstrument, // data stream id
    dataStreams, // array of data stream ids
    isEnabled
  }) {
    super()
    this.strategyInstanceId = strategyInstanceId
    this.strategyId = strategyId
    this.strategyParams = strategyParams
    this.tradingInstrument = tradingInstrument
    this.dataStreams = dataStreams
    this.isEnabled = isEnabled
  }
}
