import { knex } from '../../common/database/knex'

const broker = 'strategy_broker'
const instrument = 'strategy_instrument'
const strategy = 'strategy_strategy'
const strategyInstance = 'strategy_strategyInstance'
const dailyOutcomeScore = 'strategy_dailyOutcomeScore'

const getTopBottomTargetingStrategyInstanceIds = async tradingInstrument => {
  const strategyInstances = await knex(strategyInstance).select()
    .where({
      [`${strategyInstance}.isEnabled`]: true,
      [`${strategyInstance}.tradingInstrument`]: tradingInstrument
    })
  return strategyInstances.filter(instance => {
    return 'topNthStrategyInstance' in instance.strategyParams || 'bottomNthStrategyInstance' in instance.strategyParams
  }).map(instance => instance.id)
}

export const insertStrategyInstance = async strategyInstanceInfo => {
  await knex(strategyInstance).insert({
    ...strategyInstanceInfo,
    dataStreams: JSON.stringify(strategyInstanceInfo.dataStreams),
    strategyParams: JSON.stringify(strategyInstanceInfo.strategyParams)
  })
}

export const getAllStrategyInstances = async () => {
  return await knex(strategyInstance).select(
    `${strategy}.codePath as codePath`,
    `${strategyInstance}.*`
  ).innerJoin(strategy, `${strategy}.id`, `${strategyInstance}.strategyId`)
}

export const getBottomOutcomeScoreStrategyInstanceId = async (
  tradingInstrument,
  from,
  bottomNthStrategyInstance,
  negativeOnly
) => {
  const topBottomTargetingStrategyInstanceIds = await getTopBottomTargetingStrategyInstanceIds(tradingInstrument)
  const instances = await knex(dailyOutcomeScore)
    .select(
      `${dailyOutcomeScore}.strategyInstanceId`,
      knex.raw(`SUM("${dailyOutcomeScore}"."outcomeScore") as "outcomeScore"`)
    )
    .leftJoin(strategyInstance, `${strategyInstance}.id`, `${dailyOutcomeScore}.strategyInstanceId`)
    .whereNotIn(`${dailyOutcomeScore}.strategyInstanceId`, topBottomTargetingStrategyInstanceIds)
    .andWhere(`${strategyInstance}.tradingInstrument`, tradingInstrument)
    .andWhere(`${dailyOutcomeScore}.date`, '>=', from)
    .groupBy(`${dailyOutcomeScore}.strategyInstanceId`)
    .orderBy('outcomeScore', 'asc')
    .limit(bottomNthStrategyInstance)

  if (instances.length < bottomNthStrategyInstance) {
    return null
  } else {
    const targetStrategyInstance = instances[instances.length - 1]
    const targetScore = targetStrategyInstance.outcomeScore
    if (negativeOnly && targetScore >= 0) {
      return null
    }
    return targetStrategyInstance.strategyInstanceId
  }
}

export const getBrokerId = async name => {
  const { id } = await knex(broker).select(
    `${broker}.id`
  ).where({ name }).first()

  return id
}

export const getBrokerInstrumentId = async instrumentId => {
  const { brokerInstrumentId } = await knex(instrument).select(
    `${instrument}.brokerInstrumentId`
  ).where({
    id: instrumentId
  }).first()

  return brokerInstrumentId
}

export const getStrategyCodePath = async strategyId => {
  const result = await knex(strategy).select(
    `${strategy}.codePath as codePath`
  )
    .where({
      id: strategyId
    })
    .first()
  return result.codePath
}

export const getTopOutcomeScoreStrategyInstanceId = async (
  tradingInstrument,
  from,
  topNthStrategyInstance,
  positiveOnly
) => {
  const topBottomTargetingStrategyInstanceIds = await getTopBottomTargetingStrategyInstanceIds(tradingInstrument)
  const instances = await knex(dailyOutcomeScore)
    .select(
      `${dailyOutcomeScore}.strategyInstanceId`,
      knex.raw(`SUM("${dailyOutcomeScore}"."outcomeScore") as "outcomeScore"`)
    )
    .leftJoin(strategyInstance, `${strategyInstance}.id`, `${dailyOutcomeScore}.strategyInstanceId`)
    .whereNotIn(`${dailyOutcomeScore}.strategyInstanceId`, topBottomTargetingStrategyInstanceIds)
    .andWhere(`${strategyInstance}.tradingInstrument`, tradingInstrument)
    .andWhere(`${dailyOutcomeScore}.date`, '>=', from)
    .groupBy(`${dailyOutcomeScore}.strategyInstanceId`)
    .orderBy('outcomeScore', 'desc')
    .limit(topNthStrategyInstance)

  if (instances.length < topNthStrategyInstance) {
    return null
  } else {
    const targetStrategyInstance = instances[instances.length - 1]
    const targetScore = targetStrategyInstance.outcomeScore
    if (positiveOnly && targetScore <= 0) {
      return null
    }
    return targetStrategyInstance.strategyInstanceId
  }
}

export const isStrategyInstanceEnabled = async id => {
  const result = await knex(strategyInstance).select(
    `${strategyInstance}.isEnabled as isEnabled`
  )
    .where({ id })
    .first()
  return result.isEnabled
}
