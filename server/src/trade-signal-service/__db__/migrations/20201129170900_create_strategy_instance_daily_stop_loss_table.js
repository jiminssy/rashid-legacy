
exports.up = function (knex) {
  return knex.schema.createTable('tradeSignal_strategyInstanceDailyStopLoss', function (table) {
    table.string('strategyInstanceId').notNullable()
    table.datetime('date').nullable()
    table.decimal('stopLossOutcomeScore', 4, 2).nullable()
    table.boolean('stoppedInLive').notNullable().defaultTo(false)
    table.primary(['strategyInstanceId', 'date'])
  })
}

exports.down = function (knex) {
  return knex.schema.dropTable('tradeSignal_strategyInstanceDailyStopLoss')
}
