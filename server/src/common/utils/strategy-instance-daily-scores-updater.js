import _ from 'lodash'
import {
  getStrategyInstanceOutcomeScores,
  insertStrategyInstanceOutcomeScores,
  updateStrategyInstanceOutcomeScores
} from '../database/strategy-instance-outcome-scores'

export const update = async (table, date, newScores) => {
  const existingScores = await getStrategyInstanceOutcomeScores(table, date)
  const insertScores = {}
  const updateScores = {}
  for (const strategyInstanceId in newScores) {
    const newScore = newScores[strategyInstanceId]
    if (strategyInstanceId in existingScores) {
      updateScores[strategyInstanceId] = newScore
    } else {
      insertScores[strategyInstanceId] = newScore
    }
  }

  if (!_.isEmpty(insertScores)) {
    await insertStrategyInstanceOutcomeScores(table, date, insertScores)
  }
  if (!_.isEmpty(updateScores)) {
    await updateStrategyInstanceOutcomeScores(table, date, updateScores)
  }
}
