import { TradeDirection } from '../../common/definition/TradeDirection'

export const calcStopLoss = (
  tradeDirection,
  lastKnownBidClose,
  lastKnownAskClose,
  stopLossRange
) => {
  if (tradeDirection === TradeDirection.LONG) {
    return lastKnownBidClose - stopLossRange
  } else {
    return lastKnownAskClose + stopLossRange
  }
}
