import React from 'react'
import {
  Switch,
  Route,
  useRouteMatch
} from 'react-router-dom'
import { AdminSideBar } from '../../components/AdminSideBar'
import { Strategy } from '../Strategy'
import { Report } from '../Report'
import styles from './index.module.scss'

export const Admin = () => {
  const { path } = useRouteMatch()

  return (
    <div className="container-fluid">
      <div className="row">
        <AdminSideBar />
        <div className={`col-md-9 ml-sm-auto col-lg-10 px-md-4 ${styles.content}`}>
          <Switch>
            <Route path={`${path}/strategy`} component={Strategy} />
            <Route path={`${path}/report`} component={Report} />
            <Route path={path} exact>
              Dashboard should show up here...
            </Route>
          </Switch>
        </div>
      </div>
    </div>
  )
}
