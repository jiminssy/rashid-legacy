import { publish } from '../utils/event-bus'
import _ from 'lodash'
import { STORE_UPDATED } from '../constants/events'
import { NOT_LOGGED_IN } from '../constants/user-login-state'

import { strategy } from './strategy'
import { report } from './report'

export const store = {
  user: {
    loginState: NOT_LOGGED_IN,
    role: null
  },
  strategy,
  report
}

export const setValue = async (path, value) => {
  _.set(store, path, value)
  await new Promise(resolve => setTimeout(() => {
    publish(STORE_UPDATED, path)
    resolve()
  }, 0))
}

export const unsetValue = async path => {
  _.unset(store, path)
  await new Promise(resolve => setTimeout(() => {
    publish(STORE_UPDATED, path)
    resolve()
  }, 0))
}
