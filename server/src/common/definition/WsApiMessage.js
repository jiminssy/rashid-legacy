export const WsApiMessage = Object.freeze({
  PING: 'ping',
  PONG: 'pong',
  AUTHENTICATE: 'authenticate'
})
