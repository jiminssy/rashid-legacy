import { authorizedRoles, ApiMessage } from './ApiMessage'
import { UserRole } from '../../user-management-service/definition/UserRole'

@authorizedRoles([UserRole.READ_ONLY])
class GetStrategyInstanceRequest extends ApiMessage {
  constructor (params) {
    super()
    this.strategyInstanceId = params.strategyInstanceId
  }
}

export { GetStrategyInstanceRequest }
