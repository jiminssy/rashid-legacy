import { store, setValue, unsetValue } from '..'
import pubsub from 'pubsub.js'

const mockPublish = jest.spyOn(pubsub, 'publish')
afterEach(() => {
  mockPublish.mockReset()
})

test('setValue', async () => {
  const somePath = 'some.path'
  expect(store.some).toBe(undefined)
  await setValue(somePath, 1234)
  expect(store.some.path).toBe(1234)
  expect(mockPublish).toHaveBeenCalledTimes(1)
  // clean up
  await unsetValue(somePath)
})

test('unsetValue', async () => {
  await setValue('some.path', 1234)
  expect(store.some.path).toBe(1234)
  await unsetValue('some')
  expect(store.some).toBe(undefined)
  expect(mockPublish).toHaveBeenCalledTimes(2)
})
