import { knex } from '../../common/database/knex'

export const getStrategyInstanceOutcomeScores = async (dailyOutcomeScoresTable, date) => {
  const results = await knex(dailyOutcomeScoresTable).select()
    .where({ date })
  const scores = {}
  results.forEach(row => {
    scores[row.strategyInstanceId] = row.outcomeScore
  })
  return scores
}

export const insertStrategyInstanceOutcomeScores = async (dailyOutcomeScoresTable, date, scores) => {
  const rowsToInsert = []
  for (const strategyInstanceId in scores) {
    rowsToInsert.push({
      strategyInstanceId,
      date,
      outcomeScore: scores[strategyInstanceId]
    })
  }
  await knex(dailyOutcomeScoresTable).insert(rowsToInsert)
}

export const updateStrategyInstanceOutcomeScores = async (dailyOutcomeScoresTable, date, scores) => {
  const updates = []
  for (const strategyInstanceId in scores) {
    const update = knex(dailyOutcomeScoresTable)
      .where({
        strategyInstanceId,
        date
      })
      .update({
        outcomeScore: scores[strategyInstanceId]
      })
    updates.push(update)
  }
  await Promise.all(updates)
}
