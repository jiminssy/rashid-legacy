import moment from 'moment-timezone'
import { getResultWithRetries, GetResultRetriesError } from '../../common/utils/get-result-with-retries'
import { publish } from '../../common/event/event-bus'
import { LivePositionOpened } from '../../common/event/message/LivePositionOpened'
import { LivePositionClosed } from '../../common/event/message/LivePositionClosed'
import { setLiveOpenedPositionId, unsetLiveOpenedPositionId } from './database'

const executedTrades = {}

export const trackExecutedTrade = tradeResult => {
  executedTrades[tradeResult.tradeId] = tradeResult
}

export const onPositionOpened = async socketMessage => {
  const brokerTradeId = socketMessage.tradeId

  let tradeResult
  const closureToRetry = () => {
    return executedTrades[brokerTradeId]
  }
  const isResultSuccess = tradeResult => {
    return tradeResult != null
  }
  try {
    tradeResult = await getResultWithRetries(
      closureToRetry,
      isResultSuccess,
      500,
      120
    )
  } catch (error) {
    if (error instanceof GetResultRetriesError) {
      throw new Error(`Timeout: did not receive trade result for brokerTradeId: ${brokerTradeId}`)
    } else {
      throw error
    }
  }

  delete executedTrades[brokerTradeId]

  const openedRawTimestamp = socketMessage.time
  const openedTime = moment.utc(openedRawTimestamp, 'MMDDYYYYHHmmss').toISOString()

  await setLiveOpenedPositionId(
    tradeResult.tradingStrategyInstanceId,
    brokerTradeId,
    tradeResult.quantity,
    tradeResult.coupledSimulationPositionId
  )

  publish(new LivePositionOpened(
    tradeResult.tradingStrategyInstanceId,
    tradeResult.tradingInstrument,
    tradeResult.tradeDirection,
    tradeResult.orderType,
    tradeResult.quantity,
    tradeResult.entry,
    tradeResult.stopLoss,
    tradeResult.takeProfit,
    openedTime,
    tradeResult.openLimitPrice
  ))
}

export const onPositionClosed = async socketMessage => {
  const closedRawTimestamp = socketMessage.closeTime
  const closedTime = moment.utc(closedRawTimestamp, 'MMDDYYYYHHmmss').toISOString()
  const brokerTradeId = socketMessage.tradeId

  await unsetLiveOpenedPositionId(brokerTradeId)

  publish(new LivePositionClosed(
    brokerTradeId,
    socketMessage.close,
    closedTime
  ))
}
