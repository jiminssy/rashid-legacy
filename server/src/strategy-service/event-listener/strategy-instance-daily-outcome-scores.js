import { update } from '../../common/utils/strategy-instance-daily-scores-updater'

export const eventListener = async ({ date, scores: newScores }) => {
  await update('strategy_dailyOutcomeScore', date, newScores)
}
