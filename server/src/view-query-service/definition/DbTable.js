export const DbTable = Object.freeze({
  ASSET_CLASS: 'viewQuery_assetClass',
  BROKER: 'viewQuery_broker',
  DAILY_OUTCOME_SCORE: 'viewQuery_dailyOutcomeScores',
  INSTRUMENT: 'viewQuery_instrument',
  LIVE_ACCOUNT: 'viewQuery_liveAccount',
  LIVE_ACCOUNT_HISTORY: 'viewQuery_liveAccountHistory',
  STRATEGY: 'viewQuery_strategy',
  STRATEGY_INSTANCE: 'viewQuery_strategyInstance'
})
