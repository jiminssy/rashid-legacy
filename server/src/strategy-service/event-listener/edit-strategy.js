import { knex } from '../../common/database/knex'

export const eventListener = async (
  { strategy: { id, codePath, paramSchema } }
) => {
  await knex('strategy_strategy')
    .where({ id })
    .update({
      codePath, paramSchema
    })
}
