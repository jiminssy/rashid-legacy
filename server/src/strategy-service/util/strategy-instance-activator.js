import { getAllStrategyInstances } from './database'

const strategyInstances = {}

export const addNewStrategyInstance = (codePath, instanceInfo) => {
  const strategyCodePath = `../strategy/${codePath}`
  const StrategyClass = require(strategyCodePath).default
  const strategyInstance = new StrategyClass(instanceInfo)
  if (strategyInstance.id in strategyInstances) {
    throw new Error(`Strategy instasnce already exists. ID: ${strategyInstance.id}`)
  }
  strategyInstances[strategyInstance.id] = strategyInstance
  return strategyInstance.id
}

export const activateEnabledStrategyInstances = async () => {
  const instances = await getAllStrategyInstances()
  for (const instance of instances) {
    const strategyCodePath = `../strategy/${instance.codePath}`
    const StrategyClass = require(strategyCodePath).default
    const strategyInstance = new StrategyClass(instance)
    strategyInstances[instance.id] = strategyInstance
    if (instance.isEnabled) {
      await strategyInstance.activate()
    }
  }
}

export const activateStrategyInstance = id => {
  if (!strategyInstances[id].isActive) {
    strategyInstances[id].activate()
  }
}

export const deactivateStrategyInstance = (id, removeFromCache = false) => {
  if (strategyInstances[id].isActive) {
    strategyInstances[id].deactivate()
  }
  if (removeFromCache) {
    delete strategyInstances[id]
  }
}

export const replaceStrategyInstance = (previousId, codePath, instanceInfo) => {
  delete strategyInstances[previousId]
  const strategyCodePath = `../strategy/${codePath}`
  const StrategyClass = require(strategyCodePath).default
  const strategyInstance = new StrategyClass(instanceInfo)
  strategyInstances[instanceInfo.id] = strategyInstance
}
