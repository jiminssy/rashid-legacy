
exports.up = function (knex) {
  return knex.schema.createTable('tradeSignal_liveAccountDailyStopLoss', function (table) {
    table.string('brokerName').notNullable()
    table.string('brokerAccountId').nullable()
    table.datetime('date').nullable()
    table.decimal('stopLossOutcomeScore', 4, 2).nullable()
    table.boolean('stoppedInLive').notNullable().defaultTo(false)
    table.primary(['brokerName', 'brokerAccountId', 'date'])
  })
}

exports.down = function (knex) {
  return knex.schema.dropTable('tradeSignal_liveAccountDailyStopLoss')
}
