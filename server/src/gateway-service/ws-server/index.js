// Thought about using WS server but decided to use REST API polling from front-end
// This module is functional and working so left here to potentially use in the future.

import WebSocket from 'ws'
import { WS_SERVER_PORT, JWT_SECRET } from '../../settings'
import { WsApiMessage } from '../../common/definition/WsApiMessage'
import jwt from 'jsonwebtoken'

const CONN_CHECK_INTERVAL = 30000
const NO_AUTH_TERMINATOR_TIME = 10000

const messageTypeMap = {
  [WsApiMessage.AUTHENTICATE]: require('../api-message/WsAuthenticate').WsAuthenticate
}

export const start = () => {
  const wss = new WebSocket.Server({
    port: WS_SERVER_PORT
  })

  wss.on('connection', ws => {
    ws.isAlive = true

    const noAuthTerminator = setTimeout(() => {
      console.log('terminating due to no authentication...')
      ws.terminate()
    }, NO_AUTH_TERMINATOR_TIME)

    ws.on('message', rawMessage => {
      rawMessage = JSON.parse(rawMessage)
      if (rawMessage.type === WsApiMessage.PONG) {
        ws.isAlive = true
        return
      }

      const message = new messageTypeMap[rawMessage.type](rawMessage)
      if (rawMessage.type === WsApiMessage.AUTHENTICATE) {
        try {
          jwt.verify(message.accessToken, JWT_SECRET)
        } catch (e) {
          ws.terminate()
        }
        clearTimeout(noAuthTerminator)
      } else {

      }
    })

    ws.on('close', () => {
    })
  })

  wss.on('close', () => {
    clearInterval(cleanStaleConnections)
  })

  const cleanStaleConnections = setInterval(() => {
    wss.clients.forEach(ws => {
      try {
        if (ws.isAlive === false) return ws.terminate()
        ws.isAlive = false
        ws.send(JSON.stringify({ type: WsApiMessage.PING }))
      } catch (e) {
        console.error(e)
      }
    })
  }, CONN_CHECK_INTERVAL)

  console.log(`WS Server started at port ${WS_SERVER_PORT}`)
}
