import { update } from '../../common/utils/strategy-instance-daily-scores-updater'
import { DbTable } from '../definition/DbTable'

export const eventListener = async ({ date, scores: newScores }) => {
  await update(DbTable.DAILY_OUTCOME_SCORE, date, newScores)
}
