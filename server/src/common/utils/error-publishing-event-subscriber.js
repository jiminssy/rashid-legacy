import { subscribe as subscribeEventBus, publish } from '../event/event-bus'
import { ExceptionEventMessage } from '../event/message/ExceptionEventMessage'

const subscribeSingle = (
  serviceName,
  errorEventType,
  eventType,
  eventListener
) => {
  const errorCatchListener = async eventMessage => {
    try {
      await eventListener(eventMessage)
    } catch (e) {
      publish(new ExceptionEventMessage(
        errorEventType,
        e.name,
        `${serviceName}: exception while handling an event. ${e.message}`,
        e.stack
      ))
    }
  }

  subscribeEventBus(eventType, errorCatchListener)
}

// subscribePairs = [[eventType, eventListener], ...]
export const subscribe = (serviceName, errorEventType, subscribes) => {
  subscribes.forEach(pair => {
    const eventType = pair[0]
    const eventListener = pair[1]
    subscribeSingle(serviceName, errorEventType, eventType, eventListener)
  })
}

export const __test__ = {
  subscribeSingle
}
