import { subscribe, unsubscribe } from '../utils/event-bus'
import { notification } from '../utils/notification'

export class EventHandler {
  constructor (events) {
    this.events = events
    this.subscriptions = []
  }

  subscribe () {
    this.events.forEach(event => {
      const eventHandler = async eventMessage => {
        try {
          await this.handle(eventMessage)
        } catch (e) {
          notification.error('Unexpected Error. Please contact jiminparky@gmail.com')
        }
      }

      this.subscriptions.push(subscribe(event, eventHandler.bind(this)))
    })
  }

  unsubscribe () {
    this.subscriptions.forEach(subscription => {
      unsubscribe(subscription)
    })
  }

  async handle () {
    throw new Error('Not implemented')
  }
}
