export const OrderType = Object.freeze({
  LIMIT: 'Limit',
  MARKET: 'Market',
  STOP_LIMIT: 'StopLimit',
  STOP_MARKET: 'StopMarket'
})
