
exports.up = function (knex) {
  return knex.schema.createTable('analytics_broker', function (table) {
    table.increments()
    table.string('name')
  })
    .createTable('analytics_liveAccount', function (table) {
      table.increments()
      table.integer('brokerId').notNullable().references('id')
        .inTable('analytics_broker')
      table.string('brokerAccountId').notNullable().unique()
    })
}

exports.down = function (knex) {
  return knex.schema.dropTable('analytics_liveAccount')
    .dropTable('analytics_broker')
}
