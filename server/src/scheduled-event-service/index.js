import {
  FXCM_WEEKLY_MAINTENANCE_BEGAN,
  FXCM_WEEKLY_MAINTENANCE_ENDED,
  LSE_MARKET_OPENED,
  LSE_MARKET_CLOSED,
  NEW_FX_TRADING_DAY,
  NEW_UTC_WEEK_DAY,
  NYSE_MARKET_OPENED,
  NYSE_MARKET_CLOSED
} from '../common/event/event-type'
import { CronJob } from 'cron'
import { publish } from '../common/event/event-bus'
import { EventMessage } from '../common/event/message/EventMessage'

const LONDON_TIMEZONE = 'Europe/London'
const NEW_YORK_TIMEZONE = 'America/New_York'
const TOKYO_TIMEZONE = 'Asia/Tokyo'
const UTC = 'UTC'

const schedule = (eventType, cronTime, timezone) => {
  // tell linter to ignore because that's just how
  // cron library requires to be used
  new CronJob( // eslint-disable-line no-new
    cronTime,
    () => {
      publish(new EventMessage(eventType))
    },
    null, // on complete
    true,
    timezone
  )
}

export const start = () => {
  schedule(FXCM_WEEKLY_MAINTENANCE_BEGAN, '0 45 16 * * 5', NEW_YORK_TIMEZONE)
  schedule(FXCM_WEEKLY_MAINTENANCE_ENDED, '0 45 16 * * 0', NEW_YORK_TIMEZONE)
  schedule(LSE_MARKET_OPENED, '0 0 8 * * 1-5', LONDON_TIMEZONE)
  schedule(LSE_MARKET_CLOSED, '0 30 16 * * 1-5', LONDON_TIMEZONE)
  schedule(NEW_FX_TRADING_DAY, '0 30 7 * * 1-5', TOKYO_TIMEZONE)
  schedule(NEW_UTC_WEEK_DAY, '0 0 0 * * 1-5', UTC)
  schedule(NYSE_MARKET_OPENED, '0 30 9 * * 1-5', NEW_YORK_TIMEZONE)
  schedule(NYSE_MARKET_CLOSED, '0 0 16 * * 1-5', NEW_YORK_TIMEZONE)
}
