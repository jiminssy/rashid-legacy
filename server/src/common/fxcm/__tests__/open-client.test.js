import { OrderClient } from '../order-client'
import * as instruments from '../instruments'
import { TradeDirection } from '../../definition/TradeDirection'

describe('openTradeMarket', () => {
  instruments.getSymbolByInstrumentId = jest.fn().mockReturnValue('EUR/USD')

  test('order not executed should throw error', async () => {
    expect.assertions(2)

    const baseClient = {
      requestPost: jest.fn().mockResolvedValue({
        response: {
          executed: false
        }
      })
    }
    const orderClient = new OrderClient(baseClient)

    try {
      await orderClient.openTradeMarket(1, TradeDirection.LONG, 1, 10, 30)
    } catch (error) {
      expect(error).toBeInstanceOf(Error)
      expect(error.message).toMatchSnapshot()
    }
  })

  test('order is executed', async () => {
    const baseClient = {
      requestPost: jest.fn().mockResolvedValue({
        response: {
          executed: true
        },
        data: {
          orderId: 'order-id-1234'
        }
      })
    }
    const orderClient = new OrderClient(baseClient)
    orderClient._waitForTradeResultsUpdate = jest.fn().mockResolvedValue('mock trade results')

    await orderClient.openTradeMarket(1, TradeDirection.LONG, 1, 10, 30)

    const postRequestPayload = baseClient.requestPost.mock.calls[0][1]
    expect(postRequestPayload).toMatchSnapshot()
  })
})
