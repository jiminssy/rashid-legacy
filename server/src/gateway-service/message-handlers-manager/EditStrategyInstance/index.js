import { MessageHandler } from '../MessageHandler'
import { outMessageManager } from '../../out-message-manager'
import { publish } from '../../../common/event/event-bus'
import { doesStrategyInstanceExist } from '../../../view-query-service/query-interface/strategy'
import { EditStrategyInstance as EditStrategyInstanceMessage } from '../../../common/event/message/EditStrategyInstance'
import { StrategyInstance } from '../../../common/definition/StrategyInstance'
import { generateId } from '../../../common/utils/strategy-instance-id-generator'
import { validateStrategyParams } from '../../utils/json-schema-validator'

class EditStrategyInstance extends MessageHandler {
  async handle (message) {
    if (!(await doesStrategyInstanceExist(message.strategyInstanceId))) {
      this._handleFail(message.id, 'Strategy instance does not exist')
      return
    }

    if (!await validateStrategyParams(message.strategyId, message.strategyParams)) {
      this._handleFail(message.id, 'Invalid strategy instance parameters')
      return
    }
    const newId = generateId(
      message.strategyId,
      message.strategyParams,
      message.tradingInstrument,
      message.dataStreams
    )
    const previousId = message.strategyInstanceId
    publish(new EditStrategyInstanceMessage(
      previousId,
      new StrategyInstance({
        ...message,
        id: newId,
        dataStreams: JSON.parse(message.dataStreams),
        strategyParams: JSON.parse(message.strategyParams)
      })
    ))
    outMessageManager.sendOkResponse(message.id, { isSuccess: true })
  }

  _handleFail (messageId, failReason) {
    outMessageManager.sendOkResponse(messageId, {
      isSuccess: false,
      failReason
    })
  }
}

export const handler = new EditStrategyInstance()
