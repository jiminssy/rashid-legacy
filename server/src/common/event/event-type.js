export const ALL = 'all'

// scheduled events
export const SCHEDULED = 'all/scheduled'
export const LSE_MARKET_OPENED = 'all/scheduled/lseMarketOpened'
export const LSE_MARKET_CLOSED = 'all/scheduled/lseMarketClosed'
export const NEW_FX_TRADING_DAY = 'all/scheduled/newFxTradingDay'
export const NEW_UTC_WEEK_DAY = 'all/scheduled/newUtcDay'
export const NYSE_MARKET_OPENED = 'all/scheduled/nyseMarketOpened'
export const NYSE_MARKET_CLOSED = 'all/scheduled/nyseMarketClosed'

// data stream events
export const DATA_STREAM = 'all/dataStream'
export const SUBSCRIBE_DATA_STREAM = 'all/dataStream/subscribe'
export const UNSUBSCRIBE_DATA_STREAM = 'all/dataStream/unsubscribe'
export const DATA_STREAM_UPDATE = 'all/dataStream/update'

// api events
export const API = 'all/api'
export const ADD_NEW_STRATEGY = 'all/api/strategy/addNew'
export const ADD_NEW_STRATEGY_INSTANCE = 'all/api/strategy/addNewInstance'
export const CLOSE_SIMULATION_POSITION = 'all/api/simulation/closePosition'
export const EDIT_STRATEGY = 'all/api/strategy/edit'
export const EDIT_STRATEGY_INSTANCE = 'all/api/strategy/instance/edit'
export const REMOVE_STRATEGY = 'all/api/strategy/remove'
export const REMOVE_STRATEGY_INSTANCE = 'all/api/strategy/instance/remove'

// analytics events
export const ANALYTICS_LIVE_ACCOUNT_HISTORY_UPDATED = 'all/analytics/liveAccountHistoryUpdated'
export const ANALYTICS_STRATEGY_INSTANCE_DAILY_STOP_LOSS = 'all/analytics/strategyInstanceDailyStopLoss'
export const ANALYTICS_LIVE_ACCOUNT_DAILY_STOP_LOSS = 'all/analytics/liveAccountDailyStopLoss'

// strategy events
export const STRATEGY_TRADE_SIGNAL = 'all/strategy/tradeSignal'
export const STRATEGY_TARGET_STRATEGY_INSTANCE_UPDATED = 'all/strategy/targetStrategyInstanceUpdated'

// simulation events
export const SIMULATION_DAILY_OUTCOME_SCORES = 'all/simulation/dailyOutcomeScoresUpdate'
export const SIMULATION_POSITION_CLOSED = 'all/simulation/positionClosed'
export const SIMULATION_POSITION_OPENED = 'all/simulation/positionOpened'

// trade signal events
export const LIVE_TRADE_SIGNAL = 'all/tradeSignal/liveTradeSignal'
export const LIVE_POSITION_OPENED = 'all/tradeSignal/livePositionOpened'
export const LIVE_POSITION_CLOSED = 'all/tradeSignal/livePositionClosed'

// FXCM events
export const FXCM_SOCKET_CONNECTED = 'all/fxcm/socketConnected'
export const FXCM_WEEKLY_MAINTENANCE_BEGAN = 'all/fxcm/weeklyMaintenanceBegan'
export const FXCM_WEEKLY_MAINTENANCE_ENDED = 'all/fxcm/weeklyMaintenanceEnded'

// errors
export const ERROR = 'all/error'
export const ERROR_DATA_STREAM_PUBLISH = 'all/error/dataStream/publish'
export const ERROR_FXCM_CONNECTION = 'all/error/fxcm/connectionError'
export const ERROR_FXCM_KEEP_ALIVE_FAILED = 'all/error/fxcm/keepAliveFailed'
export const ERROR_FXCM_NO_DATA = 'all/error/fxcm/noData'
export const ERROR_FXCM_REST_REQUEST = 'all/error/fxcm/restRequest'
export const ERROR_GATEWAY_AUTHORIZATION_FAIL = 'all/error/gateway/authorizationFail'
export const ERROR_GATEWAY_MESSAGE_HANDLE = 'all/error/gateway/messageHandle'
export const ERROR_SERVICES_START = 'all/error/servicesStart'
export const ERROR_SIMULATION_OPEND_POSITION_UPDATE_FAIL = 'all/error/simulation/openedPositionUpdateFail'
export const ERROR_SIMULATION_CLOSED_POSITION_UPDATE_FAIL = 'all/error/simulation/closedPositionUpdateFail'
export const ERROR_STRATEGY_DATA_STREAM_LISTENER = 'all/error/strategy/dataStreamListener'

// errors - service level event handlers
export const ERROR_ANALYTICS_SERVICE_EVENT_LISTENER = 'all/error/analytics/eventListener'
export const ERROR_DATA_STREAM_SERVICE_EVENT_LISTENER = 'all/error/dataStream/eventListener'
export const ERROR_RELAY_SIGNAL_SERVICE_EVENT_LISTENER = 'all/error/relaySignal/eventListener'
export const ERROR_SIMULATION_SERVICE_EVENT_LISTENER = 'all/error/simulation/eventListener'
export const ERROR_STRATEGY_SERVICE_EVENT_LISTENER = 'all/error/strategy/eventListener'
export const ERROR_TRADE_SIGNAL_SERVICE_EVENT_LISTENER = 'all/error/tradeSignal/eventListener'
export const ERROR_VIEW_QUERY_SERVICE_EVENT_LISTENER = 'all/error/viewQuery/eventListener'
