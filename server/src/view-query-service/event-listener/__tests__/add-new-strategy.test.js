import { getMockKnex } from '../../../common/test-mock/mock-knex'
import { eventListener } from '../add-new-strategy'
import { Strategy } from '../../../common/definition/Strategy'

const knex = getMockKnex()

test('insert to db', async () => {
  const eventMessage = {
    strategy: new Strategy({
      id: 1,
      name: 'Test Name',
      version: 5,
      description: 'Some description',
      codePath: 'src/some/path',
      paramSchema: '{ "someParam": 1234 }'
    })
  }

  await eventListener(eventMessage)

  const insertArgs = knex.insert.mock.calls[0][0]
  expect(insertArgs).toMatchSnapshot()
  expect(knex.insert).toHaveBeenCalled()
})
