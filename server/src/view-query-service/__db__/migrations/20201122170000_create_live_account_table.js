
exports.up = function (knex) {
  return knex.schema.createTable('viewQuery_liveAccount', function (table) {
    table.increments()
    table.integer('brokerId').notNullable().references('id')
      .inTable('viewQuery_broker')
    table.string('brokerAccountId').notNullable().unique()
  })
}

exports.down = function (knex) {
  return knex.schema.dropTable('viewQuery_liveAccount')
}
