import io from 'socket.io-client'
import querystring from 'querystring'
import UUID from 'uuid-js'
import { publish } from '../event/event-bus'
import {
  ERROR_FXCM_CONNECTION,
  ERROR_FXCM_KEEP_ALIVE_FAILED,
  ERROR_FXCM_REST_REQUEST,
  FXCM_SOCKET_CONNECTED
} from '../event/event-type'
import { EventMessage } from '../event/message/EventMessage'
import { ExceptionEventMessage } from '../event/message/ExceptionEventMessage'
import { ApiListenableTable } from './ApiListenableTable'

const listenableTables = [
  ApiListenableTable.ACCOUNT,
  ApiListenableTable.CLOSED_POSITION,
  ApiListenableTable.OFFER,
  ApiListenableTable.OPEN_POSITION,
  ApiListenableTable.ORDER,
  ApiListenableTable.PROPERTIES,
  ApiListenableTable.SUMMARY
]

export class BaseClient {
  constructor (protocol, host, port, apiToken) {
    this._isInitialized = false
    this._apiProtocol = require(protocol)
    this._url = `${protocol}://${host}:${port}`
    this._host = host
    this._port = port
    this._socketOptions = querystring.stringify({
      access_token: apiToken
    })
    this._apiToken = apiToken
    this._socket = null
    this._authToken = null
    this._messageListeners = []
    this._keepAliveInterval = null
  }

  // public methods

  async initialize () {
    if (!this._isInitialized) {
      if (!this._socket) {
        await this._authenticate()
      }
      this._isInitialized = true
    }
  }

  async requestGet (path) {
    return this._httpRequest('GET', path)
  }

  async requestPost (path, payload) {
    return this._httpRequest('POST', path, payload)
  }

  subscribe (listener) {
    const subId = UUID.create().hex
    this._messageListeners[subId] = listener
    return subId
  }

  unsubscribe (subId) {
    delete this._messageListeners[subId]
  }

  // private methods

  async _authenticate () {
    return new Promise((resolve, reject) => {
      this._socket = io(this._url, {
        query: this._socketOptions,
        reconnection: true,
        reconnectionAttempts: Infinity,
        reconnectionDelay: 1000,
        reconnectionDelayMax: 1000,
        randomizationFactor: 0
      })
      this._socket.on('connect', () => {
        this._authToken = `Bearer ${this._socket.id}${this._apiToken}`

        publish(new EventMessage(FXCM_SOCKET_CONNECTED))

        this._subscribeAllTableUpdates()
          .then(() => {
            this._startKeepAlive()
            resolve()
          })
          .catch(error => {
            publish(new ExceptionEventMessage(
              ERROR_FXCM_CONNECTION,
              error.name,
              error.message,
              error.stack
            ))
            reject(error)
          })
      })
      this._socket.on('connect_error', () => {
        const error = new Error('Socket failed to connect to FXCM server.')
        publish(new ExceptionEventMessage(
          ERROR_FXCM_CONNECTION,
          error.name,
          error.message,
          error.stack
        ))
      })
      this._socket.on('connect_timeout', () => {
        const error = new Error('Socket failed to connect to FXCM server. Timed out.')
        publish(new ExceptionEventMessage(
          ERROR_FXCM_CONNECTION,
          error.name,
          error.message,
          error.stack
        ))
      })
      this._socket.on('disconnect', () => {
        const error = new Error('Socket was disconnected')
        publish(new ExceptionEventMessage(
          ERROR_FXCM_CONNECTION,
          error.name,
          error.message,
          error.stack
        ))
        this._stopKeepAlive()
      })
    })
  }

  async _httpRequest (method, path, payload) {
    return new Promise((resolve, reject) => {
      const request = this._apiProtocol.request({
        host: this._host,
        port: this._port,
        path,
        method,
        headers: {
          'User-Agent': 'request',
          Accept: 'application/json',
          'Content-Type': 'application/x-www-form-urlencoded',
          Authorization: this._authToken
        }
      }, response => {
        let data = ''
        response.on('data', chunk => { data += chunk }) // re-assemble fragmented response data
        response.on('end', () => {
          let result
          try {
            result = JSON.parse(data)
          } catch (e) {
            const error = new Error(`Failed to parse JSON, REST request method: ${method}, path: ${path}, payload: ${JSON.stringify(payload)}, received data: ${data}`)
            publish(new ExceptionEventMessage(
              ERROR_FXCM_REST_REQUEST,
              error.name,
              error.message,
              error.stack
            ))
            reject(error)
            return
          }

          if (
            result.response.executed ||
            (method === 'GET' && path.startsWith('/candles/'))
          ) {
            resolve(result)
          } else {
            const message = `Received error: ${result.response.error} || ` +
              `method: ${method} || ` +
              `path: ${path} || ` +
              `payload: ${payload ? JSON.stringify(payload) : 'null'}`
            const error = new Error(message)
            publish(new ExceptionEventMessage(
              ERROR_FXCM_REST_REQUEST,
              error.name,
              error.message,
              error.stack
            ))
            reject(error)
          }
        })
      }).on('error', error => {
        publish(new ExceptionEventMessage(
          ERROR_FXCM_REST_REQUEST,
          error.name,
          error.message,
          error.stack
        ))
        reject(error) // this is called when network request fails
      })

      if (method === 'POST') {
        const payloadQuery = querystring.stringify(payload)
        request.write(payloadQuery)
      }

      request.end()
    })
  }

  async _startKeepAlive () {
    this._keepAliveInterval = setInterval(() => {
      // poll trading account information to keep the socket alive
      if (this._socket.connected) {
        const path = '/trading/get_model/?models=Account'
        this.requestGet(path).catch(error => {
          publish(new ExceptionEventMessage(
            ERROR_FXCM_KEEP_ALIVE_FAILED,
            error.name,
            error.message,
            error.stack
          ))
        })
      }
    }, 1000 * 60)
  }

  async _stopKeepAlive () {
    clearInterval(this._keepAliveInterval)
  }

  async _subscribeAllTableUpdates () {
    // listen on the socket
    const onAnyTableMessage = message => {
      const listeners = Object.values(this._messageListeners)
      listeners.forEach(listener => {
        listener(message)
      })
    }
    listenableTables.forEach(table => {
      this._socket.on(table, message => onAnyTableMessage({
        table,
        payload: JSON.parse(message)
      }))
    })

    // send subscription request messages
    const subscribes = listenableTables.map(table => {
      return this.requestPost(
        '/trading/subscribe',
        { models: table }
      )
    })

    const subResults = await Promise.all(subscribes)
    const allExecuted = subResults.every(result => result.response.executed)
    if (!allExecuted) {
      throw new Error('Failed to subscribe to all table updates')
    }
  }
}
