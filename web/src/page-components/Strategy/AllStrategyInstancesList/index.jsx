import React, { useEffect } from 'react'
import { useSelector } from '../../../hooks/selector'
import { ALL_STRATEGY_INSTANCES_LIST_STARTED, REMOVE_STRATEGY_INSTANCE } from '../../../constants/events'
import { publish } from '../../../utils/event-bus'
import { useHistory } from 'react-router-dom'
import styles from './index.module.scss'

export const AllStrategyInstancesList = () => {
  const history = useHistory()
  const instances = useSelector('strategy.allStrategyInstancesList.strategyInstances')

  useEffect(() => {
    publish(ALL_STRATEGY_INSTANCES_LIST_STARTED)
  }, [])

  const onEditClick = instanceId => {
    history.push(`/admin/strategy/instance/edit/${instanceId}`)
  }

  const onRemoveClick = instanceId => {
    publish(REMOVE_STRATEGY_INSTANCE, instanceId)
  }

  const rows = instances.map((instance, index) => {
    return (
      <tr key={index}>
        <td className={styles.isEnabledCell}>
          <div className="custom-control custom-switch">
            <input type="checkbox" className="custom-control-input" id="isEnabledSwitch"
              checked={instance.isEnabled} disabled
            />
            <label className="custom-control-label" htmlFor="isEnabledSwitch"></label>
          </div>
        </td>
        <td>{instance.strategyName}</td>
        <td>{instance.strategyVersion}</td>
        <td>{instance.tradingInstrument}</td>
        <td>{JSON.stringify(instance.strategyParams)}</td>
        <td>{JSON.stringify(instance.dataStreams)}</td>
        <td>
          <i className={`fas fa-pencil-alt ${styles.editIcon}`}
            onClick={() => onEditClick(instance.id)}
          />
        </td>
        <td>
          <i className={`fas fa-times-circle ${styles.removeIcon}`}
            onClick={() => onRemoveClick(instance.id)}
          />
        </td>
      </tr>
    )
  })

  return (
    <div>
      <h4 className="page-title">List All Strategy Instances</h4>
      <table className="table table-striped table-dark">
        <thead>
          <tr>
            <th scope="col"></th>
            <th scope="col">Name</th>
            <th scope="col">Version</th>
            <th scope="col">Trading Instrument</th>
            <th scope="col">Parameters</th>
            <th scope="col">Data Streams</th>
            <th></th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {rows}
        </tbody>
      </table>
    </div>
  )
}
