import { getMockKnex } from '../../../common/test-mock/mock-knex'
import { eventListener } from '../remove-strategy-instance'
import { RemoveStrategyInstance } from '../../../common/event/message/RemoveStrategyInstance'

jest.mock('uuid-js', () => ({
  create: () => ({ hex: 'test-context-id' })
}))

const knex = getMockKnex()

test('success - delete', async () => {
  const eventMessage = new RemoveStrategyInstance('test-strategy-instance-id')

  await eventListener(eventMessage)

  expect(knex.where).toHaveBeenCalled()
  const whereArgs = knex.where.mock.calls[0][0]
  expect(whereArgs).toMatchSnapshot()

  expect(knex.del).toHaveBeenCalled()
})
