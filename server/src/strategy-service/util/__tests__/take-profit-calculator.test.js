import { calcTakeProfit } from '../take-profit-calculator'
import { TradeDirection } from '../../../common/definition/TradeDirection'

describe('calcTakeProfit', () => {
  test('calculate for long direction', () => {
    const takeProfit = calcTakeProfit(
      TradeDirection.LONG,
      25.3465,
      27.2342,
      19.4836,
      3
    )
    expect(takeProfit).toBe(50.486000000000004)
  })

  test('calculate for short direction', () => {
    const takeProfit = calcTakeProfit(
      TradeDirection.SHORT,
      25.3465,
      27.2342,
      33.0971,
      3
    )
    expect(takeProfit).toBe(2.094700000000003)
  })
})
