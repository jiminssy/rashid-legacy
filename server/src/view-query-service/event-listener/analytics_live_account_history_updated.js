import { knex } from '../../common/database/knex'
import { DbTable } from '../definition/DbTable'

const getDbAccountId = async (brokerName, brokerAccountId) => {
  const account = await knex(DbTable.LIVE_ACCOUNT)
    .select()
    .innerJoin(DbTable.BROKER, `${DbTable.BROKER}.id`, `${DbTable.LIVE_ACCOUNT}.brokerId`)
    .where({
      [`${DbTable.BROKER}.name`]: brokerName,
      [`${DbTable.LIVE_ACCOUNT}.brokerAccountId`]: brokerAccountId
    })
    .first()
  return account.id
}

export const eventListener = async ({
  brokerName, brokerAccountId, date, dayStartBalance,
  intradayDeposit, intradayWithdraw, dayReturn
}) => {
  const accountId = await getDbAccountId(brokerName, brokerAccountId)
  await knex(DbTable.LIVE_ACCOUNT_HISTORY).insert({
    accountId, date, dayStartBalance, intradayDeposit, intradayWithdraw, dayReturn
  })
}
