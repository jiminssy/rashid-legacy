import { ApiMessage } from './ApiMessage'

export class LogInWithCredsRequest extends ApiMessage {
  constructor ({ userName, password }) {
    super()
    this.userName = userName
    this.password = password
  }
}
