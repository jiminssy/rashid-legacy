import { EventHandler } from './EventHandler'
import { LOAD_REPORT_DP_STRATEGY_INSTANCES } from '../constants/events'
import { getStrategyInstance } from '../utils/rashid-client'
import { setValue } from '../store'

class LoadReportDPStrategyInstances extends EventHandler {
  constructor () {
    super([LOAD_REPORT_DP_STRATEGY_INSTANCES])
  }

  async handle (eventMessage) {
    const getStrategyInstances = eventMessage.strategyInstances
      .map(instance => getStrategyInstance(instance.strategyInstanceId))
    let strategyInstances = await Promise.all(getStrategyInstances)
    strategyInstances = strategyInstances.map(({
      isSuccess,
      strategyInstance
    }, index) => {
      const strategyInstanceId = eventMessage.strategyInstances[index].strategyInstanceId
      const totalPerformance = eventMessage.strategyInstances[index].totalPerformance
      if (isSuccess) {
        const {
          name,
          version,
          tradingInstrument,
          strategyParams
        } = strategyInstance
        return {
          strategyInstanceId,
          name,
          version,
          tradingInstrument,
          strategyParams,
          totalPerformance
        }
      } else {
        return {
          strategyInstanceId,
          isRemoved: true,
          totalPerformance
        }
      }
    })

    if (eventMessage.isTop) {
      await setValue(
        'report.dailyPerformance.topStrategyInstances',
        strategyInstances
      )
    } else {
      await setValue(
        'report.dailyPerformance.bottomStrategyInstances',
        strategyInstances
      )
    }
  }
}

export const eventHandler = new LoadReportDPStrategyInstances()
