import { handler } from '..'
import { outMessageManager } from '../../../out-message-manager'
import * as queryInterface from '../../../../view-query-service/query-interface/strategy'

test('get all strategy instances', async () => {
  outMessageManager.sendOkResponse = jest.fn()
  const queryResults = [
    {
      strategyName: 'Test Strategy 1',
      strategyVersion: 1,
      instrumentName: 'EUR/USD',
      strategyParams: {
        param1: 'test value 1',
        param2: 1
      },
      dataStreams: ['FXCM-1-m5']
    },
    {
      strategyName: 'Test Strategy 2',
      strategyVersion: 2,
      instrumentName: 'USD/JPY',
      strategyParams: {
        param3: 5
      },
      dataStreams: ['FXCM-2-H1']
    }
  ]
  queryInterface.getAllStrategyInstances = jest.fn().mockResolvedValue(queryResults)

  await handler.handle({ id: 'test-message-id' })

  const messageId = outMessageManager.sendOkResponse.mock.calls[0][0]
  const replyParam = outMessageManager.sendOkResponse.mock.calls[0][1]

  expect(messageId).toBe('test-message-id')
  expect(replyParam).toMatchSnapshot()
})
