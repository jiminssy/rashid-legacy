import { parseDataStreamId } from '../../common/utils/data-stream-id-parser'

export const eventListener = async liveTradeSignal => {
  const orderClients = require('../index').getOrderClients()
  if (orderClients == null) {
    return
  }

  const { brokerInstrumentId } = parseDataStreamId(liveTradeSignal.tradingInstrument)
  for (let i = 0; i < orderClients.length; i++) {
    await orderClients[i].openTradeMarket(
      brokerInstrumentId,
      liveTradeSignal.tradeDirection,
      liveTradeSignal.quantity,
      liveTradeSignal.stopLoss,
      liveTradeSignal.takeProfit
    )
  }
}
