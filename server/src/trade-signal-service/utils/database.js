import { knex } from '../../common/database/knex'

const dailyOutcomeScore = 'tradeSignal_dailyOutcomeScore'
const liveAccountDailyStopLoss = 'tradeSignal_liveAccountDailyStopLoss'
const liveStrategyInstance = 'tradeSignal_liveStrategyInstance'
const strategyInstanceDailyStopLoss = 'tradeSignal_strategyInstanceDailyStopLoss'

const doesDailyStopLossExist = async (strategyInstanceId, date) => {
  const result = await knex(strategyInstanceDailyStopLoss)
    .select()
    .where({ strategyInstanceId, date })
  return result.length > 0
}

const insertDailyStopLoss = async (strategyInstanceId, date, stopLossOutcomeScore, reversedStopLossOutcomeScore) => {
  await knex(strategyInstanceDailyStopLoss).insert({
    strategyInstanceId,
    date,
    stopLossOutcomeScore,
    reversedStopLossOutcomeScore
  })
}

const updateDailyStopLoss = async (strategyInstanceId, date, stopLossOutcomeScore, reversedStopLossOutcomeScore) => {
  await knex(strategyInstanceDailyStopLoss)
    .where({ strategyInstanceId, date })
    .update({ stopLossOutcomeScore, reversedStopLossOutcomeScore })
}

export const getCoupledLivePosition = async coupledSimulationPositionId => {
  const row = await knex(liveStrategyInstance).select()
    .where({ coupledSimulationPositionId })
    .first()

  if (!row) {
    return null
  } else {
    return {
      id: row.openedPositionId,
      size: row.positionSize
    }
  }
}

export const getDailyOutcomeScore = async (strategyInstanceId, date) => {
  return await knex(dailyOutcomeScore)
    .select()
    .where({
      strategyInstanceId,
      date
    })
    .first()
}

export const getDailyOutcomeScoresSum = async (strategyInstanceIds, date) => {
  const rows = await knex(dailyOutcomeScore)
    .select('outcomeScore')
    .whereIn('strategyInstanceId', strategyInstanceIds)
    .andWhere({ date })

  if (rows.length === 0) {
    return null
  } else {
    return rows.map(row => row.outcomeScore)
      .reduce((total, score) => total + score, 0)
  }
}

export const getLiveTradingAllowedStrategyInstsances = async () => {
  return await knex(liveStrategyInstance).select()
}

export const getStrategyInstanceDailyStopLoss = async (strategyInstanceId, date) => {
  return await knex(strategyInstanceDailyStopLoss)
    .select()
    .where({
      strategyInstanceId,
      date
    })
    .first()
}

export const getLiveAccountDailyStopLoss = async (brokerName, brokerAccountId, date) => {
  return await knex(liveAccountDailyStopLoss).select()
    .where({ brokerName, brokerAccountId, date })
    .first()
}

export const insertOrUpdateStrategyInstanceDailyStopLoss = async (
  strategyInstanceId,
  date,
  stopLossOutcomeScore,
  reversedStopLossOutcomeScore
) => {
  if (await doesDailyStopLossExist(strategyInstanceId, date)) {
    await updateDailyStopLoss(strategyInstanceId, date, stopLossOutcomeScore, reversedStopLossOutcomeScore)
  } else {
    await insertDailyStopLoss(strategyInstanceId, date, stopLossOutcomeScore, reversedStopLossOutcomeScore)
  }
}

export const insertLiveAccountDailyStopLoss = async (
  brokerName,
  brokerAccountId,
  date,
  stopLossOutcomeScore
) => {
  await knex(liveAccountDailyStopLoss).insert({
    brokerName,
    brokerAccountId,
    date,
    stopLossOutcomeScore
  })
}

export const setLiveAccountStoppedOut = async (brokerName, brokerAccountId, date) => {
  await knex(liveAccountDailyStopLoss)
    .where({ brokerName, brokerAccountId, date })
    .update({ stoppedInLive: true })
}

export const setLiveOpenedPositionId = async (
  strategyInstanceId,
  openedPositionId,
  positionSize,
  coupledSimulationPositionId
) => {
  await knex(liveStrategyInstance)
    .where('strategyInstanceId', strategyInstanceId)
    .update({
      openedPositionId,
      positionSize,
      coupledSimulationPositionId
    })
}

export const setStrategyInstanceStoppedOut = async (strategyInstanceId, date) => {
  await knex(strategyInstanceDailyStopLoss)
    .where({ strategyInstanceId, date })
    .update({ stoppedInLive: true })
}

export const unsetLiveOpenedPositionId = async positionId => {
  await knex(liveStrategyInstance)
    .where('openedPositionId', positionId)
    .update({
      openedPositionId: null,
      positionSize: null,
      coupledSimulationPositionId: null
    })
}
