
exports.up = function (knex) {
  return knex.schema.table('viewQuery_strategy', function (table) {
    table.text('description').notNullable().alter()
  })
}

exports.down = function (knex) {
  return knex.schema.table('viewQuery_strategy', function (table) {
    table.string('description').notNullable().alter()
  })
}
