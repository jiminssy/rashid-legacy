import * as baseClientModule from '../base-client'
import {
  getDataClient,
  getOrderClient,
  getPositionClient
} from '../client-factory'
import { SystemService } from '../../definition/SystemService'

class MockBaseClient {}

baseClientModule.BaseClient = MockBaseClient

test('single data client object for each service', () => {
  const dataStreamClient1 = getDataClient(SystemService.DATA_STREAM)
  const dataStreamClient2 = getDataClient(SystemService.DATA_STREAM)
  const simulationClient = getDataClient(SystemService.SIMULATION)
  expect(dataStreamClient1 === dataStreamClient2).toBe(true)
  expect(dataStreamClient1 !== simulationClient).toBe(true)
})

test('single order client object for each service', () => {
  const client1 = getOrderClient(SystemService.SIMULATION)
  const client2 = getOrderClient(SystemService.SIMULATION)
  expect(client1 === client2).toBe(true)
})

test('single position client object for each service', () => {
  const client1 = getPositionClient(SystemService.SIMULATION)
  const client2 = getPositionClient(SystemService.SIMULATION)
  expect(client1 === client2).toBe(true)
})

test('throws error for ineligible system service', () => {
  expect.assertions(2)
  try {
    getDataClient('Some Ineligible Service')
  } catch (error) {
    expect(error).toBeInstanceOf(Error)
    expect(error).toHaveProperty('message', 'Invalid service requested to get FXCM base client: Some Ineligible Service')
  }
})
