import { ApiListenableTable } from './ApiListenableTable'

export class PositionClient {
  constructor (baseClient) {
    this._baseClient = baseClient
    this._isInitialized = false
    this._closedPositionListeners = []
    this._openedPositionListeners = []

    this._onMessageReceived = this._onMessageReceived.bind(this)
  }

  // public methods

  addClosedPositionListener (listener) {
    this._closedPositionListeners.push(listener)
  }

  addOpenedPositionListener (listener) {
    this._openedPositionListeners.push(listener)
  }

  async initialize () {
    if (!this._isInitialized) {
      await this._baseClient.initialize()
      this._baseClient.subscribe(this._onMessageReceived)
      this._isInitialized = true
    }
  }

  // private methods

  _isCloseConfirmationMessage (message) {
    return message.payload.action === 'I'
  }

  _isOpenConfirmationMessage (message) {
    return message.payload.action === 'I'
  }

  _onMessageReceived (message) {
    if (message.table === ApiListenableTable.OPEN_POSITION) {
      if (this._isOpenConfirmationMessage(message)) {
        this._openedPositionListeners.forEach(listener => listener(message.payload))
      }
    } else if (message.table === ApiListenableTable.CLOSED_POSITION) {
      if (this._isCloseConfirmationMessage(message)) {
        this._closedPositionListeners.forEach(listener => listener(message.payload))
      }
    }
  }
}
