import { ApiMessage } from './ApiMessage'

export class WsAuthenticate extends ApiMessage {
  constructor (rawMessage) {
    super()
    this.accessToken = rawMessage.accessToken
  }
}
