import jwt from 'jsonwebtoken'
import { outMessageManager } from '../out-message-manager'
import { handle } from '../message-handlers-manager'
import { JWT_SECRET, SUPPORT_EMAIL } from '../../settings'
import { publish } from '../../common/event/event-bus'
import { ExceptionEventMessage } from '../../common/event/message/ExceptionEventMessage'
import { ERROR_GATEWAY_MESSAGE_HANDLE } from '../../common/event/event-type'
import { isAuthorized, isAuthNeeded } from './auth'
import { EventMessage } from '../../common/event/message/EventMessage'

const isRequestMessage = message => {
  const className = message.constructor.name
  return className.includes('Request')
}

const handleRequestMessage = async (
  message,
  sendFailedAuthentication,
  sendErrorResponse,
  sendOkResponse
) => {
  outMessageManager.waitResultToRespond(
    message.id,
    sendOkResponse,
    sendErrorResponse,
    sendFailedAuthentication
  )

  try {
    await handle(message)
  } catch (e) {
    publish(new ExceptionEventMessage(
      ERROR_GATEWAY_MESSAGE_HANDLE,
      e.name,
      `Failed to handle incoming message: ${JSON.stringify(message)}. ${e.message}`,
      e.stack
    ))
    outMessageManager.sendErrorResponse(
      message.id,
      `Unexpected error. Please contact ${SUPPORT_EMAIL}.`
    )
  }
}

const ingest = async (
  userId,
  message,
  sendFailedAuthentication,
  sendFailedAuthorization,
  sendErrorResponse,
  sendOkResponse
) => {
  if (isAuthNeeded(message)) {
    if (!await isAuthorized(userId, message)) {
      sendFailedAuthorization()
      return
    }
  }

  if (isRequestMessage(message)) {
    handleRequestMessage(
      message,
      sendFailedAuthentication,
      sendErrorResponse,
      sendOkResponse
    )
  } else {
    const eventType = message.eventType
    const apiMessageId = message.id
    delete message.eventType
    delete message.id
    publish(new EventMessage(eventType, apiMessageId, message))
    sendOkResponse('')
  }
}

export const ingestHttp = async (message, response, accessToken) => {
  const sendFailedAuthentication = () => {
    response.status(401)
    response.send()
  }

  const sendFailedAuthorization = () => {
    response.status(403)
    response.send()
  }

  const sendErrorResponse = errorMessage => {
    response.status(500)
    response.send(errorMessage)
  }

  const sendOkResponse = payload => {
    response.status(200)
    response.send(payload)
  }

  let userId
  if (isAuthNeeded(message)) {
    try {
      userId = jwt.verify(accessToken, JWT_SECRET).userId
    } catch (e) {
      sendFailedAuthentication()
      return
    }
  }

  await ingest(
    userId,
    message,
    sendFailedAuthentication,
    sendFailedAuthorization,
    sendErrorResponse,
    sendOkResponse
  )
}

export const __test__ = {
  ingest
}
