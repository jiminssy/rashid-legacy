
exports.up = function (knex) {
  return knex.schema.createTable('tradeSignal_liveStrategyInstance', function (table) {
    table.string('strategyInstanceId').primary()
  })
}

exports.down = function (knex) {
  return knex.schema.dropTable('tradeSignal_liveStrategyInstance')
}
