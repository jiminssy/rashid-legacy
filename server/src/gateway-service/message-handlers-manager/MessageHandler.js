export class MessageHandler {
  constructor () {
    this.handle.bind(this)
  }

  async handle () {
    throw new Error('Not implemented')
  }
}
