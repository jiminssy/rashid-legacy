import { subscribe } from '../common/utils/error-publishing-event-subscriber'
import {
  ANALYTICS_LIVE_ACCOUNT_DAILY_STOP_LOSS,
  ANALYTICS_STRATEGY_INSTANCE_DAILY_STOP_LOSS,
  ERROR_TRADE_SIGNAL_SERVICE_EVENT_LISTENER,
  SIMULATION_DAILY_OUTCOME_SCORES,
  SIMULATION_POSITION_CLOSED,
  STRATEGY_TRADE_SIGNAL
} from '../common/event/event-type'
import { SystemService } from '../common/definition/SystemService'
import { getDataClient, getOrderClient, getPositionClient } from '../common/fxcm/client-factory'
import { onPositionOpened, onPositionClosed } from './utils/fxcm-position-update-listener'

const subscribes = [
  [ANALYTICS_STRATEGY_INSTANCE_DAILY_STOP_LOSS, require('./event-listener/update-strategy-instance-daily-stop-loss').eventListener],
  [ANALYTICS_LIVE_ACCOUNT_DAILY_STOP_LOSS, require('./event-listener/update-live-account-daily-stop-loss').eventListener],
  [SIMULATION_DAILY_OUTCOME_SCORES, require('./event-listener/update-strategy-instance-daily-outcome-score').eventListener],
  [SIMULATION_POSITION_CLOSED, require('./event-listener/close-coupled-position').eventListener],
  [STRATEGY_TRADE_SIGNAL, require('./event-listener/strategy-trade-signal').eventListener]
]

export const start = async () => {
  const dataClient = getDataClient(SystemService.TRADE_SIGNAL)
  await dataClient.initialize()

  const orderClient = getOrderClient(SystemService.TRADE_SIGNAL)
  await orderClient.initialize()

  const positionClient = getPositionClient(SystemService.TRADE_SIGNAL)
  positionClient.addOpenedPositionListener(onPositionOpened)
  positionClient.addClosedPositionListener(onPositionClosed)
  await positionClient.initialize()

  subscribe(
    'TradeSignalService',
    ERROR_TRADE_SIGNAL_SERVICE_EVENT_LISTENER,
    subscribes
  )
}
