
exports.up = function (knex) {
  return knex.schema.createTable('viewQuery_broker', function (table) {
    table.increments()
    table.string('name').notNullable()
  })
    .createTable('viewQuery_assetClass', function (table) {
      table.increments()
      table.string('name').notNullable()
    })
    .createTable('viewQuery_instrument', function (table) {
      table.increments()
      table.integer('assetClassId').notNullable().references('id')
        .inTable('viewQuery_assetClass')
      table.integer('brokerId').notNullable().references('id')
        .inTable('viewQuery_broker')
      table.string('brokerInstrumentId').notNullable()
      table.string('name').notNullable()
      table.unique(['brokerId', 'brokerInstrumentId'])
    })
    .createTable('viewQuery_strategyInstance', function (table) {
      table.string('id').primary()
      table.integer('strategyId').notNullable().references('id')
        .inTable('viewQuery_strategy')
      table.json('strategyParams').notNullable()
      table.string('tradingInstrument').notNullable()
      table.json('dataStreams').notNullable()
      table.boolean('isEnabled').notNullable()
    })
}

exports.down = function (knex) {
  return knex.schema.dropTable('viewQuery_strategyInstance')
    .dropTable('viewQuery_instrument')
    .dropTable('viewQuery_assetClass')
    .dropTable('viewQuery_broker')
}
