
exports.up = function (knex) {
  return knex.schema.createTable('tradeSignal_dailyOutcomeScore', function (table) {
    table.string('strategyInstanceId').notNullable()
    table.datetime('date').nullable()
    table.decimal('outcomeScore', 4, 2).nullable()
    table.primary(['strategyInstanceId', 'date'])
  })
}

exports.down = function (knex) {
  return knex.schema.dropTable('tradeSignal_dailyOutcomeScore')
}
