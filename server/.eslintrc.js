module.exports = {
  parser: 'babel-eslint',
  env: {
    es2020: true,
    node: true,
    "jest/globals": true,
  },
  extends: [
    'standard'
  ],
  parserOptions: {
    ecmaVersion: 11,
    sourceType: 'module'
  },
  rules: {
  },
  plugins: [
    'jest',
  ],
}
