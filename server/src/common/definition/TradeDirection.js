export const TradeDirection = Object.freeze({
  LONG: 'Long',
  SHORT: 'Short'
})
