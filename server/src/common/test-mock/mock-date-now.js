const realDateNow = Date.now.bind(global.Date)

export const mockDateNow = () => {
  const dateNowStub = jest.fn(() => 1530518207007)
  global.Date.now = dateNowStub
}

export const unmockDateNow = () => {
  global.Date.now = realDateNow
}
