import React from 'react'
import styles from './index.module.scss'
import { useHistory } from 'react-router-dom'
import { useSelector } from '../../hooks/selector'

export const SubMenuItem = ({ faIcon, text, navigateTo, allowedRoles }) => {
  const history = useHistory()
  const userRole = useSelector('user.role')

  if (allowedRoles.includes(userRole)) {
    return (
      <div className={`card ${styles.container}`} onClick={() => history.push(navigateTo)}>
        <div className="card-body">
          <i className={`fas ${styles.icon} ${faIcon}`} />
          { text }
        </div>
      </div>
    )
  } else {
    return null
  }  
}
