import { TestFixture } from './TestFixture'

class FxcmFixture extends TestFixture {
  setUp () {
    process.env.FXCM_API_TOKEN = '247926bf94af75e3d91e8cdec883453699d4fa1f'
    process.env.FXCM_API_PROTOCOL = 'https'
    process.env.FXCM_API_HOST = 'api-demo.fxcm.com'
    process.env.FXCM_API_PORT = '443'
  }

  cleanUp () {
    process.env.FXCM_API_TOKEN = undefined
    process.env.FXCM_API_PROTOCOL = undefined
    process.env.FXCM_API_HOST = undefined
    process.env.FXCM_API_PORT = undefined
  }
}

export const fxcmFixture = new FxcmFixture()
