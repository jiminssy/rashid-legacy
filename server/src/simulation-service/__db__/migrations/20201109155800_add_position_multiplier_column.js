
exports.up = function (knex) {
  return knex.schema.table('simulation_position', function (table) {
    table.decimal('sizeMultiplier', 20, 9).notNullable().defaultTo(1)
  })
}

exports.down = function (knex) {
  return knex.schema.table('simulation_position', function (table) {
    table.dropColumn('sizeMultiplier')
  })
}
