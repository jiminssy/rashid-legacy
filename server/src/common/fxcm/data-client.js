import querystring from 'querystring'
import { CandleData } from '../definition/CandleData'

export class DataClient {
  constructor (baseClient) {
    this._baseClient = baseClient
  }

  async initialize () {
    await this._baseClient.initialize()
  }

  async getInstrumentRealtimeInfo (fxcmInstrumentId) {
    const path = '/trading/get_model/?models=Offer'
    const { offers } = await this._baseClient.requestGet(path)
    const instrument = offers.find(instrument => instrument.offerId === Number(fxcmInstrumentId))
    return instrument
  }

  async getTradingAccount () {
    const path = '/trading/get_model/?models=Account'
    const response = await this._baseClient.requestGet(path)
    return response.accounts.filter(account => account.isTotal == null)[0]
  }

  async getAllSymbols () {
    const path = '/trading/get_instruments'
    const response = await this._baseClient.requestGet(path)
    return response.data.instrument
  }

  async getHistoricalData (
    fxcmInstrumentId,
    periodId,
    from,
    to
  ) {
    const queryParams = querystring.stringify({
      from: from.unix(),
      to: to.unix()
    })
    const path = `/candles/${fxcmInstrumentId}/${periodId}/?${queryParams}`
    const response = await this._baseClient.requestGet(path)
    return response.candles.map(candle => CandleData.fromFxcmData(candle))
  }

  async getMostRecentHistoricalData (
    fxcmInstrumentId,
    periodId,
    numCandles
  ) {
    const queryParams = querystring.stringify({ num: numCandles })
    const path = `/candles/${fxcmInstrumentId}/${periodId}/?${queryParams}`
    const response = await this._baseClient.requestGet(path)
    return response.candles.map(candle => CandleData.fromFxcmData(candle))
  }
}
