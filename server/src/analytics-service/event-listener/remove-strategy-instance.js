import { removeStrategyInstance } from '../utils/database'

export const eventListener = async ({ strategyInstanceId }) => {
  await removeStrategyInstance(strategyInstanceId)
}
