import { handler } from '..'
import { EditStrategyInstanceRequest } from '../../../api-message/EditStrategyInstanceRequest'
import { outMessageManager } from '../../../out-message-manager'
import * as queryInterface from '../../../../view-query-service/query-interface/strategy'
import { mockPublish, unmockPublish } from '../../../../common/test-mock/mock-event-bus'
import { publish } from '../../../../common/event/event-bus'
import * as jsonSchemaValidator from '../../../../gateway-service/utils/json-schema-validator'

jest.mock('uuid-js', () => ({
  create: () => ({ hex: 'test-uuid' })
}))

outMessageManager.sendOkResponse = jest.fn()

const testApiMessage = new EditStrategyInstanceRequest({
  strategyInstanceId: 'test-strategy-instance-id',
  strategyId: 1,
  strategyParams: '{"test":"params"}',
  tradingInstrument: 'FXCM-1-m1',
  dataStreams: '["FXCM-1-m5", "FXCM-2-h1"]',
  isEnabled: true
})

describe('handle', () => {
  beforeEach(() => {
    outMessageManager.sendOkResponse.mockReset()
    mockPublish()
  })

  afterEach(() => {
    unmockPublish()
  })

  test('strategy instance does not exist', async () => {
    queryInterface.doesStrategyInstanceExist = jest.fn().mockResolvedValue(false)

    await handler.handle(testApiMessage)

    expect(outMessageManager.sendOkResponse).toHaveBeenCalledWith(testApiMessage.id, {
      isSuccess: false,
      failReason: 'Strategy instance does not exist'
    })
  })

  test('invalid parameters', async () => {
    queryInterface.doesStrategyInstanceExist = jest.fn().mockResolvedValue(true)
    jsonSchemaValidator.validateStrategyParams = jest.fn().mockResolvedValue(false)
    await handler.handle(testApiMessage)
    expect(outMessageManager.sendOkResponse).toHaveBeenCalledWith(testApiMessage.id, {
      isSuccess: false,
      failReason: 'Invalid strategy instance parameters'
    })
  })

  test('success', async () => {
    queryInterface.doesStrategyInstanceExist = jest.fn().mockResolvedValue(true)
    jsonSchemaValidator.validateStrategyParams = jest.fn().mockResolvedValue(true)
    await handler.handle(testApiMessage)
    expect(outMessageManager.sendOkResponse).toHaveBeenCalledWith(testApiMessage.id, {
      isSuccess: true
    })
    const publishedMessage = publish.mock.calls[0][0]
    expect(publishedMessage).toMatchSnapshot()
  })
})
