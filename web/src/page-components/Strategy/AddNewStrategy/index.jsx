import React from 'react'
import styles from './index.module.scss'
import { StrategyForm } from '../../../components/StrategyForm'

export const AddNewStrategy = () => {
  return (
    <div>
      <h4 className="page-title">Add New Strategy</h4>
      <div className={`card ${styles.formContainer}`}>
        <div className="card-body">
          <StrategyForm />
        </div>
      </div>
    </div>
  )
}
