import { editStrategyInstance } from '../utils/database'

export const eventListener = async (
  {
    previousStrategyInstanceId,
    strategyInstance: {
      id: newStrategyInstanceId,
      isEnabled
    }
  }
) => {
  await editStrategyInstance(previousStrategyInstanceId, newStrategyInstanceId, isEnabled)
}
