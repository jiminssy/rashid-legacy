import { knex } from '../../../common/database/knex'
import { DbTable } from '../../definition/DbTable'

export const getDailyPerformances = async from => {
  return await knex(DbTable.DAILY_OUTCOME_SCORE).select()
    .where('date', '>=', from)
}
