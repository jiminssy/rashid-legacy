require('dotenv').config()

export const NODE_ENV = process.env.NODE_ENV

export const HTTP_SERVER_PORT = process.env.HTTP_SERVER_PORT
export const HTTPS_SERVER_PORT = process.env.HTTPS_SERVER_PORT
export const WS_SERVER_PORT = 8080

export const SUPPORT_EMAIL = 'jiminparky@gmail.com'

export const CAPITAL_RISK_PER_TRADE_PERCENT = Number(process.env.CAPITAL_RISK_PER_TRADE_PERCENT)

export const FXCM_API_PROTOCOL = process.env.FXCM_API_PROTOCOL
export const FXCM_API_PORT = process.env.FXCM_API_PORT

export const FXCM_DEMO_API_HOST = process.env.FXCM_DEMO_API_HOST
export const FXCM_DEMO_API_TOKEN = process.env.FXCM_DEMO_API_TOKEN

export const FXCM_LIVE_ACCOUNT_ID = process.env.FXCM_LIVE_ACCOUNT_ID
export const FXCM_LIVE_API_HOST = process.env.FXCM_LIVE_API_HOST
export const FXCM_LIVE_API_TOKEN = process.env.FXCM_LIVE_API_TOKEN

export const FXCM_LIVE_RELAY_API_TOKENS = JSON.parse((process.env.FXCM_LIVE_RELAY_API_TOKENS || '[]'))

export const DB_HOST = process.env.DB_HOST
export const DB_USER = process.env.DB_USER
export const DB_PASSWORD = process.env.DB_PASSWORD
export const DB_NAME = process.env.DB_NAME

export const JWT_SECRET = process.env.JWT_SECRET

export const SERVER_TIMEZONE = process.env.SERVER_TIMEZONE

export const SLACK_TOKEN = process.env.SLACK_TOKEN
export const SLACK_ERROR_CHANNEL_ID = process.env.SLACK_ERROR_CHANNEL_ID
