import { getMockKnex } from '../../../common/test-mock/mock-knex'
import { eventListener } from '../add-new-strategy-instance'
import { StrategyInstance } from '../../../common/definition/StrategyInstance'
import * as instanceActivator from '../../util/strategy-instance-activator'
import * as database from '../../util/database'

const knex = getMockKnex()

const mockedActivate = jest.fn().mockReturnValue('test-strategy-instance-id')
instanceActivator.addNewStrategyInstance = mockedActivate

const mockedGetCodePath = jest.fn()
database.getStrategyCodePath = mockedGetCodePath

test('do not activate and insert to db', async () => {
  const eventMessage = {
    strategyInstance: new StrategyInstance({
      id: 'test-strategy-instance-id',
      strategyId: 1,
      strategyParams: '{ "lookBack": 5 }',
      tradingInstrument: 'FXCM-1-m1',
      dataStreams: '["FXCM-1-m5"]',
      isEnabled: false
    })
  }

  await eventListener(eventMessage)

  const insertArgs = knex.insert.mock.calls[0][0]
  expect(insertArgs).toMatchSnapshot()
  expect(knex.insert).toHaveBeenCalled()

  knex.insert.mockClear()
})

test('activate and insert to db', async () => {
  mockedGetCodePath.mockResolvedValue('no-op')
  instanceActivator.activateStrategyInstance = jest.fn()

  const eventMessage = {
    strategyInstance: new StrategyInstance({
      id: 'test-strategy-instance-id',
      strategyId: 1,
      strategyParams: '{ "lookBack": 5 }',
      tradingInstrument: 'FXCM-1-m1',
      dataStreams: '["FXCM-1-m5"]',
      isEnabled: true
    })
  }

  await eventListener(eventMessage)

  expect(mockedActivate).toHaveBeenCalled()

  const insertArgs = knex.insert.mock.calls[0][0]
  expect(insertArgs).toMatchSnapshot()
  expect(knex.insert).toHaveBeenCalled()

  knex.insert.mockClear()
})
