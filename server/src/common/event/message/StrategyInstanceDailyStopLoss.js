import { EventMessage } from './EventMessage'
import { ANALYTICS_STRATEGY_INSTANCE_DAILY_STOP_LOSS } from '../event-type'

export class StrategyInstanceDailyStopLoss extends EventMessage {
  constructor (
    strategyInstanceId,
    date,
    stopLossOutcomeScore,
    reversedStopLossOutcomeScore
  ) {
    super(ANALYTICS_STRATEGY_INSTANCE_DAILY_STOP_LOSS)
    this.strategyInstanceId = strategyInstanceId
    this.date = date
    this.stopLossOutcomeScore = stopLossOutcomeScore
    this.reversedStopLossOutcomeScore = reversedStopLossOutcomeScore
  }
}
