import { getMockKnex } from '../../../common/test-mock/mock-knex'
import { eventListener } from '../remove-strategy'
import { RemoveStrategy } from '../../../common/event/message/RemoveStrategy'

jest.mock('uuid-js', () => ({
  create: () => ({ hex: 'test-context-id' })
}))

const knex = getMockKnex()

test('success - delete', async () => {
  const eventMessage = new RemoveStrategy('test-strategy-id')

  await eventListener(eventMessage)

  expect(knex.where).toHaveBeenCalled()
  const whereArgs = knex.where.mock.calls[0][0]
  expect(whereArgs).toMatchSnapshot()

  expect(knex.del).toHaveBeenCalled()
})
