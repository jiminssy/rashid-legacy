import { subscribe } from '../common/utils/error-publishing-event-subscriber'
import {
  ADD_NEW_STRATEGY,
  ADD_NEW_STRATEGY_INSTANCE,
  ANALYTICS_LIVE_ACCOUNT_HISTORY_UPDATED,
  EDIT_STRATEGY,
  EDIT_STRATEGY_INSTANCE,
  ERROR_VIEW_QUERY_SERVICE_EVENT_LISTENER,
  REMOVE_STRATEGY,
  REMOVE_STRATEGY_INSTANCE,
  SIMULATION_DAILY_OUTCOME_SCORES
} from '../common/event/event-type'

const subscribes = [
  [ADD_NEW_STRATEGY, require('./event-listener/add-new-strategy').eventListener],
  [ADD_NEW_STRATEGY_INSTANCE, require('./event-listener/add-new-strategy-instance').eventListener],
  [ANALYTICS_LIVE_ACCOUNT_HISTORY_UPDATED, require('./event-listener/analytics_live_account_history_updated').eventListener],
  [EDIT_STRATEGY, require('./event-listener/edit-strategy').eventListener],
  [EDIT_STRATEGY_INSTANCE, require('./event-listener/edit-strategy-instance').eventListener],
  [REMOVE_STRATEGY, require('./event-listener/remove-strategy').eventListener],
  [REMOVE_STRATEGY_INSTANCE, require('./event-listener/remove-strategy-instance').eventListener],
  [SIMULATION_DAILY_OUTCOME_SCORES, require('./event-listener/strategy-instances-daily-outcome-scores').eventListener]
]

export const start = () => {
  subscribe(
    'ViewQueryService',
    ERROR_VIEW_QUERY_SERVICE_EVENT_LISTENER,
    subscribes
  )
}
