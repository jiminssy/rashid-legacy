
exports.up = function (knex) {
  return knex.schema.createTable('simulation_position', function (table) {
    table.increments()
    table.string('strategyInstanceId').notNullable()
    table.integer('instrumentId').notNullable().references('id')
      .inTable('simulation_instrument')
    table.string('brokerOrderId').notNullable()
    table.string('brokerTradeId').notNullable()
    table.decimal('open', 20, 9).notNullable()
    table.decimal('stopLoss', 20, 9).notNullable()
    table.decimal('takeProfit', 20, 9).notNullable()
    table.boolean('isClosed').notNullable()
    table.decimal('close', 20, 9).nullable()
    table.decimal('outcomeScore', 4, 2).nullable()
    table.datetime('openedTimeStamp').nullable()
    table.datetime('closedTimeStamp').nullable()
  })
}

exports.down = function (knex) {
  return knex.schema.dropTable('simulation_position')
}
