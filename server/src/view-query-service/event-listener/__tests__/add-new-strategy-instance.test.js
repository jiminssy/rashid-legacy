import { getMockKnex } from '../../../common/test-mock/mock-knex'
import { eventListener } from '../add-new-strategy-instance'
import { StrategyInstance } from '../../../common/definition/StrategyInstance'

const knex = getMockKnex()

test('insert to db', async () => {
  const eventMessage = {
    strategyInstance: new StrategyInstance({
      id: 'test-strategy-instance-id',
      strategyId: 1,
      strategyParams: '{ "lookBack": 5 }',
      tradingInstrument: 'FXCM-1-m1',
      dataStreams: '["FXCM-1-m5"]',
      isEnabled: false
    })
  }

  await eventListener(eventMessage)

  const insertArgs = knex.insert.mock.calls[0][0]
  expect(insertArgs).toMatchSnapshot()
  expect(knex.insert).toHaveBeenCalled()
})
