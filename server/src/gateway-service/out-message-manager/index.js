class OutMessageManager {
  constructor () {
    this.waitingResults = new Map()
  }

  waitResultToRespond (
    requestMessageId,
    sendOkResponse,
    sendErrorResponse,
    sendFailedAuthentication
  ) {
    this.waitingResults.set(requestMessageId, {
      sendOkResponse: sendOkResponse,
      sendErrorResponse: sendErrorResponse,
      sendFailedAuthentication: sendFailedAuthentication
    })
  }

  sendOkResponse (requestMessageId, response) {
    const send = this.waitingResults.get(requestMessageId).sendOkResponse
    this.waitingResults.delete(requestMessageId)
    send(response)
  }

  sendErrorResponse (requestMessageId, errorMessage) {
    const send = this.waitingResults.get(requestMessageId).sendErrorResponse
    this.waitingResults.delete(requestMessageId)
    send(errorMessage)
  }

  sendFailedAuthentication (requestMessageId) {
    const send = this.waitingResults.get(requestMessageId).sendFailedAuthentication
    this.waitingResults.delete(requestMessageId)
    send()
  }
}

export const outMessageManager = new OutMessageManager()
