import _ from 'lodash'
import { Indicator } from '..'

describe('updateWithNewData', () => {
  const numLookBack = 10
  const mockCandleData = {
    bidClose: 0.12,
    askClose: 0.34
  }
  let indicator

  beforeEach(() => {
    indicator = new Indicator('testDataStreamId', numLookBack)
    indicator._toIndicatorData = jest.fn().mockImplementation(candleData => {
      return (candleData.bidClose + candleData.askClose) / 2
    })
  })

  test('not enough look back data', () => {
    indicator._computeSingleValue = jest.fn()

    indicator.updateWithNewData(mockCandleData)

    expect(indicator._computeSingleValue).not.toHaveBeenCalled()
    expect(indicator.values.length).toBe(0)
  })

  test('calculated new value', () => {
    indicator._computeSingleValue = jest.fn().mockImplementation(
      data => data.reduce((a, b) => a + b, 0)
    )

    for (let i = 0; i < numLookBack; i++) {
      indicator.updateWithNewData(mockCandleData)
    }

    expect(indicator._computeSingleValue).toHaveBeenCalledTimes(1)
    expect(indicator.values.length).toBe(1)
    expect(_.round(indicator.values[0], 1)).toBe(2.3)

    // test updating one more value
    indicator.updateWithNewData({
      bidClose: 0.56,
      askClose: 0.78
    })

    expect(indicator._computeSingleValue).toHaveBeenCalledTimes(2)
    expect(indicator.values.length).toBe(2)
    expect(_.round(indicator.values[1], 2)).toBe(2.74)
  })
})
