import { ApiEventMessage } from './ApiEventMessage'
import { CLOSE_SIMULATION_POSITION } from '../../common/event/event-type'

export class CloseSimulationPosition extends ApiEventMessage {
  constructor ({
    brokerTradeId,
    closedTime,
    close
  }) {
    super(CLOSE_SIMULATION_POSITION)
    this.brokerTradeId = brokerTradeId
    this.closedTime = closedTime
    this.close = close
  }
}
