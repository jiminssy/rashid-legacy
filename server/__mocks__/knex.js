/*
This module is only used to set return values from db queries by mocking knex library itself.
If there is need for checking what arguments are passed to knex queries, then use
src/common/test-mock/mock-knex.js instead.
*/

var resultIndex = 0
var resultsQueue = []

const mockedKnex = {
  select: jest.fn().mockReturnThis(),
  from: jest.fn().mockReturnThis(),
  where: jest.fn().mockReturnThis(),
  first: jest.fn().mockReturnThis(),
  insert: jest.fn().mockReturnThis(),
  orderBy: jest.fn().mockReturnThis(),
  update: jest.fn().mockReturnThis(),
  del: jest.fn().mockReturnThis(),
  innerJoin: jest.fn().mockReturnThis(),
  _setResults: function(results) {
    resultIndex = 0
    resultsQueue = results
  },
  then: jest.fn(function (done) {
    done(resultsQueue[resultIndex])
    resultIndex += 1
  })
}

module.exports = () => mockedKnex
