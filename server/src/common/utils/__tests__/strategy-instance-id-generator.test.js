import { generateId } from '../strategy-instance-id-generator'

test('generates distinct IDs', () => {
  const id1 = generateId(
    1,
    '{"testStrategy":"params"}',
    1,
    '["FXCM-1-m5", "FXCM-2-h1"]'
  )
  const id2 = generateId(
    2,
    '{"testStrategy":"params"}',
    1,
    '["FXCM-1-m5", "FXCM-2-h1"]'
  )
  expect(id1).toMatchSnapshot()
  expect(id2).toMatchSnapshot()
  expect(id1 === id2).toBe(false)
})
