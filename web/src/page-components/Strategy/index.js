import React from 'react'
import {
  Switch,
  Route,
  useRouteMatch
} from 'react-router-dom'
import { SubMenuItem } from '../../components/SubMenuItem'
import { AddNewStrategy } from './AddNewStrategy'
import { AddNewStrategyInstance } from './AddNewStrategyInstance'
import { AllStrategiesList } from './AllStrategiesList'
import { AllStrategyInstancesList } from './AllStrategyInstancesList'
import { EditStrategy } from './EditStrategy'
import { EditStrategyInstance } from './EditStrategyInstance'
import { UserRole } from '../../constants/user-role'

export const Strategy = () => {
  const { path } = useRouteMatch()

  const renderSubMenuItems = () => {
    return (
      <>
        <SubMenuItem
          faIcon="fa-plus-square"
          text="Add New Strategy"
          navigateTo="/admin/strategy/add-new"
          allowedRoles={[UserRole.ADMIN]}
        />
        <SubMenuItem
          faIcon="fa-plus-square"
          text="Add New Strategy Instance"
          navigateTo="/admin/strategy/add-new-instance"
          allowedRoles={[UserRole.ADMIN]}
        />
        <SubMenuItem
          faIcon="fa-list"
          text="All Strategies List"
          navigateTo="/admin/strategy/all-strategies"
          allowedRoles={[UserRole.ADMIN]}
        />
        <SubMenuItem
          faIcon="fa-list"
          text="All Strategy Instances List"
          navigateTo="/admin/strategy/all-strategy-instances"
          allowedRoles={[UserRole.ADMIN]}
        />
      </>
    )
  }

  return (
    <div>
      <Switch>
        <Route path={`${path}/add-new`} component={AddNewStrategy} />
        <Route path={`${path}/add-new-instance`} component={AddNewStrategyInstance} />
        <Route path={`${path}/all-strategies`} component={AllStrategiesList} />
        <Route path={`${path}/all-strategy-instances`} component={AllStrategyInstancesList} />
        <Route path={`${path}/edit/:strategyId`} component={EditStrategy} />
        <Route path={`${path}/instance/edit/:strategyInstanceId`} component={EditStrategyInstance} />
        <Route path={path} exact>
          <h4 className="page-title">Strategy</h4>
          { renderSubMenuItems() }
        </Route>
      </Switch>
    </div>
  )
}
