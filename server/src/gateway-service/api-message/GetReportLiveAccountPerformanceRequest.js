import { authorizedRoles, ApiMessage } from './ApiMessage'
import { UserRole } from '../../user-management-service/definition/UserRole'

@authorizedRoles([UserRole.READ_ONLY])
class GetReportLiveAccountPerformanceRequest extends ApiMessage {
  constructor ({
    fromDate,
    toDate
  }) {
    super()
    this.fromDate = fromDate
    this.toDate = toDate
  }
}

export { GetReportLiveAccountPerformanceRequest }
