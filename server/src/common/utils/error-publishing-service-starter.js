import { publish } from '../event/event-bus'
import { ExceptionEventMessage } from '../event/message/ExceptionEventMessage'
import { ERROR_SERVICES_START } from '../event/event-type'

export const catchErrorAndPublish = async start => {
  try {
    await start()
  } catch (error) {
    publish(new ExceptionEventMessage(
      ERROR_SERVICES_START,
      error.name,
      error.message,
      error.stack
    ))
  }
}
