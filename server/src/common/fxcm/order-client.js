import { getSymbolByInstrumentId } from './instruments'
import { getResultWithRetries, GetResultRetriesError } from '../utils/get-result-with-retries'
import { ApiListenableTable } from './ApiListenableTable'
import { TradeDirection } from '../definition/TradeDirection'

export class OrderClient {
  constructor (baseClient) {
    this._baseClient = baseClient
    this._accountId = null
    this._ordersWaitingForResults = {}

    this._onMessageReceived = this._onMessageReceived.bind(this)
  }

  get _isInitialized () {
    return this._accountId != null
  }

  // public methods

  async initialize () {
    if (!this._isInitialized) {
      await this._baseClient.initialize()
      this._baseClient.subscribe(this._onMessageReceived)
      this._accountId = await this._getAccountId()
    }
  }

  async openTradeMarket (
    fxcmInstrumentId,
    tradeDirection,
    numLots,
    stopLoss,
    takeProfit
  ) {
    const payload = {
      account_id: this._accountId,
      symbol: getSymbolByInstrumentId(fxcmInstrumentId),
      is_buy: tradeDirection === TradeDirection.LONG,
      rate: 0,
      amount: numLots,
      stop: stopLoss,
      limit: takeProfit,
      is_in_pips: false,
      order_type: 'AtMarket',
      time_in_force: 'GTC'
    }
    const { response: { executed }, data } = await this._baseClient.requestPost(
      '/trading/open_trade', payload
    )

    if (!executed) {
      throw new Error(`Failed to open trade with market order. ${JSON.stringify(payload)}`)
    }

    const { orderId } = data

    try {
      return await this._waitForTradeResultsUpdate(orderId)
    } finally {
      delete this._ordersWaitingForResults[orderId]
    }
  }

  async closeTradeMarket (tradeId, numLots) {
    const payload = {
      trade_id: tradeId,
      rate: 0,
      amount: numLots,
      at_market: 0,
      order_type: 'AtMarket',
      time_in_force: 'GTC'
    }
    await this._baseClient.requestPost(
      '/trading/close_trade', payload
    )
  }

  // private methods
  async _getAccountId () {
    const { accounts } = await this._baseClient.requestGet('/trading/get_model?models=Account')
    return accounts[0].accountId
  }

  _isOrderUpdateMessage (message) {
    return message.payload.orderId && message.payload.stop && message.payload.limit
  }

  _isTradeConfirmationMessage (message) {
    // FXCM's table insert event, means order is executed with trade ID found in payload
    return message.payload.orderId && message.payload.tradeId && message.payload.action === 'I'
  }

  _onMessageReceived (message) {
    if (message.table === ApiListenableTable.ORDER) {
      if (this._isTradeConfirmationMessage(message)) {
        this._ordersWaitingForResults[message.payload.orderId] = {
          tradeId: message.payload.tradeId,
          entry: message.payload.buy !== 0 ? message.payload.buy : message.payload.sell,
          stopLoss: null,
          takeProfit: null
        }
      } else if (
        this._isOrderUpdateMessage(message) &&
        this._ordersWaitingForResults[message.payload.orderId] &&
        this._ordersWaitingForResults[message.payload.orderId].stopLoss == null &&
        this._ordersWaitingForResults[message.payload.orderId].takeProfit == null
      ) {
        this._ordersWaitingForResults[message.payload.orderId].stopLoss = message.payload.stop
        this._ordersWaitingForResults[message.payload.orderId].takeProfit = message.payload.limit
      }
    }
  }

  async _waitForTradeResultsUpdate (orderId) {
    const closureToRetry = () => {
      return this._ordersWaitingForResults[orderId]
    }
    const isResultSuccess = tradeResult => {
      return tradeResult != null &&
        tradeResult.stopLoss != null &&
        tradeResult.takeProfit != null
    }
    let tradeResult
    try {
      tradeResult = await getResultWithRetries(
        closureToRetry,
        isResultSuccess,
        500,
        120 // try 120 times for 1 minute
      )
    } catch (error) {
      if (error instanceof GetResultRetriesError) {
        throw new Error(`Timeout: did not receive trade result for orderId: ${orderId}`)
      } else {
        throw error
      }
    }

    return {
      orderId: orderId.toString(),
      ...tradeResult
    }
  }
}
