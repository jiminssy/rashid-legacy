import { handler } from '..'
import { outMessageManager } from '../../../out-message-manager'
import * as queryInterface from '../../../../view-query-service/query-interface/strategy'
import { mockPublish, unmockPublish } from '../../../../common/test-mock/mock-event-bus'
import { publish } from '../../../../common/event/event-bus'
import { AddNewStrategyRequest } from '../../../api-message/AddNewStrategyRequest'

jest.mock('uuid-js', () => ({
  create: () => ({ hex: 'test-context-id' })
}))
outMessageManager.sendOkResponse = jest.fn()

const testApiMessage = new AddNewStrategyRequest({
  name: 'Test Strategy',
  version: 1,
  description: 'Test description.',
  codePath: 'test/code/path',
  paramSchema: '{ "test": "schema" }'
})

describe('handle', () => {
  beforeEach(() => {
    outMessageManager.sendOkResponse.mockReset()
    mockPublish()
  })

  afterEach(() => {
    unmockPublish()
  })

  test('fail - strategy with same name and version exists', async () => {
    queryInterface.doesStrategyExistByNameVersion = jest.fn().mockResolvedValue(true)

    await handler.handle(testApiMessage)

    expect(outMessageManager.sendOkResponse).toHaveBeenCalledWith(testApiMessage.id, {
      isSuccess: false,
      failReason: 'Strategy with same name and version exists'
    })
  })

  test('success', async () => {
    queryInterface.doesStrategyExistByNameVersion = jest.fn().mockResolvedValue(false)

    await handler.handle(testApiMessage)

    expect(outMessageManager.sendOkResponse).toHaveBeenCalledWith(testApiMessage.id, {
      isSuccess: true
    })
    const publishedMessage = publish.mock.calls[0][0]
    expect(publishedMessage).toMatchSnapshot()
  })
})
