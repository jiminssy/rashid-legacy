import _ from 'lodash'
import moment from 'moment-timezone'

const getChronologicalDates = performances => {
  return performances.reduce((sortedDates, performance) => {
    if (!sortedDates.includes(performance.date)) {
      sortedDates.push(performance.date)
    }
    return sortedDates
  }, [])
}

const getCumulativePerformances = (chronologicalDates, performances) => {
  const restructuredPerformances = {} // { [strategyInstsanceId]: { [date]: [outucomeScore] } }
  performances.forEach(performance => {
    if (!(performance.strategyInstanceId in restructuredPerformances)) {
      restructuredPerformances[performance.strategyInstanceId] = {}
    }
    restructuredPerformances[performance.strategyInstanceId][performance.date] = performance.outcomeScore
  })

  const cumulativePerformances = {} // { [strategyInstanceId]: [cumulativeData] }
  for (let i = 0; i < chronologicalDates.length; i++) {
    const date = chronologicalDates[i]
    for (const strategyInstanceId in restructuredPerformances) {
      if (!(strategyInstanceId in cumulativePerformances)) {
        cumulativePerformances[strategyInstanceId] = []
      }

      let previousPerformance = i === 0 ? 0 : cumulativePerformances[strategyInstanceId][i - 1]
      let newPerformance
      if (date in restructuredPerformances[strategyInstanceId]) {
        previousPerformance = previousPerformance || 0
        newPerformance = previousPerformance + restructuredPerformances[strategyInstanceId][date]
      } else {
        newPerformance = i === 0 ? null : previousPerformance
      }
      newPerformance = newPerformance != null ? Number(newPerformance.toFixed(2)) : newPerformance
      cumulativePerformances[strategyInstanceId].push(newPerformance)
    }
  }

  return cumulativePerformances
}

const getTopBottomPerformances = (allPerformances, performanceTotals, numStrategyInstances, isTop) => {
  const totals = Object.entries(performanceTotals) // [ [ strategyInstanceId, totalScore ], ... ]
  if (isTop) {
    totals.sort((a, b) => b[1] - a[1])
  } else {
    totals.sort((a, b) => a[1] - b[1])
  }
  const topStrategyInstances = _.take(totals, numStrategyInstances)
  const topStrategyInstanceIds = new Set(topStrategyInstances.map(instance => instance[0]))
  return allPerformances.filter(performance => topStrategyInstanceIds.has(performance.strategyInstanceId))
}

const toChartDates = utcDates => {
  const browserTimezone = moment.tz.guess()
  return utcDates.map(utcDate => moment(utcDate).tz(browserTimezone).format('MMM DD'))
}

const toChartSeries = (cumulativePerformances, isTop) => {
  const series = []
  for (const strategyInstanceId in cumulativePerformances) {
    series.push({
      name: strategyInstanceId.substring(0, 6),
      data: cumulativePerformances[strategyInstanceId]
    })
  }

  if (isTop) {
    return series.sort((a, b) => b.data[b.data.length - 1] - a.data[a.data.length - 1])
  } else {
    return series.sort((a, b) => a.data[a.data.length - 1] - b.data[b.data.length - 1])
  }
}

export const getChartData = (allPerformances, performanceTotals, numStrategyInstancesInCharts, isTop) => {
  const chartPerformances = getTopBottomPerformances(
    allPerformances, performanceTotals, numStrategyInstancesInCharts, isTop
  )
  chartPerformances.sort((a, b) => a.date.localeCompare(b.date))
  const chronologicalDates = getChronologicalDates(chartPerformances)
  const topCumulativePerformances = getCumulativePerformances(chronologicalDates, chartPerformances)
  const chartDates = toChartDates(chronologicalDates)
  return {
    categories: chartDates,
    series: toChartSeries(topCumulativePerformances, isTop)
  }
}

export const getPerformanceTotals = allPerformances => {
  return allPerformances.reduce((totals, performance) => {
    if (!(performance.strategyInstanceId in totals)) {
      totals[performance.strategyInstanceId] = 0
    }
    totals[performance.strategyInstanceId] += performance.outcomeScore
    return totals
  }, {})
}

export const getStrategyInstancesAndTotals = (allPerformances, chartData) => {
  return chartData.series.map(series => {
    const shortenedStrategyInstanceId = series.name
    const fullStrategyInstanceId = allPerformances
      .find(element => element.strategyInstanceId.startsWith(shortenedStrategyInstanceId))
      .strategyInstanceId

    return {
      strategyInstanceId: fullStrategyInstanceId,
      totalPerformance: _.takeRight(series.data, 1)[0]
    }
  })
}
