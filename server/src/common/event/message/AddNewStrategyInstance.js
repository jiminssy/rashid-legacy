import { EventMessage } from './EventMessage'
import { ADD_NEW_STRATEGY_INSTANCE } from '../event-type'

export class AddNewStrategyInstance extends EventMessage {
  constructor (strategyInstance) {
    super(ADD_NEW_STRATEGY_INSTANCE)
    this.strategyInstance = strategyInstance
  }
}
