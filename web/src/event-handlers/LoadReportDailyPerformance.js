import { EventHandler } from './EventHandler'
import { LOAD_REPORT_DAILY_PERFORMANCE } from '../constants/events'
import { getDailyPerformances } from '../utils/rashid-client'
import { setValue, store } from '../store'

class LoadReportDailyPerformance extends EventHandler {
  constructor () {
    super([LOAD_REPORT_DAILY_PERFORMANCE])
  }

  async handle () {
    const numDays = store.report.dailyPerformance.numDays
    const dailyPerformances = await getDailyPerformances(numDays)

    await setValue('report.dailyPerformance.allPerformances', dailyPerformances)
  }
}

export const eventHandler = new LoadReportDailyPerformance()
