import { EventHandler } from './EventHandler'
import { ADD_NEW_STRATEGY_INSTANCE } from '../constants/events'
import { addNewStrategyInstance } from '../utils/rashid-client'
import { notification } from '../utils/notification'

class AddNewStrategyInstance extends EventHandler {
  constructor () {
    super([ADD_NEW_STRATEGY_INSTANCE])
  }

  async handle (eventMessage) {
    const result = await addNewStrategyInstance(eventMessage)
    if (result.isSuccess) {
      notification.success('New strategy instance added')
      window.rrHistory.push('/admin/strategy')
    } else {
      notification.error(result.failReason)
    }
  }
}

export const eventHandler = new AddNewStrategyInstance()
