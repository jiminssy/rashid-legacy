export class StrategyInstance {
  constructor ({
    id, strategyId, strategyParams, tradingInstrument, dataStreams, isEnabled
  }) {
    this.id = id
    this.strategyId = strategyId
    this.strategyParams = strategyParams
    this.tradingInstrument = tradingInstrument
    this.dataStreams = dataStreams
    this.isEnabled = isEnabled
  }
}
