import moment from 'moment-timezone'
import { getDataClient } from '../../common/fxcm/client-factory'
import { SystemService } from '../../common/definition/SystemService'
import {
  getDbAccountId,
  getLastAccountHistoryRecord,
  insertAccountHistoryRecord,
  updateLastAccountHistoryRecordDayReturn
} from '../utils/database'
import { publish } from '../../common/event/event-bus'
import { LiveAccountHistoryUpdated } from '../../common/event/message/LiveAccountHistoryUpdated'

export const eventListener = async _eventMessage => {
  const brokerName = 'FXCM'
  const dataClient = getDataClient(SystemService.ANALYTICS)
  const account = await dataClient.getTradingAccount()
  const brokerAccountId = account.accountId
  const dbAccountId = await getDbAccountId(brokerName, brokerAccountId)
  const todayStartBalance = account.balance

  const newDay = moment.utc().set({
    hour: 0,
    minute: 0,
    second: 0,
    millisecond: 0
  }).format()

  const {
    date: lastDayDate,
    dayStartBalance,
    intradayDeposit,
    intradayWithdraw
  } = await getLastAccountHistoryRecord(
    dbAccountId,
    newDay
  )

  const lastDayBeforePnL = dayStartBalance + intradayDeposit - intradayWithdraw
  const lastDayPnL = todayStartBalance - lastDayBeforePnL
  const lastDayReturn = lastDayPnL / lastDayBeforePnL
  await updateLastAccountHistoryRecordDayReturn(
    dbAccountId,
    lastDayDate,
    lastDayReturn
  )

  publish(new LiveAccountHistoryUpdated(
    brokerName,
    brokerAccountId,
    lastDayDate,
    dayStartBalance,
    intradayDeposit,
    intradayWithdraw,
    lastDayReturn
  ))

  await insertAccountHistoryRecord(
    dbAccountId,
    newDay,
    todayStartBalance
  )
}
