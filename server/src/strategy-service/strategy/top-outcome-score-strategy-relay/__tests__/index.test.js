import TopOutcomeScoreStrategyRelay from '..'
import { publish } from '../../../../common/event/event-bus'
import { mockPublish, unmockPublish } from '../../../../common/test-mock/mock-event-bus'
import { SimulationPositionOpened } from '../../../../common/event/message/SimulationPositionOpened'
import { TradeDirection } from '../../../../common/definition/TradeDirection'
import { OrderType } from '../../../../common/definition/OrderType'

jest.mock('uuid-js', () => ({
  create: () => ({ hex: 'test-context-id' })
}))

describe('_onSimulationPositionOpened', () => {
  const testStrategyInstance = new TopOutcomeScoreStrategyRelay({
    dataStreams: [],
    id: 'test-strategy-instance-id',
    tradingInstrument: 'FXCM-1-m1'
  })

  beforeEach(() => {
    mockPublish()
  })

  afterEach(() => {
    unmockPublish()
  })

  test('long market order', () => {
    const worstStrategyInstanceId = 'worst-strategy-instance-id'
    testStrategyInstance._targetStrategyInstanceId = worstStrategyInstanceId
    const openedPosition = new SimulationPositionOpened(
      worstStrategyInstanceId,
      'FXCM-1-m1',
      'mock-order-id',
      'mock-position-id',
      TradeDirection.LONG,
      OrderType.MARKET,
      35.2873,
      25.3223,
      67.3342,
      null
    )

    testStrategyInstance._onSimulationPositionOpened(openedPosition)

    const reversedSignal = publish.mock.calls[0][0]
    expect(reversedSignal).toMatchSnapshot()
  })

  test('short market order', () => {
    const worstStrategyInstanceId = 'worst-strategy-instance-id'
    testStrategyInstance._targetStrategyInstanceId = worstStrategyInstanceId
    const openedPosition = new SimulationPositionOpened(
      worstStrategyInstanceId,
      'FXCM-1-m1',
      'mock-order-id',
      'mock-position-id',
      TradeDirection.SHORT,
      OrderType.MARKET,
      550.2354,
      623.12345,
      473.2332,
      null
    )

    testStrategyInstance._onSimulationPositionOpened(openedPosition)

    const reversedSignal = publish.mock.calls[0][0]
    expect(reversedSignal).toMatchSnapshot()
  })

  test('long limit order', () => {
    const worstStrategyInstanceId = 'worst-strategy-instance-id'
    testStrategyInstance._targetStrategyInstanceId = worstStrategyInstanceId
    const openedPosition = new SimulationPositionOpened(
      worstStrategyInstanceId,
      'FXCM-1-m1',
      'mock-order-id',
      'mock-position-id',
      TradeDirection.LONG,
      OrderType.LIMIT,
      35.9753,
      25.3223,
      67.3342,
      35.9753
    )

    testStrategyInstance._onSimulationPositionOpened(openedPosition)

    const reversedSignal = publish.mock.calls[0][0]
    expect(reversedSignal).toMatchSnapshot()
  })

  test('short limit order', () => {
    const worstStrategyInstanceId = 'worst-strategy-instance-id'
    testStrategyInstance._targetStrategyInstanceId = worstStrategyInstanceId
    const openedPosition = new SimulationPositionOpened(
      worstStrategyInstanceId,
      'FXCM-1-m1',
      'mock-order-id',
      'mock-position-id',
      TradeDirection.SHORT,
      OrderType.LIMIT,
      52.8753,
      67.3342,
      25.3223,
      52.8753
    )

    testStrategyInstance._onSimulationPositionOpened(openedPosition)

    const reversedSignal = publish.mock.calls[0][0]
    expect(reversedSignal).toMatchSnapshot()
  })

  test('ignore not interested trade signal', () => {
    const notInterestedId = 'not-interested-id'
    const worstStrategyInstanceId = 'worst-strategy-instance-id'
    testStrategyInstance._targetStrategyInstanceId = worstStrategyInstanceId
    const openedPosition = new SimulationPositionOpened(
      notInterestedId,
      'FXCM-1-m1',
      'mock-order-id',
      'mock-position-id',
      TradeDirection.SHORT,
      OrderType.LIMIT,
      52.8753,
      67.3342,
      25.3223,
      52.8753
    )

    testStrategyInstance._onSimulationPositionOpened(openedPosition)

    expect(publish).not.toBeCalled()
  })
})
