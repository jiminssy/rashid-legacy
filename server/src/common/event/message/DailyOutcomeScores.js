import { EventMessage } from './EventMessage'
import { SIMULATION_DAILY_OUTCOME_SCORES } from '../event-type'

export class DailyOutcomeScores extends EventMessage {
  constructor (date, scores) {
    super(SIMULATION_DAILY_OUTCOME_SCORES)
    this.date = date
    this.scores = scores
  }
}
