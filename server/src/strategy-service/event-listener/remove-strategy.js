import { knex } from '../../common/database/knex'

export const eventListener = async ({ strategyId }) => {
  await knex('strategy_strategy')
    .where({ id: strategyId })
    .del()
}
