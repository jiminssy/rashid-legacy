import moment from 'moment-timezone'
import { BaseStrategy } from '../base-strategy'
import { publish, subscribe, unsubscribe } from '../../../common/event/event-bus'
import { StrategyTradeSignal } from '../../../common/event/message/StrategyTradeSignal'
import { TargetStrategyInstanceUpdated } from '../../../common/event/message/TargetStrategyInstanceUpdated'
import {
  SIMULATION_POSITION_OPENED,
  NEW_FX_TRADING_DAY
} from '../../../common/event/event-type'
import { getBottomOutcomeScoreStrategyInstanceId } from '../../util/database'
import { TradeDirection } from '../../../common/definition/TradeDirection'
import { OrderType } from '../../../common/definition/OrderType'
import { getDataClient } from '../../../common/fxcm/client-factory'
import { SystemService } from '../../../common/definition/SystemService'
import { parseDataStreamId } from '../../../common/utils/data-stream-id-parser'

// strategy parameter
/*
{
  lookBackDays: integer,
  bottomNthStrategyInstance: integer
}
*/
export default class extends BaseStrategy {
  async activate () {
    super.activate()

    this._getTargetStrategyInstance = this._getTargetStrategyInstance.bind(this)
    this._newFxTradingDaySubscription = subscribe(NEW_FX_TRADING_DAY, this._getTargetStrategyInstance)

    this._onSimulationPositionOpened = this._onSimulationPositionOpened.bind(this)
    this._positionOpenedSubscription = subscribe(SIMULATION_POSITION_OPENED, this._onSimulationPositionOpened)

    this._targetStrategyInstanceId = await this._getTargetStrategyInstanceId()
    this._publishTargetStrategyInstanceUpdatedEvent()
  }

  async deactivate () {
    super.deactivate()
    unsubscribe(this._newFxTradingDaySubscription)
    unsubscribe(this._positionOpenedSubscription)
  }

  async _getTargetStrategyInstanceId () {
    const today = moment.utc().set({
      hour: 0,
      minute: 0,
      second: 0,
      millisecond: 0
    })
    const lookBackDays = this.strategyInstanceInfo.strategyParams.lookBackDays
    const bottomNthStrategyInstance = this.strategyInstanceInfo.strategyParams.bottomNthStrategyInstance
    const from = today.subtract(lookBackDays, 'days')
    return await getBottomOutcomeScoreStrategyInstanceId(
      this.strategyInstanceInfo.tradingInstrument,
      from.format(),
      bottomNthStrategyInstance,
      true
    )
  }

  _calcReverseSizeMultiplier (openedPositionOpen, openedPositionStopLoss, openedPositionTakeProfit) {
    // original strategy instance size multiplier is always 1 because it is the original
    // when making reverse trade signal, size multiplier has to be calculated in order
    // to properly reverse the position of the original
    const distanceOpenToStopLoss = Math.abs(openedPositionOpen - openedPositionStopLoss)
    const distanceOpenToTakeProfit = Math.abs(openedPositionOpen - openedPositionTakeProfit)
    const ratio = distanceOpenToTakeProfit / distanceOpenToStopLoss
    return Number(ratio.toFixed(2))
  }

  async _calcReversedStopLossAndTakeProfit (
    brokerInstrumentId,
    originalTradeDirection,
    originalOpen,
    originalStopLoss,
    originalTakeProfit
  ) {
    const dataClient = getDataClient(SystemService.STRATEGY)
    const { pip, spread } = await dataClient.getInstrumentRealtimeInfo(brokerInstrumentId)
    const spreadPips = spread * pip

    const originalSlTpRatio = Math.abs(originalOpen - originalTakeProfit) / Math.abs(originalOpen - originalStopLoss)
    const reversedTakeProfit = originalStopLoss

    if (originalTradeDirection === TradeDirection.LONG) {
      const estimatedEntry = originalOpen - spreadPips
      const estimatedProfitRange = Math.abs(estimatedEntry - reversedTakeProfit)
      const reversedStopLoss = estimatedEntry + (estimatedProfitRange * originalSlTpRatio)
      return {
        reversedStopLoss,
        reversedStopLossRange: Math.abs(estimatedEntry - reversedStopLoss),
        reversedTakeProfit
      }
    } else {
      const estimatedEntry = originalOpen + spreadPips
      const estimatedProfitRange = Math.abs(estimatedEntry - reversedTakeProfit)
      const reversedStopLoss = estimatedEntry - (estimatedProfitRange * originalSlTpRatio)
      return {
        reversedStopLoss,
        reversedStopLossRange: Math.abs(estimatedEntry - reversedStopLoss),
        reversedTakeProfit
      }
    }
  }

  async _getReversedTradeSignal (openedPosition) {
    // even if the opened position was from limit order
    // we simply place market order to get in opposite position right away
    const positionMultiplier = this._calcReverseSizeMultiplier(
      openedPosition.open,
      openedPosition.stopLoss,
      openedPosition.takeProfit
    )
    const { brokerInstrumentId } = parseDataStreamId(this.strategyInstanceInfo.tradingInstrument)
    const {
      reversedStopLoss,
      reversedStopLossRange,
      reversedTakeProfit
    } = await this._calcReversedStopLossAndTakeProfit(
      brokerInstrumentId,
      openedPosition.tradeDirection,
      openedPosition.open,
      openedPosition.stopLoss,
      openedPosition.takeProfit
    )
    return new StrategyTradeSignal(
      this.strategyInstanceInfo.id,
      this.strategyInstanceInfo.tradingInstrument,
      openedPosition.tradeDirection === TradeDirection.LONG ? TradeDirection.SHORT : TradeDirection.LONG,
      OrderType.MARKET,
      reversedStopLossRange,
      reversedStopLoss,
      reversedTakeProfit,
      null,
      positionMultiplier,
      {
        isReactionaryTradeSignal: true,
        isReversedTradeSignal: true,
        sourcePositionId: openedPosition.positionId,
        sourceStrategyInstanceId: openedPosition.strategyInstanceId
      }
    )
  }

  async _onSimulationPositionOpened (openedPosition) {
    if (this._targetStrategyInstanceId == null) {
      return
    }

    if (openedPosition.strategyInstanceId === this._targetStrategyInstanceId) {
      publish(await this._getReversedTradeSignal(openedPosition))
    }
  }

  _publishTargetStrategyInstanceUpdatedEvent () {
    publish(new TargetStrategyInstanceUpdated(
      this.strategyInstanceInfo.id,
      this._targetStrategyInstanceId
    ))
  }

  _getTargetStrategyInstance () {
    (async () => {
      this._targetStrategyInstanceId = await this._getTargetStrategyInstanceId()
      this._publishTargetStrategyInstanceUpdatedEvent()
    })()
  }
}
