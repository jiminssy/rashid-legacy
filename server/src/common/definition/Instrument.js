export class Instrument {
  constructor (assetClass, symbol) {
    this.assetClass = assetClass
    this.symbol = symbol
  }
}
