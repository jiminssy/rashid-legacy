import _ from 'lodash'
import { BaseStrategy } from '../base-strategy'
import { SimpleMovingAverage } from '../../util/indicator/simple-moving-average'
import { TradeDirection } from '../../../common/definition/TradeDirection'

// strategy parameter
/*
{
  fastMaLookBack: integer,
  slowMaLookBack: integer
}
*/
export default class extends BaseStrategy {
  async activate () {
    super.activate()
    this.fastMa = new SimpleMovingAverage(
      this.strategyInstanceInfo.dataStreams[0],
      this.strategyInstanceInfo.strategyParams.fastMaLookBack
    )
    this.slowMa = new SimpleMovingAverage(
      this.strategyInstanceInfo.dataStreams[0],
      this.strategyInstanceInfo.strategyParams.slowMaLookBack
    )

    await Promise.all([
      this.fastMa.initialize(),
      this.slowMa.initialize()
    ])
  }

  async onDataStreamReceived (candleData) {
    this.fastMa.updateWithNewData(candleData)
    this.slowMa.updateWithNewData(candleData)

    const fastMaCurrent = _.nth(this.fastMa.values, -1)
    const fastMaPrevious = _.nth(this.fastMa.values, -2)
    const slowMaCurrent = _.nth(this.slowMa.values, -1)
    const slowMaPrevious = _.nth(this.slowMa.values, -2)

    if (this._isBottomToTopCross(fastMaPrevious, fastMaCurrent, slowMaPrevious, slowMaCurrent)) {
      await this.signalMarketOrderByRatio(
        TradeDirection.LONG,
        candleData.bidClose,
        candleData.askClose
      )
    } else if (this._isTopToBottomCross(fastMaPrevious, fastMaCurrent, slowMaPrevious, slowMaCurrent)) {
      await this.signalMarketOrderByRatio(
        TradeDirection.SHORT,
        candleData.bidClose,
        candleData.askClose
      )
    }
  }

  _isBottomToTopCross (fastMaPrevious, fastMaCurrent, slowMaPrevious, slowMaCurrent) {
    return fastMaPrevious < slowMaPrevious && fastMaCurrent >= slowMaCurrent
  }

  _isTopToBottomCross (fastMaPrevious, fastMaCurrent, slowMaPrevious, slowMaCurrent) {
    return fastMaPrevious > slowMaPrevious && fastMaCurrent <= slowMaCurrent
  }
}
