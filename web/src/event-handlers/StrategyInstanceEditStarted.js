import { EventHandler } from './EventHandler'
import { STRATEGY_INSTANCE_EDIT_STARTED } from '../constants/events'
import { getStrategyInstance } from '../utils/rashid-client'
import { notification } from '../utils/notification'
import { setValue } from '../store'

class StrategyEditStarted extends EventHandler {
  constructor () {
    super([STRATEGY_INSTANCE_EDIT_STARTED])
  }

  async handle (instanceId) {
    const response = await getStrategyInstance(instanceId)
    if (response.isSuccess) {
      await setValue('strategy.instance.edit.strategyInstance', response.strategyInstance)
    } else {
      notification.error('Failed to get data for strategy instance')
    }
  }
}

export const eventHandler = new StrategyEditStarted()
