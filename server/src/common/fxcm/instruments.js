import { FxcmInstrument } from '../../common/definition/FxcmInstrument'
import { AssetClass } from '../../common/definition/AssetClass'

// all FXCM instruments found on https://github.com/fxcm/RestAPI
const instruments = [
  Object.freeze(new FxcmInstrument(AssetClass.FX, 'EUR/USD', '1')),
  Object.freeze(new FxcmInstrument(AssetClass.FX, 'USD/JPY', '2')),
  Object.freeze(new FxcmInstrument(AssetClass.FX, 'GBP/USD', '3')),
  Object.freeze(new FxcmInstrument(AssetClass.FX, 'USD/CHF', '4')),
  Object.freeze(new FxcmInstrument(AssetClass.FX, 'EUR/CHF', '5')),
  Object.freeze(new FxcmInstrument(AssetClass.FX, 'AUD/USD', '6')),
  Object.freeze(new FxcmInstrument(AssetClass.FX, 'USD/CAD', '7')),
  Object.freeze(new FxcmInstrument(AssetClass.FX, 'NZD/USD', '8')),
  Object.freeze(new FxcmInstrument(AssetClass.FX, 'EUR/GBP', '9')),
  Object.freeze(new FxcmInstrument(AssetClass.FX, 'EUR/JPY', '10')),
  Object.freeze(new FxcmInstrument(AssetClass.FX, 'GBP/JPY', '11')),
  Object.freeze(new FxcmInstrument(AssetClass.FX, 'CHF/JPY', '12')),
  Object.freeze(new FxcmInstrument(AssetClass.FX, 'GBP/CHF', '13')),
  Object.freeze(new FxcmInstrument(AssetClass.FX, 'EUR/AUD', '14')),
  Object.freeze(new FxcmInstrument(AssetClass.FX, 'EUR/CAD', '15')),
  Object.freeze(new FxcmInstrument(AssetClass.FX, 'AUD/CAD', '16')),
  Object.freeze(new FxcmInstrument(AssetClass.FX, 'AUD/JPY', '17')),
  Object.freeze(new FxcmInstrument(AssetClass.FX, 'CAD/JPY', '18')),
  Object.freeze(new FxcmInstrument(AssetClass.FX, 'NZD/JPY', '19')),
  Object.freeze(new FxcmInstrument(AssetClass.FX, 'GBP/CAD', '20')),
  Object.freeze(new FxcmInstrument(AssetClass.FX, 'GBP/NZD', '21')),
  Object.freeze(new FxcmInstrument(AssetClass.FX, 'GBP/AUD', '22')),
  Object.freeze(new FxcmInstrument(AssetClass.FX, 'AUD/NZD', '28')),
  Object.freeze(new FxcmInstrument(AssetClass.FX, 'USD/SEK', '30')),
  Object.freeze(new FxcmInstrument(AssetClass.FX, 'EUR/SEK', '32')),
  Object.freeze(new FxcmInstrument(AssetClass.FX, 'EUR/NOK', '36')),
  Object.freeze(new FxcmInstrument(AssetClass.FX, 'USD/NOK', '37')),
  Object.freeze(new FxcmInstrument(AssetClass.FX, 'USD/MXN', '38')),
  Object.freeze(new FxcmInstrument(AssetClass.FX, 'AUD/CHF', '39')),
  Object.freeze(new FxcmInstrument(AssetClass.FX, 'EUR/NZD', '40')),
  Object.freeze(new FxcmInstrument(AssetClass.FX, 'USD/ZAR', '47')),
  Object.freeze(new FxcmInstrument(AssetClass.FX, 'USD/HKD', '50')),
  Object.freeze(new FxcmInstrument(AssetClass.FX, 'ZAR/JPY', '71')),
  Object.freeze(new FxcmInstrument(AssetClass.FX, 'USD/TRY', '83')),
  Object.freeze(new FxcmInstrument(AssetClass.FX, 'EUR/TRY', '87')),
  Object.freeze(new FxcmInstrument(AssetClass.FX, 'NZD/CHF', '89')),
  Object.freeze(new FxcmInstrument(AssetClass.FX, 'CAD/CHF', '90')),
  Object.freeze(new FxcmInstrument(AssetClass.FX, 'NZD/CAD', '91')),
  Object.freeze(new FxcmInstrument(AssetClass.FX, 'TRY/JPY', '98')),
  Object.freeze(new FxcmInstrument(AssetClass.FX, 'USD/ILS', '100')),
  Object.freeze(new FxcmInstrument(AssetClass.FX, 'USD/CNH', '105'))
]
const instrumentsMap = {}
instruments.forEach(instrument => {
  instrumentsMap[instrument.fxcmInstrumentId] = instrument
})

const getServerOnOffTime = dateTime => {
  const serverOnOff = dateTime.clone()
  serverOnOff.hours(16)
  serverOnOff.minutes(45)
  serverOnOff.seconds(0)
  serverOnOff.milliseconds(0)
  return serverOnOff
}

const isServerOn = dateTime => {
  const dateTimeInNewYork = dateTime.clone().tz('America/New_York')
  const dayOfWeek = dateTimeInNewYork.day()
  if (dayOfWeek === 5) {
    // FXCM server shuts down from Friday 4:45pm New York time
    const serverOffTime = getServerOnOffTime(dateTimeInNewYork)
    return dateTimeInNewYork.isBefore(serverOffTime)
  } else if (dayOfWeek === 6) {
    return false
  } else if (dayOfWeek === 0) {
    // FXCM server starts up from Sunday 4:45pm New York time
    const serverOnTime = getServerOnOffTime(dateTimeInNewYork)
    return dateTimeInNewYork.isAfter(serverOnTime)
  }

  return true
}

export const getSymbolByInstrumentId = id => {
  const instrument = instruments.find(i => i.fxcmInstrumentId === id.toString())
  return instrument.symbol
}

export const isTradingHour = (fxcmInstrumentId, dateTime) => {
  if (fxcmInstrumentId in instrumentsMap) {
    if (instrumentsMap[fxcmInstrumentId].assetClass === AssetClass.FX) {
      return isServerOn(dateTime)
    } else {
      throw new Error('Only FX instruments supported for now.')
    }
  } else {
    throw new Error('Not a tradable FXCM instrument.')
  }
}

export const isServerMaintenance = dateTime => {
  // if it's not FX trading hour, it is server maintenance time
  const eurUsdFxcmInstrumentId = '1'
  return !isTradingHour(eurUsdFxcmInstrumentId, dateTime)
}
