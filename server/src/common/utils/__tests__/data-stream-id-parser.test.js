import { parseDataStreamId } from '../data-stream-id-parser'

test('parse works correctly', () => {
  const dataStreamId = 'FXCM-1-m1'
  const result = parseDataStreamId(dataStreamId)
  expect(result).toEqual({
    brokerName: 'FXCM',
    brokerInstrumentId: '1',
    periodId: 'm1'
  })
})
