import { UserRole } from '../../user-management-service/definition/UserRole'
import { getUserRole } from '../../user-management-service'
import { publish } from '../../common/event/event-bus'
import { ExceptionEventMessage } from '../../common/event/message/ExceptionEventMessage'
import { ERROR_GATEWAY_AUTHORIZATION_FAIL } from '../../common/event/event-type'

const authNotNeededMessageTypes = [
  require('../api-message/LogInWithCredsRequest').LogInWithCredsRequest,
  require('../api-message/LogInWithTokenRequest').LogInWithTokenRequest
]

export const isAuthorized = async (userId, message) => {
  const userRole = await getUserRole(userId)
  const authorizedRoles = require(`../api-message/${message.constructor.name}`)[message.constructor.name].authorizedRoles

  if (userRole === UserRole.ADMIN) {
    return true
  } else {
    if (authorizedRoles == null) {
      const error = new Error('Non-Admin user is accessing API that has no authorization defined')
      publish(new ExceptionEventMessage(
        ERROR_GATEWAY_AUTHORIZATION_FAIL,
        error.name,
        error.message,
        error.stack
      ))
      return false
    }
  }

  if (authorizedRoles.includes(userRole)) {
    return true
  }

  return false
}

export const isAuthNeeded = message => {
  for (let i = 0; i < authNotNeededMessageTypes.length; i++) {
    const type = authNotNeededMessageTypes[i]
    if (message instanceof type) {
      return false
    }
  }
  return true
}
