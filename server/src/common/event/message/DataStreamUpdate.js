import { EventMessage } from './EventMessage'
import { DATA_STREAM_UPDATE } from '../event-type'

export class DataStreamUpdate extends EventMessage {
  constructor (
    dataStreamId,
    candleData,
    contextId
  ) {
    const eventType = `${DATA_STREAM_UPDATE}/${dataStreamId}`
    super(eventType, contextId)
    this.candleData = candleData
  }
}
