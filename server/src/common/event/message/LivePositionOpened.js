import { EventMessage } from './EventMessage'
import { LIVE_POSITION_OPENED } from '../event-type'

export class LivePositionOpened extends EventMessage {
  constructor (
    tradingStrategyInstanceId,
    tradingInstrument, // data stream id format
    tradeDirection,
    orderType,
    quantity,
    open,
    stopLoss,
    takeProfit,
    openedTime,
    openLimitPrice = null
  ) {
    super(LIVE_POSITION_OPENED)
    this.tradingStrategyInstanceId = tradingStrategyInstanceId
    this.tradingInstrument = tradingInstrument
    this.tradeDirection = tradeDirection
    this.orderType = orderType
    this.quantity = quantity
    this.open = open
    this.stopLoss = stopLoss
    this.takeProfit = takeProfit
    this.openedTime = openedTime
    this.openLimitPrice = openLimitPrice
  }
}
