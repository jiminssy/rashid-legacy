import { EventHandler } from './EventHandler'
import {
  CHANGE_DAILY_PERFORMANCE_DAYS,
  LOAD_REPORT_DAILY_PERFORMANCE
} from '../constants/events'
import { setValue } from '../store'
import { publish } from '../utils/event-bus'

class ChangeDailyPerformanceDays extends EventHandler {
  constructor () {
    super([CHANGE_DAILY_PERFORMANCE_DAYS])
  }

  async handle (numDays) {
    await setValue('report.dailyPerformance.numDays', numDays)
    publish(LOAD_REPORT_DAILY_PERFORMANCE)
  }
}

export const eventHandler = new ChangeDailyPerformanceDays()
