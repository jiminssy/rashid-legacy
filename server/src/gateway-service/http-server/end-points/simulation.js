import { CloseSimulationPosition } from '../../api-message/CloseSimulationPosition'

const baseUrl = '/api/simulation'

export const register = (registerPost, _registerGet) => {
  // GET

  // POST
  registerPost(`${baseUrl}/close-position`, CloseSimulationPosition)
}
