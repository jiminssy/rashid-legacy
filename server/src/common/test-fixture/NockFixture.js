import path from 'path'
import { back as nockBack } from 'nock'
import { TestFixture } from './TestFixture'

class NockFixture extends TestFixture {
  setUp (testFolderPath) {
    nockBack.fixtures = path.join(testFolderPath, '__nock-records__')
    nockBack.setMode('record')
  }

  cleanUp () {
    nockBack.fixtures = undefined
    nockBack.setMode('dryrun')
  }
}

export const nockFixture = new NockFixture()
