export const STORE_UPDATED = 'store/updated'

export const LOGIN_ATTEMPTED = 'user/loginAttempted'

// strategy
export const ADD_NEW_STRATEGY = 'strategy/addNew'
export const ADD_NEW_STRATEGY_INSTANCE = 'strategy/addNewInstance'
export const EDIT_STRATEGY = 'strategy/edit'
export const EDIT_STRATEGY_INSTANCE = 'strategy/instance/edit'
export const REMOVE_STRATEGY = 'strategy/remove'
export const REMOVE_STRATEGY_INSTANCE = 'strategy/instance/remove'

// report - Daily Performance
export const CHANGE_DAILY_PERFORMANCE_DAYS = 'report/dailyPerformance/changeDays'
export const LOAD_REPORT_DAILY_PERFORMANCE = 'report/dailyPerformance/load'
export const LOAD_REPORT_DP_STRATEGY_INSTANCES = 'report/dailyPerformance/loadStrategyInstances'

// report - Live Account Performance
export const LOAD_REPORT_LIVE_ACCOUNT_PERFORMANCE = 'report/liveAccountPerformance/load'

// page life cycle events
export const ALL_STRATEGIES_LIST_STARTED = 'strategy/allStrategiesListStarted'
export const ALL_STRATEGY_INSTANCES_LIST_STARTED = 'strategy/allStrategyInstancesListStarted'
export const REPORT_DAILY_PERFORMANCE_ENDED = 'report/dailyPerformance/ended'
export const STRATEGY_EDIT_STARTED = 'strategy/editStarted'
export const STRATEGY_EDIT_ENDED = 'strategy/editEnded'
export const STRATEGY_INSTANCE_EDIT_STARTED = 'strategy/instance/editStarted'
export const STRATEGY_INSTANCE_EDIT_ENDED = 'strategy/instance/editEnded'
