
exports.up = function (knex) {
  return knex.schema.table('tradeSignal_liveStrategyInstance', function (table) {
    table.string('openedPositionId').nullable().defaultTo(null)
  })
}

exports.down = function (knex) {
  return knex.schema.table('tradeSignal_liveStrategyInstance', function (table) {
    table.dropColumn('openedPositionId')
  })
}
