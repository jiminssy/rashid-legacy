import SmaCross2 from '..'
import { mockPublish, unmockPublish } from '../../../../common/test-mock/mock-event-bus'
import { publish } from '../../../../common/event/event-bus'
import * as stopLossRangeCalculator from '../../../util/stop-loss-range-calculator'

jest.mock('uuid-js', () => ({
  create: () => ({ hex: 'test-uuid' })
}))

describe('onDataStreamReceived', () => {
  const smaCross2 = new SmaCross2({
    id: 'mock-id',
    tradingInstrument: 'FXCM-1-m1',
    strategyParams: {
      overBoughtLevel: 90,
      overSoldLevel: 10
    }
  })
  smaCross2.fastMa = {
    updateWithNewData: jest.fn()
  }
  smaCross2.slowMa = {
    updateWithNewData: jest.fn()
  }
  smaCross2.rsi = {
    updateWithNewData: jest.fn()
  }
  smaCross2.trendMa = {
    updateWithNewData: jest.fn()
  }

  beforeEach(() => {
    mockPublish()
  })

  afterEach(() => {
    unmockPublish()
  })

  test('buy - bottom to top cross, up trend, over sold', async () => {
    smaCross2.fastMa.values = [10, 30]
    smaCross2.slowMa.values = [15, 25]
    smaCross2.rsi.values = [5]
    smaCross2.trendMa.values = [20]

    stopLossRangeCalculator.calcStopLossRange = jest.fn().mockResolvedValue(1)

    await smaCross2.onDataStreamReceived({
      tradingInstrument: 'FXCM-1-m1',
      bidClose: 35,
      askClose: 40
    })

    expect(publish.mock.calls[0][0]).toMatchSnapshot()
  })

  test('sell - top to bottom cross, down trend, over bought', async () => {
    smaCross2.fastMa.values = [30, 10]
    smaCross2.slowMa.values = [25, 15]
    smaCross2.rsi.values = [95]
    smaCross2.trendMa.values = [50]

    stopLossRangeCalculator.calcStopLossRange = jest.fn().mockResolvedValue(1)

    await smaCross2.onDataStreamReceived({
      tradingInstrument: 'FXCM-1-m1',
      bidClose: 35,
      askClose: 40
    })

    expect(publish.mock.calls[0][0]).toMatchSnapshot()
  })

  test('do nothing - no cross', () => {
    smaCross2.fastMa.values = [30, 30]
    smaCross2.slowMa.values = [20, 20]

    smaCross2.onDataStreamReceived({})

    expect(publish).not.toHaveBeenCalled()
  })

  test('do nothing - bottom to top cross, down trend, over sold', async () => {
    smaCross2.fastMa.values = [10, 30]
    smaCross2.slowMa.values = [15, 25]
    smaCross2.rsi.values = [5]
    smaCross2.trendMa.values = [38]

    stopLossRangeCalculator.calcStopLossRange = jest.fn().mockResolvedValue(1)

    await smaCross2.onDataStreamReceived({
      tradingInstrument: 'FXCM-1-m1',
      bidClose: 35,
      askClose: 40
    })

    expect(publish).not.toHaveBeenCalled()
  })

  test('do nothing - bottom to top cross, up trend, not over sold', async () => {
    smaCross2.fastMa.values = [10, 30]
    smaCross2.slowMa.values = [15, 25]
    smaCross2.rsi.values = [10]
    smaCross2.trendMa.values = [20]

    stopLossRangeCalculator.calcStopLossRange = jest.fn().mockResolvedValue(1)

    await smaCross2.onDataStreamReceived({
      tradingInstrument: 'FXCM-1-m1',
      bidClose: 35,
      askClose: 40
    })

    expect(publish).not.toHaveBeenCalled()
  })

  test('do nothing - top to bottom cross, up trend, over bought', async () => {
    smaCross2.fastMa.values = [30, 10]
    smaCross2.slowMa.values = [25, 15]
    smaCross2.rsi.values = [95]
    smaCross2.trendMa.values = [38]

    stopLossRangeCalculator.calcStopLossRange = jest.fn().mockResolvedValue(1)

    await smaCross2.onDataStreamReceived({
      tradingInstrument: 'FXCM-1-m1',
      bidClose: 35,
      askClose: 40
    })

    expect(publish).not.toHaveBeenCalled()
  })

  test('do nothing - top to bottom cross, down trend, not over bought', async () => {
    smaCross2.fastMa.values = [30, 10]
    smaCross2.slowMa.values = [25, 15]
    smaCross2.rsi.values = [90]
    smaCross2.trendMa.values = [50]

    stopLossRangeCalculator.calcStopLossRange = jest.fn().mockResolvedValue(1)

    await smaCross2.onDataStreamReceived({
      tradingInstrument: 'FXCM-1-m1',
      bidClose: 35,
      askClose: 40
    })

    expect(publish).not.toHaveBeenCalled()
  })
})
