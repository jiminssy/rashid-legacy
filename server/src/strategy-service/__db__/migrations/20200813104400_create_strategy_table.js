
exports.up = function (knex) {
  return knex.schema.createTable('strategy_strategy', function (table) {
    table.increments()
    table.string('codePath').notNullable()
    table.json('paramSchema').notNullable()
  })
}

exports.down = function (knex) {
  return knex.schema.dropTable('strategy_strategy')
}
