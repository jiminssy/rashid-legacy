import { EventHandler } from './EventHandler'
import { EDIT_STRATEGY } from '../constants/events'
import { editStrategy } from '../utils/rashid-client'
import { notification } from '../utils/notification'

class EditStrategy extends EventHandler {
  constructor () {
    super([EDIT_STRATEGY])
  }

  async handle (strategy) {
    await editStrategy(strategy)
    notification.success('Strategy edited')
    window.rrHistory.push('/admin/strategy')
  }
}

export const eventHandler = new EditStrategy()
