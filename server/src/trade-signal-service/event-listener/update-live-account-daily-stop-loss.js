import { insertLiveAccountDailyStopLoss } from '../utils/database'

export const eventListener = async ({
  brokerName,
  brokerAccountId,
  date,
  stopLossOutcomeScore
}) => {
  await insertLiveAccountDailyStopLoss(brokerName, brokerAccountId, date, stopLossOutcomeScore)
}
