import { EventMessage } from './EventMessage'
import { SIMULATION_POSITION_CLOSED } from '../event-type'

export class SimulationPositionClosed extends EventMessage {
  constructor (
    positionId,
    closedTime,
    close,
    outcomeScore
  ) {
    super(SIMULATION_POSITION_CLOSED)
    this.positionId = positionId
    this.closedTime = closedTime
    this.close = close
    this.outcomeScore = outcomeScore
  }
}
