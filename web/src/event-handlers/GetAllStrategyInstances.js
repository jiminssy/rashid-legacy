import { EventHandler } from './EventHandler'
import { ALL_STRATEGY_INSTANCES_LIST_STARTED } from '../constants/events'
import { getAllStrategyInstances } from '../utils/rashid-client'
import { setValue } from '../store'

class GetAllStrategyInstances extends EventHandler {
  constructor () {
    super([ALL_STRATEGY_INSTANCES_LIST_STARTED])
  }

  async handle () {
    const instances = await getAllStrategyInstances()
    await setValue('strategy.allStrategyInstancesList.strategyInstances', instances)
  }
}

export const eventHandler = new GetAllStrategyInstances()
