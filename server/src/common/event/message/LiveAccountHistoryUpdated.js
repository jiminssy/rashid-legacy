import { EventMessage } from './EventMessage'
import { ANALYTICS_LIVE_ACCOUNT_HISTORY_UPDATED } from '../event-type'

export class LiveAccountHistoryUpdated extends EventMessage {
  constructor (
    brokerName,
    brokerAccountId,
    date,
    dayStartBalance,
    intradayDeposit,
    intradayWithdraw,
    dayReturn
  ) {
    super(ANALYTICS_LIVE_ACCOUNT_HISTORY_UPDATED)
    this.brokerName = brokerName
    this.brokerAccountId = brokerAccountId
    this.date = date
    this.dayStartBalance = dayStartBalance
    this.intradayDeposit = intradayDeposit
    this.intradayWithdraw = intradayWithdraw
    this.dayReturn = dayReturn
  }
}
