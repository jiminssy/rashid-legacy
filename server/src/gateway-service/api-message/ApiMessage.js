import UUID from 'uuid-js'

export const authorizedRoles = roles => {
  return targetClass => {
    targetClass.authorizedRoles = roles
  }
}

export class ApiMessage {
  constructor (id) {
    this.id = id || UUID.create().hex
  }
}
