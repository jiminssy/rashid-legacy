import { start as startHttpServer } from './http-server'

export const start = () => {
  startHttpServer()
}
