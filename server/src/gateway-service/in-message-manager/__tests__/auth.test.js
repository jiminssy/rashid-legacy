import { isAuthorized, isAuthNeeded } from '../auth'
import { LogInWithCredsRequest } from '../../api-message/LogInWithCredsRequest'
import { mockPublish, unmockPublish } from '../../../common/test-mock/mock-event-bus'
import * as userManagementService from '../../../user-management-service'
import { UserRole } from '../../../user-management-service/definition/UserRole'
import { GetAllStrategiesRequest } from '../../api-message/GetAllStrategiesRequest'
import { ExceptionEventMessage } from '../../../common/event/message/ExceptionEventMessage'
import { ERROR_GATEWAY_AUTHORIZATION_FAIL } from '../../../common/event/event-type'
import { publish } from '../../../common/event/event-bus'

jest.mock('uuid-js', () => ({
  create: () => ({ hex: 'test-context-id' })
}))

describe('isAuthorized', () => {
  beforeEach(() => {
    mockPublish()
  })

  afterEach(() => {
    unmockPublish()
  })

  test('admin user on authorization not specified API', async () => {
    userManagementService.getUserRole = jest.fn().mockResolvedValue(UserRole.ADMIN)
    const testMessage = new LogInWithCredsRequest({ userName: 'user', password: '1234' })

    const result = await isAuthorized(1, testMessage)

    expect(result).toBe(true)
  })

  test('read-only user on read-only API', async () => {
    userManagementService.getUserRole = jest.fn().mockResolvedValue(UserRole.READ_ONLY)
    const testMessage = new GetAllStrategiesRequest()

    const result = await isAuthorized(1, testMessage)

    expect(result).toBe(true)
  })

  test('read-only user on authorization not specified API', async () => {
    userManagementService.getUserRole = jest.fn().mockResolvedValue(UserRole.READ_ONLY)
    const testMessage = new LogInWithCredsRequest({ userName: 'user', password: '1234' })

    const result = await isAuthorized(1, testMessage)

    const expectedErrorEventMessage = new ExceptionEventMessage(
      ERROR_GATEWAY_AUTHORIZATION_FAIL,
      expect.anything(),
      'Non-Admin user is accessing API that has no authorization defined',
      expect.anything()
    )

    expect(publish).toHaveBeenCalledWith(expectedErrorEventMessage)
    expect(result).toBe(false)
  })
})

describe('isAuthNeeded', () => {
  test('check if auth needed for message that does not need', () => {
    const message = new LogInWithCredsRequest('testId', 'testPassword')
    const isNeeded = isAuthNeeded(message)
    expect(isNeeded).toBe(false)
  })

  test('check if auth needed for message that is needed', () => {
    const message = {}
    const isNeeded = isAuthNeeded(message)
    expect(isNeeded).toBe(true)
  })
})
