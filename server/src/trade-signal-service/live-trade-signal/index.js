import { LiveTradeSignal } from '../../common/event/message/LiveTradeSignal'
import { SystemService } from '../../common/definition/SystemService'
import { getDataClient } from '../../common/fxcm/client-factory'
import { parseDataStreamId } from '../../common/utils/data-stream-id-parser'
import { CAPITAL_RISK_PER_TRADE_PERCENT } from '../../settings'

export const convertToLiveTradeSignal = async (strategyTradeSignal, riskResult) => {
  const dataClient = getDataClient(SystemService.TRADE_SIGNAL)
  const account = await dataClient.getTradingAccount()
  const cashBalance = account.balance
  const { brokerInstrumentId } = parseDataStreamId(strategyTradeSignal.tradingInstrument)
  const { pip, pipCost } = await dataClient.getInstrumentRealtimeInfo(brokerInstrumentId)
  const numPipsInStopLoss = strategyTradeSignal.stopLossRange / pip
  const totalCostPerLot = numPipsInStopLoss * pipCost
  const tradableBalance = cashBalance * CAPITAL_RISK_PER_TRADE_PERCENT
  const numLotsToTrade = Math.floor((tradableBalance / totalCostPerLot) * riskResult.positionSizeMultiplier)
  return new LiveTradeSignal(
    strategyTradeSignal.strategyInstanceId,
    strategyTradeSignal.tradingInstrument,
    strategyTradeSignal.tradeDirection,
    strategyTradeSignal.orderType,
    numLotsToTrade,
    strategyTradeSignal.stopLoss,
    strategyTradeSignal.takeProfit,
    strategyTradeSignal.limitPrice
  )
}
