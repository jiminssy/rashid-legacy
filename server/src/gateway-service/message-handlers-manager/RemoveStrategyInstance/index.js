import { MessageHandler } from '../MessageHandler'
import { outMessageManager } from '../../out-message-manager'
import { doesStrategyInstanceExist } from '../../../view-query-service/query-interface/strategy'
import { publish } from '../../../common/event/event-bus'
import { RemoveStrategyInstance as RemoveStrategyInstanceMessage } from '../../../common/event/message/RemoveStrategyInstance'

class RemoveStrategyInstance extends MessageHandler {
  async handle (message) {
    if (!(await doesStrategyInstanceExist(message.strategyInstanceId))) {
      outMessageManager.sendOkResponse(message.id, {
        isSuccess: false,
        failReason: 'Strategy instance does not exist'
      })
      return
    }

    publish(new RemoveStrategyInstanceMessage(message.strategyInstanceId))
    outMessageManager.sendOkResponse(message.id, { isSuccess: true })
  }
}

export const handler = new RemoveStrategyInstance()
