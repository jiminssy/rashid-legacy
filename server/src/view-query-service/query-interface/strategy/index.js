import { knex } from '../../../common/database/knex'
import { Strategy } from '../../../common/definition/Strategy'
import { DbTable } from '../../definition/DbTable'

const strategy = DbTable.STRATEGY
const strategyInstance = DbTable.STRATEGY_INSTANCE

export const doesStrategyInstanceExist = async strategyInstanceId => {
  const result = await knex(strategyInstance).select()
    .where({ id: strategyInstanceId })
  return result.length > 0
}

export const doesStrategyExistById = async strategyId => {
  const result = await knex(strategy).select()
    .where({ id: strategyId })
  return result.length > 0
}

export const doesStrategyExistByNameVersion = async (name, version) => {
  const result = await knex(strategy).select()
    .where({ name, version })
  return result.length > 0
}

export const doesStrategyHaveInstances = async strategyId => {
  const result = await knex(strategyInstance).select()
    .where({ strategyId })
    .limit(1)
  return result.length > 0
}

export const getAllStrategies = async () => {
  const rawResults = await knex(strategy).select().orderBy('name')
  return rawResults.map(raw => new Strategy(raw))
}

export const getAllStrategyInstances = async () => {
  return await knex(strategyInstance).select(
    `${strategyInstance}.id as id`,
    `${strategy}.id as strategyId`,
    `${strategy}.name as strategyName`,
    `${strategy}.version as strategyVersion`,
    `${strategyInstance}.tradingInstrument as tradingInstrument`,
    `${strategyInstance}.strategyParams as strategyParams`,
    `${strategyInstance}.dataStreams as dataStreams`,
    `${strategyInstance}.isEnabled as isEnabled`
  ).innerJoin(strategy, `${strategy}.id`, `${strategyInstance}.strategyId`)
}

export const getStrategy = async strategyId => {
  const rawResult = await knex
    .select()
    .from(strategy)
    .where({ id: strategyId })
    .first()
  if (rawResult) {
    return new Strategy(rawResult)
  } else {
    return null
  }
}

export const getStrategyInstance = async instanceId => {
  return await knex(strategyInstance)
    .select(
      `${strategy}.*`,
      `${strategyInstance}.*`,
      `${strategyInstance}.id as strategyInstanceId`
    )
    .innerJoin(strategy, `${strategy}.id`, `${strategyInstance}.strategyId`)
    .where({ [`${strategyInstance}.id`]: instanceId })
    .first()
}

export const getStrategyParamSchema = async strategyId => {
  const result = await knex(strategy).select('paramSchema')
    .where({ id: strategyId }).first()
  return result.paramSchema
}
