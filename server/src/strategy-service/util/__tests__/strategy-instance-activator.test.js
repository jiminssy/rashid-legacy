import { getMockKnex } from '../../../common/test-mock/mock-knex'
import { activateEnabledStrategyInstances } from '../strategy-instance-activator'

const knex = getMockKnex()

const mockedActivate = jest.fn()
export default class {
  activate () {
    mockedActivate()
  }
}

test('activateEnabledStrategyInstances works correctly', async () => {
  const strategyInstances = [
    {
      id: 'test-instance-id-1',
      strategyId: 1,
      strategyParams: '{ "param1": "value1" }',
      instrumentId: 1,
      dataStreams: '["FXCM-1-m1"]',
      isEnabled: false,
      codePath: '../util/__tests__/strategy-instance-activator.test.js'
    },
    {
      id: 'test-instance-id-2',
      strategyId: 2,
      strategyParams: '{ "param2": "value2" }',
      instrumentId: 2,
      dataStreams: '["FXCM-2-m1"]',
      isEnabled: true,
      codePath: '../util/__tests__/strategy-instance-activator.test.js'
    },
    {
      id: 'test-instance-id-3',
      strategyId: 3,
      strategyParams: '{ "param3": "value3" }',
      instrumentId: 3,
      dataStreams: '["FXCM-3-m1"]',
      isEnabled: true,
      codePath: '../util/__tests__/strategy-instance-activator.test.js'
    },
    {
      id: 'test-instance-id-4',
      strategyId: 4,
      strategyParams: '{ "param4": "value4" }',
      instrumentId: 4,
      dataStreams: '["FXCM-4-m1"]',
      isEnabled: false,
      codePath: '../util/__tests__/strategy-instance-activator.test.js'
    }
  ]

  knex._setResults([strategyInstances])

  await activateEnabledStrategyInstances()

  expect(mockedActivate).toHaveBeenCalledTimes(2)
})
