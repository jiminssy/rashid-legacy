import _ from 'lodash'
import { SMA } from 'technicalindicators'
import { Indicator } from '.'

export class SimpleMovingAverage extends Indicator {
  _computeSingleValue (data) {
    return SMA.calculate({
      period: data.length,
      values: data
    })[0]
  }

  _toIndicatorData (candleData) {
    return _.mean([candleData.bidClose, candleData.askClose])
  }
}
