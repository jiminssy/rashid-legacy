import { getStrategyParamSchema } from '../../view-query-service/query-interface/strategy'
import Ajv from 'ajv'

export async function validateStrategyParams (strategyId, strategyParams) {
  const ajv = new Ajv()
  const paramSchema = await getStrategyParamSchema(strategyId)
  const validate = ajv.compile(paramSchema)
  return validate(JSON.parse(strategyParams))
}
