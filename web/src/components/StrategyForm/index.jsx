import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { notification } from '../../utils/notification'
import { publish } from '../../utils/event-bus'
import { ADD_NEW_STRATEGY, EDIT_STRATEGY } from '../../constants/events'
import { isValidJson } from '../../utils/json-validator';

export const StrategyForm = ({ isEdit, strategy }) => {
  const [name, setName] = useState(strategy ? strategy.name : '')
  const [version, setVersion] = useState(strategy ? strategy.version : 1)
  const [description, setDescription] = useState(strategy ? strategy.description : '')
  const [codePath, setCodePath] = useState(strategy ? strategy.codePath : '')
  const [paramSchema, setParamSchema] = useState(
    strategy ? JSON.stringify(strategy.paramSchema, null, '  ') : ''
  )

  const parameterSchemaValidationErrorMessage = 'Parameter JSON Schema has invalid JSON'

  const onAddClick = event => {
    event.preventDefault()
    if (name === ''
      || version === ''
      || description === ''
      || paramSchema === '') {
        notification.error('All fields must be filled')
        return
    }

    if(!isValidJson(paramSchema)){
      notification.error(parameterSchemaValidationErrorMessage)
      return
    }

    publish(ADD_NEW_STRATEGY, {
      name,
      version,
      description,
      codePath,
      paramSchema
    })
  }

  const onEditClick = event => {
    event.preventDefault()
    if (name === ''
      || version === ''
      || description === ''
      || paramSchema === '') {
        notification.error('All fields must be filled')
        return
    }

    if(!isValidJson(paramSchema)){
      notification.error(parameterSchemaValidationErrorMessage)
      return
    }

    publish(EDIT_STRATEGY, {
      strategyId: strategy.id,
      name,
      version,
      description,
      codePath,
      paramSchema
    })
  }

  return (
    <div>
      <div className="form-group">
        <label htmlFor="name-input">Name</label>
        <input type="text" className="form-control" id="name-input"
          value={name} onChange={e => setName(e.target.value)}
        />
      </div>
      <div className="form-group">
        <label htmlFor="version-input">Version</label>
        <input type="number" className="form-control" id="version-input"
          aria-describedby="version-help" value={version}
          onChange={e => setVersion(e.target.value)}
        />
        <small id="version-help" className="form-text text-muted">Only whole number</small>
      </div>
      <div className="form-group">
        <label htmlFor="description-input">Description</label>
        <textarea className="form-control" id="description-input" rows="3"
          value={description} onChange={e => setDescription(e.target.value)}
        />
      </div>
      <div className="form-group">
        <label htmlFor="src-input">Source Code Path</label>
        <input type="text" className="form-control" id="src-input" aria-describedby="src-help"
          value={codePath} onChange={e => setCodePath(e.target.value)}
        />
        <small id="src-help" className="form-text text-muted">Path to the code entry point in code base</small>
      </div>
      <div className="form-group">
        <label htmlFor="param-schema-input">Parameter JSON Schema</label>
        <textarea className="form-control" id="param-schema-input" rows="3" aria-describedby="param-schema-help"
          value={paramSchema} onChange={e => setParamSchema(e.target.value)}
        />
        <small id="param-schema-help" className="form-text text-muted">
          Schema of the parameters when the strategy goes live
        </small>
      </div>
      {isEdit ?
        <button type="submit" className="btn btn-primary" onClick={onEditClick}>Save Edit</button> :
        <button type="submit" className="btn btn-primary" onClick={onAddClick}>Add New Strategy</button>        
      }
    </div>
  )
}

StrategyForm.propTypes = {
  isEdit: PropTypes.bool,
  strategy: PropTypes.object
}
