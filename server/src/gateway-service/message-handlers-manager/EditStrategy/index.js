import { MessageHandler } from '../MessageHandler'
import { outMessageManager } from '../../out-message-manager'
import { publish } from '../../../common/event/event-bus'
import { doesStrategyExistById } from '../../../view-query-service/query-interface/strategy'
import { EditStrategy as EditStrategyMessage } from '../../../common/event/message/EditStrategy'
import { Strategy } from '../../../common/definition/Strategy'

class EditStrategy extends MessageHandler {
  async handle (message) {
    if (!(await doesStrategyExistById(message.strategyId))) {
      outMessageManager.sendOkResponse(message.id, {
        isSuccess: false,
        failReason: `Strategy with ID ${message.strategyId} does not exist`
      })
      return
    }

    publish(new EditStrategyMessage(
      new Strategy({
        ...message,
        id: message.strategyId
      })
    ))
    outMessageManager.sendOkResponse(message.id, { isSuccess: true })
  }
}

export const handler = new EditStrategy()
