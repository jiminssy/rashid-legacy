export const ApiListenableTable = Object.freeze({
  ACCOUNT: 'Account',
  CLOSED_POSITION: 'ClosedPosition',
  OFFER: 'Offer',
  OPEN_POSITION: 'OpenPosition',
  ORDER: 'Order',
  PROPERTIES: 'Properties',
  SUMMARY: 'Summary'
})
