
exports.up = function (knex) {
  return knex.schema.createTable('strategy_broker', function (table) {
    table.increments()
    table.string('name').notNullable()
  })
    .createTable('strategy_assetClass', function (table) {
      table.increments()
      table.string('name').notNullable()
    })
    .createTable('strategy_instrument', function (table) {
      table.increments()
      table.integer('assetClassId').notNullable().references('id')
        .inTable('strategy_assetClass')
      table.integer('brokerId').notNullable().references('id')
        .inTable('strategy_broker')
      table.string('brokerInstrumentId').notNullable()
      table.string('name').notNullable()
      table.unique(['brokerId', 'brokerInstrumentId'])
    })
    .createTable('strategy_strategyInstance', function (table) {
      table.string('id').primary()
      table.integer('strategyId').notNullable().references('id')
        .inTable('strategy_strategy')
      table.json('strategyParams').notNullable()
      table.string('tradingInstrument').notNullable()
      table.json('dataStreams').notNullable()
      table.boolean('isEnabled').notNullable()
    })
}

exports.down = function (knex) {
  return knex.schema.dropTable('strategy_strategyInstance')
    .dropTable('strategy_instrument')
    .dropTable('strategy_assetClass')
    .dropTable('strategy_broker')
}
