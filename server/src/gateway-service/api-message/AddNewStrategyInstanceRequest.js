import { ApiMessage } from './ApiMessage'

export class AddNewStrategyInstanceRequest extends ApiMessage {
  constructor ({
    strategyId,
    strategyParams,
    tradingInstrument, // data stream id
    dataStreams, // array of data stream ids
    isEnabled
  }) {
    super()
    this.strategyId = strategyId
    this.strategyParams = strategyParams
    this.tradingInstrument = tradingInstrument
    this.dataStreams = dataStreams
    this.isEnabled = isEnabled
  }
}
