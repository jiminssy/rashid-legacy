import _ from 'lodash'
import { knex } from '../../../common/database/knex'
import { FXCM_LIVE_ACCOUNT_ID } from '../../../settings'
import { DbTable } from '../../definition/DbTable'

const MAX_NUM_DATA_POINTS = 20

const broker = DbTable.BROKER
const liveAccount = DbTable.LIVE_ACCOUNT
const liveAccountHistory = DbTable.LIVE_ACCOUNT_HISTORY

const getLiveAccountHistory = async (brokerName, brokerAccountId, fromDate, toDate) => {
  const results = await knex(liveAccountHistory)
    .select(
      `${liveAccountHistory}.date as date`,
      `${liveAccountHistory}.dayStartBalance as dateStartBalance`,
      `${liveAccountHistory}.intradayDeposit as intradayDeposit`,
      `${liveAccountHistory}.intradayWithdraw as intradayWithdraw`,
      `${liveAccountHistory}.dayReturn as dayReturn`
    )
    .innerJoin(liveAccount, `${liveAccount}.id`, `${liveAccountHistory}.accountId`)
    .innerJoin(broker, `${broker}.id`, `${liveAccount}.brokerId`)
    .where(`${broker}.name`, brokerName)
    .where(`${liveAccount}.brokerAccountId`, brokerAccountId)
    .whereBetween(`${liveAccountHistory}.date`, [fromDate, toDate])
    .orderBy(`${liveAccountHistory}.date`, 'asc')

  return results.map(result => ({
    date: result.date,
    dateStartBalance: result.dateStartBalance,
    intradayDeposit: result.intradayDeposit,
    intradayWithdraw: result.intradayWithdraw,
    dayReturn: result.dayReturn
  }))
}

const reduceDataPoints = (originalData, maxDataPoints) => {
  const reducer = (accumulator, currentValue) => {
    return {
      date: accumulator.date,
      dateStartBalance: accumulator.dateStartBalance,
      intradayDeposit: accumulator.intradayDeposit + currentValue.intradayDeposit,
      intradayWithdraw: accumulator.intradayWithdraw + currentValue.intradayWithdraw,
      dayReturn: (1 + accumulator.dayReturn) * (1 + currentValue.dayReturn) - 1
    }
  }

  const chunkSize = originalData.length % maxDataPoints === 0
    ? originalData.length / maxDataPoints
    : Math.floor(originalData.length / (maxDataPoints - 1))
  const chunks = _.chunk(originalData, chunkSize)
  const reduced = chunks.map(chunkDataPoints => {
    return chunkDataPoints.reduce(reducer, {
      date: chunkDataPoints[0].date,
      dateStartBalance: chunkDataPoints[0].dateStartBalance,
      intradayDeposit: 0,
      intradayWithdraw: 0,
      dayReturn: 0
    })
  })
  return reduced
}

const toAccumulatedReturns = liveAccountHistory => {
  let accumulatedReturn = 0
  return liveAccountHistory.map(record => {
    accumulatedReturn = (1 + accumulatedReturn) * (1 + record.dayReturn) - 1
    return {
      date: record.date,
      accumulatedReturn: accumulatedReturn
    }
  })
}

const toDiscreteReturns = liveAccountHistory => {
  return liveAccountHistory.map(record => ({
    date: record.date,
    discreteReturn: record.dayReturn
  }))
}

export const getLiveAccountPerformanceReportData = async (fromDate, toDate) => {
  let liveAccountHistory = await getLiveAccountHistory('FXCM', FXCM_LIVE_ACCOUNT_ID, fromDate, toDate)
  if (liveAccountHistory.length > MAX_NUM_DATA_POINTS) {
    liveAccountHistory = reduceDataPoints(liveAccountHistory, MAX_NUM_DATA_POINTS)
  }
  const accumulatedReturns = toAccumulatedReturns(liveAccountHistory)
  const discreteReturns = toDiscreteReturns(liveAccountHistory)

  return {
    accumulatedReturns,
    discreteReturns
  }
}

export const __test__ = {
  reduceDataPoints
}
