import { handler } from '..'
import * as queryInterface from '../../../../view-query-service/query-interface/strategy'
import { outMessageManager } from '../../../out-message-manager'
import { GetStrategyRequest } from '../../../api-message/GetStrategyRequest'
import { Strategy } from '../../../../common/definition/Strategy'

outMessageManager.sendOkResponse = jest.fn()

const testApiMessage = new GetStrategyRequest({
  strategyId: 1
})

test('strategy does not exist', async () => {
  queryInterface.getStrategy = jest.fn().mockResolvedValue(null)

  await handler.handle(testApiMessage)

  expect(outMessageManager.sendOkResponse)
    .toHaveBeenCalledWith(testApiMessage.id, {
      isSuccess: false
    })
})

test('success', async () => {
  const testStrategy = new Strategy({
    id: 1,
    name: 'Test Strategy',
    version: 1,
    description: 'Test description.',
    codePath: 'test/code/path',
    paramSchema: '{"test":"schema"}'
  })

  queryInterface.getStrategy = jest.fn().mockResolvedValue(testStrategy)

  await handler.handle(testApiMessage)

  expect(outMessageManager.sendOkResponse)
    .toHaveBeenCalledWith(testApiMessage.id, {
      isSuccess: true,
      strategy: testStrategy
    })
})
