import { insertOrUpdateStrategyInstanceDailyStopLoss } from '../utils/database'

export const eventListener = async ({
  strategyInstanceId,
  date,
  stopLossOutcomeScore,
  reversedStopLossOutcomeScore
}) => {
  await insertOrUpdateStrategyInstanceDailyStopLoss(
    strategyInstanceId,
    date,
    stopLossOutcomeScore,
    reversedStopLossOutcomeScore
  )
}
