import { getCoupledLivePosition } from '../utils/database'
import { getOrderClient } from '../../common/fxcm/client-factory'
import { SystemService } from '../../common/definition/SystemService'

const CHECK_LIVE_POSITION_TIME = 1000 * 10

export const eventListener = async closedSimulationPosition => {
  await new Promise(resolve => setTimeout(() => resolve(), CHECK_LIVE_POSITION_TIME))

  const { positionId } = closedSimulationPosition
  const livePosition = await getCoupledLivePosition(positionId)
  if (livePosition != null) {
    const orderClient = getOrderClient(SystemService.TRADE_SIGNAL)
    await orderClient.closeTradeMarket(livePosition.id, livePosition.size)
  }
}
