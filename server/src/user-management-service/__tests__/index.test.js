import { getMockKnex } from '../../common/test-mock/mock-knex'
import { logIn, verifyAccessToken } from '../index'
import * as settings from '../../settings'
import { mockDateNow, unmockDateNow } from '../../common/test-mock/mock-date-now'

const knex = getMockKnex()

settings.JWT_SECRET = 'Rashid-Dev-Secret'

describe('logIn', () => {
  beforeAll(() => {
    mockDateNow()
  })

  afterAll(() => {
    unmockDateNow()
  })

  test('user exists', async () => {
    knex._setResults([
      {
        id: 12,
        email: 'test@test.com',
        password: 'pass123',
        firstName: 'John',
        lastName: 'Doe'
      }
    ])

    const token = await logIn('test@test.com', 'pass123')

    expect(token).toBe('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjEyLCJpYXQiOjE1MzA1MTgyMDcsImV4cCI6MzE1NzI1ODk2MDYwN30.RNk4ExR6sI9H4495OBBXleL5hLoFolviKbiCWDNgXuQ')
  })

  test('user does not exist', async () => {
    knex._setResults([null])

    const token = await logIn('test@test.com', 'pass123')

    expect(token).toBe(null)
  })
})

describe('verifyAccessToken', () => {
  test('valid access token', () => {
    const accessToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjEyLCJpYXQiOjE1MzA1MTgyMDcsImV4cCI6MzE1NzI1ODk2MDYwN30.RNk4ExR6sI9H4495OBBXleL5hLoFolviKbiCWDNgXuQ'
    expect(verifyAccessToken(accessToken)).toBe(true)
  })

  test('invalid access token', () => {
    const accessToken = 'some-invalid-token'
    expect(verifyAccessToken(accessToken)).toBe(false)
  })
})
