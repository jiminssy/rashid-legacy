import { authorizedRoles, ApiMessage } from './ApiMessage'
import { UserRole } from '../../user-management-service/definition/UserRole'

@authorizedRoles([UserRole.READ_ONLY])
class GetDailyPerformancesRequest extends ApiMessage {
  constructor ({
    numDays
  }) {
    super()
    this.numDays = numDays
  }
}

export { GetDailyPerformancesRequest }
