import { knex } from '../../common/database/knex'
import { DbTable } from '../definition/DbTable'

export const eventListener = async ({ strategyId }) => {
  await knex(DbTable.STRATEGY)
    .where({ id: strategyId })
    .del()
}
