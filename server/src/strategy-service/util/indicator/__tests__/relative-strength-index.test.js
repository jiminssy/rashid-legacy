import { RelativeStrengthIndex } from '../relative-strength-index'

const rsi = new RelativeStrengthIndex()

test('_computeSingleValue max RSI value', () => {
  const result = rsi._computeSingleValue([1, 2, 3, 4, 5, 6, 7, 8])
  expect(result).toBe(100)
})

test('_computeSingleValue min RSI value', () => {
  const result = rsi._computeSingleValue([8, 7, 6, 5, 4, 3, 2, 1])
  expect(result).toBe(0)
})

test('_computeSingleValue mixed value', () => {
  const result = rsi._computeSingleValue([5, 7, 2, 9, 4, 1, 5, 7, 9])
  expect(result).toBe(56.67)
})

test('_toIndicatorData', () => {
  const result = rsi._toIndicatorData({
    bidClose: 2,
    askClose: 6
  })
  expect(result).toBe(4)
})
