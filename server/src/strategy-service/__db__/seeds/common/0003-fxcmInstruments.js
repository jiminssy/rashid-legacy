exports.seed = function (knex) {
  return knex('strategy_instrument').insert([
    {
      assetClassId: 1,
      brokerId: 1,
      brokerInstrumentId: '1',
      name: 'EUR/USD'
    },
    {
      assetClassId: 1,
      brokerId: 1,
      brokerInstrumentId: '2',
      name: 'USD/JPY'
    }
  ])
}
