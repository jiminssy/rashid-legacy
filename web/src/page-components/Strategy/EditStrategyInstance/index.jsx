import React, { useEffect } from 'react'
import { useParams } from 'react-router-dom'
import styles from './index.module.scss'
import { StrategyInstanceForm } from '../../../components/StrategyInstanceForm'
import { useSelector } from '../../../hooks/selector'
import {
  STRATEGY_INSTANCE_EDIT_STARTED,
  STRATEGY_INSTANCE_EDIT_ENDED
} from '../../../constants/events'
import { publish } from '../../../utils/event-bus'

export const EditStrategyInstance = () => {
  const { strategyInstanceId: instanceId } = useParams()
  const instance = useSelector('strategy.instance.edit.strategyInstance')

  useEffect(() => {
    publish(STRATEGY_INSTANCE_EDIT_STARTED, instanceId)
    return () => {
      publish(STRATEGY_INSTANCE_EDIT_ENDED)
    }
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <div>
      <h4 className="page-title">Edit Strategy Instance</h4>
      <div className={`card ${styles.formContainer}`}>
        <div className="card-body">
          {instance && <StrategyInstanceForm isEdit instance={instance} />}
        </div>
      </div>
    </div>
  )
}
