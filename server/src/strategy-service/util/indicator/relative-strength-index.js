import _ from 'lodash'
import { RSI } from 'technicalindicators'
import { Indicator } from '.'

export class RelativeStrengthIndex extends Indicator {
  constructor (dataStreamId, numLookBack) {
    super(dataStreamId, numLookBack)
    // RSI requires to look back one more extra data for its calculations
    this.numLookBack += 1
  }

  _computeSingleValue (data) {
    return RSI.calculate({
      period: data.length - 1,
      values: data
    })[0]
  }

  _toIndicatorData (candleData) {
    return _.mean([candleData.bidClose, candleData.askClose])
  }
}
