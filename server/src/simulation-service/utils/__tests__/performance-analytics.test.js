import moment from 'moment-timezone'
import { getDailyOutcomeScores } from '../performance-analytics'
import * as database from '../database'

describe('getDailyOutcomeScores', () => {
  test('simple - 1 position 1 strategy', async () => {
    const testDate = moment.utc('2020-10-28T12:12:12Z')
    database.getClosedPositions = jest.fn().mockResolvedValue([
      {
        strategyInstanceId: '1111',
        outcomeScore: 1
      }
    ])

    const scores = await getDailyOutcomeScores(testDate)

    const expectedScores = {
      1111: 1
    }
    expect(scores).toEqual(expectedScores)

    const startTimeParam = database.getClosedPositions.mock.calls[0][0]
    expect(startTimeParam).toBe('2020-10-28T00:00:00Z')

    const endTimeParam = database.getClosedPositions.mock.calls[0][1]
    expect(endTimeParam).toBe('2020-10-28T23:59:59Z')
  })

  test('aggregation works correctly', async () => {
    const testDate = moment.utc('2020-10-28T12:12:12Z')
    database.getClosedPositions = jest.fn().mockResolvedValue([
      {
        strategyInstanceId: '1111',
        outcomeScore: 1 // strings because database query result gies you that
      },
      {
        strategyInstanceId: '2222',
        outcomeScore: -1
      },
      {
        strategyInstanceId: '3333',
        outcomeScore: 2
      },
      {
        strategyInstanceId: '1111',
        outcomeScore: 3
      },
      {
        strategyInstanceId: '2222',
        outcomeScore: -1
      }
    ])

    const scores = await getDailyOutcomeScores(testDate)

    const expectedScores = {
      1111: 4,
      2222: -2,
      3333: 2
    }
    expect(scores).toEqual(expectedScores)
  })
})
