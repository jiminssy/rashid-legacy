
exports.up = function (knex) {
  return knex.schema.createTable('analytics_liveAccountDailyOutcomeScore', function (table) {
    table.datetime('date').primary()
    table.decimal('outcomeScore', 4, 2).nullable()
  })
}

exports.down = function (knex) {
  return knex.schema.dropTable('analytics_liveAccountDailyOutcomeScore')
}
