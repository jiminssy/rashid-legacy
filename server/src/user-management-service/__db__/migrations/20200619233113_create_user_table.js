
exports.up = function (knex) {
  return knex.schema.createTable('userManagement_user', function (table) {
    table.increments()
    table.string('email').notNullable()
    table.string('password').notNullable()
    table.string('firstName').notNullable()
    table.string('lastName').notNullable()
  })
}

exports.down = function (knex) {
  return knex.schema.dropTable('userManagement_user')
}
