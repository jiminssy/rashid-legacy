import { ApiMessage } from './ApiMessage'

export class RemoveStrategyInstanceRequest extends ApiMessage {
  constructor (params) {
    super()
    this.strategyInstanceId = params.strategyInstanceId
  }
}
