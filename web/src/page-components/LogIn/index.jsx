import React from 'react'
import { LoginForm } from '../../components/LoginForm'
import styles from './index.module.scss'

export const LogIn = () => {
  return (
    <div className={styles.container}>
      <LoginForm />
    </div >
  )
}
