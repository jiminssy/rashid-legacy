import axios from 'axios'

const requestConfig = {
  headers: {
    Authorization: null
  }
}

export const addNewStrategy = async newStratInfo => {
  const response = await axios.post(
    '/api/strategies/add-new',
    newStratInfo,
    requestConfig
  )
  return response.data
}

export const addNewStrategyInstance = async newStratInstInfo => {
  const response = await axios.post(
    '/api/strategies/instances/add-new',
    newStratInstInfo,
    requestConfig
  )
  return response.data
}

export const editStrategy = async strategy => {
  await axios.post(
    '/api/strategies/edit',
    strategy,
    requestConfig
  )
}

export const editStrategyInstance = async instance => {
  const response = await axios.post(
    '/api/strategies/instances/edit',
    instance,
    requestConfig
  )
  return response.data
}

export const getAllStrategies = async () => {
  const response = await axios.get('/api/strategies', requestConfig)
  return response.data
}

export const getAllStrategyInstances = async () => {
  const response = await axios.get('/api/strategies/instances', requestConfig)
  return response.data
}

export const getDailyPerformances = async numDays => {
  const response = await axios.get(`/api/report/daily-performances/${numDays}`, requestConfig)
  return response.data
}

export const getReportLiveAccountPerformance = async (fromDate, toDate) => {
  const response = await axios.get(
    `/api/report/live-account-performance/${fromDate}/${toDate}`,
    requestConfig
  )
  return response.data
}

export const getStrategy = async strategyId => {
  const response = await axios.get(`/api/strategies/${strategyId}`, requestConfig)
  return response.data
}

export const getStrategyInstance = async instanceId => {
  const response = await axios.get(
    `/api/strategies/instances/${instanceId}`,
    requestConfig
  )
  return response.data
}

export const logInWithCreds = async (userName, password) => {
  const response = await axios.post('/api/users/login-with-creds', {
    userName: userName,
    password: password
  })
  const accessToken = response.data
  requestConfig.headers.Authorization = accessToken
  return accessToken
}

export const logInWithToken = async accessToken => {
  const response = await axios.post('/api/users/login-with-token', {
    accessToken: accessToken
  })
  requestConfig.headers.Authorization = accessToken
  return response.data
}

export const removeStrategy = async strategyId => {
  const response = await axios.post(
    '/api/strategies/remove',
    { strategyId },
    requestConfig
  )
  return response.data
}

export const removeStrategyInstance = async strategyInstanceId => {
  const response = await axios.post(
    '/api/strategies/instances/remove',
    { strategyInstanceId },
    requestConfig
  )
  return response.data
}
