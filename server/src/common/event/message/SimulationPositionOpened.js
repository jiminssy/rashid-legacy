import { EventMessage } from './EventMessage'
import { SIMULATION_POSITION_OPENED } from '../event-type'

export class SimulationPositionOpened extends EventMessage {
  constructor (
    strategyInstanceId,
    tradingInstrument, // data stream id format
    orderId,
    positionId,
    tradeDirection,
    orderType,
    open,
    stopLoss,
    takeProfit,
    openLimitPrice,
    positionSizeMultiplier
  ) {
    super(SIMULATION_POSITION_OPENED)
    this.strategyInstanceId = strategyInstanceId
    this.tradingInstrument = tradingInstrument
    this.orderId = orderId
    this.positionId = positionId
    this.tradeDirection = tradeDirection
    this.orderType = orderType
    this.open = open
    this.stopLoss = stopLoss
    this.takeProfit = takeProfit
    this.openLimitPrice = openLimitPrice
    this.positionSizeMultiplier = positionSizeMultiplier
  }
}
