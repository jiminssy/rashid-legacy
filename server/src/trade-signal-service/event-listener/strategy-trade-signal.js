import _ from 'lodash'
import { validateRisk } from '../risk-management'
import { placeMarketOrder } from '../order-execution'
import { OrderType } from '../../common/definition/OrderType'
import { convertToLiveTradeSignal } from '../live-trade-signal'
import { publish } from '../../common/event/event-bus'
import { trackExecutedTrade } from '../utils/fxcm-position-update-listener'

export const eventListener = async strategyTradeSignal => {
  const riskResult = await validateRisk(strategyTradeSignal)
  if (!riskResult.isSuccess) {
    return
  }

  const liveTradeSignal = await convertToLiveTradeSignal(strategyTradeSignal, riskResult)
  publish(liveTradeSignal)

  if (liveTradeSignal.orderType === OrderType.MARKET) {
    const tradeResult = await placeMarketOrder(liveTradeSignal)
    const coupledSimulationPositionId = _.get(
      strategyTradeSignal,
      'miscData.sourcePositionId',
      null
    )
    trackExecutedTrade({
      ...tradeResult,
      tradingStrategyInstanceId: liveTradeSignal.tradingStrategyInstanceId,
      coupledSimulationPositionId,
      tradingInstrument: liveTradeSignal.tradingInstrument,
      tradeDirection: liveTradeSignal.tradeDirection,
      orderType: liveTradeSignal.orderType,
      quantity: liveTradeSignal.quantity,
      openLimitPrice: liveTradeSignal.limitPrice
    })
  }
}
