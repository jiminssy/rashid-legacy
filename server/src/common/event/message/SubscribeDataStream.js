import { EventMessage } from './EventMessage'
import { SUBSCRIBE_DATA_STREAM } from '../event-type'

export class SubscribeDataStream extends EventMessage {
  constructor (dataStreamId) {
    super(SUBSCRIBE_DATA_STREAM)
    this.dataStreamId = dataStreamId
  }
}
