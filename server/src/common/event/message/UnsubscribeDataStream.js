import { EventMessage } from './EventMessage'
import { UNSUBSCRIBE_DATA_STREAM } from '../event-type'

export class UnsubscribeDataStream extends EventMessage {
  constructor (dataStreamId) {
    super(UNSUBSCRIBE_DATA_STREAM)
    this.dataStreamId = dataStreamId
  }
}
