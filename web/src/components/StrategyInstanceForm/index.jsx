import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { notification } from '../../utils/notification'
import { publish } from '../../utils/event-bus'
import { ADD_NEW_STRATEGY_INSTANCE, EDIT_STRATEGY_INSTANCE } from '../../constants/events'
import { Link } from 'react-router-dom'
import { isValidJson } from '../../utils/json-validator';

export const StrategyInstanceForm = ({ isEdit, instance }) => {
  const [strategyId, setStrategyId] = useState(instance ? instance.strategyId : '')
  const [strategyParams, setStrategyParams] = useState(
    instance ? JSON.stringify(instance.strategyParams, null, '  ') : ''
  )
  const [tradingInstrument, setTradingInstrument] = useState(instance ? instance.tradingInstrument : '')
  const [dataStreams, setDataStreams] = useState(
    instance ? JSON.stringify(instance.dataStreams, null, '  ') : ''
  )
  const [isEnabled, setIsEnabled] = useState(instance ? instance.isEnabled : true)

  const strategyParametersValidationErrorMessage = 'Strategy Parameters has invalid JSON'
  const dataStreamsValidationErrorMessage = 'Data Streams has invalid JSON'

  const onAddClick = event => {
    event.preventDefault()
    if (strategyId === ''
      || strategyParams === ''
      || tradingInstrument === ''
      || dataStreams === '') {
        notification.error('All fields must be filled')
        return
    }

    if(!isValidJson(strategyParams)){
      notification.error(strategyParametersValidationErrorMessage)
      return
    }

    if(!isValidJson(dataStreams)){
      notification.error(dataStreamsValidationErrorMessage)
      return
    }

    publish(ADD_NEW_STRATEGY_INSTANCE, {
      strategyId,
      strategyParams,
      tradingInstrument,
      dataStreams,
      isEnabled
    })
  }

  const onEditClick = event => {
    event.preventDefault()
    if (strategyId === ''
      || strategyParams === ''
      || tradingInstrument === ''
      || dataStreams === '') {
        notification.error('All fields must be filled')
        return
    }

    if(!isValidJson(strategyParams)){
      notification.error(strategyParametersValidationErrorMessage)
      return
    }
    
    if(!isValidJson(dataStreams)){
      notification.error(dataStreamsValidationErrorMessage)
      return 
    }

    publish(EDIT_STRATEGY_INSTANCE, {
      strategyInstanceId: instance.id,
      strategyId,
      strategyParams,
      tradingInstrument,
      dataStreams,
      isEnabled
    })
  }
  
  return (
    <div>

      <div className="form-group">
        <label htmlFor="strategy-id-input">Strategy ID</label>
        {isEdit? <div>{strategyId}</div> :  
        <input type="text" className="form-control" id="strategy-id-input"
          aria-describedby="strategy-id-help" value={strategyId}
          onChange={e => setStrategyId(e.target.value)}
        />}

       {isEdit ? 
       null : 
       <small id="strategy-id-help" className="form-text text-muted">
       All strategy IDs can be found <Link to="/admin/strategy/all-strategies">here</Link>.
      </small>
       }
      </div>

      <div className="form-group">
        <label htmlFor="params-input">Strategy Parameters</label>
        <textarea className="form-control" id="params-input" rows="3" aria-describedby="params-help"
          value={strategyParams} onChange={e => setStrategyParams(e.target.value)}
        />
        <small id="params-help" className="form-text text-muted">
          Parameters that should be populated according to the schema in JSON format.
        </small>
      </div>

      <div className="form-group">
        <label htmlFor="instrument-id-input">Trading Instrument</label>
        <input type="text" className="form-control" id="instrument-id-input"
          aria-describedby="instrument-id-help" value={tradingInstrument}
          onChange={e => setTradingInstrument(e.target.value)}
        />
        <small id="instrument-id-help" className="form-text text-muted">
          Trading instrument in data stream ID format.
        </small>
      </div>

      <div className="form-group">
        <label htmlFor="data-streams-input">Data Streams</label>
        <textarea className="form-control" id="data-streams-input" rows="3" aria-describedby="data-streams-help"
          value={dataStreams} onChange={e => setDataStreams(e.target.value)}
        />
        <small id="data-streams-help" className="form-text text-muted">
          Data streams the strategy instance is interested in JSON format.
        </small>
      </div>

      <div className="form-group">
        <div className="custom-control custom-switch">
          <input type="checkbox" className="custom-control-input" id="isEnabledSwitch"
            checked={isEnabled} onChange={() => setIsEnabled(!isEnabled)}
          />
          <label className="custom-control-label" htmlFor="isEnabledSwitch">Enabled</label>
        </div>
      </div>

      {isEdit ?
        <button type="submit" className="btn btn-primary" onClick={onEditClick}>Save Edit</button> :
        <button type="submit" className="btn btn-primary" onClick={onAddClick}>Add New Strategy Instance</button>
      }
    </div>
  )
}

StrategyInstanceForm.propTypes = {
  isEdit: PropTypes.bool,
  instance: PropTypes.object
}
