import _ from 'lodash'
import { handler } from '..'
import { getMockKnex } from '../../../../common/test-mock/mock-knex'
import { Strategy } from '../../../../common/definition/Strategy'
import { outMessageManager } from '../../../out-message-manager'

const knex = getMockKnex()

test('gets all strategies', async () => {
  outMessageManager.sendOkResponse = jest.fn()
  const mockedStrategies = [
    [
      {
        id: 'id-1',
        name: 'test-name-1',
        version: 2,
        description: 'test-description-1',
        codePath: 'code-path-1',
        paramSchema: '{"some":"schema"}'
      },
      {
        id: 'id-2',
        name: 'test-name-2',
        version: 7,
        description: 'test-description-2',
        codePath: 'code-path-2',
        paramSchema: '{"some":"schema"}'
      }
    ]
  ]
  const expectedResults = [
    new Strategy(mockedStrategies[0][0]),
    new Strategy(mockedStrategies[0][1])
  ]

  knex._setResults(mockedStrategies)

  await handler.handle({ id: 'test-message-id' })

  const messageId = outMessageManager.sendOkResponse.mock.calls[0][0]
  const replyParam = outMessageManager.sendOkResponse.mock.calls[0][1]

  expect(messageId).toBe('test-message-id')
  expect(_.isEqual(expectedResults, replyParam)).toBe(true)
})
