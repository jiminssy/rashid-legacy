import { MessageHandler } from '../MessageHandler'
import { outMessageManager } from '../../out-message-manager'
import {
  doesStrategyExistById,
  doesStrategyInstanceExist
} from '../../../view-query-service/query-interface/strategy'
import { publish } from '../../../common/event/event-bus'
import { AddNewStrategyInstance as AddNewStrategyInstanceMessage } from '../../../common/event/message/AddNewStrategyInstance'
import { StrategyInstance } from '../../../common/definition/StrategyInstance'
import { generateId } from '../../../common/utils/strategy-instance-id-generator'
import { validateStrategyParams } from '../../utils/json-schema-validator'

class AddNewStrategyInstance extends MessageHandler {
  async handle (message) {
    const {
      strategyId,
      strategyParams,
      tradingInstrument,
      dataStreams
    } = message
    const strategyInstanceId = generateId(
      strategyId, strategyParams, tradingInstrument, dataStreams
    )

    if (!await doesStrategyExistById(message.strategyId)) {
      this._handleFail(message.id, `Strategy does not exist for ID: ${strategyId}`)
      return
    }

    if (await doesStrategyInstanceExist(strategyInstanceId)) {
      this._handleFail(message.id, 'Equivalent strategy instance already exists')
      return
    }

    await this._handleNewStrategyInstance(message, strategyInstanceId)
  }

  async _handleNewStrategyInstance (message, strategyInstanceId) {
    const {
      strategyId,
      strategyParams
    } = message

    if (!await validateStrategyParams(strategyId, strategyParams)) {
      this._handleFail(message.id, 'Invalid strategy parameters')
      return
    }

    publish(new AddNewStrategyInstanceMessage(
      new StrategyInstance({
        ...message,
        id: strategyInstanceId,
        dataStreams: JSON.parse(message.dataStreams),
        strategyParams: JSON.parse(message.strategyParams)
      })
    ))
    outMessageManager.sendOkResponse(message.id, { isSuccess: true })
  }

  _handleFail (messageId, failReason) {
    outMessageManager.sendOkResponse(messageId, {
      isSuccess: false,
      failReason
    })
  }
}

export const handler = new AddNewStrategyInstance()
