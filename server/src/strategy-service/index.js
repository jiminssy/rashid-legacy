import { subscribe } from '../common/utils/error-publishing-event-subscriber'
import {
  ADD_NEW_STRATEGY,
  ADD_NEW_STRATEGY_INSTANCE,
  EDIT_STRATEGY,
  EDIT_STRATEGY_INSTANCE,
  ERROR_STRATEGY_SERVICE_EVENT_LISTENER,
  REMOVE_STRATEGY,
  REMOVE_STRATEGY_INSTANCE,
  SIMULATION_DAILY_OUTCOME_SCORES
} from '../common/event/event-type'
import { activateEnabledStrategyInstances } from './util/strategy-instance-activator'
import { SystemService } from '../common/definition/SystemService'
import { getDataClient } from '../common/fxcm/client-factory'

const subscribes = [
  [ADD_NEW_STRATEGY, require('./event-listener/add-new-strategy').eventListener],
  [ADD_NEW_STRATEGY_INSTANCE, require('./event-listener/add-new-strategy-instance').eventListener],
  [EDIT_STRATEGY, require('./event-listener/edit-strategy').eventListener],
  [EDIT_STRATEGY_INSTANCE, require('./event-listener/edit-strategy-instance').eventListener],
  [REMOVE_STRATEGY, require('./event-listener/remove-strategy').eventListener],
  [REMOVE_STRATEGY_INSTANCE, require('./event-listener/remove-strategy-instance').eventListener],
  [SIMULATION_DAILY_OUTCOME_SCORES, require('./event-listener/strategy-instance-daily-outcome-scores').eventListener]
]

export const start = async () => {
  const dataClient = getDataClient(SystemService.STRATEGY)
  await dataClient.initialize()

  subscribe(
    'StrategyService',
    ERROR_STRATEGY_SERVICE_EVENT_LISTENER,
    subscribes
  )
  await activateEnabledStrategyInstances()
}
