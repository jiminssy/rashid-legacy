import { knex } from '../../common/database/knex'
import { deactivateStrategyInstance } from '../util/strategy-instance-activator'

export const eventListener = async ({ strategyInstanceId }) => {
  deactivateStrategyInstance(strategyInstanceId, true)
  await knex('strategy_strategyInstance')
    .where({ id: strategyInstanceId })
    .del()
}
