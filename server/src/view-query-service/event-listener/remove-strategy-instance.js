import { knex } from '../../common/database/knex'
import { DbTable } from '../definition/DbTable'

export const eventListener = async ({ strategyInstanceId }) => {
  await knex(DbTable.STRATEGY_INSTANCE)
    .where({ id: strategyInstanceId })
    .del()
}
