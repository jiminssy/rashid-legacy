# Dev Environment Setup

## 1. Runtime environment setup
- Node: v14.14.0
- Postgres: v12.4
- Install Grunt globally: `npm install -g grunt-cli`
- Install Knex globally: `npm install -g knex`

## 2. Setup local PostgreSQL database
1. Create a user with all privileges with the ID: `rashid_server`, password: `VFR$3edc`
1. Create an empty database called `rashid`

## 3. Obtain your own FXCM demo account
1. Goto [FXCM demo page](https://tradingstation.fxcm.com/FreeDemo?lc=en_US&sb=true) and sign up a new demo account.
1. Log in to the demo account to load the FXCM Trading Station web app.
1. Click top right profile icon to open up the menu. Click API Token from the menu.
1. Generate API token. This token will be included in the `.env` file later.

## 4. Server
1. `git clone git@bitbucket.org:jiminssy/rashid.git`
1. `cd` into `server` folder.
1. `yarn`
1. Create `.env` file with following contents.

```
NODE_ENV=development

HTTP_SERVER_PORT = 3001

FXCM_API_PROTOCOL=https
FXCM_API_PORT=443

FXCM_DEMO_API_HOST=api-demo.fxcm.com
FXCM_DEMO_API_TOKEN=<<your FXCM API token>>

DB_HOST=127.0.0.1
DB_USER=rashid_server
DB_PASSWORD=VFR$3edc
DB_NAME=rashid

JWT_SECRET=Rashid-Dev-Secret

SERVER_TIMEZONE=Europe/London
```

4. `knex migrate:latest`
4. `knex seed:run`
4. Run `grunt build` to check if all tests and linting passes
4. `grunt dev-server` to start the server

(* There is no nodemon watch setup for the server yet. Whenever you make changes to the server you need to manually restart the server with `grunt dev-server` for now)

## 5. Web
1. `cd` into `web` folder.
1. `yarn`
1. `touch .env.development`
1. `touch .env.production`
1. `yarn start`
1. Try login to the web app with ID: `test@test.com` and password: `1234`

## Tip
If you need production replica data to populate realistic data, please contact Jimin on Slack.