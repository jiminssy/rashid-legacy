import moment from 'moment-timezone'
import { mean, std } from 'mathjs'
import {
  getDailyOutcomeScores,
  getEnabledStrategyInstances
} from '../utils/database'
import { publish } from '../../common/event/event-bus'
import { StrategyInstanceDailyStopLoss } from '../../common/event/message/StrategyInstanceDailyStopLoss'

const NUM_MOST_RECENT_DAILY_OUTCOME_SCORE = 100

const getStopLossOutcomeScore = (scoresMean, scoresStd) => {
  const getAtleastStd = (boundary, scoresStd) => {
    if (boundary > (-1 * scoresStd)) {
      return -1 * scoresStd
    } else {
      return boundary
    }
  }

  // 1 STD
  let boundary = scoresMean - scoresStd
  if (boundary < 0) {
    return getAtleastStd(boundary, scoresStd)
  }

  // 2 STD
  boundary -= scoresStd
  if (boundary < 0) {
    return getAtleastStd(boundary, scoresStd)
  }

  // 3 STD
  boundary -= scoresStd
  return getAtleastStd(boundary, scoresStd)
}

const getReversedStopLossOutcomeScore = (scoresMean, scoresStd) => {
  const getAtleastStd = (boundary, scoresStd) => {
    return boundary < scoresStd ? scoresStd : boundary
  }

  // 1 STD
  let boundary = scoresMean + scoresStd
  if (boundary > 0) {
    return getAtleastStd(boundary, scoresStd)
  }

  // 2 STD
  boundary += scoresStd
  if (boundary > 0) {
    return getAtleastStd(boundary, scoresStd)
  }

  // 3 STD
  boundary += scoresStd
  return getAtleastStd(boundary, scoresStd)
}

const publishStrategyInstanceDailyStopLoss = async (strategyInstanceId) => {
  const outcomeScores = await getDailyOutcomeScores(strategyInstanceId, NUM_MOST_RECENT_DAILY_OUTCOME_SCORE)

  const scoresMean = mean(outcomeScores)
  const scoresStd = std(outcomeScores)

  const today = moment.utc().set({
    hour: 0,
    minute: 0,
    second: 0,
    millisecond: 0
  })

  publish(new StrategyInstanceDailyStopLoss(
    strategyInstanceId,
    today.format(),
    Number(getStopLossOutcomeScore(scoresMean, scoresStd).toFixed(2)),
    Number(getReversedStopLossOutcomeScore(scoresMean, scoresStd).toFixed(2))
  ))
}

export const eventListener = async () => {
  const enabledStrategyInstances = await getEnabledStrategyInstances()
  for (const strategyInstanceId of enabledStrategyInstances) {
    publishStrategyInstanceDailyStopLoss(strategyInstanceId)
  }
}

export const __test__ = {
  publishStrategyInstanceDailyStopLoss
}
