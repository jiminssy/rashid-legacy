import { AddNewStrategyRequest } from '../api-message/AddNewStrategyRequest'
import { AddNewStrategyInstanceRequest } from '../api-message/AddNewStrategyInstanceRequest'
import { EditStrategyRequest } from '../api-message/EditStrategyRequest'
import { EditStrategyInstanceRequest } from '../api-message/EditStrategyInstanceRequest'
import { GetAllStrategiesRequest } from '../api-message/GetAllStrategiesRequest'
import { GetAllStrategyInstancesRequest } from '../api-message/GetAllStrategyInstancesRequest'
import { GetDailyPerformancesRequest } from '../api-message/GetDailyPerformancesRequest'
import { GetStrategyRequest } from '../api-message/GetStrategyRequest'
import { GetStrategyInstanceRequest } from '../api-message/GetStrategyInstanceRequest'
import { LogInWithCredsRequest } from '../api-message/LogInWithCredsRequest'
import { LogInWithTokenRequest } from '../api-message/LogInWithTokenRequest'
import { RemoveStrategyRequest } from '../api-message/RemoveStrategyRequest'
import { RemoveStrategyInstanceRequest } from '../api-message/RemoveStrategyInstanceRequest'
import { GetReportLiveAccountPerformanceRequest } from '../api-message/GetReportLiveAccountPerformanceRequest'

const handlers = {
  [AddNewStrategyRequest.name]: require('./AddNewStrategy').handler,
  [AddNewStrategyInstanceRequest.name]: require('./AddNewStrategyInstance').handler,
  [EditStrategyRequest.name]: require('./EditStrategy').handler,
  [EditStrategyInstanceRequest.name]: require('./EditStrategyInstance').handler,
  [GetAllStrategiesRequest.name]: require('./GetAllStrategies').handler,
  [GetAllStrategyInstancesRequest.name]: require('./GetAllStrategyInstances').handler,
  [GetDailyPerformancesRequest.name]: require('./GetDailyPerformances').handler,
  [GetReportLiveAccountPerformanceRequest.name]: require('./GetReportLiveAccountPerformance').handler,
  [GetStrategyRequest.name]: require('./GetStrategy').handler,
  [GetStrategyInstanceRequest.name]: require('./GetStrategyInstance').handler,
  [LogInWithCredsRequest.name]: require('./LogInWithCreds').handler,
  [LogInWithTokenRequest.name]: require('./LogInWithToken').handler,
  [RemoveStrategyRequest.name]: require('./RemoveStrategy').handler,
  [RemoveStrategyInstanceRequest.name]: require('./RemoveStrategyInstance').handler
}

export const handle = async message => {
  const messageName = message.constructor.name
  if (!(messageName in handlers)) {
    throw new Error(`Handler does not exist for ${messageName}`)
  }

  const handler = handlers[messageName]
  await handler.handle(message)
}
