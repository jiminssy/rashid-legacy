import { calcStopLossRange } from '../stop-loss-range-calculator'
import * as database from '../../../simulation-service/utils/database'
import { BaseClient } from '../../../common/fxcm/base-client'
import { DataClient } from '../../../common/fxcm/data-client'
import * as clientFactory from '../../../common/fxcm/client-factory'

jest.mock('../../../common/fxcm/client-factory', () => ({
  getDataClient: jest.fn()
}))

test('calcStopLossRange', async () => {
  database.getBrokerInstrumentId = jest.fn().mockResolvedValue(1)

  const baseClient = new BaseClient()
  const dataClient = new DataClient(baseClient)
  dataClient.getMostRecentHistoricalData = jest.fn().mockResolvedValue([
    {
      askHigh: 11,
      bidHigh: 9,
      askLow: 5,
      bidLow: 3,
      askClose: 7,
      bidClose: 6
    },
    {
      askHigh: 16,
      bidHigh: 12,
      askLow: 8,
      bidLow: 5,
      askClose: 15,
      bidClose: 13
    },
    {
      askHigh: 10,
      bidHigh: 8,
      askLow: 7,
      bidLow: 5,
      askClose: 7,
      bidClose: 5
    },
    {
      askHigh: 25,
      bidHigh: 20,
      askLow: 20,
      bidLow: 16,
      askClose: 24,
      bidClose: 23
    }
  ])
  clientFactory.getDataClient.mockReturnValue(dataClient)

  const stopLossRange = await calcStopLossRange(1, 'm1', 2)

  const historicalDataFetchParams = dataClient.getMostRecentHistoricalData.mock.calls[0]
  expect(historicalDataFetchParams).toMatchSnapshot()
  expect(stopLossRange).toBe(7.75)
})
