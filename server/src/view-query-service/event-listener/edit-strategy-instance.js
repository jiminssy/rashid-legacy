import { knex } from '../../common/database/knex'
import { DbTable } from '../definition/DbTable'

export const eventListener = async (
  {
    previousStrategyInstanceId,
    strategyInstance: {
      id,
      strategyId,
      strategyParams,
      tradingInstrument,
      dataStreams,
      isEnabled
    }
  }
) => {
  await knex(DbTable.STRATEGY_INSTANCE)
    .where({ id: previousStrategyInstanceId })
    .update({
      id,
      strategyId,
      tradingInstrument,
      isEnabled,
      dataStreams: JSON.stringify(dataStreams),
      strategyParams: JSON.stringify(strategyParams)
    })
}
