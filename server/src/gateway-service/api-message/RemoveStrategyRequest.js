import { ApiMessage } from './ApiMessage'

export class RemoveStrategyRequest extends ApiMessage {
  constructor (params) {
    super()
    this.strategyId = params.strategyId
  }
}
