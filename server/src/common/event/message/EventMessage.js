import UUID from 'uuid-js'
import moment from 'moment-timezone'

class EventMessageHeader {
  constructor (eventType, contextId) {
    this.eventType = eventType
    this.contextId = contextId
    this.created = moment.utc().format()
  }
}

export class EventMessage {
  constructor (eventType, contextId = UUID.create().hex, payload = null) {
    this.header = new EventMessageHeader(eventType, contextId)
    if (payload) {
      for (const property in payload) {
        this[property] = payload[property]
      }
    }
  }
}
