export const DataPeriod = Object.freeze({
  m1: 'm1',
  m5: 'm5',
  m15: 'm15',
  m30: 'm30',
  H1: 'H1',
  H2: 'H2',
  H3: 'H3',
  H4: 'H4',
  H6: 'H6',
  H8: 'H8',
  D1: 'D1',
  W1: 'W1',
  M1: 'M1'
})
