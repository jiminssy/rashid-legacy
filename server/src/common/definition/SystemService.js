export const SystemService = Object.freeze({
  ANALYTICS: 'Analytics',
  DATA_STREAM: 'DataStream',
  EVENT_LOG: 'EventLog',
  GATEWAY: 'Gateway',
  SCHEDULED_EVENT: 'ScheduledEvent',
  SIMULATION: 'Simulation',
  STRATEGY: 'Strategy',
  TRADE_SIGNAL: 'TradeSignal',
  USER_MANAGEMENT: 'UserManagement',
  VIEW_QUERY: 'ViewQuery'
})
