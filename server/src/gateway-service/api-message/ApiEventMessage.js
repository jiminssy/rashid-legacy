import { ApiMessage } from './ApiMessage'

export class ApiEventMessage extends ApiMessage {
  constructor (eventType) {
    super()
    this.eventType = eventType
  }
}
