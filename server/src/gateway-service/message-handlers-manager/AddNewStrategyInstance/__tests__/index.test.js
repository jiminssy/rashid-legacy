import { handler } from '..'
import { getMockKnex } from '../../../../common/test-mock/mock-knex'
import { outMessageManager } from '../../../out-message-manager'
import { mockPublish, unmockPublish } from '../../../../common/test-mock/mock-event-bus'
import { publish } from '../../../../common/event/event-bus'
import { AddNewStrategyInstanceRequest } from '../../../api-message/AddNewStrategyInstanceRequest'

const knex = getMockKnex()

jest.mock('uuid-js', () => ({
  create: () => ({ hex: 'test-uuid' })
}))
outMessageManager.sendOkResponse = jest.fn()

describe('handle', () => {
  const testMessageId = 'test-uuid'
  const testApiMessage = new AddNewStrategyInstanceRequest({
    strategyId: 1,
    strategyParams: '{ "lookBack": 5 }',
    tradingInstrument: 'FXCM-1-m1',
    dataStreams: '["FXCM-1-m5"]',
    isEnabled: true
  })
  const testSchema = {
    $schema: 'http://json-schema.org/draft-07/schema',
    $id: 'http://example.com/example.json',
    type: 'object',
    title: 'The root schema',
    description: 'The root schema comprises the entire JSON document.',
    default: {},
    examples: [
      {
        lookBack: 5
      }
    ],
    required: [
      'lookBack'
    ],
    properties: {
      lookBack: {
        $id: '#/properties/lookBack',
        type: 'integer',
        title: 'The lookBack schema',
        description: 'An explanation about the purpose of this instance.',
        default: 0,
        examples: [
          5
        ]
      }
    },
    additionalProperties: true
  }

  beforeEach(() => {
    outMessageManager.sendOkResponse.mockReset()
    mockPublish()
  })

  afterEach(() => {
    unmockPublish()
  })

  test('strategy does not exist', async () => {
    knex._setResults([[]]) // db returns nothing

    await handler.handle(testApiMessage)

    expect(outMessageManager.sendOkResponse).toHaveBeenCalledWith(testMessageId, {
      isSuccess: false,
      failReason: 'Strategy does not exist for ID: 1'
    })
  })

  test('same strategy instance does exist', async () => {
    knex._setResults([
      [{}], // return something for strategy exist check
      [{}] // return something for strategy instance exist check
    ])

    await handler.handle(testApiMessage)

    expect(outMessageManager.sendOkResponse).toHaveBeenCalledWith(testMessageId, {
      isSuccess: false,
      failReason: 'Equivalent strategy instance already exists'
    })
  })

  test('param schema fail', async () => {
    testApiMessage.strategyParams = '{ "lookBack": "invalid_value" }'

    knex._setResults([
      [{}], // return something for strategy exist check
      [], // return nothing for strategy instance exist check
      { paramSchema: testSchema } // return nothing
    ])

    await handler.handle(testApiMessage)

    expect(outMessageManager.sendOkResponse).toHaveBeenCalledWith(testMessageId, {
      isSuccess: false,
      failReason: 'Invalid strategy parameters'
    })

    testApiMessage.strategyParams = '{ "lookBack": 5 }' // reverting back
  })

  test('successful add', async () => {
    knex._setResults([
      [{}], // return something for strategy exist check
      [], // return nothing for strategy instance exist check
      { paramSchema: testSchema } // return nothing
    ])

    await handler.handle(testApiMessage)

    expect(outMessageManager.sendOkResponse).toHaveBeenCalledWith(testMessageId, {
      isSuccess: true
    })
    const publishedMessage = publish.mock.calls[0][0]
    expect(publishedMessage).toMatchSnapshot()
  })
})
