import { validateRisk } from '../index'
import * as database from '../../utils/database'

jest.mock('moment-timezone', () => {
  const today = '2020-11-03T00:00:00.000Z'
  const mockedMomentTimezone = () => jest.requireActual('moment-timezone')(today)
  mockedMomentTimezone.utc = () => jest.requireActual('moment-timezone').utc(today)
  return mockedMomentTimezone
})

describe('validateRisk', () => {
  const mockAllowedInstances = [
    {
      strategyInstanceId: 'mock-strategy-instance-1',
      openedPositionId: 'mock-position-id-1'
    },
    {
      strategyInstanceId: 'mock-strategy-instance-2',
      openedPositionId: null
    }
  ]

  const mockLiveAccountDailyStopLoss = {
    brokerName: 'FXCM',
    brokerAccountId: 'mock-account-id',
    date: 'mock-date',
    stopLossOutcomeScore: -6.23,
    stoppedInLive: false
  }

  test('success - still above daily stop loss', async () => {
    database.getLiveAccountDailyStopLoss = jest.fn().mockResolvedValue(mockLiveAccountDailyStopLoss)
    database.getDailyOutcomeScoresSum = jest.fn().mockResolvedValue(-5)
    database.getLiveTradingAllowedStrategyInstsances = jest.fn().mockResolvedValue(mockAllowedInstances)
    database.getStrategyInstanceDailyStopLoss = jest.fn().mockResolvedValue({
      stopLossOutcomeScore: -1,
      stoppedInLive: false
    })
    database.getDailyOutcomeScore = jest.fn().mockResolvedValue({ outcomeScore: 0 })
    const tradeSignal = {
      strategyInstanceId: 'mock-strategy-instance-2',
      miscData: {
        isReactionaryTradeSignal: true,
        sourcePositionId: 'mock-broker-position-id',
        sourceStrategyInstanceId: 'mock-source-strategy-instance-id'
      }
    }

    const result = await validateRisk(tradeSignal)

    expect(result).toEqual({
      isSuccess: true,
      positionSizeMultiplier: 1
    })
  })

  test('success - first trade of the day for strategy instance', async () => {
    database.getLiveAccountDailyStopLoss = jest.fn().mockResolvedValue(mockLiveAccountDailyStopLoss)
    database.getDailyOutcomeScoresSum = jest.fn().mockResolvedValue(-5)
    database.getLiveTradingAllowedStrategyInstsances = jest.fn().mockResolvedValue(mockAllowedInstances)
    database.getStrategyInstanceDailyStopLoss = jest.fn().mockResolvedValue({
      stopLossOutcomeScore: -1,
      stoppedInLive: false
    })
    database.getDailyOutcomeScore = jest.fn().mockResolvedValue(undefined)
    const tradeSignal = {
      strategyInstanceId: 'mock-strategy-instance-2',
      miscData: {
        isReactionaryTradeSignal: true,
        sourcePositionId: 'mock-broker-position-id',
        sourceStrategyInstanceId: 'mock-source-strategy-instance-id'
      }
    }

    const result = await validateRisk(tradeSignal)

    expect(result).toEqual({
      isSuccess: true,
      positionSizeMultiplier: 1
    })
  })

  test('success - losing money but reversed', async () => {
    database.getLiveAccountDailyStopLoss = jest.fn().mockResolvedValue(mockLiveAccountDailyStopLoss)
    database.getDailyOutcomeScoresSum = jest.fn().mockResolvedValue(-5)
    database.getLiveTradingAllowedStrategyInstsances = jest.fn().mockResolvedValue(mockAllowedInstances)
    database.getStrategyInstanceDailyStopLoss = jest.fn().mockResolvedValue({
      stopLossOutcomeScore: -1,
      reversedStopLossOutcomeScore: 3,
      stoppedInLive: false
    })
    database.getDailyOutcomeScore = jest.fn().mockResolvedValue({ outcomeScore: -2 })
    database.setStrategyInstanceStoppedOut = jest.fn()
    const mockSourceStrategyInstanceId = 'mock-source-strategy-instance-id'
    const tradeSignal = {
      strategyInstanceId: 'mock-strategy-instance-2',
      miscData: {
        isReactionaryTradeSignal: true,
        isReversedTradeSignal: true,
        sourcePositionId: 'mock-broker-position-id',
        sourceStrategyInstanceId: mockSourceStrategyInstanceId
      }
    }

    const result = await validateRisk(tradeSignal)

    expect(result).toEqual({
      isSuccess: true,
      positionSizeMultiplier: 1
    })
  })

  test('failed - not an allowed strategy instance', async () => {
    database.getLiveAccountDailyStopLoss = jest.fn().mockResolvedValue(mockLiveAccountDailyStopLoss)
    database.getDailyOutcomeScoresSum = jest.fn().mockResolvedValue(-5)
    database.getLiveTradingAllowedStrategyInstsances = jest.fn().mockResolvedValue(mockAllowedInstances)
    const tradeSignal = {
      strategyInstanceId: 'mock-strategy-instance-3'
    }

    const result = await validateRisk(tradeSignal)

    expect(result).toEqual({ isSuccess: false })
  })

  test('failed - allowed instance but position already opened', async () => {
    database.getLiveAccountDailyStopLoss = jest.fn().mockResolvedValue(mockLiveAccountDailyStopLoss)
    database.getDailyOutcomeScoresSum = jest.fn().mockResolvedValue(-5)
    database.getLiveTradingAllowedStrategyInstsanceIds = jest.fn().mockResolvedValue(mockAllowedInstances)
    const tradeSignal = {
      strategyInstanceId: 'mock-strategy-instance-1'
    }

    const result = await validateRisk(tradeSignal)

    expect(result).toEqual({ isSuccess: false })
  })

  test('failed - strategy instance stopped out today', async () => {
    database.getLiveAccountDailyStopLoss = jest.fn().mockResolvedValue(mockLiveAccountDailyStopLoss)
    database.getDailyOutcomeScoresSum = jest.fn().mockResolvedValue(-5)
    database.getLiveTradingAllowedStrategyInstsances = jest.fn().mockResolvedValue(mockAllowedInstances)
    database.getStrategyInstanceDailyStopLoss = jest.fn().mockResolvedValue({
      stopLossOutcomeScore: -1,
      stoppedInLive: true
    })
    database.getDailyOutcomeScore = jest.fn().mockResolvedValue({ outcomeScore: 0 })
    const tradeSignal = {
      strategyInstanceId: 'mock-strategy-instance-2',
      miscData: {
        isReactionaryTradeSignal: true,
        sourcePositionId: 'mock-broker-position-id',
        sourceStrategyInstanceId: 'mock-source-strategy-instance-id'
      }
    }

    const result = await validateRisk(tradeSignal)

    expect(result).toEqual({ isSuccess: false })
  })

  test('failed - strategy instance lost too much today', async () => {
    database.getLiveAccountDailyStopLoss = jest.fn().mockResolvedValue(mockLiveAccountDailyStopLoss)
    database.getDailyOutcomeScoresSum = jest.fn().mockResolvedValue(-5)
    database.getLiveTradingAllowedStrategyInstsances = jest.fn().mockResolvedValue(mockAllowedInstances)
    database.getStrategyInstanceDailyStopLoss = jest.fn().mockResolvedValue({
      stopLossOutcomeScore: -1,
      stoppedInLive: false
    })
    database.getDailyOutcomeScore = jest.fn().mockResolvedValue({ outcomeScore: -2 })
    database.setStrategyInstanceStoppedOut = jest.fn()
    const mockSourceStrategyInstanceId = 'mock-source-strategy-instance-id'
    const tradeSignal = {
      strategyInstanceId: 'mock-strategy-instance-2',
      miscData: {
        isReactionaryTradeSignal: true,
        sourcePositionId: 'mock-broker-position-id',
        sourceStrategyInstanceId: mockSourceStrategyInstanceId
      }
    }

    const result = await validateRisk(tradeSignal)

    expect(result).toEqual({ isSuccess: false })
    expect(database.setStrategyInstanceStoppedOut).toHaveBeenCalledWith(mockSourceStrategyInstanceId, '2020-11-03T00:00:00Z')
  })

  test('failed - original makes profit but reversed', async () => {
    database.getLiveAccountDailyStopLoss = jest.fn().mockResolvedValue(mockLiveAccountDailyStopLoss)
    database.getDailyOutcomeScoresSum = jest.fn().mockResolvedValue(-5)
    database.getLiveTradingAllowedStrategyInstsances = jest.fn().mockResolvedValue(mockAllowedInstances)
    database.getStrategyInstanceDailyStopLoss = jest.fn().mockResolvedValue({
      stopLossOutcomeScore: -1,
      reversedStopLossOutcomeScore: 2,
      stoppedInLive: false
    })
    database.getDailyOutcomeScore = jest.fn().mockResolvedValue({ outcomeScore: 3 })
    database.setStrategyInstanceStoppedOut = jest.fn()
    const mockSourceStrategyInstanceId = 'mock-source-strategy-instance-id'
    const tradeSignal = {
      strategyInstanceId: 'mock-strategy-instance-2',
      miscData: {
        isReactionaryTradeSignal: true,
        isReversedTradeSignal: true,
        sourcePositionId: 'mock-broker-position-id',
        sourceStrategyInstanceId: mockSourceStrategyInstanceId
      }
    }

    const result = await validateRisk(tradeSignal)

    expect(result).toEqual({ isSuccess: false })
    expect(database.setStrategyInstanceStoppedOut).toHaveBeenCalledWith(mockSourceStrategyInstanceId, '2020-11-03T00:00:00Z')
  })

  test('failed - live account daily stop loss not available', async () => {
    database.getLiveAccountDailyStopLoss = jest.fn().mockResolvedValue(null)
    database.getLiveTradingAllowedStrategyInstsances = jest.fn().mockResolvedValue(mockAllowedInstances)
    database.getStrategyInstanceDailyStopLoss = jest.fn().mockResolvedValue({
      stopLossOutcomeScore: -1,
      reversedStopLossOutcomeScore: 2,
      stoppedInLive: false
    })
    database.getDailyOutcomeScore = jest.fn().mockResolvedValue({ outcomeScore: 0 })
    database.setStrategyInstanceStoppedOut = jest.fn()
    const mockSourceStrategyInstanceId = 'mock-source-strategy-instance-id'
    const tradeSignal = {
      strategyInstanceId: 'mock-strategy-instance-2',
      miscData: {
        isReactionaryTradeSignal: true,
        isReversedTradeSignal: true,
        sourcePositionId: 'mock-broker-position-id',
        sourceStrategyInstanceId: mockSourceStrategyInstanceId
      }
    }

    const result = await validateRisk(tradeSignal)

    expect(result).toEqual({ isSuccess: false })
  })

  test('failed - live account daily stop loss was hit', async () => {
    database.getLiveAccountDailyStopLoss = jest.fn().mockResolvedValue(mockLiveAccountDailyStopLoss)
    database.getDailyOutcomeScoresSum = jest.fn().mockResolvedValue(-8)
    database.setLiveAccountStoppedOut = jest.fn()
    database.getLiveTradingAllowedStrategyInstsances = jest.fn().mockResolvedValue(mockAllowedInstances)
    database.getStrategyInstanceDailyStopLoss = jest.fn().mockResolvedValue({
      stopLossOutcomeScore: -1,
      reversedStopLossOutcomeScore: 2,
      stoppedInLive: false
    })
    database.getDailyOutcomeScore = jest.fn().mockResolvedValue({ outcomeScore: 0 })
    database.setStrategyInstanceStoppedOut = jest.fn()
    const mockSourceStrategyInstanceId = 'mock-source-strategy-instance-id'
    const tradeSignal = {
      strategyInstanceId: 'mock-strategy-instance-2',
      miscData: {
        isReactionaryTradeSignal: true,
        isReversedTradeSignal: true,
        sourcePositionId: 'mock-broker-position-id',
        sourceStrategyInstanceId: mockSourceStrategyInstanceId
      }
    }

    const result = await validateRisk(tradeSignal)

    expect(result).toEqual({ isSuccess: false })
    expect(database.setLiveAccountStoppedOut).toHaveBeenCalled()
  })
})
