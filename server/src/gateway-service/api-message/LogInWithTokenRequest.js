import { ApiMessage } from './ApiMessage'

export class LogInWithTokenRequest extends ApiMessage {
  constructor ({ accessToken }) {
    super()
    this.accessToken = accessToken
  }
}
