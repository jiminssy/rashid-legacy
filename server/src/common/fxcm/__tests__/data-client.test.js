import _ from 'lodash'
import moment from 'moment-timezone'
import { BaseClient } from '../base-client'
import { DataClient } from '../data-client'
import { CandleData } from '../../definition/CandleData'
import { DataPeriod } from '../../definition/DataPeriod'

test('getHistoricalData works with valid params', async () => {
  const mockCandle1 = [
    1594328400, 1.12872,
    1.1274, 1.12899,
    1.1269, 1.12879,
    1.12745, 1.12906,
    1.12696, 21382
  ]
  const mockCandle2 = [
    1594393200, 1.13192,
    1.12969, 1.13197,
    1.12942, 1.13198,
    1.13017, 1.13203,
    1.12955, 57076
  ]
  const mockData = {
    candles: [mockCandle1, mockCandle2]
  }

  const baseClient = new BaseClient()
  baseClient.requestGet = jest.fn().mockResolvedValue(mockData)
  const dataClient = new DataClient(baseClient)

  const data = await dataClient.getHistoricalData(
    '1',
    DataPeriod.H6,
    moment('2020-07-10 00:00:00'),
    moment('2020-07-11 00:00:00')
  )

  const expectedCandel1 = new CandleData(
    1594328400, 1.12872,
    1.1274, 1.12899,
    1.1269, 1.12879,
    1.12745, 1.12906,
    1.12696, 21382
  )
  const expectedCandle2 = new CandleData(
    1594393200, 1.13192,
    1.12969, 1.13197,
    1.12942, 1.13198,
    1.13017, 1.13203,
    1.12955, 57076
  )

  expect(data.length).toBe(2)
  expect(_.isEqual(data[0], expectedCandel1)).toBe(true)
  expect(_.isEqual(data[1], expectedCandle2)).toBe(true)
})

test('getMostRecentHistoricalData works correctly', async () => {
  const mockCandle1 = [
    1594328400, 1.12872,
    1.1274, 1.12899,
    1.1269, 1.12879,
    1.12745, 1.12906,
    1.12696, 21382
  ]
  const mockCandle2 = [
    1594393200, 1.13192,
    1.12969, 1.13197,
    1.12942, 1.13198,
    1.13017, 1.13203,
    1.12955, 57076
  ]
  const mockData = {
    candles: [mockCandle1, mockCandle2]
  }

  const baseClient = new BaseClient()
  baseClient.requestGet = jest.fn().mockResolvedValue(mockData)
  const dataClient = new DataClient(baseClient)

  const data = await dataClient.getMostRecentHistoricalData(
    '1',
    DataPeriod.m15,
    2
  )

  const expectedCandel1 = new CandleData(
    1594328400, 1.12872,
    1.1274, 1.12899,
    1.1269, 1.12879,
    1.12745, 1.12906,
    1.12696, 21382
  )
  const expectedCandle2 = new CandleData(
    1594393200, 1.13192,
    1.12969, 1.13197,
    1.12942, 1.13198,
    1.13017, 1.13203,
    1.12955, 57076
  )

  expect(data.length).toBe(2)
  expect(_.isEqual(data[0], expectedCandel1)).toBe(true)
  expect(_.isEqual(data[1], expectedCandle2)).toBe(true)
})
