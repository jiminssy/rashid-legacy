import { EventMessage } from './EventMessage'
import { ADD_NEW_STRATEGY } from '../event-type'

export class AddNewStrategy extends EventMessage {
  constructor (strategy) {
    super(ADD_NEW_STRATEGY)
    this.strategy = strategy
  }
}
