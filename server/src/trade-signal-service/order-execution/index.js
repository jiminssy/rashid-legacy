import { SystemService } from '../../common/definition/SystemService'
import { parseDataStreamId } from '../../common/utils/data-stream-id-parser'
import { getOrderClient } from '../../common/fxcm/client-factory'

export const placeMarketOrder = async liveTradeSignal => {
  const { brokerInstrumentId } = parseDataStreamId(liveTradeSignal.tradingInstrument)
  const orderClient = getOrderClient(SystemService.TRADE_SIGNAL)
  const tradeResult = await orderClient.openTradeMarket(
    brokerInstrumentId,
    liveTradeSignal.tradeDirection,
    liveTradeSignal.quantity,
    liveTradeSignal.stopLoss,
    liveTradeSignal.takeProfit
  )
  return tradeResult
}
