import { parseDataStreamId } from '../../common/utils/data-stream-id-parser'
import { getOrderClient } from '../../common/fxcm/client-factory'
import { SystemService } from '../../common/definition/SystemService'
import {
  getInstrumentId,
  isPositionOpen,
  savePositionOpened
} from '../utils/database'
import { OrderType } from '../../common/definition/OrderType'
import { publish } from '../../common/event/event-bus'
import { SimulationPositionOpened } from '../../common/event/message/SimulationPositionOpened'

const handleMarketOrder = async (orderClient, tradeSignal
) => {
  const {
    strategyInstanceId,
    tradingInstrument,
    tradeDirection,
    stopLoss,
    takeProfit,
    positionSizeMultiplier
  } = tradeSignal

  const {
    brokerName,
    brokerInstrumentId
  } = parseDataStreamId(tradingInstrument)

  const tradeResults = await orderClient.openTradeMarket(
    brokerInstrumentId,
    tradeDirection,
    1,
    stopLoss,
    takeProfit
  )

  publish(new SimulationPositionOpened(
    strategyInstanceId,
    tradingInstrument,
    tradeResults.orderId,
    tradeResults.tradeId,
    tradeDirection,
    OrderType.MARKET,
    tradeResults.entry,
    tradeResults.stopLoss,
    tradeResults.takeProfit,
    null,
    positionSizeMultiplier
  ))

  const instrumentId = await getInstrumentId(brokerName, brokerInstrumentId)

  await savePositionOpened({
    strategyInstanceId,
    instrumentId,
    brokerOrderId: tradeResults.orderId,
    brokerTradeId: tradeResults.tradeId,
    open: tradeResults.entry,
    stopLoss: tradeResults.stopLoss,
    takeProfit: tradeResults.takeProfit,
    isClosed: false,
    sizeMultiplier: positionSizeMultiplier
  })
}

export const eventListener = async tradeSignal => {
  if (await isPositionOpen(tradeSignal.strategyInstanceId)) {
    return
  }

  const orderClient = getOrderClient(SystemService.SIMULATION)

  if (tradeSignal.orderType === OrderType.MARKET) {
    await handleMarketOrder(orderClient, tradeSignal)
  }
}
