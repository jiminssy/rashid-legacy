import { EventMessage } from './EventMessage'
import { STRATEGY_TRADE_SIGNAL } from '../event-type'

export class StrategyTradeSignal extends EventMessage {
  constructor (
    strategyInstanceId,
    tradingInstrument, // data stream id format
    tradeDirection,
    orderType,
    stopLossRange,
    stopLoss,
    takeProfit,
    limitPrice = null,
    positionSizeMultiplier = 1,
    miscData = null
  ) {
    super(STRATEGY_TRADE_SIGNAL)
    this.strategyInstanceId = strategyInstanceId
    this.tradingInstrument = tradingInstrument
    this.tradeDirection = tradeDirection
    this.orderType = orderType
    this.stopLossRange = stopLossRange
    this.stopLoss = stopLoss
    this.takeProfit = takeProfit
    this.limitPrice = limitPrice
    this.positionSizeMultiplier = positionSizeMultiplier
    this.miscData = miscData
  }
}
