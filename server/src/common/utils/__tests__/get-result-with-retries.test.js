import { getResultWithRetries, GetResultRetriesError } from '../get-result-with-retries'

test('getResultWithRetries works on the first try', async () => {
  const sumClosure = (a, b) => {
    return function () {
      return a + b
    }
  }
  const closureToExecute = sumClosure(1, 2)
  const isResultSuccess = result => result === 3
  const intervalLength = 0
  const maxRetries = 10

  const result = await getResultWithRetries(closureToExecute, isResultSuccess, intervalLength, maxRetries)

  expect(result).toBe(3)
})

test('getResultWithRetries works on the tenth try', async () => {
  expect.assertions(1)
  const addClosure = () => {
    let counter = 1
    return function () {
      counter += 1
      return counter
    }
  }
  const closureToExecute = addClosure()
  const isResultSuccess = result => result === 10
  const maxRetries = 10
  const intervalLength = 0

  const result = await getResultWithRetries(closureToExecute, isResultSuccess, intervalLength, maxRetries)

  expect(result).toBe(10)
})

test('getResultWithRetries could not get successful result', async () => {
  expect.assertions(2)
  const addClosure = () => {
    let counter = 0
    return function () {
      counter += 1
      return counter
    }
  }
  const closureToExecute = addClosure()
  const isResultSuccess = result => result === 12
  const maxRetries = 10
  const intervalLength = 0

  try {
    await getResultWithRetries(closureToExecute, isResultSuccess, intervalLength, maxRetries)
  } catch (e) {
    expect(e).toBeInstanceOf(GetResultRetriesError)
    expect(e).toHaveProperty('message', 'Could not get successful result.')
  }
})

test('getResultWithRetries got an unexpected error', async () => {
  expect.assertions(2)
  const erroringClosure = () => {
    return function () {
      throw new Error('Test error')
    }
  }
  const closureToExecute = erroringClosure()
  const isResultSuccess = result => result === 10
  const maxRetries = 10
  const intervalLength = 0

  try {
    await getResultWithRetries(closureToExecute, isResultSuccess, intervalLength, maxRetries)
  } catch (e) {
    expect(e).toBeInstanceOf(Error)
    expect(e).toHaveProperty('message', 'Test error')
  }
})
