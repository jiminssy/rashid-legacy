import { knex } from '../../common/database/knex'
import { getStrategyCodePath } from '../util/database'
import {
  activateStrategyInstance,
  deactivateStrategyInstance,
  replaceStrategyInstance
} from '../util/strategy-instance-activator'

export const eventListener = async (
  {
    previousStrategyInstanceId,
    strategyInstance
  }
) => {
  // 1. deactivate
  deactivateStrategyInstance(previousStrategyInstanceId)

  // 2. update DB
  await knex('strategy_strategyInstance')
    .where({ id: previousStrategyInstanceId })
    .update({
      ...strategyInstance,
      dataStreams: JSON.stringify(strategyInstance.dataStreams),
      strategyParams: JSON.stringify(strategyInstance.strategyParams)
    })

  // 3. replace cached instance
  const codePath = await getStrategyCodePath(strategyInstance.strategyId)
  replaceStrategyInstance(
    previousStrategyInstanceId,
    codePath,
    strategyInstance
  )

  // 4. activate the newly updated strategy instance if necessary
  if (strategyInstance.isEnabled) {
    activateStrategyInstance(strategyInstance.id)
  }
}
