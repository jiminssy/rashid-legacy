import moment from 'moment-timezone'
import { isTradingHour } from '../instruments'

describe('isTradingHour', () => {
  test('throw exception for non tradable FXCM instrument', () => {
    expect.assertions(2)
    const nonTradableId = 'FXCM-99'
    try {
      isTradingHour(nonTradableId, moment())
    } catch (e) {
      expect(e).toBeInstanceOf(Error)
      expect(e).toHaveProperty('message', 'Not a tradable FXCM instrument.')
    }
  })

  describe('FX instruments', () => {
    test('valid trading hour', () => {
      const instrumentId = '1'

      const justAfterServerStart = moment('2020-07-19T16:45:00.001-04:00')
      expect(isTradingHour(instrumentId, justAfterServerStart)).toBe(true)

      const midWeek = moment('2020-07-22T03:30:26-04:00')
      expect(isTradingHour(instrumentId, midWeek)).toBe(true)

      const justBeforeServerShutdown = moment('2020-07-24T16:44:59.999-04:00')
      expect(isTradingHour(instrumentId, justBeforeServerShutdown)).toBe(true)
    })

    test('invalid trading hour', () => {
      const instrumentId = '1'

      const serverShutDown = moment('2020-07-24T16:45:00-04:00')
      expect(isTradingHour(instrumentId, serverShutDown)).toBe(false)

      const midSaturday = moment('2020-07-25T16:45:00-04:00')
      expect(isTradingHour(instrumentId, midSaturday)).toBe(false)

      const serverStartTime = moment('2020-07-26T16:45:00-04:00')
      expect(isTradingHour(instrumentId, serverStartTime)).toBe(false)
    })
  })
})
