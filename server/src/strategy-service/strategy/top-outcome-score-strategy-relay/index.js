import moment from 'moment-timezone'
import { BaseStrategy } from '../base-strategy'
import { publish, subscribe, unsubscribe } from '../../../common/event/event-bus'
import { StrategyTradeSignal } from '../../../common/event/message/StrategyTradeSignal'
import { TargetStrategyInstanceUpdated } from '../../../common/event/message/TargetStrategyInstanceUpdated'
import {
  SIMULATION_POSITION_OPENED,
  NEW_FX_TRADING_DAY
} from '../../../common/event/event-type'
import { getTopOutcomeScoreStrategyInstanceId } from '../../util/database'
import { OrderType } from '../../../common/definition/OrderType'

// strategy parameter
/*
{
  lookBackDays: integer,
  topNthStrategyInstance: integer
}
*/
export default class extends BaseStrategy {
  async activate () {
    super.activate()

    this._getTargetStrategyInstance = this._getTargetStrategyInstance.bind(this)
    this._newFxTradingDaySubscription = subscribe(NEW_FX_TRADING_DAY, this._getTargetStrategyInstance)

    this._onSimulationPositionOpened = this._onSimulationPositionOpened.bind(this)
    this._positionOpenedSubscription = subscribe(SIMULATION_POSITION_OPENED, this._onSimulationPositionOpened)

    this._targetStrategyInstanceId = await this._getTargetStrategyInstanceId()
    this._publishTargetStrategyInstanceUpdatedEvent()
  }

  async deactivate () {
    super.deactivate()
    unsubscribe(this._newFxTradingDaySubscription)
    unsubscribe(this._positionOpenedSubscription)
  }

  async _getTargetStrategyInstanceId () {
    const today = moment.utc().set({
      hour: 0,
      minute: 0,
      second: 0,
      millisecond: 0
    })
    const lookBackDays = this.strategyInstanceInfo.strategyParams.lookBackDays
    const topNthStrategyInstance = this.strategyInstanceInfo.strategyParams.topNthStrategyInstance
    const from = today.subtract(lookBackDays, 'days')
    return await getTopOutcomeScoreStrategyInstanceId(
      this.strategyInstanceInfo.tradingInstrument,
      from.format(),
      topNthStrategyInstance,
      true
    )
  }

  _getRelayTradeSignal (openedPosition) {
    const stopLossRange = Math.abs(openedPosition.open - openedPosition.stopLoss)
    return new StrategyTradeSignal(
      this.strategyInstanceInfo.id,
      this.strategyInstanceInfo.tradingInstrument,
      openedPosition.tradeDirection,
      OrderType.MARKET,
      stopLossRange,
      openedPosition.stopLoss,
      openedPosition.takeProfit,
      null,
      1,
      {
        isReactionaryTradeSignal: true,
        sourcePositionId: openedPosition.positionId,
        sourceStrategyInstanceId: openedPosition.strategyInstanceId
      }
    )
  }

  _onSimulationPositionOpened (openedPosition) {
    if (this._targetStrategyInstanceId == null) {
      return
    }

    if (openedPosition.strategyInstanceId === this._targetStrategyInstanceId) {
      publish(this._getRelayTradeSignal(openedPosition))
    }
  }

  _publishTargetStrategyInstanceUpdatedEvent () {
    publish(new TargetStrategyInstanceUpdated(
      this.strategyInstanceInfo.id,
      this._targetStrategyInstanceId
    ))
  }

  _getTargetStrategyInstance () {
    (async () => {
      this._targetStrategyInstanceId = await this._getTargetStrategyInstanceId()
      this._publishTargetStrategyInstanceUpdatedEvent()
    })()
  }
}
