export class GetResultRetriesError extends Error {
  constructor (message) {
    super(message)
    this.name = 'GetResultRetriesError'
  }
}

export const getResultWithRetries = async (
  closureToExecute,
  isResultSuccess,
  intervalLength,
  maxRetries
) => {
  let counter = 1
  const result = await closureToExecute()
  if (isResultSuccess(result)) {
    return result
  }

  return new Promise((resolve, reject) => {
    const retryInterval = setInterval(async () => {
      try {
        counter += 1
        if (counter <= maxRetries) {
          const result = await closureToExecute()
          if (isResultSuccess(result)) {
            clearInterval(retryInterval)
            resolve(result)
          }
        } else {
          clearInterval(retryInterval)
          reject(new GetResultRetriesError('Could not get successful result.'))
        }
      } catch (e) {
        clearInterval(retryInterval)
        reject(e)
      }
    }, intervalLength)
  })
}
