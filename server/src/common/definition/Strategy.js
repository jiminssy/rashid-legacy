export class Strategy {
  constructor ({
    id, name, version, description, codePath, paramSchema
  }) {
    this.id = id
    this.name = name
    this.version = version
    this.description = description
    this.codePath = codePath
    this.paramSchema = paramSchema
  }
}
