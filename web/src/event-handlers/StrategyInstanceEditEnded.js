import { EventHandler } from './EventHandler'
import { STRATEGY_INSTANCE_EDIT_ENDED } from '../constants/events'
import { setValue } from '../store'

class StrategyInstanceEditEnded extends EventHandler {
  constructor () {
    super([STRATEGY_INSTANCE_EDIT_ENDED])
  }

  async handle () {
    await setValue('strategy.instance.edit.strategyInstance', null)
  }
}

export const eventHandler = new StrategyInstanceEditEnded()
